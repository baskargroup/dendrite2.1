cmake_minimum_required(VERSION 2.8)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/external/taly_fem/cmake_modules")  # for FindPETSc
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/CMakeModules")
project(dendrite2.0)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -ftemplate-depth-2000")

find_package(OpenMP REQUIRED)
find_package(MPI REQUIRED)

if(OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
endif()


if(MPI_COMPILE_FLAGS)
    set(COMPILE_FLAGS "${COMPILE_FLAGS} ${MPI_COMPILE_FLAGS}")
endif()

if(MPI_LINK_FLAGS)
    set(LINK_FLAGS "${LINK_FLAGS} ${MPI_LINK_FLAGS}")
endif()

##  DENDRO5 OPTIONS BEGIN

option(USE_64BIT_INDICES "Use 64-Bit indices. Reverts to 32-bit if turned off" ON)
option(HILBERT_ORDERING "Use Hilbert ordering instead of Morton" ON)
option(CLENSHAW_CURTIS "Use Clenshaw - curtis" OFF)
option(ALLTOALLV_FIX "Use K-way all to all v" OFF)
option(SPLITTER_SELECTION_FIX "Turn on Splitter Selection fix" ON)
option(DIM_2 "use the two dimentional sorting" OFF)
option(PROFILE_TREE_SORT "Profile the tree sort code, construction and balancing. " OFF)
option(WITH_BLAS_LAPACK "build using BLAS and LAPACk" OFF)
option(MANUAL_BLAS_LAPACK "configure BLAS and LAPACK Manually" OFF)
option(DENDRO_VTK_BINARY "write vtk/vtu files in binary mode " ON)
option(DENDRO_VTK_ZLIB_COMPRES "write vtk/vtu files in binary mode with zlib compression (only compatible with binary mode) " OFF)
option(ALLTOALL_SPARSE "uses isend irecv for ghost exchange (for some mpi implementations this might be efficient)" OFF)
option(ENABLE_DENDRO_PROFILE_COUNTERS " enables dendro internal profile counters. " OFF)
option(RK_SOLVER_OVERLAP_COMM_AND_COMP " enables RK solver comm. & comp. overlap. (might be good in large scale runs) " ON)
option(WITH_CUDA " build dendro with cuda " OFF)
option(BUILD_WITH_PETSC " build dendrite with petsc " ON)
option(TENSOR " enable tensor operation" OFF)


#option(KWAY "K parameter for alltoallv_kway" 128)
set(KWAY 128 CACHE INT 128)
set(NUM_NPES_THRESHOLD 2 CACHE INT 2)

#set the build type to release by default.
if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release" CACHE STRING
            "Choose the type of build, options are: Debug Release " FORCE)
endif()


if(WITH_CUDA)
    find_package(CUDA REQUIRED)
    if(CUDA_FOUND)
        set(CUDA_PROPAGATE_HOST_FLAGS OFF)
        set(CUDA_SEPARABLE_COMPILATION ON)

        #list( APPEND CUDA_NVCC_FLAGS -gencode arch=compute_30,code=sm_30)
        #list( APPEND CUDA_NVCC_FLAGS -gencode arch=compute_50,code=sm_50)
        #list( APPEND CUDA_NVCC_FLAGS -gencode arch=compute_60,code=sm_60)
        #list( APPEND CUDA_NVCC_FLAGS --generate-line-info)
        #list( APPEND CUDA_NVCC_FLAGS -gencode arch=compute_61,code=sm_61)

        #list( APPEND CUDA_NVCC_FLAGS -use_fast_math )
        #list( APPEND CUDA_NVCC_FLAGS -maxrregcount 64 )
        list( APPEND CUDA_NVCC_FLAGS -std=c++11)



    endif()
endif()



if(WITH_BLAS_LAPACK)
    add_definitions(-DWITH_BLAS_LAPACK)
    if(MANUAL_BLAS_LAPACK)
        if("$ENV{BLAS}" STREQUAL "")
            message("Environment Variable BLAS is not set. Please set it to BLAS directory")
        endif()

        if( "$ENV{LAPACK}" STREQUAL "" )
            message("Enviroment Variable LAPACK is note set. Please set it to LAPACK directory. ")
        endif()
        set(LAPACKE_DIR $ENV{LAPACK}/LAPACKE)
        set(BLAS_LIBS $ENV{BLAS}/lib )
        set(LAPACK_LIBS $ENV{LAPACK}/lib)
        set(LAPACK_LINKER_FLAGS -llapacke -llapack -lblas -lgfortran -lquadmath)
        set(LAPACK_LIBRARIES ${LAPACK_LIBS}/liblapacke.a ${LAPACK_LIBS}/liblapack.a ${BLAS_LIBS}/libblas.a -static libgfortran.a libquadmath.a)
        set(LINK_FLAGS "${LINK_FLAGS} ${LAPACK_LINKER_FLAGS}")
    else ()
        find_package(BLAS REQUIRED)
        find_package(LAPACK REQUIRED)
        set(LAPACK_LINKER_FLAGS -llapacke -llapack -lblas -lgfortran -lquadmath)
        set(LAPACKE_DIR $ENV{LAPACK}/LAPACKE)
        set(LINK_FLAGS "${LINK_FLAGS} ${LAPACK_LINKER_FLAGS}")
        find_library(LAPACKE_LIB
                NAMES lapacke lapackelib liblapacke
                HINTS "/usr/lib/"
                )
        set(LAPACK_LIBRARIES ${LAPACK_LIBRARIES} ${LAPACKE_LIB})
        message(STATUS ${LAPACK_LIBRARIES})
    endif()
endif()


if(BUILD_WITH_PETSC)
    list (APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake-modules")
    find_package(PETSc 3.7 REQUIRED)
    add_definitions(-DBUILD_WITH_PETSC)

endif()


if(DIM_2)
    add_definitions(-DDIM_2)
endif()

if(PROFILE_TREE_SORT)
    add_definitions(-DPROFILE_TREE_SORT)
endif()


if(USE_64BIT_INDICES)
    add_definitions(-DUSE_64BIT_INDICES)
    #message('Configuring 64BIT indices')
endif()

if(HILBERT_ORDERING)
    add_definitions(-DHILBERT_ORDERING)
    #message (Configuring with HIlbert)
endif()

#if(GAUSSQUADRATURE)
#    add_definitions(-DGAUSSQUADRATURE)
#    message (Running with Gauss Quadrature point)
#endif()

if(CLENSHAW_CURTIS)
    add_definitions(-DCLENSHAW_CURTIS)
    message (Running with Clenshaw Curtis point)
    else()
    message (Running with Gauss Quadrature point)
endif()

if(RUN_WEAK_SCALING)
    add_definitions(-DRUN_WEAK_SCALING)
    #message('Configuring with Weak Scaling')
endif()

if(ALLTOALLV_FIX)
    add_definitions(-DALLTOALLV_FIX)
    add_definitions(-DKWAY=${KWAY})
endif()

if(SPLITTER_SELECTION_FIX)
    add_definitions(-DSPLITTER_SELECTION_FIX)
    add_definitions(-DNUM_NPES_THRESHOLD=${NUM_NPES_THRESHOLD})
endif()

if(ALLTOALL_SPARSE)
    add_definitions(-DALLTOALL_SPARSE)
endif()

if(ENABLE_DENDRO_PROFILE_COUNTERS)
    add_definitions(-DENABLE_DENDRO_PROFILE_COUNTERS)
endif()

if(RK_SOLVER_OVERLAP_COMM_AND_COMP)
    add_definitions(-DRK_SOLVER_OVERLAP_COMM_AND_COMP)
endif()

if(DENDRO_VTK_BINARY)
else()
    set(DENDRO_VTK_ZLIB_COMPRES OFF)
endif()

if(DENDRO_VTK_BINARY)
    add_definitions(-DDENDRO_VTU_BINARY)
    if(DENDRO_VTK_ZLIB_COMPRES)
        add_definitions(-DDENDRO_VTU_ZLIB)
    endif()
else()
    add_definitions(-DDENDRO_VTU_ASCII)
endif()

if(TENSOR)
    add_definitions(-DTENSOR)
endif()
## DENDRO5 OPTIONS END

add_subdirectory(external/Dendro5)
add_subdirectory(external/Ray_tracing)


find_package(PETSc 3.7 COMPONENTS C REQUIRED)

set(TALYFEM_BUILD_TUTORIALS OFF CACHE BOOL "Build tutorial applications (HT, BT)")
set(TALYFEM_BUILD_TESTS OFF CACHE BOOL "Build unit tests")
add_subdirectory(external/taly_fem)
set(DENDRITE_INC
        include/TalyMat.h
        include/TalyDendroSync.h
        include/OctToPhysical.h
        include/DataTypes.h
        include/Globals.h
        include/BasisCache.h
        include/TalyVec.h
        include/TalyMesh.h
        include/TalyEquation.h
        include/PETSc/BoundaryConditions.h
        include/PETSc/Solver/Solver.h
        include/PETSc/Solver/LinearSolver.h
        include/PETSc/Solver/NonLinearSolver.h
        include/DendriteUtils.h
        include/PETSc/VecInfo.h
        include/TimeInfo.h
        include/PETSc/PetscUtils.h
        include/PETSc/PetscTimings.h
        include/Refinement/Refinement.h
        include/Refinement/RefinementDataTypes.h

        include/Checkpoining/checkpointer.hpp

        include/IBM/IBMSolver.h
        include/IBM/Geom.h
        include/IBM/GeomDef.h
        include/IBM/IBMDataTypes.h
        include/IBM/IBMLinearSolver.h
        include/IBM/IBMNonLinearSolver.h
        include/IBM/IBMUtils.h
        include/IBM/VTKIO.h
        include/IBM/IBMFeUtils.h
        include/IBM/MathOperation.h
        include/Profiling/GlobalProfiler.h
        )

set(DENDRITE_SRC
        src/TalyDendroSync.cpp
        src/OctToPhysical.cpp
        src/Globals.cpp
        src/BasisCache.cpp
        src/PETSc/BoundaryConditions.cpp
        src/PETSc/Solver/Solver.cpp
        src/PETSc/Solver/LinearSolver.cpp
        src/DendriteUtils.cpp
        src/PETSc/Solver/NonLinearSolver.cpp
        src/PETSc/VecInfo.cpp
        src/PETSc/PetscUtils.cpp
        src/Refinement/Refinement.cpp
        src/IBM/MathOperation.cpp

        src/Checkpointing/checkpointer.cpp
        )

add_library(dendrite2 ${DENDRITE_INC} ${DENDRITE_SRC})
target_include_directories(dendrite2 PUBLIC include)
target_link_libraries(dendrite2 dendro5 talyfem ${MPI_LIBRARIES} m raytracer)

option(DENDRITE2_BUILD_EXAMPLES "Build example projects" ON)
if (DENDRITE2_BUILD_EXAMPLES)
    if(TENSOR)
        add_subdirectory(examples/Basic/Navier-Stokes)
    else()
    #Basic Examples
add_subdirectory(examples/Basic/SSHT)
add_subdirectory(examples/Basic/bratu)
add_subdirectory(examples/Basic/TSHT)
add_subdirectory(examples/Basic/PredatorPrey)
add_subdirectory(examples/Basic/Cahn_Hilliard)
add_subdirectory(examples/Basic/SSHT-neumann)
add_subdirectory(examples/Basic/funcToODA)
add_subdirectory(examples/Basic/Navier-Stokes)
add_subdirectory(examples/Basic/Checkpointing)
    #Adaptivity Examples
add_subdirectory(examples/Adaptivity/Aposteriori)
    add_subdirectory(examples/Adaptivity/PositionBased)
        endif()
    #IBM Examples
#add_subdirectory(examples/IBM)
endif()
