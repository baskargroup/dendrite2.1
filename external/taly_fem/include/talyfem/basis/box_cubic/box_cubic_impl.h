/*
  Copyright 2014-2016 Baskar Ganapathysubramanian

  This file is part of TALYFem.

  TALYFem is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  TALYFem is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with TALYFem.  If not, see <http://www.gnu.org/licenses/>.
*/
// --- end license text --- //
#pragma once

#include <array>

#include <talyfem/grid/zeroptv.h>
#include <talyfem/basis/nonlinear.h>

namespace TALYFEMLIB {

/**
 * Maps tensor product-ordered N values calculated in NonlinearBasis to
 * the TalyFEM-ordered values.
 */
static constexpr int cubic_n_map[3][64] = {
  { 0, 2, 3, 1 },  // 1D
  { 0, 4, 5, 1, 11, 12, 13, 6, 10, 15, 14, 7, 3, 9, 8, 2 }, // 2D
  { 0, 16, 17, 1, 23, 24, 25, 18, 22, 27, 26, 19, 3, 21, 20, 2, 8, 40, 41, 9,
    47, 48, 49, 42, 46, 51, 50, 43, 11, 45, 44, 10, 12, 52, 53, 13, 59, 60,
    61, 54, 58, 63, 62, 55, 15, 57, 56, 14, 4, 28, 29, 5, 35, 36, 37, 30, 34,
    39, 38, 31, 7, 33, 32, 6 }  // 3D
};

/**
 * Implementation for the cubic "box" basis functions (1D line, 2D box, 3D
 * hexahedron).
 */
template <int _nsd>
class BoxCubicBasisImpl {
 public:
  static constexpr int nsd = _nsd;  ///< number of spatial dimensions
  static constexpr int nbf = constexpr_pow(4, _nsd);  ///< number of shape funcs
  static constexpr int nbf_per_node = 1;  ///< shape funcs per node
  static constexpr double jacobian_scale = 1.0;  ///< multiplier for jacobian

  /**
   * Calculate shape functions.
   * @param localPt integration point
   * @param[out] n_out shape functions evaluated at localPt (output)
   */
  static void calc_N(const ZEROPTV& localPt, double (&n_out)[nbf]) {
    NonlinearBasisImpl<nsd, 4>::template calc_N<N_1D>(cubic_n_map[nsd - 1], localPt, n_out);
  }

  /**
   * Calculate derivative of N in isoparametric space.
   * @param localPt integration point
   * @param[out] dnde_out derivative of N in isoparametric space (output)
   */
  static void calc_dNde(const ZEROPTV& localPt, double (&dnde_out)[nbf][nsd]) {
    NonlinearBasisImpl<nsd, 4>::template calc_dNde<N_1D, dN_1D>(cubic_n_map[nsd - 1], localPt, dnde_out);
  }

  /**
   * Calculate second derivative of N in isoparametric space.
   * @param localPt integration point
   * @param[out] d2nde_out second derivative of N in isoparametric space
   */
  static void calc_d2Nde(const ZEROPTV& localPt, double (&d2nde_out)[nbf][nsd * (nsd + 1) / 2]) {
    NonlinearBasisImpl<nsd, 4>::template calc_d2Nde<N_1D, dN_1D, d2N_1D>(cubic_n_map[nsd - 1], localPt, d2nde_out);
  }

 private:
  static std::array<double, 4> N_1D(double x) {
    return std::array<double, 4> {{
      (-9. / 16.) * (x * x * x - x * x - (1. / 9.) * x + (1. / 9.)),
      (27. / 16.) * (x * x * x - (1. / 3.) * x * x - x + (1. / 3.)),
      (-27. / 16.) * (x * x * x + (1. / 3.) * x * x - x - (1. / 3.)),
      (9. / 16.) * (x * x * x + x * x - (1. / 9.) * x - (1. / 9.))
    }};
  }

  static std::array<double, 4> dN_1D(double x) {
    return std::array<double, 4> {{
      (-9. / 16.) * (3 * x * x - 2 * x - (1. / 9.)),
      (27. / 16.) * (3 * x * x - (2. / 3.) * x - 1),
      (-27. / 16.) * (3 * x * x + (2. / 3.) * x - 1),
      (9. / 16.) * (3 * x * x + 2 * x - (1. / 9.))
    }};
  }

  // not verified - blindly copied from old library
  static std::array<double, 4> d2N_1D(double x) {
    return std::array<double, 4> {{
      (-9. / 16.) * (6 * x - 2),
      (27. / 16.) * (6 * x - (2. / 3.)),
      (-27. / 16.) * (6 * x + (2. / 3.)),
      (9. / 16.) * (6 * x + 2)
    }};
  }
};

}  // namespace TALYFEMLIB
