/*
  Copyright 2014-2016 Baskar Ganapathysubramanian

  This file is part of TALYFem.

  TALYFem is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  TALYFem is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with TALYFem.  If not, see <http://www.gnu.org/licenses/>.
*/
// --- end license text --- //
#pragma once

#include <talyfem/data_structures/matrix4.h>

namespace TALYFEMLIB {

// 1D surfaces (always one point)
/**
 * Left 1D surface gauss point (surface ID -1).
 */
template<int order>
struct BoxItgPts<order, 1, -1>  {
  static constexpr int n_itg_pts = 1;  ///< number of integration points
  ///! integration points
  static constexpr constexpr_array<ZEROPTV, n_itg_pts> itg_pts = {{ ZEROPTV(-1, 0, 0) }};
  ///! integration point weights
  static constexpr constexpr_array<double, n_itg_pts> weights = {{ 1 }};
};

/**
 * Right 1D surface gauss point (surface ID +1).
 */
template<int order>
struct BoxItgPts<order, 1, +1> {
  static constexpr int n_itg_pts = 1;  ///< number of integration points
  ///! integration points
  static constexpr constexpr_array<ZEROPTV, n_itg_pts> itg_pts = {{ ZEROPTV(+1, 0, 0) }};
  ///! integration point weights
  static constexpr constexpr_array<double, n_itg_pts> weights = {{ 1 }};
};

// 2D surfaces

/**
 * Left (X-) surface gauss points for 2D box (surface ID -1).
 */
template <int order>
struct BoxItgPts<order, 2, -1> {
  ///! number of integration points
  static constexpr int n_itg_pts = BoxItgPts<order, 1, 0>::n_itg_pts;
  ///! integration points
  static constexpr constexpr_array<ZEROPTV, n_itg_pts> itg_pts = Matrix4::translate(-1, 0, 0) * Matrix4::rotateZ(-90) * BoxItgPts<order, 1, 0>::itg_pts;
  ///! integration point weights
  static constexpr constexpr_array<double, n_itg_pts> weights = BoxItgPts<order, 1, 0>::weights;
};

/**
 * Right (X+) surface gauss points for 2D box (surface ID +1).
 */
template <int order>
struct BoxItgPts<order, 2, +1> {
  ///! number of integration points
  static constexpr int n_itg_pts = BoxItgPts<order, 1, 0>::n_itg_pts;
  ///! integration points
  static constexpr constexpr_array<ZEROPTV, n_itg_pts> itg_pts = Matrix4::translate(+1, 0, 0) * Matrix4::rotateZ(+90) * BoxItgPts<order, 1, 0>::itg_pts;
  ///! integration point weights
  static constexpr constexpr_array<double, n_itg_pts> weights = BoxItgPts<order, 1, 0>::weights;
};

/**
 * Bottom (Y-) surface gauss points for 2D box (surface ID -2).
 */
template <int order>
struct BoxItgPts<order, 2, -2> {
  ///! number of integration points
  static constexpr int n_itg_pts = BoxItgPts<order, 1, 0>::n_itg_pts;
  ///! integration points
  static constexpr constexpr_array<ZEROPTV, n_itg_pts> itg_pts = Matrix4::translate(0, -1, 0) * Matrix4::rotateZ(0) * BoxItgPts<order, 1, 0>::itg_pts;
  ///! integration point weights
  static constexpr constexpr_array<double, n_itg_pts> weights = BoxItgPts<order, 1, 0>::weights;
};

/**
 * Bottom (Y+) surface gauss points for 2D box (surface ID +2).
 */
template <int order>
struct BoxItgPts<order, 2, +2> {
  ///! number of integration points
  static constexpr int n_itg_pts = BoxItgPts<order, 1, 0>::n_itg_pts;
  ///! integration points
  static constexpr constexpr_array<ZEROPTV, n_itg_pts> itg_pts = Matrix4::translate(0, +1, 0) * Matrix4::rotateZ(0) * BoxItgPts<order, 1, 0>::itg_pts;
  ///! integration point weights
  static constexpr constexpr_array<double, n_itg_pts> weights = BoxItgPts<order, 1, 0>::weights;
};

// 3D surfaces

/**
 * Left (X-) surface gauss points for 3D hexahedron (surface ID -1).
 */
template<int order>
struct BoxItgPts<order, 3, -1> {
  ///! number of integration points
  static constexpr int n_itg_pts = BoxItgPts<order, 2, 0>::n_itg_pts;
  ///! integration points
  static constexpr constexpr_array<ZEROPTV, n_itg_pts> itg_pts = Matrix4::translate(-1, 0, 0) * Matrix4::rotateY(-90) * BoxItgPts<order, 2, 0>::itg_pts;
  ///! integration point weights
  static constexpr constexpr_array<double, n_itg_pts> weights = BoxItgPts<order, 2, 0>::weights;
};

/**
 * Right (X+) surface gauss points for 3D hexahedron (surface ID +1).
 */
template<int order>
struct BoxItgPts<order, 3, +1> {
  ///! number of integration points
  static constexpr int n_itg_pts = BoxItgPts<order, 2, 0>::n_itg_pts;
  ///! integration points
  static constexpr constexpr_array<ZEROPTV, n_itg_pts> itg_pts = Matrix4::translate(+1, 0, 0) * Matrix4::rotateY(90) * BoxItgPts<order, 2, 0>::itg_pts;
  ///! integration point weights
  static constexpr constexpr_array<double, n_itg_pts> weights = BoxItgPts<order, 2, 0>::weights;
};

/**
 * Bottom (Y-) surface gauss points for 3D hexahedron (surface ID -2).
 */
template<int order>
struct BoxItgPts<order, 3, -2> {
  ///! number of integration points
  static constexpr int n_itg_pts = BoxItgPts<order, 2, 0>::n_itg_pts;
  ///! integration points
  static constexpr constexpr_array<ZEROPTV, n_itg_pts> itg_pts = Matrix4::translate(0, -1, 0) * Matrix4::rotateX(-90) * BoxItgPts<order, 2, 0>::itg_pts;
  ///! integration point weights
  static constexpr constexpr_array<double, n_itg_pts> weights = BoxItgPts<order, 2, 0>::weights;
};

/**
 * Top (Y+) surface gauss points for 3D hexahedron (surface ID +2).
 */
template<int order>
struct BoxItgPts<order, 3, +2> {
  ///! number of integration points
  static constexpr int n_itg_pts = BoxItgPts<order, 2, 0>::n_itg_pts;
  ///! integration points
  static constexpr constexpr_array<ZEROPTV, n_itg_pts> itg_pts = Matrix4::translate(0, +1, 0) * Matrix4::rotateX(90) * BoxItgPts<order, 2, 0>::itg_pts;
  ///! integration point weights
  static constexpr constexpr_array<double, n_itg_pts> weights = BoxItgPts<order, 2, 0>::weights;
};

/**
 * Back (Z-) surface gauss points for 3D hexahedron (surface ID -3).
 */
template<int order>
struct BoxItgPts<order, 3, -3> {
  ///! number of integration points
  static constexpr int n_itg_pts = BoxItgPts<order, 2, 0>::n_itg_pts;
  ///! integration points
  static constexpr constexpr_array<ZEROPTV, n_itg_pts> itg_pts = Matrix4::translate(0, 0, -1) * BoxItgPts<order, 2, 0>::itg_pts;
  ///! integration point weights
  static constexpr constexpr_array<double, n_itg_pts> weights = BoxItgPts<order, 2, 0>::weights;
};

/**
 * Front (Z+) surface gauss points for 3D hexahedron (surface ID +3).
 */
template<int order>
struct BoxItgPts<order, 3, +3> {
  ///! number of integration points
  static constexpr int n_itg_pts = BoxItgPts<order, 2, 0>::n_itg_pts;
  ///! integration points
  static constexpr constexpr_array<ZEROPTV, n_itg_pts> itg_pts = Matrix4::translate(0, 0, +1) * BoxItgPts<order, 2, 0>::itg_pts;
  ///! integration point weights
  static constexpr constexpr_array<double, n_itg_pts> weights = BoxItgPts<order, 2, 0>::weights;
};

#define DEF_BOX_SURF(nsd, surf_id) \
template<int order> \
constexpr constexpr_array<ZEROPTV, BoxItgPts<order, nsd, surf_id>::n_itg_pts> BoxItgPts<order, nsd, surf_id>::itg_pts; \
template<int order> \
constexpr constexpr_array<double, BoxItgPts<order, nsd, surf_id>::n_itg_pts> BoxItgPts<order, nsd, surf_id>::weights;

DEF_BOX_SURF(1, -1)
DEF_BOX_SURF(1, +1)

DEF_BOX_SURF(2, -1)
DEF_BOX_SURF(2, +1)
DEF_BOX_SURF(2, -2)
DEF_BOX_SURF(2, +2)

DEF_BOX_SURF(3, -1)
DEF_BOX_SURF(3, +1)
DEF_BOX_SURF(3, -2)
DEF_BOX_SURF(3, +2)
DEF_BOX_SURF(3, -3)
DEF_BOX_SURF(3, +3)

}  // namespace TALYFEMLIB
