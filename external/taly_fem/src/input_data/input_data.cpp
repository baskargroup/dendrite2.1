/*
  Copyright 2014-2017 Baskar Ganapathysubramanian

  This file is part of TALYFem.

  TALYFem is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  TALYFem is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with TALYFem.  If not, see <http://www.gnu.org/licenses/>.
*/
// --- end license text --- //
#include <talyfem/input_data/input_data.h>

#include <map>
#include <string>


namespace TALYFEMLIB {

InputData::InputData()
    : nsd(0),
      ifBoxGrid(false),
      ifTriElem(false),
      basisFunction(BASIS_LINEAR),
      basisRelativeOrder(0),
      ifDD(false),
      L(0),
      Nelem(0),
      // The following values are set based on the default values of the global
      // variables they are associated with. This ensures consistancy between
      // the values. To change these values, edit Utils.h to change the
      // default values.
      ifPrintStat(GLOBALS::gPrintStat),
      ifPrintLog(GLOBALS::gPrintLog),
      ifPrintWarn(GLOBALS::gPrintWarn),
      ifPrintInfo(GLOBALS::gPrintInfo),
      inputFilenameGridField(""),
      inputFilenameGrid(""),
      varListInput(""),
      varListOutput(""),
      typeOfIC(-1),
      ifLoadNodeIndicators(false),
      ifWriteNodeIndicators(false) {
}

// destructor
InputData::~InputData() {
  delete[] L;
  delete[] Nelem;
}


void InputData::ReadConfigFile(std::string filename) {
  try {
    cfg.setAutoConvert(true);  // deprecated
//    this is the correct autoconvert call for the current libconfig version:
//    cfg.setOptions(libconfig::Config::OptionAutoConvert);
    cfg.readFile(filename.c_str());
  } catch (const libconfig::FileIOException &fioex) {
    throw TALYException() << "I/O error while reading file.";
  } catch (const libconfig::ParseException &pex) {
    throw TALYException() << "Parse error at " << pex.getFile() << ":" <<
                             pex.getLine() << " - " << pex.getError();
  }
}

bool InputData::ReadFromFile(const std::string& filename) {
  ReadConfigFile(filename);

  // initialize the basic fields
  InputData::Initialize();
  return true;
}

bool InputData::CheckInputData() const {
  const int size = GetMPISize();

  if ((size > 1) && (nsd == 1)) {
    PrintWarning("1D problem should not be run on more than 1 processor ");
    // return false;
  }

  if (nsd == 0) {
    PrintError("Problem with input data settings! - 0D nsd = ", nsd);
    return false;
  }

  if (ifBoxGrid) {
    if ((nsd == 1) && ((Nelem[0] == 0) || (fabs(L[0]) < 1e-20))) {
      PrintError("Problem with input data settings! - 1D ", "Nelemx ", Nelem[0],
                 " Lx: ", L[0]);
      return false;
    }

    if ((nsd == 2)
        && ((Nelem[0] == 0) || (fabs(L[0]) < 1e-20) || (Nelem[1] == 0)
            || (fabs(L[1]) < 1e-20))) {
      PrintError("Problem with input data settings! - 2D ", " Nelemx: ",
                 Nelem[0], " Nelemy: ", Nelem[1], " Lx: ", L[0], " Ly: ", L[1]);
      return false;
    }

    if ((nsd == 3)
        && ((Nelem[0] == 0) || (fabs(L[0]) < 1e-20) || (Nelem[1] == 0)
            || (fabs(L[1]) < 1e-20) || (Nelem[2] == 0) ||
            (fabs(L[2]) < 1e-20))) {
      PrintError("Problem with input data settings! - 3D ", " Nelemx: ",
                 Nelem[0], " Nelemy: ", Nelem[1], " Nelemz: ", Nelem[2],
                 " Lx: ", L[0], " Ly: ", L[1], " Lz: ", L[2]);
      return false;
    }
  } else {  // end-ifBoxGrid
    if ((inputFilenameGrid == "") && (inputFilenameGridField == "")) {
      PrintError("No grid generated nor loaded! ");
      return false;
    }
  }
  return true;
}

bool InputData::Initialize() {
  // ifPrintStat needs to be read first or else it will not apply to other
  // values that are read before it.
  if (ReadValue("ifPrintStat", ifPrintStat)) {
    GLOBALS::gPrintStat = ifPrintStat;
  }
  if (ReadValue("ifPrintLog", ifPrintLog)) {
    GLOBALS::gPrintLog = ifPrintLog;
  }
  if (ReadValue("ifPrintInfo", ifPrintInfo)) {
    GLOBALS::gPrintInfo = ifPrintInfo;
  }
  if (ReadValue("ifPrintWarn", ifPrintWarn)) {
    GLOBALS::gPrintWarn = ifPrintWarn;
  }
  if (ReadValue("ifPrintTime", ifPrintTime)) {
    GLOBALS::gPrintTime = ifPrintTime;
  }

  if (ReadValue("nsd", nsd)) { }
  if (ReadValue("ifBoxGrid", ifBoxGrid)) { }
  if (ReadValue("ifTriElem", ifTriElem)) { }

  std::string bf_str;
  ReadValue("basisFunction", bf_str);
  basisFunction = bf_str.empty() ? basisFunction : basis_string_to_enum(bf_str);

  if (ReadValue("basisRelativeOrder", basisRelativeOrder)) { }
  if (ReadValue("ifDD", ifDD)) { }
  if (ReadValue("typeOfIC", typeOfIC)) { }
  if (ReadValue("ifLoadNodeIndicators", ifLoadNodeIndicators)) { }
  if (ReadValue("ifWriteNodeIndicators", ifWriteNodeIndicators)) { }

  if (ifBoxGrid) {
    this->L = new double[nsd];
    this->Nelem = new int[nsd];
    PrintStatusStream(std::cerr, "", "arrays created!");

    if (ReadValue("Lx", L[0])) { }
    if (ReadValue("Nelemx", Nelem[0])) { }
    if (nsd > 1) {
      if (ReadValue("Ly", L[1])) { }
      if (ReadValue("Nelemy", Nelem[1])) { }
    }
    if (nsd > 2) {
      if (ReadValue("Lz", L[2])) { }
      if (ReadValue("Nelemz", Nelem[2])) { }
    }
  } else {  // end-ifBoxGrid
    if (ReadValue("inputFilenameGrid", inputFilenameGrid)) { }
  }

  if (ReadValue("inputFilenameGridField", inputFilenameGridField)) { }
  if (ReadValue("varListInput", varListInput)) { }
  if (ReadValue("varListOutput", varListOutput)) { }
  return true;
}

std::ostream& InputData::print(std::ostream& oss) const {
  PrintLogStream(oss, "", "nsd = ", nsd);
  PrintLogStream(oss, "", "ifBoxGrid = ", ifBoxGrid);
  PrintLogStream(oss, "", "ifTriElem = ", ifTriElem);
  PrintLogStream(oss, "", "basisFunction = ",
                 basis_enum_to_string(basisFunction));
  PrintLogStream(oss, "", "basisRelativeOrder = ", basisRelativeOrder);
  PrintLogStream(oss, "", "ifDD = ", ifDD);

  if (ifBoxGrid) {
    for (int i = 0; i < nsd; i++) {
      PrintLogStream(oss, "", "L[", i, "] = ", L[i]);
      PrintLogStream(oss, "", "Nelem[", i, "] = ", Nelem[i]);
    }
  }
  PrintLogStream(oss, "", "inputFilenameGridField  = ", inputFilenameGridField);
  PrintLogStream(oss, "", "inputFilenameGrid  = ", inputFilenameGrid);
  PrintLogStream(oss, "", "varListOutput  = ", varListOutput);
  PrintLogStream(oss, "", "varListInput  = ", varListInput);
  PrintLogStream(oss, "", "typeOfIC = ", typeOfIC);
  PrintLogStream(oss, "", "ifLoadNodeIndicators = ", ifLoadNodeIndicators);
  PrintLogStream(oss, "", "ifWriteNodeIndicators = ", ifWriteNodeIndicators);
  return oss;
}

std::ostream& InputData::printAll(std::ostream& oss) const {
  oss << "nsd = (default: 0)" << std::endl;
  oss << "ifBoxGrid = (Lx=, Ly=, Lz=, Nelemx=, Nelemy=, Nelemz=) " << std::endl;
  oss << "ifTriElem = (false-quadrilateral true-triangle/tet, default:false)"
      << std::endl;
  oss << "orderOfBF = (default: 0)" << std::endl;
  oss << "ifDD = (default: false)" << std::endl;
  oss << "inputFilenameGridField =" << std::endl;
  oss << "inputFilenameGrid =" << std::endl;
  oss << "varListOutput  = (default: all)" << std::endl;
  oss << "varListInput  = (default: all available in file)" << std::endl;
  oss << "typeOfIC = (0-loadFromFile, default: -1)" << std::endl;
  oss << "ifLoadNodeIndicators = (default: false)" << std::endl;
  oss << "ifWriteNodeIndicators = (default: false)" << std::endl;
  return oss;
}

}  // namespace TALYFEMLIB
