/*
  Copyright 2014-2016 Baskar Ganapathysubramanian

  This file is part of TALYFem.

  TALYFem is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  TALYFem is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with TALYFem.  If not, see <http://www.gnu.org/licenses/>.
*/
// --- end license text --- //
#include <talyfem/grid/elem_types/elem2dbox.h>

#include <talyfem/grid/grid_types/grid.h>
#include <talyfem/grid/node.h>


namespace TALYFEMLIB {

const int* ELEM2dBox::GetSurfaceCheckArray() const {
  static const int B4n2DCheckArray[] = {
  // {surface_id, node_id1, node_id2}
     -1, 3, 0,
     +1, 1, 2,
     -2, 0, 1,
     +2, 2, 3
  };

  static const int B9n2DCheckArray[] = {
  // {surface_id, node_id1, node_id2}
     -1, 3, 0, 7,
     +1, 1, 2, 5,
     -2, 0, 1, 4,
     +2, 2, 3, 6
  };

  /* layout of local nodes in element:
   *
   *  3-- 9-- 8-- 2
   *  |   |   |   |
   * 10--15--14-- 7
   *  |   |   |   |
   * 11--12--13-- 6
   *  |   |   |   |
   *  0-- 4-- 5-- 1
   */
  static const int B16n2DCheckArray[] = {
  // {surface_id, node_id1, node_id2}
     -1, 3, 0, 10, 11,
     +1, 1, 2, 6, 7,
     -2, 0, 1, 4, 5,
     +2, 2, 3, 8, 9,
  };

  switch (n_nodes()) {
    case 4: return B4n2DCheckArray;
    case 9: return B9n2DCheckArray;
    case 16: return B16n2DCheckArray;
    default: throw NotImplementedException();
  }
}

int ELEM2dBox::GetNodesPerSurface() const {
  switch (n_nodes()) {
    case 4: return 2;
    case 9: return 3;
    case 16: return 4;
    default: throw NotImplementedException();
  }
}

// This implementation assumes counter-clockwise connectivity
// and surface array node order. If this not the case, the normal
// will point inwards (bad!).
ZEROPTV ELEM2dBox::CalculateNormal(const GRID* grid, int surface_id) const {
  const int* surf_nodes = GetNodesInSurface(surface_id);
  const int nodes[] = {
    surf_nodes[0], surf_nodes[1], (surf_nodes[1] + 1) % n_nodes()
  };

  const ZEROPTV& p1 = grid->GetNode(ElemToLocalNodeID(nodes[0]))->location();
  const ZEROPTV& p2 = grid->GetNode(ElemToLocalNodeID(nodes[1]))->location();
  const ZEROPTV& p3 = grid->GetNode(ElemToLocalNodeID(nodes[2]))->location();
  ZEROPTV elem_normal;
  elem_normal.crossProduct(p2 - p1, p3 - p1);

  ZEROPTV edge_normal;
  edge_normal.crossProduct(p2 - p1, elem_normal);
  edge_normal.Normalize();

  return edge_normal;
}

kBasisFunction ELEM2dBox::basis_function() const {
  switch (n_nodes()) {
    case 4: return BASIS_LINEAR;
    case 9: return BASIS_QUADRATIC;
    case 16: return BASIS_CUBIC;
    default: throw NotImplementedException();
  }
}

}  // namespace TALYFEMLIB
