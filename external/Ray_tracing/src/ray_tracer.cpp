//
// Created by lofquist on 1/18/18.
//

#include <cassert>
#include <iostream>
#include <cstdlib>
#include "raytracer/ray_tracer.h"

bool RayTracer::OutsideBoundingSphere(const double *orig, const double *dir, const double *center, const double radius) {
  double w[3], b, Pb[3], distance_square;
  Vec_Sub(w, center, orig);
  b = Vec_Dot(w, dir) / Vec_Dot(dir, dir);
  Pb[0] = orig[0] + b * dir[0];
  Pb[1] = orig[1] + b * dir[1];
  Pb[2] = orig[2] + b * dir[2];
  distance_square = (Pb[0] - center[0]) * (Pb[0] - center[0]) +
                    (Pb[1] - center[1]) * (Pb[1] - center[1]) +
                    (Pb[2] - center[2]) * (Pb[2] - center[2]);
  return (distance_square > radius * radius);
}

IntersectionType::Type
RayTracer::ifLineIntersectTriangle(const double *orig, const double *dir, const double *vert0, const double *vert1,
                                   const double *vert2) {
  double edge1[3], edge2[3], tvec[3], pvec[3], qvec[3], ray_length;
  Vec_Sub(edge1, vert1, vert0);
  Vec_Sub(edge2, vert2, vert0);
  Vec_Cross(pvec, dir, edge2);
  double det = Vec_Dot(edge1, pvec);
  const double epsilon = 1e-6;
  if (det < 1e-10 && det > -1e-10) {
    /// Ray parallel to the triangle
    return IntersectionType::PARALLEL;
  } else {
    const double inv_det = 1.0 / det;
    Vec_Sub(tvec, orig, vert0);
    const double u = Vec_Dot(tvec, pvec) * inv_det;
    Vec_Cross(qvec, tvec, edge1);
    const double v = Vec_Dot(dir, qvec) * inv_det;
    const double t = Vec_Dot(edge2, qvec) * inv_det;
    /// Judge if the intersection point is inside the triangle
    /// Outside the triangle && outside reach
    ray_length = sqrt(Vec_Dot(dir, dir));
    if (u < -epsilon || v < -epsilon ||
        u > 1 + epsilon || v > 1 + epsilon || (u + v) > 1 + epsilon || t > 1 + epsilon / ray_length || t < 0) {
      return IntersectionType::NO_INTERSECTION;
    } else {
      /// On the triangle edge (possible outcome are on edge or on surface)
      if (u < epsilon || v < epsilon || (u + v) > 1 - epsilon) {
        /// Judge t (t already satisfies condition)
        if (t > 1 - epsilon / ray_length) {
          return IntersectionType::ON_SURFACE;
        } else {
          return IntersectionType::ON_EDGE;
        }
      } else {
        /// Inside triangle (possible outcome are on surface or intersect)
        if (t > 1 - epsilon / ray_length) {
          return IntersectionType::ON_SURFACE;
        } else {
          return IntersectionType::INTERSECT;
        }
      }
    }
    /*    /// Judge if the intersection Vector3d is inside the Triangle, works slow
        if (u < -epsilon || v < -epsilon ||
            u > 1 + epsilon || v > 1 + epsilon || (u + v) > 1 + epsilon || t > 1 + epsilon / ray_length) {
          return IntersectionType::NO_INTERSECTION;
        } else {
          if (u < 1 - epsilon && v < 1 - epsilon && u > epsilon && v > epsilon && u + v < 1 - epsilon) {
            /// Judge t
            if (t < 1 - epsilon / ray_length && t > epsilon / ray_length) {
              return IntersectionType::INTERSECT;
            } else {
              if (t < epsilon / ray_length || t > 1 - epsilon / ray_length) {
                return IntersectionType::ON_SURFACE;
              }
              return IntersectionType::NO_INTERSECTION;
            }
          } else {
            return IntersectionType::ON_EDGE;
          }
        }*/
  }
}

bool RayTracer::ifOutsideBoundingBox(const double *ray_end, const Box3d& bounds) {
  return (ray_end[0] < bounds.min[0]) || (ray_end[0] > bounds.max[0])
         || (ray_end[1] < bounds.min[1]) || (ray_end[1] > bounds.max[1])
         || (ray_end[2] < bounds.min[2]) || (ray_end[2] > bounds.max[2]);
}

InsideType::Type RayTracer::ifInside(const double *ray_start, const double *ray_end) const {

  /// Ignore points that are outside the bounding box for STL file
  if (ifOutsideBoundingBox(ray_end, bounds_)) {
    return InsideType::OUTSIDE;
  }

  int noOfIntersections = 0;
  double dir[3];
  Vec_Sub(dir, ray_end, ray_start);
  for (unsigned int i = 0; i < triangles_.size(); i++) {
    const auto& tri = triangles_[i];

    ///  Determine if the ray intersects with the bounding sphere or a triangle, skipped if not
    if (OutsideBoundingSphere(ray_start, dir, tri.boundingSphere.center, tri.boundingSphere.radius)) {
      continue;
    }

    IntersectionType::Type result_from_ifIntersect =
        ifLineIntersectTriangle(ray_start,
                                dir,
                                tri.v[0].data,
                                tri.v[1].data,
                                tri.v[2].data);
    /// If ON_EDGE or ON_SURFACE or ON_STARTPOINT happens, then this position is vague, jump out of the loop and choose another ray
    if (result_from_ifIntersect == IntersectionType::ON_EDGE ||
        result_from_ifIntersect == IntersectionType::ON_STARTPOINT) {
      return InsideType::UNKNOWN;
    }
    if (result_from_ifIntersect == IntersectionType::ON_SURFACE) {
      return InsideType::ON_SURFACE;
    }
    if (result_from_ifIntersect == IntersectionType::INTERSECT) {
      noOfIntersections++;
    }
  }
  if ((noOfIntersections % 2) != 0) {
    return InsideType::INSIDE;
  } else {
    return InsideType::OUTSIDE;
  }
}

void RayTracer::generateRay(int pick, double *result) {
  const auto& boundingBox = bounds_;

  double random_perturb[3] = {
      1 + double(rand()) / RAND_MAX,
      1 + double(rand()) / RAND_MAX,
      1 + double(rand()) / RAND_MAX
  };
  double x_length = boundingBox.max[0] - boundingBox.min[0];
  double y_length = boundingBox.max[1] - boundingBox.min[1];
  double z_length = boundingBox.max[2] - boundingBox.min[2];

  switch (pick) {
    case 0:
      result[0] = (boundingBox.min[0] - x_length * random_perturb[0]);
      result[1] = (boundingBox.min[1] - y_length * random_perturb[1]);
      result[2] = (boundingBox.min[2] - z_length * random_perturb[2]);
      break;
    case 1:
      result[0] = (boundingBox.max[0] + x_length * random_perturb[0]);
      result[1] = (boundingBox.min[1] - y_length * random_perturb[1]);
      result[2] = (boundingBox.min[2] - z_length * random_perturb[2]);
      break;
    case 2:
      result[0] = (boundingBox.min[0] - x_length * random_perturb[0]);
      result[1] = (boundingBox.max[1] + y_length * random_perturb[1]);
      result[2] = (boundingBox.min[2] - z_length * random_perturb[2]);
      break;
    case 3:
      result[0] = (boundingBox.max[0] + x_length * random_perturb[0]);
      result[1] = (boundingBox.max[1] + y_length * random_perturb[1]);
      result[2] = (boundingBox.min[2] - z_length * random_perturb[2]);
      break;
    case 4:
      result[0] = (boundingBox.min[0] - x_length * random_perturb[0]);
      result[1] = (boundingBox.min[1] - y_length * random_perturb[1]);
      result[2] = (boundingBox.max[2] + z_length * random_perturb[2]);
      break;
    case 5:
      result[0] = (boundingBox.max[0] + x_length * random_perturb[0]);
      result[1] = (boundingBox.min[1] - y_length * random_perturb[1]);
      result[2] = (boundingBox.max[2] + z_length * random_perturb[2]);
      break;
    case 6:
      result[0] = (boundingBox.min[0] - x_length * random_perturb[0]);
      result[1] = (boundingBox.max[1] + y_length * random_perturb[1]);
      result[2] = (boundingBox.max[2] + z_length * random_perturb[2]);
      break;
    case 7:
    default:
      result[0] = (boundingBox.max[0] + x_length * random_perturb[0]);
      result[1] = (boundingBox.max[1] + y_length * random_perturb[1]);
      result[2] = (boundingBox.max[2] + z_length * random_perturb[2]);
      break;
  }
}

Box3d RayTracer::calcBoundingBox(const std::vector<RayTracer::TriangleData> &triangles) {
  Box3d box;
  for (int d = 0; d < 3; d++) {
    box.min[d] = triangles[0].v[0].data[d];
    box.max[d] = box.min[d];
  }

  for (unsigned int i = 0; i < triangles.size(); i++) {
    for (int vert = 0; vert < 3; vert++) {
      for (int dim = 0; dim < 3; dim++) {
        box.min[dim] = std::min(box.min[dim], triangles[i].v[vert].data[dim]);
        box.max[dim] = std::max(box.max[dim], triangles[i].v[vert].data[dim]);
      }
    }
  }

  double eps = 1e-6;
  for (int d = 0; d < 3; d++) {
    box.min[d] -= eps;
    box.max[d] += eps;
  }

  return box;
}

InsideType::Type RayTracer::ifInside(const double *point) const {
  for (int i = 0; i < 8; i++) {
    InsideType::Type res = ifInside(ray_starts_[i].data, point);
    if (res == InsideType::UNKNOWN)
      continue;

    return res;
  }

  return InsideType::UNKNOWN;
}

void RayTracer::setTriangles(std::vector<stl::Triangle> &triangles) {
  triangles_.resize(triangles.size());
  for (unsigned int i = 0; i < triangles.size(); i++) {
    const auto& from = triangles[i];
    auto& to = triangles_[i];

    for (int vert = 0; vert < 3; vert++)
      to.v[vert] = from.v[vert];

    to.boundingSphere = Sphere::fromTriangle(to.v);
  }

  bounds_ = calcBoundingBox(triangles_);

  for (int i = 0; i < 8; i++) {
    generateRay(i, ray_starts_[i].data);
  }
}

void RayTracer::setTriangles(std::vector<RayTracer::TriangleData> &&triangles) {
  triangles_ = triangles;
  for (unsigned int i = 0; i < triangles.size(); i++) {
    triangles_[i].boundingSphere = Sphere::fromTriangle(triangles_[i].v);
  }

  bounds_ = calcBoundingBox(triangles_);

  for (int i = 0; i < 8; i++) {
    generateRay(i, ray_starts_[i].data);
  }
}
