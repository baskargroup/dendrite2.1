function fv_normalized = fvNormalize(fv, xlength, ylength, zlength)
fv_normalized = fv;
xmin = min(fv.vertices(:, 1));
ymin = min(fv.vertices(:, 2));
zmin = min(fv.vertices(:, 3));
fv_normalized.vertices = ... 
    [(fv.vertices(: ,1) - xmin - xlength / 2) / xlength * 2,...
     (fv.vertices(: ,2) - ymin - ylength / 2) / ylength * 2,... 
     (fv.vertices(: ,3) - zmin - zlength / 2) / zlength * 2];
