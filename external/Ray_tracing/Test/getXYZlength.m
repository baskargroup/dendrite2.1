function [x, y, z] = getXYZlength(fv)
x = max(fv.vertices(:, 1)) - min(fv.vertices(:, 1));
y = max(fv.vertices(:, 2)) - min(fv.vertices(:, 2));
z = max(fv.vertices(:, 3)) - min(fv.vertices(:, 3));