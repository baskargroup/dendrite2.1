#include <cassert>
#include <iostream>
#include <fstream>
#include <ctime>

#include "raytracer/stl_reader.h"
#include "raytracer/ray_tracer.h"

using namespace std;
using namespace InsideType;
using namespace IntersectionType;

struct test_point {
  double xyz[3];
  InsideType::Type flag;
  test_point() : xyz{}, flag(InsideType::UNKNOWN) {}
  test_point(float xp, float yp, float zp) : xyz{xp, yp, zp}, flag(InsideType::UNKNOWN) {}
};

int main(int argc, char *argv[]) {
//  std::string stl_file_name = "./Box_normalized.stl";
  std::string stl_file_name = "sphere_bin.stl";
//  string stl_file_name = "./Greenhouse_normalized.stl";
  if (argc == 2) {
    stl_file_name = argv[1];
  } else if (argc > 2) {
    cout << "ERROR: Too many command line arguments" << endl;
  }

  stl::STLData info = stl::parse_stl(stl_file_name);

  vector<stl::Triangle> triangles = info.triangles;
//  std::cout << "STL HEADER = " << info.name << std::endl;
  cout << "# triangles = " << triangles.size() << endl;


/*  /// Output parsed STL file
  fstream filestream;
  filestream.open("STL.txt", ios::out);\
  for (unsigned int i = 0; i < info.triangles.size(); i++) {
    auto &t = info.triangles[i];

    filestream << t.v1.data[0] << "\t" << t.v1.data[1] << "\t" << t.v1.data[2] << "\n"
               << t.v2.data[0] << "\t" << t.v2.data[1] << "\t" << t.v2.data[2] << "\n"
               << t.v3.data[0] << "\t" << t.v3.data[1] << "\t" << t.v3.data[2] << "\n"
               << t.sphere.center[0] << "\t" << t.sphere.center[1] << "\t" << t.sphere.center[2] << "\n"
               << t.sphere.radius << endl;
  }
  filestream.close();*/

  RayTracer raytracer;
  raytracer.setTriangles(triangles);


  /// Generating test points
  double delta = 0.125 / 4;
  vector<test_point> test_points;
  for (double i = -4; i <= 4; i += delta) {
    for (double j = -4; j <= 4; j += delta) {
      for (double k = -4; k <= 4; k += delta) {
        test_point temp(i + double(rand()) / RAND_MAX, j + double(rand()) / RAND_MAX, k + double(rand()) / RAND_MAX);
//        test_point temp(i, j, k);
        test_points.push_back(temp);
      }
    }
  }


  /// Testing points
  cout << "Testing: " << test_points.size() << " points." << endl;
  clock_t start;
  double duration;
  start = clock();

  /// Testing all the points
  for (int i = 0; i < test_points.size(); i++) {
    test_points[i].flag = raytracer.ifInside(test_points[i].xyz);
    if (test_points[i].flag == InsideType::UNKNOWN) {
      cout << "Could not test point " << i << "\n";
      assert(false);
    }

    /*bool truthInside = (sqrt(test_points[i].xyz[0] * test_points[i].xyz[0] +
                 test_points[i].xyz[1] * test_points[i].xyz[1] +
                 test_points[i].xyz[2] * test_points[i].xyz[2])) < 1.0;

    if (truthInside && test_points[i].flag == InsideType::OUTSIDE) {
      bad++;
    } else if (!truthInside && test_points[i].flag != InsideType::OUTSIDE) {
      bad++;
    }*/

    /// Output progress
    if (i % 50000 == 0) {
      cout << "Points tested: " << i << "\tPoints remaining: " << test_points.size() - i << endl;
    }
  }

  duration = (clock() - start) / (double) CLOCKS_PER_SEC;
  cout << "printf: " << duration << '\n';


  /// output result for matlab
  ofstream myfile;
  myfile.open("test.txt", ios::out);
  myfile << "x\ty\tz\tin_or_out\n";
  for (int i = 0; i < test_points.size(); i++) {
    myfile.precision(14);
    myfile << test_points[i].xyz[0] << "\t"
           << test_points[i].xyz[1] << "\t"
           << test_points[i].xyz[2] << "\t"
           << test_points[i].flag << endl;
  }
  myfile.close();

  /// output time
  myfile.open("time.txt", ios::app);
  myfile << test_points.size() << "\t" << triangles.size() << "\t" << duration << endl;
  myfile.close();

}