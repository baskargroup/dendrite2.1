#pragma once

#include <talyfem/fem/cequation.h>
#include <IBM/Geom.h>
#include "SSHTNodeData.h"
#include <point.h>

class SSHTEquation : public TALYFEMLIB::CEquation<SSHTNodeData> {
public:
    void Solve(double dt, double t) override {
        assert(false);
    }

    void Integrands(const TALYFEMLIB::FEMElm& fe, TALYFEMLIB::ZeroMatrix<double>& Ae,
                    TALYFEMLIB::ZEROARRAY<double>& be) override {
        assert(false);
    }

    void Integrands_Ae(const TALYFEMLIB::FEMElm& fe, TALYFEMLIB::ZeroMatrix<double>& Ae) {
        using namespace TALYFEMLIB;

        const int tIndex = fe.nsd() - 1;
        const int nsd = fe.nsd() - 1;
        const double detJ = fe.jacc();
        const double h = pow(detJ * 8.0, 1.0 / 3.0);
        const double detJxW = fe.detJxW();
        const ZEROPTV pt = fe.position();
        const int n_basis_functions = fe.nbf();
        const double Coe_diff = 1.0;
        const double time_stab = h*0.5*0.25;
        const double space_stab = 0.0;
        const double Ci_f = 1.0;

        /// Get the cofactor matrix from FEELm class
        ZeroMatrix<double> ksiX;
        ksiX.redim(nsd + 1, nsd + 1);

        for (int i = 0; i < nsd + 1; i++) {
            for (int j = 0; j < nsd + 1; j++) {
                ksiX(i, j) = fe.cof(j, i) / fe.jacc();
            }
        }

        /// G_{ij} in equation 65 in Bazilevs et al. (2007)
        ZeroMatrix<double> Ge;
        Ge.redim(nsd + 1, nsd + 1);
        for (int i = 0; i < nsd + 1; i++) {
            for (int j = 0; j < nsd + 1; j++) {
                Ge(i, j) = 0.0;
                for (int k = 0; k < nsd + 1; k++) {
                    Ge(i, j) += ksiX(k, i) * ksiX(k, j);
                }
            }
        }

        double G_G_u = 0.0;
        for (int i = 0; i < nsd; i++) {
            for (int j = 0; j < nsd; j++) {
                G_G_u += Ci_f * Coe_diff * Coe_diff * Ge(i, j) * Ge(i, j);
            }
        }


        double tauM = 1.0 / sqrt( Ge(2,2) +  G_G_u);
//        std::cout << Ge(2,2) << "\n";



        for (int a = 0; a < n_basis_functions; a++) {


            for (int b = 0;  b < n_basis_functions; b++) {
                double M = fe.N(a) * fe.dN(b, tIndex)*detJxW;
                double N = 0;

                for (int k = 0; k < nsd; k++) {
                    N += Coe_diff * fe.dN(a, k) * fe.dN(b, k) * detJxW;
                }

                /** Residual on space term : <w,(grad^2 (f + grad^2 u - u_t))>**/
//                double space_res = fe.N(a)*calc_d2f_at(pt)*detJxW;

                /** Residual on time term : <w,(f + grad^2 u (= 0) - u_t)_t>**/
                double time_res = fe.N(a)*(calc_dfdt_at(pt) - fe.d2N(b,tIndex))*detJxW;

                M = M - tauM*time_res;
//                N = N - space_stab*space_res;

                Ae(a, b) += M + N;
            }
        }

    }

    void Integrands_be(const TALYFEMLIB::FEMElm& fe, TALYFEMLIB::ZEROARRAY<double>& be) {
        using namespace TALYFEMLIB;

        const int n_basis_functions = fe.nbf();  // # of basis functions
        const double detJxW = fe.detJxW();      // (determinant of J) cross W

        const ZEROPTV p = fe.position();
        const double force = calc_f_at(p);

        for (int a = 0; a < n_basis_functions; a++) {
            be(a) += fe.N(a) * force * detJxW;
        }

    }
    void ibm_Integrands4side_Ae(const TALYFEMLIB::FEMElm &fe,
                                TALYFEMLIB::ZeroMatrix<double> &Ae,
                                const Geom<SSHTNodeData> *geo,
                                const Geom<SSHTNodeData>::GaussPointInfo &gpinfo,
                                const Point &node,
                                const Point &h) {

    }
    void ibm_Integrands4side_be(const TALYFEMLIB::FEMElm &fe,
                                ZEROARRAY<double> &be,
                                const Geom<SSHTNodeData> *geo,
                                const Geom<SSHTNodeData>::GaussPointInfo &gpinfo,
                                const Point &node,
                                const Point &h) {

    }

protected:
    double calc_f_at(const TALYFEMLIB::ZEROPTV& pt) const {
        return ((exp(pt.z())*sin(M_PI*pt.x())*sin(M_PI*pt.y()))*(2*M_PI*M_PI + 1));
    }
    double calc_dfdt_at(const TALYFEMLIB::ZEROPTV& pt) const {
        return calc_f_at(pt);
    }
    double calc_d2f_at(const TALYFEMLIB::ZEROPTV& pt) const {
        return -((exp(pt.z())*sin(M_PI*pt.x())*sin(M_PI*pt.y()))*(2*M_PI*M_PI + 1))*(2*M_PI*M_PI);
    }

};
