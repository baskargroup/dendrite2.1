//
// Created by maksbh on 11/24/18.
//
#include <oda.h>
#include <SSHTEquation.h>
#include "TalyEquation.h"
#include <DendriteUtils.h>
#include "Globals.h"
#include "IBM/IBMSolver.h"


#include "SSHTNodeData.h"

#include "SSHTEquation.h"
#include "PETSc/Solver/LinearSolver.h"
#include "PETSc/PetscUtils.h"
#include <IBM/IBMSolver.h>
#include <IBM/IBMNonLinearSolver.h>
#include  <IBM/IBMUtils.h>
using namespace PETSc;

int main (int argc, char** argv) {
    dendrite_init(argc, argv);
    ot::DA *octDA = createRegularDA(4, 5);
    double problem_size[3]{4.0,4.0,4.0};

//    std::cout << octDA->getMaxNode() << " == " << octDA->getMinNode() << "\n";
    libconfig::Config cfg;  ///< libconfig config object
    cfg.readFile("config.txt");
    std::vector<GeomDef> geom_defs;
    std::vector<Geom<SSHTNodeData> * > geoms;
    if (cfg.exists("geometries")) {
        const auto &geometries = cfg.getRoot()["geometries"];
        geom_defs.resize(geometries.getLength());
        for (unsigned int i = 0; i < geom_defs.size(); i++) {
            geom_defs[i].read_from_config(geometries[i]);
        }
    }

    for (unsigned int i = 0; i < geom_defs.size(); i++) {
        auto geo = new Geom<SSHTNodeData>(geom_defs[i]);
        geoms.push_back(geo);
        // todo set refinement level to the mesh_def, in the future, this should be independent of each geometry.
//        geoms[i]->setRefineLvl(5);
    }

    IBMSolver<SSHTNodeData> ibm(octDA,geoms,problem_size);
    ibm.partitionGeometries();

    Point domain_min(0.0, 0.0, 0.0);
    Point domain_max(4.0, 4.0, 4.0 );
    auto talyEq = new TalyEquation<SSHTEquation,SSHTNodeData>(octDA,domain_min,domain_max,1);
    auto talyMat = talyEq->mat;
    auto talyVec = talyEq->vec;

    auto ts =  setIBMNonLinearSolver<SSHTEquation,SSHTNodeData>(talyEq,octDA,geoms,&ibm);
    ts->updateBoundaries(octDA);

}