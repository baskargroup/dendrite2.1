//
// Created by maksbh on 2/23/19.
//
#include <oda.h>
#include <DendriteUtils.h>
#include <PETSc/PetscUtils.h>
#include <PosRefine.h>
using namespace PETSc;

static unsigned int getNodeWeight(const ot::TreeNode * t) {
  if(t->getLevel() == 8){
    return 7;
  }
  else{
    return 1;
  }

}

int main(int argc, char **argv) {
  /// Initialize dendrite
  dendrite_init(argc, argv);

  if(argc < 2){
    if(!TALYFEMLIB::GetMPIRank()){
      std::cout << "maxLevel minLevel\n";
      return 0;
    }
  }
  double problemSize[3]{10.0,10.0,10.0};
  int maxLevel = std::atoi(argv[1]);
  int minLevel = std::atoi(argv[2]);

  ot::DA * octDA = createRegularDA(minLevel);
  double t = 0.0;
  double center[3] {5.0,5.0,8.0};
  double velocity[3] {0.0,0.1,-0.4};
  double radius = 2.0;
  double dt = 0.1;
  Vec vec;
  int ndof = 4;
  int counter = 0;
  octDA->petscCreateVector(vec, false, false,ndof);
  Vec out;
  while (t < 10.0){
    counter ++;
    t = t+dt;
    TALYFEMLIB::PrintStatus("Time = ",t );
    center[0] = center[0] + velocity[0]*dt;
    center[1] = center[1] + velocity[1]*dt;
    center[2] = center[2] + velocity[2]*dt;
    TALYFEMLIB::PrintStatus("Refining ",t );
    while(true) {
      PosRefine posRefine(octDA, problemSize, center, radius, maxLevel, minLevel,false);
      ot::DA * newDA = posRefine.getRefineDA(getNodeWeight);
      if(newDA == octDA){
        break;
      }
      posRefine.performIntergridTransfer(newDA,vec,out,ndof);
      posRefine.performVectorExchange(newDA,vec,out,ndof);
      posRefine.finalizeAMR(newDA);
      std::swap(octDA,newDA);

    }
    TALYFEMLIB::PrintStatus("Coarsening ",t );
    {

        PosRefine posRefine(octDA, problemSize, center, radius, maxLevel, minLevel,true);
        ot::DA * newDA = posRefine.getRefineDA(getNodeWeight);


        posRefine.performIntergridTransfer(newDA,vec,out,ndof);
//      MPI_Barrier(MPI_COMM_WORLD);
//      TALYFEMLIB::PrintStatus("Coarsening ",t );
//      MPI_Barrier(MPI_COMM_WORLD);
//      exit(0);
        posRefine.performVectorExchange(newDA,vec,out,ndof);
        posRefine.finalizeAMR(newDA);
        std::swap(octDA,newDA);

    }
    TALYFEMLIB::PrintStatus("Complete" );

    if(counter % 100) {
      std::string fname = "file" + std::to_string(0);
      if(octDA->isActive()) {
        octDA->petscVecTopvtu(vec, fname.c_str(), nullptr, false, false, ndof, problemSize);
      }
    }

  }

  dendrite_finalize(octDA);

}