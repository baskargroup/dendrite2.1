//
// Created by maksbh on 3/11/19.
//

#ifndef DENDRITE2_0_SSHTREFINE_H
#define DENDRITE2_0_SSHTREFINE_H

#include <Refinement/Refinement.h>
#include <DendriteUtils.h>

class PosRefine : public Refinement {

  double * coords;
  double center_[m_uiDim];
  double radius_;
  int maxLevel_;
  int minLevel_;
  bool doCoarse_ = false;
public:
    PosRefine(ot::DA *da, double * problemSize,const  double center[3],
        const double radius, const int maxLevel, const int minLevel, bool doCoarse);

    ~PosRefine();

  virtual ot::DA_FLAGS::Refine calcRefineFlags() override;

};

PosRefine::PosRefine(ot::DA *da, double * problemSize, const double center[3],
    const double radius, const int maxLevel, const int minLevel,  bool doCoarse)
        : Refinement(da,problemSize) {
    coords = new double[da->getNumNodesPerElement()*m_uiDim];
    center_[0] = center[0];
    center_[1] = center[1];
    center_[2] = center[2];
    radius_ =  radius;
    maxLevel_ = maxLevel;
    minLevel_ = minLevel;
    doCoarse_ = doCoarse;
    if (da->isActive()) {
        this->calcRefinementVector();
    }
}

ot::DA_FLAGS::Refine PosRefine::calcRefineFlags() {

  this->baseDA_->getElementalCoords(baseDA_->curr(),coords);
  for(int i = 0; i < baseDA_->getNumNodesPerElement(); i++){
    convertOctToPhys(&coords[3*i],problemSize_);
  }
  double dist = 0;
  for(int i = 0;i <  baseDA_->getNumNodesPerElement(); i++){
    double xDist = center_[0] - coords[3*i + 0];
    double yDist = center_[1] - coords[3*i + 1];
    double zDist = center_[2] - coords[3*i + 2];
    dist = xDist*xDist + yDist*yDist + zDist*zDist;

    if((dist < (radius_*radius_)) and (not(doCoarse_))){
      if(this->baseDA_->getLevel(baseDA_->curr()) < maxLevel_){
        return ot::DA_FLAGS::Refine::DA_REFINE;
      }
    }
    if((dist >= (radius_*radius_)) and (doCoarse_) and (this->baseDA_->getLevel(baseDA_->curr()) > minLevel_)){
      return ot::DA_FLAGS::Refine::DA_COARSEN;
    }
  }

  return ot::DA_FLAGS::Refine::DA_NO_CHANGE;


}

PosRefine::~PosRefine(){
  delete[] coords;
}
#endif //DENDRITE2_0_SSHTREFINE_H
