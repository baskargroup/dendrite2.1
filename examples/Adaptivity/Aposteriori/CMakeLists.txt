set(EXAMPLE_INC
        include/SSHTNodeData.h
        include/SSHTEquation.h
        include/SSHTRefine.h
        include/SSHTUtils.h
        )

set(EXAMPLE_SRC
        src/main.cpp
        )

add_executable(adap  ${EXAMPLE_SRC} ${EXAMPLE_INC})
target_include_directories(adap PUBLIC include)
target_link_libraries(adap dendrite2 dendro5 ${MPI_LIBRARIES} m)