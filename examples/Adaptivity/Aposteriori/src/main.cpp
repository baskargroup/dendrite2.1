//
// Created by maksbh on 2/23/19.
//
#include <oda.h>
#include <SSHTEquation.h>
#include "TalyEquation.h"
#include <DendriteUtils.h>
#include "Globals.h"

#include "SSHTNodeData.h"
#include "SSHTEquation.h"
#include <SSHTRefine.h>
#include "PETSc/Solver/LinearSolver.h"
#include "PETSc/PetscUtils.h"
#include "SSHTUtils.h"

using namespace PETSc;

int main(int argc, char **argv) {
  /// Initialize dendrite
  dendrite_init(argc, argv);

  /// get mpi rank and npe count (cpu count)
  int rank, npes;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &npes);

  ot::DA *octDA = createRegularDA(1, 8);
  MPI_Barrier(MPI_COMM_WORLD);

  Point domain_min(0, 0, 0);
  Point domain_max(1, 1, 1);

  auto talyEq = new TalyEquation<SSHTEquation, SSHTNodeData>(octDA, domain_min, domain_max, 1);

  const double problemSize[3]{1.0, 1.0, 1.0};

  LinearSolver *solver = setLinearSolver(talyEq, octDA, 1, false);

  solver->setBoundaryCondition(BoundaryFunc);

  MPI_Barrier(MPI_COMM_WORLD);

/*  /// Analytical solution
  auto analytic_sol = [&](double x, double y, double z, double * val) {
    *//*const double omega(2 * M_PI), b(0.5), d(0.05), a(0.2);
    const double omegat(omega *z), sigma(d *d);
    const double x0(a * cos(omegat) + b), y0(a * sin(omegat) + b);
    const double u = exp(-(pow((x - x0), 2) + pow((y - y0), 2)) / sigma);*//*
    val[0] =  (x+y+z);
  };*/
  std::function<void(double, double, double,
                     double *)> f_rhs=[]( const double x, const double y, const double z, double *var){
    double coord[]{x,y,z};
      convertOctToPhys(coord);
      var[0] =  (coord[0]+coord[1]+coord[2]);
  };

  double *err = NULL;
  Vec local;
  octDA->petscCreateVector(local, false, false,1);
  octDA->petscSetVectorByFunction(local,f_rhs, false, false,1);
  /// Number of times you want to run the refine loop.
  for (int time = 0; time < 1; time++) {

//    PrintStatus("Starting solve");
//    solver->solve();
//    PrintStatus("Finished solve");

    /// Vector you want to connect while using refinement criteria.
    Vec soln = local;


    std::vector<VecInfo> v(1);
    v[0] = VecInfo(PLACEHOLDER_REFINE, soln, 1, 0);

    /// Constructor for refinement for value based refinement
    SSHTRefine refine(octDA, v, 1, problemSize, RefineType::APOSTERIORI, true);

    /// Get the new refined DA.
    octDA = refine.getRefineDA();

    Vec out;

    /// Perform the intergrid Transfer between old DA and new DA.
    refine.performIntergridTransfer(octDA, solver->getCurrentSolution(), out, 1);
    /// Perform the vector exchange .
    refine.performVectorExchange(octDA,soln,out,1, false);

    /// Finalize the refinement. This will delete the old DA if required.
    refine.finalizeAMR(octDA);
    int lsize;
    VecGetSize(soln,&lsize);
    std::cout << "Local Size = " << lsize << "\n";
    /// Update the new solver and equation and destroy the old ones.
    updateEquationAndSolver(octDA, talyEq, solver);

    VecCopy(soln,solver->getCurrentSolution());




  }

  dendrite_finalize(octDA);

}