#pragma once

#include <talyfem/fem/cequation.h>
#include "SSHTNodeData.h"

class SSHTEquation : public TALYFEMLIB::CEquation<SSHTNodeData> {
public:
    void Solve(double dt, double t) override {
        assert(false);
    }

    void Integrands(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae,
                    TALYFEMLIB::ZEROARRAY<double> &be) override {
        assert(false);
    }

    void Integrands_Ae(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae) {
        using namespace TALYFEMLIB;
        // Basic things first
        int tIndex = fe.nsd() - 1;
        int nSpatialDim = fe.nsd() - 1;
        const double detJ = fe.jacc();
        const double h = pow(detJ * 8.0, 1.0 / 3.0);
        ZEROPTV pt = fe.position();

        // Data related to the problem
        const double kdif = 100;

        // Loop on local matrix
        for (int a = 0; a < fe.nbf(); a++) {
            for (int b = 0; b < fe.nbf(); b++) {
                double M = fe.N(a) * fe.dN(b, tIndex);
                double N = 0;
                for (int k = 0; k < nSpatialDim; k++) {
                    N += fe.dN(a, k) * fe.dN(b, k);
                }
                Ae(a, b) += (M + kdif * N) * fe.detJxW();
            }
        }
    }

    void Integrands_be(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZEROARRAY<double> &be) {
        using namespace TALYFEMLIB;
        // Basic things first
        int tIndex = fe.nsd() - 1;
        int nSpatialDim = fe.nsd() - 1;
        const double detJ = fe.jacc();
        const double h = pow(detJ * 8.0, 1.0 / 3.0);
        ZEROPTV pt = fe.position();

        // Data related to the problem
        double force = CalcForce(pt, 100);

        // Loop on local vector
        for (int a = 0; a < fe.nbf(); a++) {
            be(a) += fe.N(a) * force * fe.detJxW();
        }
    }

    double CalcForce(TALYFEMLIB::ZEROPTV const &pt, double const kdif) {
        double x = pt.x(), y = pt.y(), z = pt.z();
        double t = z;
        int if2D_ = 1;

        double a = 0.2, b = 0.5, omega = (2.0 * M_PI), sigma = 0.05, S = sigma * sigma;
        double x0 = b + a * cos(omega * t);
        double y0 = b + a * sin(omega * t);

        double l1 = (x - x0);
        double l2 = (y - y0);

        double g = (l1 * l1) / S + if2D_ * (l2 * l2) / S;
        double dgt = 2.0 * a * omega * l1 * sin(omega * t) / S
                     - if2D_ * 2.0 * a * omega * l2 * cos(omega * t) / S;

        double dgx = 2.0 * l1 / S;
        double dgy = if2D_ * 2.0 * l2 / S;

        double d2gx2 = 2.0 / S;
        double d2gy2 = if2D_ * 2.0 / S;

        double u = exp(-g);
        double dudt = -exp(-g) * dgt;

        double dudx = -exp(-g) * dgx;
        double dudy = -exp(-g) * dgy;
        double dudz = 0;

        double d2udx2 = exp(-g) * (dgx * dgx - d2gx2);
        double d2udy2 = exp(-g) * (dgy * dgy - d2gy2);
        double d2udz2 = 0;

        double f = dudt - kdif * (d2udx2 + d2udy2);
        return f;
    }

protected:
//    double calc_d2u_at(const TALYFEMLIB::ZEROPTV &pt) const {
////        return (1);
//        return (-3 * M_PI * M_PI * sin(M_PI * pt.x()) * sin(M_PI * pt.y()) * sin(M_PI * pt.z()));
//    }
};
