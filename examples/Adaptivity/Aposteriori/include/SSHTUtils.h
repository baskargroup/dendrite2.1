//
// Created by maksbh on 3/11/19.
//

#ifndef DENDRITE2_0_SSHTUTILS_H
#define DENDRITE2_0_SSHTUTILS_H

#include <PETSc/BoundaryConditions.h>

using namespace PETSc;

double CalcBoundaryCondition(double x, double y, double z) {
    return 0.0;
}

double CalcInitialCondition(double x, double y, double z) {
    double t = z;
    double a = 0.2, b = 0.5, omega = (2.0 * M_PI), sigma = 0.05, S = sigma * sigma;
    double x0 = b + a * cos(omega * t);
    double y0 = b + a * sin(omega * t);

    int if2D_ = 1;
    double l1 = (x - x0);
    double l2 = (y - y0);

    double g = (l1 * l1) / S + if2D_ * (l2 * l2) / S;

    double u = exp(-g);
    return u;
}

Boundary BoundaryFunc(double x, double y, double z, unsigned int nodeID) {
    double xmax = 1.0;
    double ymax = 1.0;
    Boundary b;
    static constexpr double eps = 1e-14;
    bool onTimeBoundary = fabs(z) < eps;
    bool onSpatialBoundary = fabs(x) < eps || fabs(y) < eps || fabs(x - xmax) < eps || fabs(y - ymax) < eps;


    if (onTimeBoundary || onSpatialBoundary) {
        double val = 0;

        //Initial condition
        if (onTimeBoundary) {
            val = CalcInitialCondition(x, y, z);
        }

        // Boundary condition
        if (onSpatialBoundary) {
            val = CalcBoundaryCondition(x, y, z);
        }

        // Add this value as a Dirichlet value
        b.addDirichlet(0, val);
    }
    return b;
}

#endif //DENDRITE2_0_SSHTUTILS_H
