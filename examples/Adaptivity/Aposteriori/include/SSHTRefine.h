//
// Created by maksbh on 3/11/19.
//

#ifndef DENDRITE2_0_SSHTREFINE_H
#define DENDRITE2_0_SSHTREFINE_H

#include <Refinement/Refinement.h>
#include <DendriteUtils.h>

class SSHTRefine : public Refinement {
  octNeighbours octNeighbor_;
protected:
    using Refinement::calcElementalError;

public:
    SSHTRefine(ot::DA *da,std::vector<VecInfo> & vec, int ndof, const DENDRITE_REAL *problemSize,const RefineType refineType, const bool surfaceError);

    DENDRITE_REAL calcElementalError(FEMElm & fe, const octVolume & oct) override;

    DENDRITE_REAL calcSurfaceError(const TALYFEMLIB::ZEROPTV & pos,
                                   const octSurface &current, const octSurface &neighbour);

    DENDRITE_REAL calcForcingFunction(const TALYFEMLIB::ZEROPTV &pt);

    ot::DA_FLAGS::Refine calcRefineFlags() override;

};

SSHTRefine::SSHTRefine(ot::DA *da, std::vector<VecInfo> & vec, int ndof, const DENDRITE_REAL *problemSize, const RefineType refineType,const bool surfaceError)
        : Refinement(da, vec, problemSize,refineType, surfaceError) {

    if (da->isActive()) {
        this->calcRefinementVector();

    }
}

DENDRITE_REAL SSHTRefine:: calcElementalError(FEMElm & fe,const octVolume & oct){

  for(int i = 0;i < 8;i++){
    double sum = oct.coords[3*i+0] + oct.coords[3*i+1] + oct.coords[3*i+2];
    if(not(FEQUALS(sum,oct.soln[i]))) {
      std::cout << oct.coords[3 * i + 0] << " " << oct.coords[3 * i + 1] << " " << oct.coords[3 * i + 2] << " "
                << oct.soln[i] << "\n";
    }
  }
  return 0;
}


ot::DA_FLAGS::Refine SSHTRefine::calcRefineFlags() {

  return ot::DA_FLAGS::Refine::DA_REFINE ;

}

DENDRITE_REAL SSHTRefine::calcForcingFunction(const TALYFEMLIB::ZEROPTV &pt) {
    return (-3 * M_PI * M_PI * sin(M_PI * pt.x()) * sin(M_PI * pt.y()) * sin(M_PI * pt.z()));
}

DENDRITE_REAL SSHTRefine::calcSurfaceError(const TALYFEMLIB::ZEROPTV & pos,
                               const octSurface &current, const octSurface &neighbour){

  std::cout << pos << " " << current.valueFEM[0] << " " << neighbour.valueFEM[0] <<  "\n";
}

#endif //DENDRITE2_0_SSHTREFINE_H
