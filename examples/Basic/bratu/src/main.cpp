/* ------------------------------------------------------------------------

    Solid Fuel Ignition (SFI) problem.  This problem is modeled by
    the partial differential equation

            -Laplacian u - lambda*exp(u) = 0,  0 < x,y < 1,

    with boundary conditions

             u = 0  for  x = 0, x = 1, y = 0, y = 1.
             
    For checking with analytical solution, we solve 1D problem given by
    
            u" - pi^2 * exp(u) = 0,  0 < x < 1,
            u(x) = -ln(1+cos(pi*(x)))
    It is solved for 0.0 <= x <= 0.5 range, as u(x=1)=inf

  ------------------------------------------------------------------------- */

#include <vector>

#include <oda.h>
#include "TalyEquation.h"
#include <DendriteUtils.h>
#include "PETSc/Solver/NonLinearSolver.h"
#include "PETSc/PetscUtils.h"

#include "BTEquation.h"
#include "BTInputData.h"

using namespace PETSc;

int main(int argc, char **argv) {
  /// Initialize dendrite
  dendrite_init(argc, argv);
  /// get mpi rank and npe count (cpu count)
  int rank, npes;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &npes);
  /// read parameters from config.txt
  BTInputData idata;
  if (!idata.ReadFromFile()) {  /// read from file named "config.txt"
    throw std::runtime_error("[ERR] Error reading input data, check the config file!");
  }
  if (!idata.CheckInputData()) {
    throw std::runtime_error("[ERR] Problem with input data, check the config file!");
  }

  /// specify mesh dimension
  int refine_lvl = idata.mesh_def.refine_lvl;
  ot::DA *octDA = createRegularDA(refine_lvl);
  MPI_Barrier(MPI_COMM_WORLD);

  Point domain_min(idata.mesh_def.channel_min.x(), idata.mesh_def.channel_min.y(), idata.mesh_def.channel_min.z());
  Point domain_max(idata.mesh_def.channel_max.x(), idata.mesh_def.channel_max.y(), idata.mesh_def.channel_max.z());

  /// create solver
  // number of degree of freedom
  int ndof = 1;
  // matrix based or matrix free
  bool mfree = idata.mfree;

  auto talyEq = new TalyEquation<BTEquation, BTNodeData>(octDA, domain_min, domain_max, ndof);
  auto talyMat = talyEq->mat;
  auto talyVec = talyEq->vec;

  talyMat->setVector({VecInfo(PLACEHOLDER_GUESS, ndof, 0)});
  talyVec->setVector({VecInfo(PLACEHOLDER_GUESS, ndof, 0)});

  NonlinearSolver *btSolver = setNonLinearSolver(talyEq, octDA, ndof, mfree);
  /// apply solver parameter from config.txt
  idata.solverOptionsBT.apply_to_petsc_options("-bt_");
  {
    SNES m_snes = btSolver->snes();
    SNESSetOptionsPrefix(m_snes, "bt_");
    SNESSetFromOptions(m_snes);
  }

  const auto analytic_sol = [&](double x, double y, double z, int dof) {
    return -log(1 + cos(M_PI * (x)));
  };

  /// set boundary conditions - dirichlet w/ analytic solution on the walls
  btSolver->setBoundaryCondition([&](double x, double y, double z, int nodeID) -> Boundary {
    Boundary b;
    static constexpr double eps = 1e-14;
    if ((fabs(x - domain_min.x()) < eps) ||
        (fabs(x - domain_max.x()) < eps)) {
      b.addDirichlet(0, analytic_sol(x, y, z, 0));
    }
    return b;
  });

  PrintStatus("Starting solving");
  btSolver->solve();
  PrintStatus("Done solving");
  octDA->petscVecTopvtu(btSolver->getCurrentSolution(), "bratu");
  /// Save vector file (only for regression test)
  if (idata.dump_vec) {
    petscDumpFilesforRegressionTest(octDA,btSolver->getCurrentSolution(), "solution_vec.vec");
  }

  /// compare with analytical solution
  const double problemSize[3]{domain_max.x() - domain_min.x(),
                              domain_max.y() - domain_min.y(),
                              domain_max.z() - domain_min.z()};
  double *err = petscCalcL2Error(octDA, problemSize, btSolver->getCurrentSolution(), ndof, analytic_sol);
  PrintStatus("Error = ", err[0]);

  /// Clean up
  delete btSolver;
  dendrite_finalize(octDA);

}
