#!/usr/bin/env python3

import os
import sys
import subprocess
import re

this_dir = os.path.dirname(os.path.realpath(__file__))
scripts_dir = os.path.join(this_dir, '..', '..', 'scripts')
sys.path.append(scripts_dir)

executable = os.path.join(scripts_dir, '..', 'cmake-build-debug', 'examples', 'bratu', 'bt')
assert os.path.exists(executable), "Program executable missing ({})".format(executable)

runner = 'mpirun'
flags = ['refine_lvl', '5', '-ksp_rtol', '1e-12', '-no_output']

tests = [
    ('Serial (assembled)', subprocess.check_output([runner, '-n', '1', executable] + flags, stderr=subprocess.STDOUT)),
    ('Parallel (assembled), n = 3', subprocess.check_output([runner, '-n', '3', executable] + flags, stderr=subprocess.STDOUT)),
    ('Parallel (assembled), n = 7', subprocess.check_output([runner, '-n', '7', executable] + flags, stderr=subprocess.STDOUT)),
    ('Serial (matrix-free)', subprocess.check_output([runner, '-n', '1', executable, '-mfree', 'True'] + flags, stderr=subprocess.STDOUT)),
    ('Parallel (matrix-free), n = 3', subprocess.check_output([runner, '-n', '3', executable, '-mfree', 'True'] + flags, stderr=subprocess.STDOUT)),
    ('Parallel (matrix-free), n = 7', subprocess.check_output([runner, '-n', '7', executable, '-mfree', 'True'] + flags, stderr=subprocess.STDOUT)),
]


def get_error(output):
    match = re.search(r'L2 error = ([\d.e-]+)', str(output))
    if match:
        return float(match.group(1))
    return None


max_err = -9999999999
min_err = 9999999999
results = {}
for name, output in tests:
    err = get_error(output)
    max_err = max(max_err, err)
    min_err = min(min_err, err)
    results[name] = err

diff = max_err - min_err
ok = (diff < 1e-9)
color = '' if ok else '\033[91m'
end_color = '\033[0m'
print(color + "Error range:", diff, end_color)

if not ok or '-verbose' in sys.argv[1:]:
    print(results)

    # log results to file
    with open('output.log', 'wb') as f:
        for name, output in tests:
            f.write(str.encode('#####' + name + '#####\n'))
            f.write(output)
            f.write(str.encode('\n\n'))

    print("Program output saved in {}/output.log.".format(os.getcwd()))
