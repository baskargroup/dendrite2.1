#!/usr/bin/env python3

import os
import sys
import subprocess
import re

this_dir = os.path.dirname(os.path.realpath(__file__))
scripts_dir = os.path.join(this_dir, '..', '..', 'scripts')
sys.path.append(scripts_dir)

executable = os.path.join(scripts_dir, '..', 'cmake-build-relwithdebinfo', 'examples', 'bratu', 'bt')
assert os.path.exists(executable), "Program executable missing ({})".format(executable)

runner = 'mpirun'
flags = ['refine_lvl', '6', '-no_output', '-pc_type', 'none', '-ksp_rtol', '1e-5']

if not os.path.exists('asm.py') or not os.path.exists('mfree.py'):
    print('Generating results...')
    tests = [
        ('Serial (assembled)', subprocess.check_output([runner, '-n', '7', executable, '-mfree', '0'] + flags + ['-log_view', ':asm.py:ascii_info_detail'], stderr=subprocess.STDOUT)),
        ('Serial (matrix-free)', subprocess.check_output([runner, '-n', '7', executable, '-mfree', '1'] + flags + ['-log_view', ':mfree.py:ascii_info_detail'], stderr=subprocess.STDOUT)),
    ]
else:
    print('Using existing results.')

def avg_time(ev):
    avg = 0.0
    for proc_result in ev.values():
      avg += (proc_result['time'] / proc_result['count'])
    avg = avg / len(ev)
    return avg

def process(name, stages):
    jac_time = avg_time(stages['solve']['SNESJacobianEval'])
    matmult_time = avg_time(stages['solve']['MatMult'])
    n_kspsolves = stages['solve']['KSPSolve'][0]['count']  # should equal # of snes iterations
    n_matmults = stages['solve']['MatMult'][0]['count']
    n_matmults_per_kspsolve = n_matmults / n_kspsolves
    kspsolve_time = avg_time(stages['solve']['KSPSolve'])

    print('{}\n  Average assembly time: {}, average MatMult time: {}, average number of MatMults per KSPSolve: {}\n  ksp_time = {} + {} * n_iterations\n    expected: {:.3f} vs actual: {:.3f})'.format(name, jac_time, matmult_time, n_matmults_per_kspsolve, jac_time, matmult_time, jac_time + matmult_time * n_matmults_per_kspsolve, jac_time + kspsolve_time))


from asm import Stages as asmStages
process('Assembled', asmStages)
from mfree import Stages as mfStages
process('Matrix-free', mfStages)
