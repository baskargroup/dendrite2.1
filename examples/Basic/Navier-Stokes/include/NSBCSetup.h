//
// Created by boshun on 8/11/18.
//

#pragma once

#include "NSInputData.h"
#include "NSNodeData.h"
#include <PETSc/BoundaryConditions.h>

/**
 * Class calculates Appropriate boundary conditions for NSHT objects
 * Usage:
 * BoundaryConditions bc(b);
 * bc.setBC;
 */

class NSBCSetup {

 public:
  NSBCSetup(NSInputData *inputData, TimeInfo *time)
      : input_data_(inputData), time_(time) {
    PrintInfo("Setting up boundary conditions");
  }

  /**
   * Method to setup Boundary conditions for different cases of NS equations.
   * @param b Boundary object to setup dirichlet or other conditions
   * @param position position
   * @param boundary_def input_data_ boundary definition vector
   */
  void setBoundaryConditionsNS(PETSc::Boundary &b, const ZEROPTV &position, std::vector<BoundaryDef> boundary_def) {

    /// All the possible cases
    static const double eps = 1e-15;

    bool x_minus_wall = fabs(position.x() - input_data_->mesh_def.channel_min[0]) < eps;
    bool y_minus_wall = fabs(position.y() - input_data_->mesh_def.channel_min[1]) < eps;
    bool z_minus_wall = fabs(position.z() - input_data_->mesh_def.channel_min[2]) < eps;
    bool x_max_wall = fabs(position.x() - input_data_->mesh_def.channel_max[0]) < eps;
    bool y_max_wall = fabs(position.y() - input_data_->mesh_def.channel_max[1]) < eps;
    bool z_max_wall = fabs(position.z() - input_data_->mesh_def.channel_max[2]) < eps;

    /// walls are vectors in this exact order, the order matches with boundary_def.side
    std::vector<bool> walls = {x_minus_wall, x_max_wall, y_minus_wall, y_max_wall, z_minus_wall, z_max_wall};
    int wall_count = std::count(walls.begin(), walls.end(), true);

    /// Choose no-slip walls for velocity
    bool wallForNoSlip = false;
    bool onWall = false;
    for (int wallID = 0; wallID < 6; wallID++) {
      wallForNoSlip =
          wallForNoSlip || (walls[wallID] && boundary_def[wallID].vel_type == BoundaryDef::Vel_Type::NO_SLIP);
      onWall = onWall || walls[wallID];
    }

    /// fix velocity to zero(no-slip) on all boundaries
    if (wallForNoSlip) {
      for (int dim = 0; dim < input_data_->nsd; dim++) {
        b.addDirichlet(dim, 0.0);
      }
    }
    bool anyPressureBC = false;
    for (const auto &bc : boundary_def) {
      if (bc.pressure_type == BoundaryDef::Pressure_Type::DIRICHLET_P) {
        anyPressureBC = true;
      }
    }
    if (!anyPressureBC and x_minus_wall and y_minus_wall and z_minus_wall) {
      b.addDirichlet(NSNodeData::PRESSURE, 0.0);
    }
    /// add dirichlet bc
    for (int wallID = 0; wallID < 6; wallID++) {
      /// not duplicate bc && on wall && dirichlet bc specified for velocity
      if (!wallForNoSlip && walls[wallID] && boundary_def[wallID].vel_type == BoundaryDef::Vel_Type::DIRICHLET_VEL) {
        for (int dim = 0; dim < input_data_->nsd; dim++) {
          b.addDirichlet(dim, boundary_def[wallID].getRamping(time_->getCurrentTime(), boundary_def[wallID].wall_velocity)[dim]);
        }
      }
      /// add dirichlet bc for pressure
      if (walls[wallID] && boundary_def[wallID].pressure_type == BoundaryDef::Pressure_Type::DIRICHLET_P) {
        b.addDirichlet(NSNodeData::PRESSURE, boundary_def[wallID].pressure);
      }
    }
  };
 private:
  NSInputData *input_data_;
  TimeInfo *time_;
};
