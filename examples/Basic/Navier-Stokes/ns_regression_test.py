#!/usr/bin/env python3

import os
import sys
import re

this_dir = os.path.dirname(os.path.realpath(__file__))
scripts_dir = os.path.join(this_dir, '..', '..', '..', 'scripts')
sys.path.append(scripts_dir)

# choose release mode when possible
found_exec = False
for build_folder in ['cmake-build-release-petsc', 'cmake-build-release', 'cmake-build-debug-petsc', 'cmake-build-debug']:
    executable = os.path.join(this_dir, '..', '..', '..', build_folder, 'examples', 'Basic', 'Navier-Stokes',
                              'ns')
    if os.path.exists(executable):
        found_exec = True
        break

assert found_exec, "Program executable missing ({})".format(executable)
print("Executable: {}".format(executable))

base_cfg = {}

cases = [
    # LDC with linear/quadratic, w/wo refinement, serial/parallel
    {'order': 1, 'refine_l': 3, 'refine_h': 3, 'ntasks': 1},
    {'order': 1, 'refine_l': 3, 'refine_h': 3, 'ntasks': 4},
    {'order': 2, 'refine_l': 2, 'refine_h': 2, 'ntasks': 1},
    {'order': 2, 'refine_l': 2, 'refine_h': 2, 'ntasks': 2},
    {'order': 1, 'refine_l': 2, 'refine_h': 4, 'ntasks': 1},
    {'order': 1, 'refine_l': 2, 'refine_h': 4, 'ntasks': 8},
    # {'order': 2, 'refine_l': 1, 'refine_h': 3, 'ntasks': 1},  # too much time
    {'order': 2, 'refine_l': 1, 'refine_h': 3, 'ntasks': 4},

]

from regression import RegressionTester, RegexDiffMetric, VecDiffMetric

reg = RegressionTester()

reg.add_cases(base_cfg, cases)
reg.set_exclude_patterns(["*.vtu", "*.pvtu", "*.info"])

reg.add_metric(VecDiffMetric('solution_vec.vec'))
reg.add_metric(RegexDiffMetric('SNES', 'output.txt', re.compile(r'SNES Function norm (.*)')))
reg.add_file_generator("config.txt", """
dt = 10000
totalT = 10000

BasisFunction = {order}

NavierStokesSolverType = "rbvmsNS"

dump_vec = true

background_mesh = {{
  refine_l = {refine_l}
  refine_h = {refine_h}
  enable_subda = false
  min = [0, 0, 0]
  max = [1, 1, 1]
  refine_walls = true
}}

### Coe setup
Ci_f = 36
tauM_scale = 1

### Non-dim parameters
Re = 100
Fr = 0

### BCs
boundary = (
  {{
    side = "x-"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "x+"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "y-"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "y+"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "z-"
    vel_type = "no_slip"
    ifRefine = true
  }},
  {{
    side = "z+"
    vel_type = "dirichlet"
    wall_vel = [1.0, 0.0, 0.0]
    ifRefine = true
  }}
)

### ICs
initial_condition = {{
  vel_ic_type = "user_defined"
  vel_ic = [0.0, 0.0, 0.0]
  pressure_ic_type = "user_defined"
  pressure_ic = 0.0
}}

OutputStartTime = 0

#################### solver setting ####################
solver_options = {{
  snes_atol = 1e-8
  snes_rtol = 1e-8
  snes_stol = 1e-10
  snes_max_it = 40
  snes_max_funcs = 80000
  ksp_max_it = 2000
  ksp_rtol = 1e-12
  ksp_atol = 1e-10
  ksp_type = "bcgs"
  pc_type = "asm"

# monitor
  snes_monitor = ""
}}

""")

reg.set_run_cmd(executable)
reg.run_main()
