//
// Created by maksbh on 11/24/18.
//
#include <oda.h>
#include "TalyEquation.h"
#include <DendriteUtils.h>
#include "Globals.h"

#include "NSNodeData.h"
#include "NSInputData.h"
#include "NSEquation.h"
#include "NSBCSetup.h"
#include "NSRefine.h"
#include "IBM/Geom.h"
#include "IBM/IBMSolver.h"
#include <IBM/IBMNonLinearSolver.h>
#include  <IBM/IBMUtils.h>

#include "PETSc/Solver/LinearSolver.h"
#include "PETSc/PetscUtils.h"
#include <sys/stat.h>
#include <IBM/IBMFeUtils.h>

/**
 * Save the octree mesh to vtk binary file.
 * @param da
 * @param daScalingFactor
 * @param ti
 * @param ns
 * @param ht
 * @param prefix
 */
PetscErrorCode save_timestep(ot::DA *octDA,
                             Vec vec,
                             unsigned int ndof,
                             const TimeInfo &ti,
                             const std::string &prefix = "timestep",
                             const double *scalingFactor = nullptr) {
  if (octDA->isActive()) {
    // create directory for this timestep (if it doesnt already exist)
    char folder[PATH_MAX];
    snprintf(folder, sizeof(folder), "results_%05d", ti.getTimeStepNumber());
    int ierr = mkdir(folder, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (ierr != 0 && errno != EEXIST) {
      PrintError("Could not create folder for storing results (", strerror(errno), ").");
      return 1;
    }

    char fname[PATH_MAX];
    snprintf(fname, sizeof(fname), "%s/%s_%05d", folder, prefix.c_str(), ti.getTimeStepNumber());

    //  octree2VTK(da, daScalingFactor, {
    //      VecOutInfo(ns, NSNodeData::NS_DOF, {Attribute::vector("velocity", 0), Attribute::scalar("pressure", 3)}),
    //      VecOutInfo(ht, NSNodeData::HT_DOF, {Attribute::scalar("temperature", 0)}),
    //  }, fname);
    octDA->petscVecTopvtu(vec, fname, nullptr, false, false, ndof, scalingFactor);
  }
}

using namespace PETSc;

int main(int argc, char **argv) {
  dendrite_init(argc, argv);

  int rank, npes;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &npes);

  // read inputData
  NSInputData idata;
  if (!idata.ReadFromFile()) {  /// read from file named "config.txt"
    throw std::runtime_error("[ERR] Error reading input data, check the config file!");
  }

  if (!idata.CheckInputData()) {
    throw std::runtime_error("[ERR] Problem with input data, check the config file!");
  }

  idata.solverOptionsNS.apply_to_petsc_options("-");

  libconfig::Config cfg;  ///< libconfig config object
  cfg.readFile("config.txt");

  ot::DA *octDA = createRegularDA(idata.mesh_def.refine_l, 20);
  MPI_Barrier(MPI_COMM_WORLD);

  /// Set max and min of the domain for initial conditions
  Point d_min(idata.mesh_def.channel_min.x(),
              idata.mesh_def.channel_min.y(),
              idata.mesh_def.channel_min.z());
  Point d_max(idata.mesh_def.channel_max.x(),
              idata.mesh_def.channel_max.y(),
              idata.mesh_def.channel_max.z());

  /// Time tracking object
  TimeInfo ti(0, idata.dt, idata.totalT);

  /// scaling of the mesh
  const double scalingFactor[3]{idata.mesh_def.channel_max.x(),
                                idata.mesh_def.channel_max.y(),
                                idata.mesh_def.channel_max.z()};


  /// Refinement setup for the mesh
  int iterOfRefinement = 0;
  while (iterOfRefinement < 10) {
    /// Refine class contains all the methods to setup refinement.
    NSRefine refine(octDA, scalingFactor, &idata);
    ot::DA *newDA = refine.getRefineDA();
    if (newDA != octDA) {
      delete octDA;// swap out the old mesh
    } else {
      // if the mesh is the same, break out.
      break;
    }
    octDA = newDA;
    PrintStatus(++iterOfRefinement, " iteration of refinement, element: ", octDA->getTotalElemSz());
  }

  /// Setup the equation object based on the nodedata and inputdata.
  auto nsEq =
      new TalyEquation<NSEquation, NSNodeData>(octDA, d_min, d_max, NSNodeData::NS_DOF, false, &idata);
  auto nsMat = nsEq->mat;
  auto nsVec = nsEq->vec;
  nsMat->setTime(&ti);
  nsVec->setTime(&ti);

  Vec prev_solution;
  octDA->petscCreateVector(prev_solution, false, false, NSNodeData::NS_DOF);


  /// The non-linear solver object is instantiated with the equation object. This object is used to call all solving
  /// routines
  NonlinearSolver *NSSolver = setNonLinearSolver(nsEq, octDA, NSNodeData::NS_DOF, false);

  if (octDA->isActive()) {
    nsMat->setVector({
                         VecInfo(PLACEHOLDER_GUESS, NSNodeData::NS_DOF, NSNodeData::VEL_X),
                         VecInfo(prev_solution,
                                 NSNodeData::NS_DOF,
                                 NSNodeData::NUM_VARS + NSNodeData::VEL_X,
                                 PLACEHOLDER_NONE)
                     });
    nsVec->setVector({
                         VecInfo(PLACEHOLDER_GUESS, NSNodeData::NS_DOF, NSNodeData::VEL_X),
                         VecInfo(prev_solution,
                                 NSNodeData::NS_DOF,
                                 NSNodeData::NUM_VARS + NSNodeData::VEL_X,
                                 PLACEHOLDER_NONE)
                     });

    /// Setup petsc options for the nonlinear snes object
    {
      SNES m_snes = NSSolver->snes();
      SNESSetFromOptions(m_snes);
    }

    /// set boundary conditions for NS
    NSBCSetup bc(&idata, &ti);

    PrintStatus("Setting BC for NS");
    NSSolver->setBoundaryCondition([&](double x, double y, double z, unsigned int nodeID) -> Boundary {
      Boundary b;
      ZEROPTV position = {x, y, z};
      bc.setBoundaryConditionsNS(b, position, idata.boundary_def);
      return b;
    });

    /// Setup intial conditions
    const auto initial_condition = [&](double x, double y, double z, int dof) {
      //  return vel, default is 0
      if (dof != NSNodeData::PRESSURE) {
        //return dof * 1.5 + x + y + z;
        return idata.ic_def.vel_ic[dof];
      }
      // return pressure, default is 0
      return idata.ic_def.p_ic;
    };
    petscSetInitialCondition(octDA, prev_solution, NSNodeData::NS_DOF, initial_condition, d_min, d_max);

    ///initial condition copied in the current solution vector.
    VecCopy(prev_solution, NSSolver->getCurrentSolution());

    /// Print initial condition
    save_timestep(octDA, prev_solution, NSNodeData::NS_DOF, ti, "ns", scalingFactor);

    ///**************************Solve loop***************************************///
    PrintStatus("Starting solving \n");
    while (ti.getCurrentTime() < (ti.getEndTime() - 1e-16)) {

      PrintInfo("\nTimestep ", ti.getTimeStepNumber(), " - ", ti.getCurrentTime());

      PrintStatus("Solve NS");
      NSSolver->solve();

      /// update previous solution
      VecCopy(NSSolver->getCurrentSolution(), prev_solution);
      ti.increment();

      /// Print solution
      if ((ti.getCurrentTime() >= idata.OutputStartTime) && (ti.getTimeStepNumber() % idata.OutputInterval == 0)) {
        save_timestep(octDA, prev_solution, NSNodeData::NS_DOF, ti, "ns", scalingFactor);

      }
    }
    ///*************************Solve loop complete *****************************///
    PrintStatus("Done solving \n");
  }

  /// Save vector file (only for regression test)
  if (idata.dump_vec) {
    petscDumpFilesforRegressionTest(octDA,NSSolver->getCurrentSolution(), "solution_vec.vec");
  }
  delete NSSolver;
  dendrite_finalize(octDA);

}