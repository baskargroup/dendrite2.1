import os
import glob

path = os.path.dirname(os.path.realpath(__file__))
folders = []
mpi = 0
for i in sorted(os.listdir(path)):
    if os.path.isdir(os.path.join(path, i)) and 'results_' in i and 'dendro' not in i:
        folders.append(i)

ifgeoms = len(glob.glob((folders[0] + "/*.vtp")))
with open(path + "/visit_auto_mesh_ns.visit", 'w+') as visit_file:
    visit_file.write('!NBLOCKS ' + str(1) + '\n')
    for folder in folders:
        visit_file.write(folder + '/ns_' + folder[-5:] + '.pvtu\n')

if (ifgeoms != 0):
    with open(path + "/visit_auto_geom.visit", 'w+') as visit_file:
        visit_file.write('!NBLOCKS ' + str(ifgeoms) + '\n')
        for folder in folders:
            geoms = glob.glob((folder + "/*.vtp"))
            for geom in geoms:
                visit_file.write(geom + '\n')
        visit_file.seek(0, 0)
