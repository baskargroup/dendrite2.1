//
// Created by maksbh on 12/4/18.
//
#include <oda.h>
#include "TalyEquation.h"
#include <DendriteUtils.h>
#include <TimeInfo.h>
#include "PETSc/Solver/LinearSolver.h"
#include "PETSc/PetscUtils.h"
#include "PETSc/VecInfo.h"
#include "HTEquation.h"
#include "HTNodeData.h"
#include "HTInputData.h"
#include <Checkpoining/checkpointer.hpp>

using namespace PETSc;

int main(int argc, char **argv) {
  /// Initialize dendrite
  dendrite_init(argc, argv);
  /// get mpi rank and npe count (cpu count)
  int rank, npes;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &npes);
  /// read parameters from config.txt
  HTInputData idata;
  if (!idata.ReadFromFile()) {  /// read from file named "config.txt"
    throw std::runtime_error("[ERR] Error reading input data, check the config file!");
  }
  if (!idata.CheckInputData()) {
    throw std::runtime_error("[ERR] Problem with input data, check the config file!");
  }

  bool resume_from_checkpoint = false;
  {
    PetscBool resume = PETSC_FALSE;
    PetscOptionsGetBool(nullptr, nullptr, "-resume_from_checkpoint", &resume, nullptr);
    resume_from_checkpoint = (resume == PETSC_TRUE);
  }

  /// specify mesh dimension
  checkpointer Checkpoint;
  Checkpoint.set_filename_prefix("Checkpoint_HT");
  Checkpoint.set_checkpoint_frequency(2);
  Checkpoint.set_n_backups(1);

  int refine_lvl = idata.mesh_def.refine_lvl;
  ot::DA *octDA = NULL;
  Vec prev_solution;
  /// Time info for timestepping
  TimeInfo ti(0.0, idata.dt, idata.totalT);
  Point domain_min(idata.mesh_def.channel_min.x(), idata.mesh_def.channel_min.y(), idata.mesh_def.channel_min.z());
  Point domain_max(idata.mesh_def.channel_max.x(), idata.mesh_def.channel_max.y(), idata.mesh_def.channel_max.z());

  if (not resume_from_checkpoint) {
    octDA = createRegularDA(refine_lvl);
    MPI_Barrier(MPI_COMM_WORLD);
  } else {
    // Checkpoint
    std::vector<VecInfo> vecs;
    Checkpoint.loadFromCheckPointing(octDA, vecs, &ti);
    prev_solution = vecs[0].v;
  }

  /// create solver
  // number of degree of freedom
  int ndof = 1;
  // matrix based or matrix free
  bool mfree = idata.mfree;

  auto talyEq = new TalyEquation<HTEquation, HTNodeData>(octDA, domain_min, domain_max, ndof);
  auto talyMat = talyEq->mat;
  auto talyVec = talyEq->vec;
  talyMat->setTime(&ti);
  talyVec->setTime(&ti);

  LinearSolver *solver = setLinearSolver(talyEq, octDA, ndof, mfree);
  /// apply solver parameter from config.txt
  idata.solverOptionsHT.apply_to_petsc_options("-ht_");
  {
    KSP m_ksp = solver->ksp();
    KSPSetOptionsPrefix(m_ksp, "ht_");
    KSPSetFromOptions(m_ksp);
  }
  const auto analytic_sol = [&](double x, double y, double z, int dof) {
    return sin(M_PI * x) * sin(M_PI * y) * sin(M_PI * z) * exp(-ti.getCurrentTime());
  };

  if (not resume_from_checkpoint) {
    octDA->petscCreateVector(prev_solution, false, false, ndof);
    const auto initial_condition = [&](double x, double y, double z, int ndof) {
      return sin(M_PI * x) * sin(M_PI * y) * sin(M_PI * z);
    };
    petscSetInitialCondition(octDA, prev_solution, ndof, initial_condition, domain_min, domain_max);
    octDA->petscVecTopvtu(prev_solution, "init");
  }

  // connect prev_solution to HTNodeData's U_PRE value (read-only)
  talyVec->setVector({VecInfo(prev_solution, ndof, U_PRE)});

  /// compare with analytical solution
  const double problemSize[3]{domain_max.x() - domain_min.x(),
                              domain_max.y() - domain_min.y(),
                              domain_max.z() - domain_min.z()};
  {
    double *err = petscCalcL2Error(octDA, problemSize, prev_solution, ndof, analytic_sol);
    PrintStatus("Initial Error = ", err[0]);
    delete err;
  }

  /// setup boundary condition (T on the walls are 0)
  int twall = 0;
  solver->setBoundaryCondition([&](double x, double y, double z, unsigned int nodeID) -> Boundary {
    Boundary b;
    static constexpr double eps = 1e-14;
    bool on_wall = (fabs(x - domain_min.x()) < eps) ||
        (fabs(y - domain_min.y()) < eps) ||
        (fabs(z - domain_min.z()) < eps) ||
        (fabs(x - domain_max.x()) < eps) ||
        (fabs(y - domain_max.y()) < eps) ||
        (fabs(z - domain_max.z()) < eps);
    if (on_wall) {
      b.addDirichlet(0, twall);
    }
    return b;
  });
  MPI_Barrier(MPI_COMM_WORLD);
  if (octDA->isActive()) {
    PrintStatus("Starting solving");
    while (ti.getCurrentTime() < (ti.getEndTime() - 1e-16)) {
      PrintInfo("\nTimestep ", ti.getTimeStepNumber(), " - ", ti.getCurrentTime());
      solver->solve();
      VecCopy(solver->getCurrentSolution(), prev_solution);
      ti.increment();
      /// Save solution when required
      if ((ti.getCurrentTime() >= (idata.OutputStartTime - 1e-16))
          && (ti.getTimeStepNumber() % idata.OutputInterval == 0)) {
        char fname[PATH_MAX];
        snprintf(fname, sizeof(fname), "%s_%03d", "ht", ti.getTimeStepNumber());
        octDA->petscVecTopvtu(solver->getCurrentSolution(), fname, nullptr, false, false, ndof);
        // checkpointing
        std::vector<VecInfo> vecs;
        vecs.push_back(VecInfo(prev_solution,
                               ndof,
                               U_PRE,
                               PLACEHOLDER_NONE));
        Checkpoint.storeCheckpoint(octDA, vecs, &ti);
      }
    }
    PrintStatus("Done solving");

  double *err = petscCalcL2Error(octDA, problemSize, prev_solution, ndof, analytic_sol);
  /// Only processor 0 has the correct error
  PrintStatus("Error = ", err[0]);

  /// Save vector file (only for regression test)
  if (idata.dump_vec) {
    petscDumpFilesforRegressionTest(octDA, solver->getCurrentSolution(), "solution_vec.vec");
  }
    /// Clean up
    delete solver;
  }
  dendrite_finalize(octDA);
}