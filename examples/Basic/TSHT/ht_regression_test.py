#!/usr/bin/env python3

import os
import sys
import re

this_dir = os.path.dirname(os.path.realpath(__file__))
scripts_dir = os.path.join(this_dir, '..', '..', '..', 'scripts')
sys.path.append(scripts_dir)

# choose release mode when possible
found_exec = False
for build_folder in ['cmake-build-release-petsc', 'cmake-build-release', 'cmake-build-debug-petsc', 'cmake-build-debug']:
    executable = os.path.join(this_dir, '..', '..', '..', build_folder, 'examples', 'Basic', 'TSHT', 'ht')
    if os.path.exists(executable):
        found_exec = True
        break

assert found_exec, "Program executable missing ({})".format(executable)
print("Executable: {}".format(executable))

base_cfg = {}

cases = [
    # check serial/parallel for linear/quadratic, and mfree/matrix
    {'mfree': "false", 'order': 1, 'refine_lvl': 3, 'dt': 0.1, 'ntasks': 1},
    {'mfree': "false", 'order': 2, 'refine_lvl': 3, 'dt': 0.1, 'ntasks': 1},
    {'mfree': "true", 'order': 1, 'refine_lvl': 3, 'dt': 0.1, 'ntasks': 1},
    {'mfree': "true", 'order': 2, 'refine_lvl': 3, 'dt': 0.1, 'ntasks': 1},  # quite slow
    {'mfree': "false", 'order': 1, 'refine_lvl': 3, 'dt': 0.1, 'ntasks': 4},
    {'mfree': "false", 'order': 2, 'refine_lvl': 3, 'dt': 0.1, 'ntasks': 4},
    {'mfree': "true", 'order': 1, 'refine_lvl': 3, 'dt': 0.1, 'ntasks': 4},
    {'mfree': "true", 'order': 2, 'refine_lvl': 3, 'dt': 0.1, 'ntasks': 4},

    # check convergence for backward-euler timestepping
    {'mfree': "false", 'order': 1, 'refine_lvl': 5, 'dt': 0.01, 'ntasks': 8},
    {'mfree': "false", 'order': 1, 'refine_lvl': 5, 'dt': 0.02, 'ntasks': 8},
    {'mfree': "false", 'order': 1, 'refine_lvl': 5, 'dt': 0.04, 'ntasks': 8},
    {'mfree': "false", 'order': 1, 'refine_lvl': 5, 'dt': 0.1, 'ntasks': 8},

    # user can check the slope of linear basis functions (not included in the regression test)
    # {'order': 1, 'refine_lvl': 2, 'ntasks': 2},
    # {'order': 1, 'refine_lvl': 3, 'ntasks': 4},
    # {'order': 1, 'refine_lvl': 4, 'ntasks': 4},
    # {'order': 1, 'refine_lvl': 5, 'ntasks': 8},
    # user can check the slope of quadratic basis functions (not included in the regression test)
    # {'order': 2, 'refine_lvl': 2, 'ntasks': 2},
    # {'order': 2, 'refine_lvl': 3, 'ntasks': 4},
    # {'order': 2, 'refine_lvl': 4, 'ntasks': 4},
    # {'order': 2, 'refine_lvl': 5, 'ntasks': 8},
]

cases_checkpoint_save = [
    # check checkpoint (save)
    {'mfree': "false", 'order': 1, 'refine_lvl': 4, 'dt': 0.1, 'totalT': 1.0, 'ntasks': 1},
    {'mfree': "false", 'order': 1, 'refine_lvl': 4, 'dt': 0.1, 'totalT': 1.0, 'ntasks': 4},
]

cases_checkpoint_load = [
    # check checkpoint (load)
    {'mfree': "false", 'order': 1, 'refine_lvl': 4, 'dt': 0.1, 'totalT': 2.0, 'ntasks': 1},
    {'mfree': "false", 'order': 1, 'refine_lvl': 4, 'dt': 0.1, 'totalT': 2.0, 'ntasks': 4},
]

from regression import RegressionTester, RegexDiffMetric, VecDiffMetric, FileLinkGenerator, FileCopyGenerator, FolderCopyGenerator
reg = RegressionTester()

reg.add_cases(base_cfg, cases)
reg.set_exclude_patterns(["*.vtu", "*.pvtu", "*.info"])

reg.add_metric(VecDiffMetric('solution_vec.vec'))
reg.add_metric(RegexDiffMetric('Final L2 error', 'output.txt', re.compile(r'Error = (.*)')))
reg.add_file_generator("config.txt", """
BasisFunction = {order}

dt = {dt}
totalT = 1

OutputStartTime = 1  # do not output anything other than initial condition
OutputInterval = 1

background_mesh = {{
  refine_lvl = {refine_lvl}
  min = [0, 0, 0]
  max = [1, 1, 1]
}}

mfree = {mfree}

dump_vec = true

#################### solver setting ####################
solver_options_ht = {{
  ksp_max_it = 2000
  ksp_type = "bcgs"
  pc_type = "asm"
  ksp_atol = 1e-7
  ksp_rtol = 1e-10
  ksp_converged_reason = ""
  ksp_monitor = ""
}}
""")

reg.set_run_cmd(executable)
reg.run_main()


# for checkpoint save
reg_save = RegressionTester()

reg_save.add_cases(base_cfg, cases_checkpoint_save)
reg_save.set_exclude_patterns(["*.vtu", "*.pvtu", "*.info"])

reg_save.add_metric(VecDiffMetric('solution_vec.vec'))
reg_save.add_metric(RegexDiffMetric('Final L2 error', 'output.txt', re.compile(r'Error = (.*)')))
reg_save.add_file_generator("config.txt", """
BasisFunction = {order}

dt = {dt}
totalT = {totalT}

OutputStartTime = 0  # do not output anything other than initial condition
OutputInterval = 1

background_mesh = {{
  refine_lvl = {refine_lvl}
  min = [0, 0, 0]
  max = [1, 1, 1]
}}

mfree = {mfree}

dump_vec = true

#################### solver setting ####################
solver_options_ht = {{
  ksp_max_it = 2000
  ksp_type = "bcgs"
  pc_type = "asm"
  ksp_atol = 1e-7
  ksp_rtol = 1e-10
  ksp_converged_reason = ""
  ksp_monitor = ""
}}
""")

reg_save.set_run_cmd(executable)
reg_save.run_main()


reg_load = RegressionTester()

reg_load.add_cases(base_cfg, cases_checkpoint_load)
reg_load.set_exclude_patterns(["*.vtu", "*.pvtu", "*.info", "Checkpoint*"])

reg_load.add_metric(VecDiffMetric('solution_vec.vec'))
reg_load.add_metric(RegexDiffMetric('Final L2 error', 'output.txt', re.compile(r'Error = (.*)')))
reg_load.add_file_generator("config.txt", """
BasisFunction = {order}

dt = {dt}
totalT = {totalT}

OutputStartTime = 0  # do not output anything other than initial condition
OutputInterval = 1

background_mesh = {{
  refine_lvl = {refine_lvl}
  min = [0, 0, 0]
  max = [1, 1, 1]
}}

mfree = {mfree}

dump_vec = true

#################### solver setting ####################
solver_options_ht = {{
  ksp_max_it = 2000
  ksp_type = "bcgs"
  pc_type = "asm"
  ksp_atol = 1e-7
  ksp_rtol = 1e-10
  ksp_converged_reason = ""
  ksp_monitor = ""
}}
""")
reg_load.add_generator(FolderCopyGenerator('Checkpoint_HT', os.path.join(this_dir, 'regression_reference_data', 'Checkpoint_np1')), {"ntasks": 1})
reg_load.add_generator(FolderCopyGenerator('Checkpoint_HT', os.path.join(this_dir, 'regression_reference_data', 'Checkpoint_np4')), {"ntasks": 4})

reg_load.set_run_cmd(executable + " -resume_from_checkpoint")
reg_load.run_main()
