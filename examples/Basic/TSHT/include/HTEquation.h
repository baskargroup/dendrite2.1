/*
  Copyright 2014-2016 Baskar Ganapathysubramanian

  This file is part of TALYFem.

  TALYFem is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 2.1 of the
  License, or (at your option) any later version.

  TALYFem is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with TALYFem.  If not, see <http://www.gnu.org/licenses/>.
*/
// --- end license text --- //
#ifndef INCLUDE_HTEQUATION_H_
#define INCLUDE_HTEQUATION_H_

#include <talyfem/fem/cequation.h>
#include "HTNodeData.h"

class HTEquation : public TALYFEMLIB::CEquation<HTNodeData> {
 public:

  explicit HTEquation()
      : TALYFEMLIB::CEquation<HTNodeData>(false, TALYFEMLIB::kAssembleGaussPoints) {
    K_ = 1.0 / (3 * M_PI * M_PI);
  }

  // never called, but required for HTEquation to be instantiable (pure virtual in CEquation)
  virtual void Solve(double dt, double t) { assert(false); }

  void Integrands_Ae(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae) {
    using namespace TALYFEMLIB;
    // # of dimensions: 1D, 2D, or 3D
    const int n_dimensions = fe.nsd();
    // # of basis functions
    const int n_basis_functions = fe.nbf();
    // (determinant of J) cross W
    const double detJxW = fe.detJxW();
    // thermal diffusivity in heat equation
    double k_val = K_;

    // in order to assemble the gauss point, we loop over each pair of basis
    // functions and calculate the individual contributions to the 'Ae' matrix
    // and 'be' vector.
    for (int a = 0; a < n_basis_functions; a++) {
      for (int b = 0; b < n_basis_functions; b++) {
        double M = fe.N(a) * fe.N(b) * detJxW;
        double N = 0;
        for (int k = 0; k < n_dimensions; k++) {
          N += k_val * fe.dN(a, k) * fe.dN(b, k) * detJxW;
        }
        // Add term to the A element matrix
        Ae(a, b) += M / dt_ + N;
      }
    }
  }

  void Integrands_be(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZEROARRAY<double> &be) {
    using namespace TALYFEMLIB;
    // # of basis functions
    const int n_basis_functions = fe.nbf();
    // (determinant of J) cross W
    const double detJxW = fe.detJxW();

    const double u_pre_curr = p_data_->valueFEM(fe, U_PRE);
    for (int a = 0; a < n_basis_functions; a++) {
      be(a) += fe.N(a) / dt_ * u_pre_curr * detJxW;
    }
  }

 private:
  double K_;
};

#endif  // INCLUDE_HTEQUATION_H_
