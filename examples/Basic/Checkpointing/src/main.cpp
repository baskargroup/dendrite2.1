//
// Created by maksbh on 10/8/19.
//
#include <oda.h>
#include <DendriteUtils.h>
#include <Checkpoining/checkpointer.hpp>
int main(int argc, char **argv) {

  /// Initialize dendrite
  dendrite_init(argc, argv);
  /// get mpi rank and npe count (cpu count)
  int rank, npes;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &npes);

  bool resumeFromCheckPointing = true;
  ot::DA * octDA = NULL;
  if(resumeFromCheckPointing){
    checkpointer checkpointer1;
    std::vector<VecInfo> vecs;
    checkpointer1.loadFromCheckPointing(octDA,vecs,NULL);
    octDA->petscVecTopvtu(vecs[0].v,"solution1",NULL, false, false,2);
    octDA->petscVecTopvtu(vecs[1].v,"solution2",NULL, false, false,3);
    std::cout << vecs.size() << "\n";
    std::cout << vecs[0].ndof << " " << vecs[1].ndof << "\n";
  } else{
    octDA = createRegularDA(3);
    std::vector<ot::DA_FLAGS::Refine> refine(octDA->getLocalElemSz(),ot::DA_FLAGS::Refine::DA_NO_CHANGE);
    int counter = 0;
    for (octDA->init<ot::DA_FLAGS::LOCAL_ELEMENTS>();
         octDA->curr() < octDA->end<ot::DA_FLAGS::LOCAL_ELEMENTS>();
         octDA->next<ot::DA_FLAGS::LOCAL_ELEMENTS>()) {
      if(octDA->isBoundaryOctant(octDA->curr())){
        refine[counter++] = ot::DA_FLAGS::Refine::DA_REFINE;
      }
      else{
        refine[counter++] = ot::DA_FLAGS::Refine::DA_NO_CHANGE;
      }
    }
    ot::DA * newDA = octDA->remesh(refine.data(),octDA->getLocalElemSz());
    delete octDA;
    octDA = newDA;
    Vec solution;
    octDA->petscCreateVector(solution, false, false,2);
    Vec solution2;
    octDA->petscCreateVector(solution2, false, false,3);
    VecSet(solution,1.0);
    VecSet(solution2,5.0);
    checkpointer checkpointer1;
    checkpointer1.set_checkpoint_frequency(1);
    std::vector<VecInfo> vecs(2);
    vecs[0] = VecInfo{solution,2,2,PLACEHOLDER_NONE};
    vecs[1] = VecInfo{solution2,3,5,PLACEHOLDER_NONE};
    checkpointer1.storeCheckpoint(octDA,vecs,NULL);
    octDA->petscVecTopvtu(vecs[0].v,"solutionbefore1",NULL, false, false,2);
    octDA->petscVecTopvtu(vecs[1].v,"solutionbefore2",NULL, false, false,3);
  }

//
    dendrite_finalize(octDA);

}