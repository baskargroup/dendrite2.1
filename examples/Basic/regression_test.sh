this_dir=$(pwd)
parentdir="$(dirname "$this_dir")"
exec_dir="$(dirname "$parentdir")"
mpirun_command=$(which mpirun)


# compile everything (assume already cmaked)
echo ---------------------- Compile ---------------------
# cd $exec_dir/$build_type/examples/Basic
for build_folder in $exec_dir/cmake-build*
do
  cd $build_folder/examples/Basic
  for sub_dir in ./*
  do
    cd $sub_dir
    echo ---------------------- Compile $sub_dir ---------------------
    make -j 8
    echo -------------------------------------------------------------
    cd ..
  done
  cd $this_dir
done

# run every regression test
echo ---------------------- Test ---------------------
cd $this_dir
if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    for sub_dir in */ ;
    do
      cd $sub_dir
      echo ---------------------- Testing $sub_dir ---------------------
      python3 *_regression*.py --runner=$mpirun_command
      echo -------------------------------------------------------------
      cd $this_dir
    done
  else
    for sub_dir in $this_dir/${1}/ ;
      do
        cd $sub_dir
        echo ---------------------- Testing $sub_dir ---------------------
        python3 *_regression*.py --runner=$mpirun_command
        echo -------------------------------------------------------------
        cd $this_dir
      done
fi
