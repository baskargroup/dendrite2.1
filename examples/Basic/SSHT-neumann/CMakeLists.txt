set(SSHT_neumann_INCLUDES
        include/SSHTEquation.h
        include/SSHTNodeData.h
        include/SSHTInputData.h
        )

set(SSHT_neumann_SOURCES
        src/main.cpp
        )

add_executable(ssht-neumann ${SSHT_neumann_INCLUDES} ${SSHT_neumann_SOURCES})
target_include_directories(ssht-neumann PUBLIC include)
target_link_libraries(ssht-neumann dendrite2 dendro5 ${MPI_LIBRARIES} m)

