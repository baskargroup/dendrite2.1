BasisFunction = 1

background_mesh = {
  refine_lvl = 3
  min = [0, 0, 0]
  max = [1, 1, 1]
}


#################### solver setting ####################
solver_options_ssht = {
  ksp_max_it = 2000
  ksp_type = "bcgs"
  pc_type = "asm"
  ksp_atol = 1e-7
  ksp_rtol = 1e-10
  ksp_converged_reason = ""
  ksp_monitor = ""
}
