#pragma once

#include <talyfem/fem/cequation.h>
#include "SSHTNodeData.h"

class SSHTEquation : public TALYFEMLIB::CEquation<SSHTNodeData> {
 public:
  void Solve(double dt, double t) override {
    assert(false);
  }

  void Integrands(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae,
                  TALYFEMLIB::ZEROARRAY<double> &be) override {
    assert(false);
  }

  void Integrands_Ae(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZeroMatrix<double> &Ae) {
    using namespace TALYFEMLIB;
    // # of dimensions: 1D, 2D, or 3D
    const int n_dimensions = fe.nsd();
    // # of basis functions
    const int n_basis_functions = fe.nbf();
    // (determinant of J) cross W
    const double detJxW = fe.detJxW();

    for (int a = 0; a < n_basis_functions; a++) {
      for (int b = 0; b < n_basis_functions; b++) {
        double N = 0;
        for (int k = 0; k < n_dimensions; k++) {

          N += fe.dN(a, k) * fe.dN(b, k) * detJxW;
        }
        Ae(a, b) -= N;
      }
    }

  }

  void Integrands_be(const TALYFEMLIB::FEMElm &fe, TALYFEMLIB::ZEROARRAY<double> &be) {
    using namespace TALYFEMLIB;
    // # of basis functions
    const int n_basis_functions = fe.nbf();
    // (determinant of J) cross W
    const double detJxW = fe.detJxW();

    const ZEROPTV p = fe.position();
    double force = calc_d2u_at(p);

    for (int a = 0; a < n_basis_functions; a++) {
      be(a) += fe.N(a) * force * detJxW;
    }

  }

  void Integrands4side_Ae(const TALYFEMLIB::FEMElm &fe, int side_idx, TALYFEMLIB::ZeroMatrix<double> &Ae) override {

  }

  void Integrands4side_be(const TALYFEMLIB::FEMElm &fe, int side_idx, TALYFEMLIB::ZEROARRAY<double> &be) override {
    // neumann conditions on X-, X+, Z-, and Z+ walls
    using namespace TALYFEMLIB;
    if (side_idx == 1 || side_idx == 2 || side_idx == 5 || side_idx == 6) {
      const ZEROPTV &p = fe.position();
      const ZEROPTV &normal = fe.surface()->normal();

      double flux = calc_grad_u_at(p).innerProduct(normal);

      for (int i = 0; i < fe.nbf(); i++) {
        be(i) -= fe.N(i) * fe.detJxW() * flux;
      }
    }
  }

 protected:
  double calc_d2u_at(const TALYFEMLIB::ZEROPTV &pt) const {
    return -3 * M_PI * M_PI * sin(M_PI * pt.x()) * sin(M_PI * pt.y()) * sin(M_PI * pt.z());
  }

  inline TALYFEMLIB::ZEROPTV calc_grad_u_at(const TALYFEMLIB::ZEROPTV &pt) const {
    return TALYFEMLIB::ZEROPTV(M_PI * cos(M_PI * pt.x()) * sin(M_PI * pt.y()) * sin(M_PI * pt.z()),
                               M_PI * sin(M_PI * pt.x()) * cos(M_PI * pt.y()) * sin(M_PI * pt.z()),
                               M_PI * sin(M_PI * pt.x()) * sin(M_PI * pt.y()) * cos(M_PI * pt.z()));
  }

};
