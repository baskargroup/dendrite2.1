//
// Created by maksbh on 1/19/19.
//

#include <vector>
#include <TalyEquation.h>
#include <DendriteUtils.h>
#include <PETSc/PetscUtils.h>
#include <cmath>
#include "PPEquation.h"
#include "PPInputData.h"

using TALYFEMLIB::PrintInfo;
using namespace PETSc;

/***
 * This function solves a Predator Prey Model to
 * demonstrate the handling of multiple dof in
 * Dendrite. It solves the following equaiton:
 *
 * u_t = u + 2*v;
 * v_t = -u + v;
 *
 * The analytical solution is given by:
 *   u(t) = 2*exp(t)*cos(2^(1/2)*t) - 2^(1/2)*exp(t)*sin(2^(1/2)*t)
 *   v(t) = - exp(t)*cos(2^(1/2)*t) - 2^(1/2)*exp(t)*sin(2^(1/2)*t)
 */



int main(int argc, char **argv) {
  /// Initialize dendrite
  dendrite_init(argc, argv);
  /// get mpi rank and npe count (cpu count)
  int rank, npes;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &npes);
  /// read parameters from config.txt
  HTInputData idata;
  if (!idata.ReadFromFile()) {  /// read from file named "config.txt"
    throw std::runtime_error("[ERR] Error reading input data, check the config file!");
  }
  if (!idata.CheckInputData()) {
    throw std::runtime_error("[ERR] Problem with input data, check the config file!");
  }

  /// specify mesh dimension
  int refine_lvl = idata.mesh_def.refine_lvl;
  ot::DA *octDA = createRegularDA(refine_lvl);
  MPI_Barrier(MPI_COMM_WORLD);

  Point domain_min(idata.mesh_def.channel_min.x(), idata.mesh_def.channel_min.y(), idata.mesh_def.channel_min.z());
  Point domain_max(idata.mesh_def.channel_max.x(), idata.mesh_def.channel_max.y(), idata.mesh_def.channel_max.z());

  /// Time info for timestepping
  TimeInfo ti(0.0, idata.dt, idata.totalT);

  /// create solver
  // number of degree of freedom
  int ndof = 2;
  // matrix based or matrix free
  bool mfree = idata.mfree;

  auto talyEq = new TalyEquation<PPEquation, PPNodeData>(octDA, domain_min, domain_max, ndof);
  auto talyMat = talyEq->mat;
  auto talyVec = talyEq->vec;
  talyMat->setTime(&ti);
  talyVec->setTime(&ti);

  LinearSolver *solver = setLinearSolver(talyEq, octDA, ndof, mfree);
  /// apply solver parameter from config.txt
  idata.solverOptionsHT.apply_to_petsc_options("-ht_");
  {
    KSP m_ksp = solver->ksp();
    KSPSetOptionsPrefix(m_ksp, "ht_");
    KSPSetFromOptions(m_ksp);
  }

  /// setup initial condition
  Vec prev_solution;
  octDA->petscCreateVector(prev_solution, false, false, ndof);
  const auto initial_condition = [&](double x, double y, double z, int dof) {
    if (dof == 0) {
      return 2.0;
    }
    if (dof == 1) {
      return -1.0;
    }
  };
  petscSetInitialCondition(octDA, prev_solution, ndof, initial_condition, domain_min, domain_max);
  octDA->petscVecTopvtu(prev_solution, "initial", NULL, false, false, ndof);
  talyVec->setVector({VecInfo(prev_solution, ndof, U_PRE)});

  std::cout << ti.getCurrentTime() << " " << ti.getEndTime() << " "  << "\n";
  PrintStatus("Starting solving");
  while (ti.getCurrentTime() < (ti.getEndTime() - 1e-16)) {
    solver->solve();
    ti.increment();
    VecCopy(solver->getCurrentSolution(),prev_solution);
    /// Save solution when required
    if ((ti.getCurrentTime() >= (idata.OutputStartTime - 1e-16))
        && (ti.getTimeStepNumber() % idata.OutputInterval == 0)) {
      char fname[PATH_MAX] {"final"};
      snprintf(fname, sizeof(fname), "%s_%03d", "pp", ti.getTimeStepNumber());
      octDA->petscVecTopvtu(solver->getCurrentSolution(), fname, nullptr, false, false, ndof);
    }
  }
  PrintStatus("Done solving");
  octDA->petscVecTopvtu(solver->getCurrentSolution(), "final", nullptr, false, false, ndof);

  /// compare with analytical solution
  double time = ti.getCurrentTime();
  const auto analytic_sol = [&](double x, double y, double z, int dof) {
    if (dof == 0) {
      return (exp(time) * (2 * cos(sqrt(2) * time) - sqrt(2) * sin(sqrt(2) * time)));
    }
    if (dof == 1) {
      return (-exp(time) * (cos(sqrt(2) * time) + sqrt(2) * sin(sqrt(2) * time)));
    }
  };
  const double problemSize[3]{1.0, 1.0, 1.0};
  auto all_err = petscCalcL2Error(octDA, problemSize, prev_solution, ndof, analytic_sol);

  /// Print Error
  PrintStatus("u Error = ", all_err[0]);
  PrintStatus("v Error = ", all_err[1]);

  /// Save vector file (only for regression test)
  if (idata.dump_vec) {
    petscDumpFilesforRegressionTest(octDA,solver->getCurrentSolution(), "solution_vec.vec");
  }
  /// Clean up
  delete solver;
  dendrite_finalize(octDA);
}