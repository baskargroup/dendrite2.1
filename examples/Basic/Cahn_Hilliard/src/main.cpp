

#include "PETSc/Solver/NonLinearSolver.h"
#include "PETSc/PetscUtils.h"

#include "CHEquation.h"
#include "CHInputData.h"

using namespace PETSc;

int main(int argc, char **argv) {
  /// Initialize dendrite
  dendrite_init(argc, argv);
  /// get mpi rank and npe count (cpu count)
  int rank, npes;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &npes);
  /// read parameters from config.txt
  CHInputData idata;
  if (!idata.ReadFromFile()) {  /// read from file named "config.txt"
    throw std::runtime_error("[ERR] Error reading input data, check the config file!");
  }
  if (!idata.CheckInputData()) {
    throw std::runtime_error("[ERR] Problem with input data, check the config file!");
  }

  /// specify mesh dimension
  int refine_lvl = idata.mesh_def.refine_lvl;
  ot::DA *octDA = createRegularDA(refine_lvl);
  MPI_Barrier(MPI_COMM_WORLD);

  Point domain_min(idata.mesh_def.channel_min.x(), idata.mesh_def.channel_min.y(), idata.mesh_def.channel_min.z());
  Point domain_max(idata.mesh_def.channel_max.x(), idata.mesh_def.channel_max.y(), idata.mesh_def.channel_max.z());

  /// Time info for timestepping
  TimeInfo ti(0.0, idata.dt, idata.totalT);

  /// create solver
  // number of degree of freedom
  int ndof = 2;
  // matrix based or matrix free
  bool mfree = idata.mfree;

  auto talyEq = new TalyEquation<CHEquation, CHNodeData>(octDA, domain_min, domain_max, ndof);
  auto talyMat = talyEq->mat;
  auto talyVec = talyEq->vec;
  talyMat->setTime(&ti);
  talyVec->setTime(&ti);

  NonlinearSolver *ts = setNonLinearSolver(talyEq, octDA, ndof, false);
  /// apply solver parameter from config.txt
  idata.solverOptionsCH.apply_to_petsc_options("-ch_");
  {
    SNES m_snes = ts->snes();
    SNESSetOptionsPrefix(m_snes, "ch_");
    SNESSetFromOptions(m_snes);
  }

  Vec prev_solution;
  octDA->petscCreateVector(prev_solution, false, false, ndof);
  talyMat->setVector({
                         VecInfo(PLACEHOLDER_GUESS, ndof, 0),
                         VecInfo(prev_solution, ndof, 2, PLACEHOLDER_NONE)
                     });
  talyVec->setVector({
                         VecInfo(PLACEHOLDER_GUESS, ndof, 0),
                         VecInfo(prev_solution, ndof, 2, PLACEHOLDER_NONE)
                     });

  /// setup initial condition
  const auto initial_condition = [&](double x, double y, double z, int dof) {
    if (dof == 0) {
      return (0.1 * cos(2.0 * M_PI * x) * cos(2.0 * M_PI * y) * cos(2.0 * M_PI * z));
    }
    if (dof == 1) {
      return (2.0);
    }
  };
  petscSetInitialCondition(octDA, prev_solution, ndof, initial_condition, domain_min, domain_max);

  /// setup boundary condition
  ts->setBoundaryCondition([&](double x, double y, double z, int nodeID) -> Boundary {
    Boundary b;
    return b;
  });

  octDA->petscVecTopvtu(prev_solution, "CH_init", NULL, false, false, ndof);
  VecCopy(prev_solution, ts->getCurrentSolution());

  const auto freeEnergy = [&](double x, double y, double z, double *valueFEM, double *ValueDerivatveFEM) {
    double val = 0.25 * (valueFEM[0] * valueFEM[0] - 1) * (valueFEM[0] * valueFEM[0] - 1);
    return (val);
  };

  const auto interfacialEnergy = [&](double x, double y, double z, double *valueFEM, double *ValueDerivatveFEM) {
    const double epsilon = 0.002;
    double val = 0.5 * (ValueDerivatveFEM[0] * ValueDerivatveFEM[0] +
        ValueDerivatveFEM[1] * ValueDerivatveFEM[1] + ValueDerivatveFEM[2] * ValueDerivatveFEM[2]);
    return (val * epsilon);
  };
  std::vector<EvalFunction> func_(2);
  func_[0] = freeEnergy;
  func_[1] = interfacialEnergy;
  const double problemSize[3]{1.0, 1.0, 1.0};
  const PetscScalar *array;
  DENDRITE_REAL *energy = new DENDRITE_REAL[2];
  {
    petscEvalFunction(octDA, problemSize, prev_solution, ndof, func_, energy);
    if (TALYFEMLIB::GetMPIRank() == 0) {
      std::ofstream energyFile("Energy.txt");
      energyFile << std::setprecision(10) << 0.0 << " " << energy[0] << " " << energy[1] << " "
                 << energy[0] + energy[1] << "\n";
      energyFile.close();
    }
  }

  PrintStatus("Starting solving");
  while (ti.getCurrentTime() < (ti.getEndTime() - 1e-16)) {
    PrintInfo("\nTimestep ", ti.getTimeStepNumber(), " - ", ti.getCurrentTime());
    ts->solve();
    ti.increment();
    VecCopy(ts->getCurrentSolution(), prev_solution);
    VecGetArrayRead(ts->getCurrentSolution(), &array);
    {
      petscEvalFunction(octDA, problemSize, prev_solution, ndof, func_, energy);
      std::ofstream energyFile("Energy.txt", std::ios::app);
      if (TALYFEMLIB::GetMPIRank() == 0) {
        energyFile << std::setprecision(10) << ti.getTimeStepNumber() * ti.getCurrentStep()
                   << "\t" << energy[0] << "\t" << energy[1] << " " << energy[0] + energy[1] << "\n";
        energyFile.close();
      }
    }
    /// Save solution when required
    if ((ti.getCurrentTime() >= (idata.OutputStartTime - 1e-16))
        && (ti.getTimeStepNumber() % idata.OutputInterval == 0)) {
      if (octDA->isActive()) {
        const std::string file_ = "CH_" + std::to_string(ti.getTimeStepNumber());
        octDA->petscVecTopvtu(prev_solution, file_.c_str(), NULL, false, false, ndof);
      }
    }
  }
  PrintStatus("Done solving");

  /// Save vector file (only for regression test)
  if (idata.dump_vec) {
    petscDumpFilesforRegressionTest(octDA,ts->getCurrentSolution(), "solution_vec.vec");
  }
  PrintStatus("Final energy = ", energy[0] + energy[1]);
  /// Clean up
  delete[] energy;
  delete ts;
  dendrite_finalize(octDA);

}

