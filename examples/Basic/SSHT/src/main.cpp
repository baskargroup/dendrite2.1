//
// Created by maksbh on 11/24/18.
//
#include <oda.h>
#include "TalyEquation.h"
#include <DendriteUtils.h>
#include "PETSc/Solver/LinearSolver.h"
#include "PETSc/PetscUtils.h"
#include "SSHTNodeData.h"
#include "SSHTEquation.h"
#include "SSHTInputData.h"
#include <PETSc/PetscTimings.h>

using namespace PETSc;

int main(int argc, char **argv) {
  /// Initialize dendrite
  dendrite_init(argc, argv);
  /// get mpi rank and npe count (cpu count)
  int rank, npes;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &npes);
  /// read parameters from config.txt
  SSHTInputData idata;
  /** Timer for PetscLogView **/
  PetscLogEvent fileReading;
  PetscTimings fileReadingTimer(fileReading,"fileReading");
  fileReadingTimer.start();
  if (!idata.ReadFromFile()) {  /// read from file named "config.txt"
    throw std::runtime_error("[ERR] Error reading input data, check the config file!");
  }
  if (!idata.CheckInputData()) {
    throw std::runtime_error("[ERR] Problem with input data, check the config file!");
  }
  fileReadingTimer.stop();

  /// specify mesh dimension
  int refine_lvl = idata.mesh_def.refine_lvl;
  ot::DA *octDA = createRegularDA(refine_lvl);

  MPI_Barrier(MPI_COMM_WORLD);

  Point domain_min(idata.mesh_def.channel_min.x(), idata.mesh_def.channel_min.y(), idata.mesh_def.channel_min.z());
  Point domain_max(idata.mesh_def.channel_max.x(), idata.mesh_def.channel_max.y(), idata.mesh_def.channel_max.z());

  /// create solver
  // number of degree of freedom
  int ndof = 1;
  // matrix based or matrix free
  bool mfree = false;

  auto talyEq = new TalyEquation<SSHTEquation, SSHTNodeData>(octDA, domain_min, domain_max, ndof);
  LinearSolver *solver = setLinearSolver(talyEq, octDA, ndof, mfree);
  /// apply solver parameter from config.txt
  idata.solverOptionsSSHT.apply_to_petsc_options("-ssht_");
  {
    KSP m_ksp = solver->ksp();
    KSPSetOptionsPrefix(m_ksp, "ssht_");
    KSPSetFromOptions(m_ksp);
  }

  /// setup boundary condition (T on the walls are 0)
  int twall = 0;
  solver->setBoundaryCondition([&](double x, double y, double z, unsigned int nodeID) -> Boundary {
    Boundary b;
    static constexpr double eps = 1e-14;
    bool on_wall = (fabs(x - domain_min.x()) < eps) ||
        (fabs(y - domain_min.y()) < eps) ||
        (fabs(z - domain_min.z()) < eps) ||
        (fabs(x - domain_max.x()) < eps) ||
        (fabs(y - domain_max.y()) < eps) ||
        (fabs(z - domain_max.z()) < eps);
    if (on_wall) {
      b.addDirichlet(0, twall);
    }
    return b;
  });

  MPI_Barrier(MPI_COMM_WORLD);

  PrintStatus("Starting solving");
  solver->solve();
  PrintStatus("Done solving");

  /// compare with analytical solution
  const double problemSize[3]{domain_max.x() - domain_min.x(),
                              domain_max.y() - domain_min.y(),
                              domain_max.z() - domain_min.z()};
  const auto analytic_sol = [&](double x, double y, double z, int dof) {
    return sin(M_PI * x) * sin(M_PI * y) * sin(M_PI * z);
  };
  double *err = petscCalcL2Error(octDA, problemSize, solver->getCurrentSolution(), ndof, analytic_sol);

  /// Only processor 0 has the correct error
  PrintStatus("Error = ", err[0]);

  /// Save vector file (only for regression test)
  if (idata.dump_vec) {
    petscDumpFilesforRegressionTest(octDA, solver->getCurrentSolution(), "solution_vec.vec");
  }

  /// Clean up
  delete solver;
  dendrite_finalize(octDA);

}
