#pragma once

#include <iomanip>
#include <talyfem/input_data/input_data.h>
#include "SSHTNodeData.h"

using TALYFEMLIB::ZEROPTV;
using TALYFEMLIB::PrintStatusStream;
using TALYFEMLIB::PrintWarning;
using TALYFEMLIB::PrintInfo;
using TALYFEMLIB::PrintStatus;
using TALYFEMLIB::PrintError;

/**
 * Parameters for background mesh
 */
struct MeshDef {
  /// two corners of domain
  ZEROPTV channel_min;
  ZEROPTV channel_max;
  /// refinement level
  int refine_lvl = 0;
  /**
   * read channel mesh from config
   * @param root
   */
  void read_from_config(const libconfig::Setting &root) {
    refine_lvl = (int) root["refine_lvl"];
    channel_min = ZEROPTV((double) root["min"][0], (double) root["min"][1], (double) root["min"][2]);
    channel_max = ZEROPTV((double) root["max"][0], (double) root["max"][1], (double) root["max"][2]);
  }
};

/// Solver Options
struct SolverOptions {
  std::map<std::string, std::string> vals;

  /**
   * Applies val to a PetscOptions database, i.e. adds "[prefix][key] [value]"
   * to a PETSc options database
   * (i.e. the command line).
   * Note that prefix MUST START WITH A -. (PETSc automatically strips the
   * first character...)
   * If you do not call this function, SolverOptions basically does nothing.
   * @param prefix command line prefix, must start with a hyphen (-)
   * @param opts PETSc options database, use NULL for the global options
   * database
   */
  inline void apply_to_petsc_options(const std::string &prefix = "-",
                                     PetscOptions opts = nullptr) {
    for (auto it = vals.begin(); it != vals.end(); it++) {
      const std::string name = prefix + it->first;

      // warn if the option was also specified as a command line option
      PetscBool exists = PETSC_FALSE;
      PetscOptionsHasName(opts, nullptr, name.c_str(), &exists);
      if (exists)
        PrintWarning("Overriding command-line option '", name,
                     "' with config file value '", it->second, "'.");

      PetscOptionsSetValue(opts, name.c_str(), it->second.c_str());
    }
  }
};

struct SSHTInputData : public TALYFEMLIB::InputData {
  /// Mesh definition
  MeshDef mesh_def;

  /// PETSC options (in config.txt)
  SolverOptions solverOptionsSSHT;

  /// dump_vec (for regression test)
  bool dump_vec = false;

  SSHTInputData() : InputData() {}

  ~SSHTInputData() = default;

  static SolverOptions read_solver_options(libconfig::Config &cfg,
                                           const char *name,
                                           bool required = true) {
    if (!cfg.exists(name)) {
      if (!required) {
        PrintInfo(name, " not found (no additional options in config file)");
        return SolverOptions();
      } else {
        throw TALYFEMLIB::TALYException() << "Missing required solver options '" << name
                                          << "'";
      }
    }

    SolverOptions opts;
    const auto &setting = cfg.getRoot()[name];

    for (int i = 0; i != setting.getLength(); ++i) {
      std::string val;
      switch (setting[i].getType()) {
        case libconfig::Setting::TypeInt:
        case libconfig::Setting::TypeInt64:val = std::to_string((long) (setting[i]));
          break;
        case libconfig::Setting::TypeFloat: {
          std::stringstream ss;
          ss << std::setprecision(16) << ((double) setting[i]);
          val = ss.str();
          break;
        }
        case libconfig::Setting::TypeBoolean:val = ((bool) setting[i]) ? "1" : "0";
          break;
        case libconfig::Setting::TypeString:val = (const char *) setting[i];
          break;
        default:
          throw TALYFEMLIB::TALYException() << "Invalid solver option value type ("
                                            << setting[i].getPath() << ") "
                                            << "(type ID " << setting[i].getType() << ")";
      }

      PrintInfo(name, ".", setting[i].getName(), " = ", val);
      opts.vals[setting[i].getName()] = val;
    }
    return opts;
  }

  /// read configure from file
  bool ReadFromFile(const std::string &filename = std::string("config.txt")) {
    /// fill cfg, but don't initialize default fields
    ReadConfigFile(filename);

    nsd = 3;

    ifDD = true;

    mesh_def.read_from_config(cfg.getRoot()["background_mesh"]);

    solverOptionsSSHT = read_solver_options(cfg, "solver_options_ssht");

    if (ReadValue("dump_vec", dump_vec)) {}

    return true;
  }

  /// check if the input are valid
  bool CheckInputData() {
    return true;
  }

};
