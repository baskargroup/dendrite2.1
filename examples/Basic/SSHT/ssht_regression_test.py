#!/usr/bin/env python3

import os
import sys
import re

this_dir = os.path.dirname(os.path.realpath(__file__))
scripts_dir = os.path.join(this_dir, '..', '..', '..', 'scripts')
sys.path.append(scripts_dir)

# choose release mode when possible
found_exec = False
for build_folder in ['cmake-build-release-petsc', 'cmake-build-release', 'cmake-build-debug-petsc', 'cmake-build-debug']:
    executable = os.path.join(this_dir, '..', '..', '..', build_folder, 'examples', 'Basic', 'SSHT',
                              'ssht')
    if os.path.exists(executable):
        found_exec = True
        break

assert found_exec, "Program executable missing ({})".format(executable)
print("Executable: {}".format(executable))

base_cfg = {}

cases = [
    # check serial and parallel
    {'order': 1, 'refine_lvl': 3, 'ntasks': 1},
    {'order': 1, 'refine_lvl': 3, 'ntasks': 4},
    {'order': 2, 'refine_lvl': 3, 'ntasks': 1},
    {'order': 2, 'refine_lvl': 3, 'ntasks': 4},
    # user can check the slope of linear basis functions
    {'order': 1, 'refine_lvl': 2, 'ntasks': 2},
    {'order': 1, 'refine_lvl': 3, 'ntasks': 4},
    {'order': 1, 'refine_lvl': 4, 'ntasks': 4},
    {'order': 1, 'refine_lvl': 5, 'ntasks': 8},
    # user can check the slope of quadratic basis functions
    {'order': 2, 'refine_lvl': 2, 'ntasks': 2},
    {'order': 2, 'refine_lvl': 3, 'ntasks': 4},
    {'order': 2, 'refine_lvl': 4, 'ntasks': 4},
    {'order': 2, 'refine_lvl': 5, 'ntasks': 8},
]

from regression import RegressionTester, RegexDiffMetric, VecDiffMetric

reg = RegressionTester()

reg.add_cases(base_cfg, cases)
reg.set_exclude_patterns(["*.vtu", "*.pvtu", "*.info"])

reg.add_metric(VecDiffMetric('solution_vec.vec'))
reg.add_metric(RegexDiffMetric('Final L2 error', 'output.txt', re.compile(r'Error = (.*)')))
reg.add_file_generator("config.txt", """
BasisFunction = {order}

background_mesh = {{
  refine_lvl = {refine_lvl}
  min = [0, 0, 0]
  max = [1, 1, 1]
}}

dump_vec = true

#################### solver setting ####################
solver_options_ssht = {{
  ksp_max_it = 2000
  ksp_type = "bcgs"
  pc_type = "asm"
  ksp_atol = 1e-7
  ksp_rtol = 1e-10
  ksp_converged_reason = ""
  ksp_monitor = ""
}}
""")

reg.set_run_cmd(executable)
reg.run_main()
