//
// Created by maksbh on 11/30/18.
//

#ifndef DENDRITE2_0_DENDRITEUTILS_H
#define DENDRITE2_0_DENDRITEUTILS_H

#include <vector>
#include <functional>
#include <talyfem/grid/femelm.h>
#include <oda.h>
#include <DataTypes.h>
#include <TalyEquation.h>
#include <iostream>
#include <vector>
#include <numeric>
#include <string>
#include <functional>


typedef std::function<double(double x, double y, double z, int dof)> AnalyticFunction;

typedef std::function<double(unsigned  int dof)> IBMboundaryCondition;


typedef std::function<DENDRITE_REAL(DENDRITE_REAL x, DENDRITE_REAL y, DENDRITE_REAL z,
                                    DENDRITE_REAL *ValueFEM, DENDRITE_REAL *ValueDerivativeFEM)> EvalFunction;

double *calcL2Errors(ot::DA *da, const double *problemSize, const double *u, int ndof, const AnalyticFunction &f);

void evalFunction(ot::DA *da, const double *problemSize, const double *u, int ndof,
                  const std::vector<EvalFunction> &f, double *val_func);

//double calcValueFEM(TALYFEMLIB::FEMElm & fe, int dof, double * value, DENDRITE_INT ndof = 1);
void calcValueFEM(TALYFEMLIB::FEMElm &fe, DENDRITE_INT ndof, double *value, double *val_c);

void
calcValueDerivativeFEM(TALYFEMLIB::FEMElm &fe, DENDRITE_INT ndof, const DENDRITE_REAL *value, DENDRITE_REAL *val_d);

void setScalarByFunction(ot::DA *da, double *u, int ndof,
                         const std::function<double(double, double, double, int)> &f,
                         const Point &domain_min, const Point &domain_max);

void dendrite_init(int argc, char **argv);

void dendrite_finalize(ot::DA *da);


/**
 *
 * @brief This is forked out from the Dendro5 parallelrank to return the value
 * that is greater than the prescribed fraction.
 * @tparam T datatype of vector
 * @param in the vector to sort
 * @param localSize localSize of each vector
 * @param frac fractionID
 * @param comm commWorld
 * @return the border value;
 */
template <typename T>
DENDRITE_REAL getFractionOfValues(const T * in,const unsigned int localSize, T frac, MPI_Comm comm){
  int rank, npes;

  MPI_Comm_rank(comm, &rank);
  MPI_Comm_size(comm, &npes);


  std::vector< _T<T> > key;
  std::vector< _T<T> > key_sorted;
  key.resize(localSize);

  for(unsigned int i=0; i < localSize ; i++)
  {

    key[i].p = rank;
    key[i].idx = i;
    key[i].val = in[i];

  }
  par::bitonicSort(key,comm);
  std::swap(key,key_sorted);
  key.clear();
  DENDRITE_UINT *  sorted_couts = new DENDRITE_UINT [npes];
  DENDRITE_UINT *  sorted_offset = new DENDRITE_UINT [npes];

  DENDRITE_UINT sz = localSize;
  par::Mpi_Allgather(&sz,sorted_couts,1,comm);

  sorted_offset[0] = 0;
  omp_par::scan(sorted_couts,sorted_offset,npes);

  DENDRITE_UINT globalSize= std::accumulate(&sorted_couts[0],&sorted_couts[npes],0);

  DENDRITE_UINT numberOfValues= static_cast<DENDRITE_UINT>(globalSize*frac);

  bool sendRank = false;
  for (int i = 1;i < npes; i++){
    if((sorted_couts[i] <= numberOfValues) and (sorted_couts[i] > numberOfValues)){
      sendRank = true;
    }
  }

  DENDRITE_REAL border_error = 0;
  if(rank == sendRank){
    border_error = key_sorted[numberOfValues-sorted_offset[rank]].val;
  }
  MPI_Bcast(&border_error,1,MPI_DOUBLE,rank,MPI_COMM_WORLD);

  delete [] sorted_couts;
  delete [] sorted_offset;

  return border_error;


}

/**
 * @author: Kumar Saurabh
 * @brief: Creates a Regular DA, given the refinement level.
 *
 */
ot::DA *createRegularDA(const DENDRITE_UINT refine_lvl, const DENDRITE_UINT max_depth = 30, const DENDRITE_UINT grainSz = 100);

void createGrid(const double *coords, TALYFEMLIB::GRID &grid, TALYFEMLIB::ELEM *elem);

/**
 * Prints the statistics of DA
 * @param da DA.
 */
void printDAStatistics(ot::DA * da);

/**
 * @author: maksbh
 * @brief: Useful in IBM .
 * Given the location of Coordinates for the triangles for the geometries, it
 * computes the tree nodes which is useful in sorting.
 * vec.values = The xyz coordinates of the triangles
 * vec.node = The treeNode corresponding to it.
 * @param [in] problemSize: problemSize
 * @param [in] da: DA
 * @param [in] vec: The vec which is filled with vec.values
 */
void computeTreeNodesAndCordinates(ot::DA *da, const double *problemSize,
                                   std::vector<NodeAndValues<DENDRITE_REAL, 3> > &vec);


template<typename Equation, typename NodeData>
void setProblemDimension(TalyEquation<Equation, NodeData> *talyEq,
                         const Point &domain_min, const Point &domain_max) {
    talyEq->mat->setProblemDimensions(domain_min, domain_max);
    talyEq->vec->setProblemDimensions(domain_min, domain_max);

}


inline int GetMPIRank() {
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    return rank;
}




template<typename T>
void writeCellDatatoVtu(ot::DA *da, T * cell_data, const char *fprefix, const DENDRITE_REAL *scalingFactor, bool isOnlyLocal = false) {

    if (!(da->isActive())) {
        return;
    }
    // start writing to file
    const DENDRITE_UINT nPe = da->getNumNodesPerElement();
    const DENDRITE_UINT num_cells = da->getLocalElemSz();
    const DENDRITE_UINT num_vertices = nPe * num_cells;
    const DENDRITE_UINT eleOrder = da->getElementOrder();
    char fname[FNAME_LENGTH];
    char str[2048];
    int rank = TALYFEMLIB::GetMPIRank();
    int npes = TALYFEMLIB::GetMPISize();
    sprintf(fname,"%s_%d_%d.vtu",fprefix,rank,npes);

    std::ofstream out;
    out.open(fname);

    DENDRITE_REAL *coords = new DENDRITE_REAL[nPe * m_uiDim];

    // VTK header
    out << "<?xml version=\"1.0\"?> " << std::endl;
    out << "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" >" << std::endl;
    out << "<UnstructuredGrid >" << std::endl;
    out << "<Piece NumberOfPoints=\" " << num_vertices << "\" NumberOfCells=\" " << num_cells << "\" >\n";


    {                                         /** Points data **/

        out << "<Points>\n";
        out << "<DataArray type=\"Float64\" NumberOfComponents=\"3\" format=\"ascii\">\n";

        for (da->init<ot::DA_FLAGS::LOCAL_ELEMENTS>();
             da->curr() < da->end<ot::DA_FLAGS::LOCAL_ELEMENTS>(); da->next<ot::DA_FLAGS::LOCAL_ELEMENTS>()) {
            da->getElementalCoords(da->curr(), coords);
            for(int i = 0; i < nPe; i++) {
                convertOctToPhys(&coords[3*i], scalingFactor);
            }
            for (int i = 0; i < nPe * m_uiDim; i++) {
                out << coords[i] << " ";
            }
        }
        out << "</DataArray>\n";
        out << "</Points>\n";
    }

    {                                  /** Write connectivity **/

        out << "<Cells>\n";
        out << "<DataArray type=\"UInt32\" Name=\"connectivity\" format=\"ascii\">\n";

        for (unsigned int i = 0; i < num_cells; i++) {
            out
                    << (i * nPe + 0 * (eleOrder + 1) * (eleOrder + 1) + 0 * (eleOrder + 1) + 0) << "  "
                    << (i * nPe + 0 * (eleOrder + 1) * (eleOrder + 1) + 0 * (eleOrder + 1) + eleOrder) << "  "
                    << (i * nPe + 0 * (eleOrder + 1) * (eleOrder + 1) + eleOrder * (eleOrder + 1) + eleOrder) << "  "
                    << (i * nPe + 0 * (eleOrder + 1) * (eleOrder + 1) + eleOrder * (eleOrder + 1) + 0) << "  "
                    << (i * nPe + eleOrder * (eleOrder + 1) * (eleOrder + 1) + 0 * (eleOrder + 1) + 0) << "  "
                    << (i * nPe + eleOrder * (eleOrder + 1) * (eleOrder + 1) + 0 * (eleOrder + 1) + eleOrder) << "  "
                    << (i * nPe + eleOrder * (eleOrder + 1) * (eleOrder + 1) + eleOrder * (eleOrder + 1) + eleOrder)
                    << "  "
                    << (i * nPe + eleOrder * (eleOrder + 1) * (eleOrder + 1) + eleOrder * (eleOrder + 1) + 0) << " ";
        }

        out << "</DataArray>\n";
    }

    {                                 /** Write offsets **/

        out << "<DataArray type=\"UInt32\" Name=\"offsets\" format=\"ascii\">\n";
        for (int i = 1; i <= num_cells; i++) {
            out << i * NUM_CHILDREN << " ";
        }
        out << "</DataArray>\n";

    }

    {                                   /** Write cellTypes **/
        out << "<DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\">\n";
        for (int i = 1; i <= num_cells; i++) {
            out << VTK_HEXAHEDRON << " ";
        }
        out << "</DataArray>\n";
        out << "</Cells>\n";
    }
    {
        /** Write cell data **/

        out << "<CellData SCALARS = \"u\" > \n";
        out << "<DataArray type=\"Float64\" Name=\"u\" format=\"ascii\">\n";
        int counter = 0;

        for (da->init<ot::DA_FLAGS::LOCAL_ELEMENTS>();
             da->curr() < da->end<ot::DA_FLAGS::LOCAL_ELEMENTS>(); da->next<ot::DA_FLAGS::LOCAL_ELEMENTS>()) {
          if(isOnlyLocal) {
            out << cell_data[counter++] << " ";
          }
          else {
            out << cell_data[da->curr()] << " ";
          }
        }
        out << "</DataArray>\n";
        out << "</CellData>\n";
    }
    out << "</Piece>\n";
    out << "</UnstructuredGrid>\n";
    out << "</VTKFile>\n";


}

template <typename T>
void writeCellDatatoPvtu(ot::DA *da, T * cell_data, const char *fprefix, const DENDRITE_REAL *scalingFactor, bool isOnlyLocal = false) {
    int rank = TALYFEMLIB::GetMPIRank();
    if(!rank) {
        char pfname[FNAME_LENGTH];
        int npes = TALYFEMLIB::GetMPISize();

        sprintf(pfname, "%s.pvtu", fprefix);
        std::ofstream file(pfname);
        file << "<?xml version=\"1.0\"?> " << std::endl;
        file << "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" >" << std::endl;
        file << "<PUnstructuredGrid GhostLevel=\"0\">\n";
        file << "<PPoints>\n";
        file << "<PDataArray type=\"Float64\" NumberOfComponents=\"3\"/>\n";
        file << "</PPoints>\n";
        file << "<PCellData Scalars=\"u\">\n";
        file << "<PDataArray type=\"Float64\" Name=\"u\" />\n";
        file << "</PCellData>\n";
        for (int i = 0; i < npes; i++) {
            char fname[PATH_MAX];
            sprintf(fname, "%s_%d_%d.vtu", fprefix, i, npes);
            file << "<Piece Source=\"" << fname << "\" />\n";
        }
        file << "</PUnstructuredGrid>\n";
        file << "</VTKFile>\n";
        file.close();
    }
    writeCellDatatoVtu(da,cell_data,fprefix,scalingFactor,isOnlyLocal);
}





#endif //DENDRITE2_0_DENDRITEUTILS_H
