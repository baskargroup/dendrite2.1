//
// Created by maksbh on 11/23/18.
//

/**
 * @author Kumar Saurabh
 * @brief converts the octant coordinates to the physical coordinates
 * */


#ifndef DENDRITE2_0_OCTTOPHYSICAL_H
#define DENDRITE2_0_OCTTOPHYSICAL_H

#include <point.h>
#include "DataTypes.h"
#include <assert.h>


/**
  @brief returns the physical coordinates given the octant coordinates, problem size and offset.
  @param oct octant coordinates
  @param problem_size problem size in the physical coordinates of the whole domain
  @return Physical coordinate point
  @param offset used when the domain doesn't start from [0,0,0]. Used to offset the physical domain.
  **/
Point  convertOctToPhys(Point & oct, const DENDRITE_REAL * problem_size, const DENDRITE_REAL * offset);

/**
  @brief returns the physical coordinates given the octant coordinates and problem size.
  @param oct octant coordinates
  @param problem_size problem size in the physical coordinates of the whole domain
  @return Physical coordinate point
  @remark offset is set to be [0,0,0].
  **/
Point  convertOctToPhys(Point & oct, const DENDRITE_REAL * problem_size);

/**
  @brief returns the physical coordinates given the octant coordinates.
  @param oct octant coordinates
  @return Physical coordinate point
  @remark offset is set to be [0,0,0] and problem size to be [1.0,1.0,1.0].
  **/

Point  convertOctToPhys( Point & oct,const Point & domain_min,const Point & domain_max);

Point  convertOctToPhys(Point & oct);

void  convertOctToPhys(DENDRITE_REAL * oct );

void  convertOctToPhys(DENDRITE_REAL * oct, const double * problem_size );


/**
  @brief builds the Taly coordinates
  @param [out] coords  Taly coordinates : first x, then y , then z
  @param [in] pt lower most corner of the cell
  @param [in] h dx, dy, dz
  @param [in] ele_order element order
 **/
void build_taly_coordinates(double *coords , const Point &pt, const Point &h,
        const DENDRITE_UINT ele_order);

/**
  @brief builds the Taly coordinates
  @param [out] coords  Taly coordinates : first x, then y , then z
  @param [in] pt lower most corner of the cell
  @param [in] h dx, dy, dz
  @param [in] ele_order element order
 **/

void build_taly_coordinates(double *coords , const ZEROPTV &pt, const ZEROPTV &h, const DENDRITE_UINT ele_order);

/**
  @brief builds coordinates in ZEROPTV vector.
  @param [in] coords: octant coordinates
  @param [out] coords: physical coordinates
  @param [in] ele_order element order
  @param [in] problemSize : problem size / scaling factor
  @param [out] node: ZEROPTV vector of node position. The order is first x, then y and then z.
  @param [in] isAllocated : true if size of node is allocated, false otherwise
 **/
void coordsToZeroptv(double *coords , std::vector<ZEROPTV >&node, const DENDRITE_REAL * problem_size, const DENDRITE_UINT ele_order, bool isAllocated);

/**
 *
 * @param treeNode treeNode
 * @param coords[out] ZEROPTV of coords. Size must be allocated outside. Size of vec = 8 in 3D
 * @param problem_size problem size
 */

void treeNodesToZEROPTV(const ot::TreeNode *treeNode , std::vector<ZEROPTV >&coords, const DENDRITE_REAL * problem_size);
#endif //DENDRITE2_0_OCTTOPHYSICAL_H


