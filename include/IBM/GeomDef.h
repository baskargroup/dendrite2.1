//
// Created by maksbh on 6/10/19.
//// This is  the class originally developed by Boshun.
//

/**
 * @author maksbh
 * Changes:
 * 1. temperature renamed to dirichlet
 */

#ifndef DENDRITE2_0_GEOMDEF_H
#define DENDRITE2_0_GEOMDEF_H
#include <talyfem/input_data/input_data.h>
#include <iomanip>

using TALYFEMLIB::ZEROPTV;
using TALYFEMLIB::PrintStatusStream;
using TALYFEMLIB::PrintWarning;
using TALYFEMLIB::PrintInfo;
using TALYFEMLIB::PrintStatus;
using TALYFEMLIB::PrintError;

struct GeomDef {
  enum Type {
    INVALID = 0,
    SPHERE = 1,
    PILLAR = 2,
    MESHOBJECT = 3,
    MESHOBJECT_2D = 4,
    CYLINDER = 5,
    CUBE = 6,
  };
  enum FileFormat {
    NOTKNOWN = 0,
    GMSH = 1,
    STL = 2,
  };
  enum BCType {
    INVALID_BC = 0,
    DIRICHLET = 1,
    NEUMANN = 2,
    ROBIN = 3,
  };
  enum InoutTYPE {
    UNKNOWN = -1,
    PRIMITIVE = 0,
    RT = 1,
  };
  enum PrescribedMov {
    STATIONARY = 0,
    ROTATION = 1,
    TRANSALATION = 2,
  };
  ///< path to surface mesh (currently must be triangular)
  std::string mesh_path;
  ///< name of the geometry (used for printing file and etc..)
  std::string name;
  ///< level of gauss point split for triangles
  unsigned gp_level = 0;
  ///< file format depending on the file extension
  FileFormat fileformat = NOTKNOWN;
  ///< type of geometry, controls which analytical in/out function to use
  Type type = INVALID;
  ///< type for checking in_out
  InoutTYPE io_type = UNKNOWN;
  ///< refinement level for future use
  unsigned int refine_lvl = 0;
  ///< initial displacement for the geometry (used only once to move geometry to desired location)
  ZEROPTV InitialDisplacement;
  ///< if postprocessing
  bool ifPostprocess = true;

  /// variables depending on type...
  ZEROPTV center_of_mass;
  ///< radius for sphere or cylinder types
  double radius = 0.0;
  ///< cylinder orientation, used for in_out test
  int cylinderDir = 2;
  double height = -1.0;
  ZEROPTV bottom_center;
  ///< first one is bottom left back corner, the second one is dimension in x-y-z
  std::vector<ZEROPTV> cube_dim{2};

  /// boundary conditions for geometry
  std::vector<GeomDef::BCType> bc_type_V = {};
  std::vector<double> dirichlet_V = {};
  std::vector<double> flux_V = {};
  std::vector<double> G_constant_V = {};
  std::vector<double> a_constant_V = {};


  bool is_static = true;
  std::vector<bool> translation_dof;
  bool if_buoyancy = false;
  bool if_rotation = false;
  double acc_limiter = 1e6;
  double rho = 1.0;

  ZEROPTV ic_vel;
  ZEROPTV ic_omega;
  double until_time = -1.0;
  double moving_until_time = -1.0;
  ZEROPTV bounding_min;
  ZEROPTV bounding_max;

  PrescribedMov move_type = PrescribedMov::STATIONARY;
  ZEROPTV rotation_axis = {1.0, 0.0, 0.0};
  double rotation_speed = 0.0;
  ZEROPTV translation_speed = {0.0, 0.0, 0.0};


  std::vector<double> getBC(int dof) {
    assert(dof < bc_type_V.size());
    if (bc_type_V[dof] == GeomDef::BCType::DIRICHLET) {
      return std::vector<double>{dirichlet_V[dof]};
    }
    if (bc_type_V[dof] == GeomDef::BCType::NEUMANN) {
      return std::vector<double>{flux_V[dof]};
    }
    if (bc_type_V[dof] == GeomDef::BCType::ROBIN) {
      return std::vector<double>{G_constant_V[dof], a_constant_V[dof]};
    }
  }

  void read_from_config(const libconfig::Setting &root) {
    mesh_path = (const char *) root["mesh_path"];
    if (root.exists("name")) {
      name = (const char *) root["name"];
    } else {
      if (mesh_path.find_last_of('/')) {
        name = mesh_path.substr(mesh_path.find_last_of('/') + 1,
                                mesh_path.find_last_of('.') - mesh_path.find_last_of('/') - 1);
      } else {
        name = mesh_path.substr(0, mesh_path.find_last_of('.') - 1);
      }
    }
    if (root.exists("gp_level")) {
      gp_level = (int) root["gp_level"];
    }

    type = str_to_type(root["type"]);
    fileformat = mesh_path_to_file_format(mesh_path);
    InitialDisplacement =
        ZEROPTV((double) root["position"][0], (double) root["position"][1], (double) root["position"][2]);
    io_type = type_to_iotype(type);
    if (root.exists("refine_lvl")) {
      refine_lvl = (unsigned int) root["refine_lvl"];
    }

    if (root.exists("ifPostprocess")) {
      ifPostprocess = (bool) root["ifPostprocess"];
    }

    /// extra parameter for each primitive type
    switch (type) {
      case Type::SPHERE:radius = (double) root["radius"];
        center_of_mass = ZEROPTV((double) root["com"][0], (double) root["com"][1], (double) root["com"][2]);
        break;
      case Type::PILLAR:radius = (double) root["radius"];
        center_of_mass = ZEROPTV((double) root["com"][0], (double) root["com"][1], (double) root["com"][2]);
        if (root.exists("cylinderDir")) {
          cylinderDir = (int) root["cylinderDir"];
        }
        break;
      case Type::CYLINDER:radius = (double) root["radius"];
        center_of_mass = ZEROPTV((double) root["com"][0], (double) root["com"][1], (double) root["com"][2]);
        height = (double) root["height"];
        if (root.exists("cylinderDir")) {
          cylinderDir = (int) root["cylinderDir"];
        }
        bottom_center = ZEROPTV((double) root["bottom_center"][0],
                                (double) root["bottom_center"][1],
                                (double) root["bottom_center"][2]);
        bottom_center += InitialDisplacement;
        break;
      case Type::CUBE:
        cube_dim[0] = ZEROPTV((double) root["min_corner"][0],
                              (double) root["min_corner"][1],
                              (double) root["min_corner"][2]);
        cube_dim[1] = ZEROPTV((double) root["dim"][0],
                              (double) root["dim"][1],
                              (double) root["dim"][2]);
        break;
      default:assert(type == GeomDef::MESHOBJECT || type == GeomDef::MESHOBJECT_2D);
    }

//    if (root.exists("bc_type_V")) {
    const libconfig::Setting &bc_dof = root["bc_type_V"];
    for (int i = 0; i < bc_dof.getLength(); ++i) {
      std::string a = bc_dof[i];
      GeomDef::BCType temp = str_to_bctype(bc_dof[i]);
      bc_type_V.push_back(temp);
    }

    int d_iter = 0, n_iter = 0, r_iter = 0;
    for (int i = 0; i < bc_type_V.size(); i++) {
      if (bc_type_V.at(i) == GeomDef::BCType::DIRICHLET) {
        const libconfig::Setting &temp1 = root["dirichlet_V"];
        dirichlet_V.push_back(double(temp1[d_iter++]));
        flux_V.push_back(-100.0);
        G_constant_V.push_back(-100.0);
        a_constant_V.push_back(-100.0);
      }
      if (bc_type_V.at(i) == GeomDef::BCType::NEUMANN) {
        dirichlet_V.push_back(-100.0);
        const libconfig::Setting &temp2 = root["flux_V"];
        flux_V.push_back(double(temp2[n_iter++]));
        G_constant_V.push_back(-100.0);
        a_constant_V.push_back(-100.0);
      }
      if (bc_type_V.at(i) == GeomDef::BCType::ROBIN) {
        dirichlet_V.push_back(-100.0);
        flux_V.push_back(-100.0);
        const libconfig::Setting &temp3 = root["G_constant_V"];
        const libconfig::Setting &temp4 = root["a_constant_V"];
        G_constant_V.push_back(double(temp3[r_iter]));
        a_constant_V.push_back(double(temp4[r_iter++]));
      }
    }


    if (root.exists(("moving_parameters"))) {
      const auto &root_m = root["moving_parameters"];
      if (root_m.exists("is_static")) {
        is_static = (bool) root_m["is_static"];
        if (!is_static) {
          if_buoyancy = (bool) root_m["if_buoyancy"];
          if (if_buoyancy) {
            rho = (double) root_m["rho"];
          }
          if_rotation = (bool) root_m["if_rotation"];
          translation_dof.resize(3);
          if (root_m.exists("translation_dof")) {
            for (int i = 0; i < 3; ++i) {
              translation_dof[i] = (bool) root_m["translation_dof"][i];
            }
          } else {
            for (int i = 0; i < 3; ++i) {
              translation_dof[i] = true;
            }
          }
        }
        if (root_m.exists("acc_limiter")) {
          acc_limiter = (double) root_m["acc_limiter"];
        }
      }
      if (root_m.exists("bounding_min")) {
        bounding_min = ZEROPTV((double) root_m["bounding_min"][0],
                               (double) root_m["bounding_min"][1],
                               (double) root_m["bounding_min"][2]);
      } else {
        bounding_min = {-1e6, -1e6, -1e6};
      }
      if (root_m.exists("bounding_max")) {
        bounding_max = ZEROPTV((double) root_m["bounding_max"][0],
                               (double) root_m["bounding_max"][1],
                               (double) root_m["bounding_max"][2]);
      } else {
        bounding_max = {1e6, 1e6, 1e6};
      }
    }

    if (root.exists("prescribed_mov")) {
      const auto &root_p = root["prescribed_mov"];
      if (root_p.exists("is_static")) {
        is_static = (bool) root_p["is_static"];
      }
      if (not is_static) {
        throw TALYFEMLIB::TALYException() << "Not supported move with prescribed_mov";
      } else {
        move_type = str_to_movetype(root_p["move_type"]);
        if (move_type == PrescribedMov::ROTATION) {
          rotation_axis = ZEROPTV((double) root_p["rotation_axis"][0],
                                  (double) root_p["rotation_axis"][1],
                                  (double) root_p["rotation_axis"][2]);
          rotation_speed = (double) root_p["rotation_speed"];
        }
      }
    }

  }

 protected:
  void checkInput() {

  }

  /// read mesh type
  Type str_to_type(const std::string &str) const {
    if (str == "sphere") {
      return Type::SPHERE;
    } else if (str == "pillar") {
      return Type::PILLAR;
    } else if (str == "cylinder") {
      return Type::CYLINDER;
    } else if (str == "cube") {
      return Type::CUBE;
    } else if (str == "meshobject") {
      return Type::MESHOBJECT;
    } else if (str == "meshobject_2d") {
      return Type::MESHOBJECT_2D;
    } else {
      throw TALYFEMLIB::TALYException() << "Invalid geometry type '" << str
                                        << "' (expected sphere, pillar, cylinder cube or meshobject(2D) )";
    }
  }

  FileFormat mesh_path_to_file_format(const std::string &str) const {
    if (str.substr(str.find_last_of('.') + 1) == "msh") {
      return FileFormat::GMSH;
    } else if (str.substr(str.find_last_of('.') + 1) == "stl") {
      return FileFormat::STL;
    } else {
      throw TALYFEMLIB::TALYException() << "Invalid file extension '" << str.substr(str.find_last_of('.') + 1)
                                        << "' (support .msh, .stl)";
    }
  }

  InoutTYPE type_to_iotype(const Type &type) const {
    if (type == Type::MESHOBJECT) {
      return RT;
    } else {
      return PRIMITIVE;
    }
  }

  /// read bc type
  BCType str_to_bctype(const std::string &str) const {
    if (str == "dirichlet") {
      return BCType::DIRICHLET;
    } else if (str == "neumann") {
      return BCType::NEUMANN;
    } else if (str == "robin") {
      return BCType::ROBIN;
    } else {
      throw TALYFEMLIB::TALYException() << "Invalid bounday condition type '" << str
                                        << "' (expected dirichlet or neumann)";
    }
  }

  /// read prescribed move type
  PrescribedMov str_to_movetype(const std::string &str) const {
    if (str == "stationary") {
      return PrescribedMov::STATIONARY;
    } else if (str == "rotation") {
      return PrescribedMov::ROTATION;
    } else if (str == "translation") {
      return PrescribedMov::TRANSALATION;
    } else {
      throw TALYFEMLIB::TALYException() << "Invalid prescribed movement '" << str
                                        << "' (expected rotation or translation)";
    }
  }
};
#endif //DENDRITE2_0_GEOMDEF_H
