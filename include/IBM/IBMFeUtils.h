//
// Created by maksbh on 8/8/19.
//

#ifndef DENDRITE2_0_IBMFEUTILS_H
#define DENDRITE2_0_IBMFEUTILS_H
#include "FEUtils.h"
#include <queue>
#include <TalyDendroSync.h>
#include <oda.h>
#include <OctToPhysical.h>
#include "IBMDataTypes.h"
#include "IBMSolver.h"

template<class Equation, class NodeData>
class IBMFEUtils : public FEUtils<Equation, NodeData> {
 protected:
  using FEUtils<Equation, NodeData>::m_grid;
  using FEUtils<Equation, NodeData>::m_gaussPoints;

 private:
  DENDRITE_UINT maxRefineLevel_;
  IBMSolver<NodeData> *ibmSolver_ = NULL;

 public:
  void doSplitting(eleInfo &BaseElement);

  IBMFEUtils(IBMSolver<NodeData> *ibmSolver, DENDRITE_UINT maxRefineLevel, ot::DA *da) {
    ibmSolver_ = ibmSolver;
    maxRefineLevel_ = maxRefineLevel;
  }

  void performPartition() override {
    m_gaussPoints.clear();
    ibmSolver_->performPartition(m_grid, m_gaussPoints,maxRefineLevel_);
  }

};


#endif //DENDRITE2_0_IBMFEUTILS_H
