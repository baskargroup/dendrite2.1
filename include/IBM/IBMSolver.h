//
// Created by maksbh on 6/10/19.
//// This is  the class originally developed by Boshun.
//

/**
 * @author maksbh
 * Changes:
 * 1. Constructor of IBMSolver
 * 2. findBackgroundElements changed to partitionGeometries
 * 3. Added support for finding the triangles in the octant
 */

#ifndef DENDRITE2_0_IBMSOLVER_H
#define DENDRITE2_0_IBMSOLVER_H

#include <PETSc/Solver/Solver.h>
#include <talyfem/grid/nodedata.h>
#include <DendriteUtils.h>
#include <IBM/VTKIO.h>
#include <IBM/IBMDataTypes.h>
#include <queue>
#include <Refinement/RefinementDataTypes.h>


template<typename NodeData>
class IBMSolver {
 private:
  ot::DA *da_;
  unsigned int npe_;
  TalyDendroSync sync_;
  DENDRITE_UINT maxRefineLevel_;
  DENDRITE_UINT isVariableGP_;
  bool ifStrongBC_ = false;

  struct surfaceExchangeData {
    int globalID;
    int temp;
    double values;
  };

  bool setInteriorDirichlet_ = false;
  /// IBM boundary condition
  IBMboundaryCondition m_IBMboundaryValues;

 public:
  struct GeomData {
    Geom<NodeData> *geo;
    std::vector<NodeAndValues<double, 3> > my_gauss_points;
    std::vector<NodeAndValues<double, 3> > my_surface_points;
    std::vector<unsigned int> backgroundElementGP;
    std::vector<unsigned int> backgroundElementSP;
  };


  const vector<triInfo> &getTriInOctant(int elemID) const;
  const inOutInfo getInOutElementGP(int elemID) const;
  void setInOutElementGP(int elemID, inOutInfo inOutInfo);
  const inOutInfo getInOutElementNODE(int elemID) const;
  const vector<GeomData> &getGeometries() const {
    return geometries_;
  };
  const bool isStrongBC() const{
    return ifStrongBC_;
  }

  void doSplitting(eleInfo &BaseElement, TALYFEMLIB::GRID *m_grid, int maxRefineLevel,
                   std::queue<eleInfo> &RefinedElement, std::vector<GPInfo> &m_gaussPoints);
  /**
   * @brief syncs the should solve buffer.
   * Must be called before applying the boundary conditions.
   * Otherwise the result will be inconsistent across different processors.
   */
  void syncShouldSolveBuffer();

  inline void getIBMValues(std::vector<double> & bcValues){
    for(DENDRITE_UINT i = 0; i < bcValues.size();i++){
      bcValues[i] = m_IBMboundaryValues(i);
    }
  }

  inline void setIBMBC(IBMboundaryCondition & f){
    m_IBMboundaryValues = f;
  }

  void performPartition(TALYFEMLIB::GRID *m_grid,
                        std::vector<GPInfo> &m_gaussPoints,
                        const DENDRITE_UINT maxRefineLevel);
  /**
   * [in/out] get all the freshly cleared nodes (element id and node idx), there are duplicates
   * @param localFreshlyOutwoNeighbour
   */
  void get_freshly_cleared(std::vector<localFreshlyOut> &localFreshlyOutwoNeighbour);

  /**
   * get all the neighbourhoud of a freshly cleared node
   * @param localin
   */
  void modify_freshly_cleared(std::vector<localFreshlyOut> &localin,
                              std::vector<VecInfo> &vec);

  void compute_freshly_cleared(std::vector<localFreshlyOut> &localin,
                               std::vector<VecInfo> &vec);

  void interpolate(const localFreshlyOut &lcl, std::vector<DENDRITE_REAL> &value, const int dof, const int offset = 0);

  void computeSurfaceValue(VecInfo _in, const int index, const int offset = 0);

  IBMSolver(ot::DA *da,
            const std::vector<Geom<NodeData> *> &geometries,
            const double *problem_size,
            const IBMboundaryCondition & ibmBC,
            const DENDRITE_UINT isVariableGP = 0,
            const DENDRITE_UINT maxRefineLevel = 0,
            const bool setInteriorDirichlet = false,
            const bool ifWeakBC = true) {


    da_ = da;
    m_IBMboundaryValues = ibmBC;
    m_problemSize_[0] = problem_size[0];
    m_problemSize_[1] = problem_size[1];
    m_problemSize_[2] = problem_size[2];

    geometries_.resize(geometries.size());

    for (unsigned int i = 0; i < geometries_.size(); i++) {
      geometries_[i].geo = geometries[i];
    }
    da_->createVector(m_inOutElementGP, true, true, 1);
    da_->createVector(m_inOutElementNODE, true, true, 1);
    da_->createVector(nodal_inout, false, true, 1);

    npe_ = da_->getNumNodesPerElement();
    da->createVector(shouldSolve, false, true, 1);
    maxRefineLevel_ = maxRefineLevel;
    isVariableGP_ = isVariableGP;
    if (geometries_.empty()) {
      std::fill(m_inOutElementGP.begin(), m_inOutElementGP.end(), inOutInfo::OUT_GP);
      std::fill(m_inOutElementNODE.begin(), m_inOutElementNODE.end(), inOutInfo::OUT_NODE);
      std::fill(shouldSolve.begin(), shouldSolve.end(), SolveInfo::SOLVE);
      std::fill(nodal_inout.begin(), nodal_inout.end(), inOutInfo::INVALID);
    } else {
      std::fill(m_inOutElementGP.begin(), m_inOutElementGP.end(), inOutInfo::INVALID);
      std::fill(m_inOutElementNODE.begin(), m_inOutElementNODE.end(), inOutInfo::INVALID);
      std::fill(shouldSolve.begin(), shouldSolve.end(), SolveInfo::NOSOLVE);
      std::fill(nodal_inout.begin(), nodal_inout.end(), inOutInfo::INVALID);
    }
    setInteriorDirichlet_ = setInteriorDirichlet;
    ifStrongBC_ = not(ifWeakBC);


  }

  ~IBMSolver() {

  }

  void partitionGeometries();

  /***
   * @author maksbh
   * @brief This function updates the octant id of the gauss points and surface points.
   * [New in Dendrite 2.0] This function also updates the octants with the surface points.
   * @param [in] pts can be gauss point or surface points.
   * @param [out] backGroundElement : BackGround octant id
   * @param [in] geometryID : Geometry id
   * @param [in] computeTriInOctants true for surface points.
   */
  void findBackGroundElements(const std::vector<NodeAndValues<double, 3> > &pts,
                              std::vector<unsigned int> &backGroundElement, unsigned int geometryID = 0,
                              bool computeTriInOctants = false);

  void performInOutTest();

  bool point_in_any_geometry(const ZEROPTV &pt) const;

  const vector<int> &getShouldSolve() const;

  void setShouldSolve(int nodeIdx, SolveInfo solveInfo);

 protected:

  double m_problemSize_[3];
  std::vector<int> shouldSolve;
  std::vector<std::vector<triInfo>> m_triInOctant;
  std::vector<inOutInfo> m_inOutElementGP;
  std::vector<inOutInfo> m_inOutElementNODE;
  std::vector<GeomData> geometries_;
  /// use double for debug, but use int for normal use
  std::vector<double> nodal_inout;
};

/**
 * @tparam NodeData
 * There is a change in the logic from the previous version of Dendro4.
 * Required: Every processor has the complete information of the geometry as the config is read by every processor.
 * Each processor on the fly stores only the information about the local gauss points and the surface nodes.
 * Each processor will be responsible for sorting its local gauss point and surface nodes.
 * This might help in space-time as no communication is needed and sorting can be made efficient.
 */

template<typename NodeData>
void IBMSolver<NodeData>::partitionGeometries() {
  if (not(geometries_.empty())) {
    for (auto &geom_data : geometries_) {
      Geom<NodeData> *geo = geom_data.geo;
      std::vector<NodeAndValues<double, 3> > gps;
      std::vector<NodeAndValues<double, 3> > surfaceNode;
      for (int surfaceElemID = 0; surfaceElemID < geo->surface_mesh.Nelem(); surfaceElemID++) {
        const auto gpinfo = geo->gpCoor_len_normal(surfaceElemID);
        // add gauss points for surfaceElemID to gps vector
        for (int gp_idx = 0; gp_idx < geo->gpno; gp_idx++) {
          NodeAndValues<double, 3> node;

          // loop over x/y/z (dimensions)
          for (int d = 0; d < geo->nsd; d++) {
            node.values[d] = gpinfo.gp_position[gp_idx][d];
          }
          node.label = surfaceElemID;
          gps.push_back(node);
        }
      }

      // surface nodes
      for (int surfaceNodeID = 0; surfaceNodeID < geo->surface_mesh.Nnode(); surfaceNodeID++) {
        NodeAndValues<double, 3> node;
        // loop over x/y/z (dimensions)
        for (int d = 0; d < geo->nsd; d++) {
          node.values[d] = geo->surface_mesh.node_array[surfaceNodeID].position[d];
        }
        node.label = surfaceNodeID;
        surfaceNode.push_back(node);
      }


      // Compute Tree nodes and coordinates corresponding to gauss point (GP) and surface points (SP)
      computeTreeNodesAndCordinates(da_, m_problemSize_, gps);
      computeTreeNodesAndCordinates(da_, m_problemSize_, surfaceNode);

      // Vector to hold Compute local Gauss and Surface points
      std::vector<NodeAndValues<double, 3>> localGP;
      std::vector<NodeAndValues<double, 3>> localSP;

      // Vector to hold nodes of  local Gauss and Surface points. Used in computing the owner rank.
      std::vector<ot::TreeNode> ownerLocalGP(gps.size());
      std::vector<ot::TreeNode> ownerLocalSP(surfaceNode.size());

      for (int i = 0; i < gps.size(); i++) {
        ownerLocalGP[i] = gps[i].node;
      }


      // Compute the owner rank of gauss point
      std::vector<int> ownerRank(ownerLocalGP.size(), -1);
      for (int i = 0; i < ownerLocalGP.size(); i++) {
        int rank;
        assert((gps[i].values[0] > 0) and (gps[i].values[1] > 0) and (gps[i].values[2] > 0));
//        std::cout << "In" << gps[i].values[0] << " "<< gps[i].values[1] << " "<< gps[i].values[2]  << "\n";
//        da_->computeTreeNodeOwnerProc(&ownerLocalGP[i], 1, &rank);
//        std::cout << "Out" << ownerLocalGP[i] << "\n";
      }
      da_->computeTreeNodeOwnerProc(ownerLocalGP.data(), ownerLocalGP.size(), ownerRank.data());

      //Fill local GP with the oneslocalto processor.
      for (int i = 0; i < ownerRank.size(); i++) {
        if (ownerRank[i] == da_->getRankActive()) {
          localGP.push_back(gps[i]);
        }
      }

      ownerLocalGP.clear();
      ownerRank.clear();

      // Compute the owner rank of surface point
      ownerRank.resize(surfaceNode.size());
      for (int i = 0; i < surfaceNode.size(); i++) {
        ownerLocalSP[i] = surfaceNode[i].node;
      }
      //Fill local SP with the ones local to processor.
      da_->computeTreeNodeOwnerProc(ownerLocalSP.data(), ownerLocalSP.size(), ownerRank.data());
      for (int i = 0; i < ownerRank.size(); i++) {
        if (ownerRank[i] == da_->getRankActive()) {
          localSP.push_back(surfaceNode[i]);
        }
      }

      ownerLocalSP.clear();
      ownerRank.clear();

      // Sort only the local ones
      std::sort(localGP.begin(), localGP.end());
      std::sort(localSP.begin(), localSP.end());

      // Push into the global data structure
      geom_data.my_gauss_points = std::move(localGP);
      geom_data.my_surface_points = std::move(localSP);

      geom_data.backgroundElementGP.clear();
      geom_data.backgroundElementGP.resize(geom_data.my_gauss_points.size());

      geom_data.backgroundElementSP.clear();
      geom_data.backgroundElementSP.resize(geom_data.my_surface_points.size());

    }

    m_triInOctant.clear();
    da_->createVector(m_triInOctant, true, true, 1);
    for (unsigned int i = 0; i < geometries_.size(); i++) {
      findBackGroundElements(geometries_[i].my_gauss_points, geometries_[i].backgroundElementGP, i, false);
      findBackGroundElements(geometries_[i].my_surface_points, geometries_[i].backgroundElementSP, i, true);
    }
    performInOutTest();
  }
}



template<typename NodeData>
void IBMSolver<NodeData>::findBackGroundElements(const std::vector<NodeAndValues<double, 3> > &pts,
                                                 std::vector<unsigned int> &backGroundElement, unsigned int geometryID,
                                                 bool computeTriInOctants) {

  const unsigned int nodesPerElement = da_->getNumNodesPerElement();
  double coords[nodesPerElement * 3];

  unsigned int pt_idx = 0;
  triInfo triID;
  for (da_->init<ot::DA_FLAGS::WRITABLE>();
       da_->curr() < da_->end<ot::DA_FLAGS::WRITABLE>(); da_->next<ot::DA_FLAGS::WRITABLE>()) {
    unsigned int level = da_->getLevel(da_->curr());

    da_->getElementalCoords(da_->curr(), coords);

    ot::TreeNode curNode(static_cast<unsigned int>(coords[0]), static_cast<unsigned int>(coords[1]),
                         static_cast<unsigned int>(coords[2]), level, 3, da_->getMaxDepth());
    for (int i = 0; i < nodesPerElement; i++) {
      convertOctToPhys(&coords[3 * i], m_problemSize_);
    }
    while (pt_idx < pts.size()) {

      const ot::TreeNode &checkNode = pts[pt_idx].node;

      if ((curNode.isAncestor(checkNode)) or (curNode == checkNode)) {
        if (computeTriInOctants) {
          // todo this works because of stl have connectivity of 0~2, 3~5, etc..., not the same with gmsh
          unsigned int surfID = static_cast<unsigned int>(geometries_[geometryID].my_surface_points[pt_idx].label / 3);
          auto &geo = geometries_[geometryID].geo;
          ZEROPTV normal = geo->gpCoor_len_normal(surfID).gp_normal;
          triID.geoID = geometryID;
          triID.id = surfID;
          triID.normal = normal;
          triID.centroid =
              (geo->surface_mesh.node_array[geo->surface_mesh.elem_array[surfID].connectivity[0]].position +
                  geo->surface_mesh.node_array[geo->surface_mesh.elem_array[surfID].connectivity[1]].position +
                  geo->surface_mesh.node_array[geo->surface_mesh.elem_array[surfID].connectivity[2]].position)
                  * (1.0 / 3.0);
          m_triInOctant[da_->curr()].push_back(triID);
          m_inOutElementGP[da_->curr()] = inOutInfo::INTERCEPTED_GP;
          m_inOutElementNODE[da_->curr()] = inOutInfo::INTERCEPTED_NODE;
        }

        /*** Just for debugging"**/
        const double *checkPt = pts[pt_idx].values;
        int lnode = nodesPerElement - 1;
        if (not((coords[0] <= checkPt[0]) and (coords[1] <= checkPt[1]) and (coords[2] <= checkPt[2])
            and (coords[lnode * 3 + 0] >= checkPt[0]) and (coords[lnode * 3 + 1] >= checkPt[1])
            and (coords[lnode * 3 + 2] >= checkPt[2]))) {
          std::cout << " Something went wrong in computation of backGround ELement. Exiting \n";
          std::cout << "The value of the element =  " << " " << pt_idx << " " << checkPt[0] << " " << checkPt[1] << " "
                    << checkPt[2]
                    << "\n";
          for (int i = 0; i < nodesPerElement; i++) {
            std::cout << coords[3 * i + 0] << " " << coords[3 * i + 1] << " " << coords[3 * i + 2] << "\n";
          }
          TALYFEMLIB::TALYException() << "Something went wrong in computation of backGround Element. Exiting  \n";

        }

        backGroundElement[pt_idx] = da_->curr();
        pt_idx++;

      } else {
        break;
      }

    }
    // remove duplicate triangles
    if (computeTriInOctants && !m_triInOctant.at(da_->curr()).empty()) {
      // Sorting the array
      std::sort(m_triInOctant[da_->curr()].begin(), m_triInOctant[da_->curr()].end(), triSort);
      // remove consecutive duplicate
      auto s = std::unique(m_triInOctant[da_->curr()].begin(), m_triInOctant[da_->curr()].end(), triEqual);
      // resize array
      m_triInOctant[da_->curr()].resize(std::distance(m_triInOctant[da_->curr()].begin(), s));
    }
  }

}

template<typename NodeData>
bool IBMSolver<NodeData>::point_in_any_geometry(const ZEROPTV &pt) const {
  for (const auto &geom_data : geometries_) {
    if (geom_data.geo->point_in_geometry(pt))
      return true;
  }
  return false;
}

template<typename NodeData>
void IBMSolver<NodeData>::performInOutTest() {
  const unsigned int eleOrder = da_->getElementOrder();
  TalyMesh<TALYFEMLIB::NODEData> taly_mesh(eleOrder);
  TALYFEMLIB::GRID *taly_grid = &taly_mesh.grid;

  double *coords = new double[npe_ * 3];
  DendroIntL *localNodeIdx = new DendroIntL[npe_];
  bool triangleCheck = true;

  /// nodal in/out
  for (da_->init<ot::DA_FLAGS::WRITABLE>();
       da_->curr() < da_->end<ot::DA_FLAGS::WRITABLE>(); da_->next<ot::DA_FLAGS::WRITABLE>()) {

    da_->getElementalCoords(da_->curr(), coords);

    for (int i = 0; i < npe_; i++) {
      convertOctToPhys(&coords[3 * i], m_problemSize_);
    }
    if (eleOrder == 1) {
      sync_.syncCoords<1, 3>(coords, taly_grid);
    } else if (eleOrder == 2) {
      sync_.syncCoords<2, 3>(coords, taly_grid);
    }
    TALYFEMLIB::FEMElm fe(taly_grid, TALYFEMLIB::BASIS_ALL);
    if (eleOrder == 1) {
      fe.refill(taly_grid->elm_array_[0], TALYFEMLIB::BASIS_LINEAR, 0);
    } else if (eleOrder == 2) {
      fe.refill(taly_grid->elm_array_[0], TALYFEMLIB::BASIS_QUADRATIC, 0);
    }


    /**
     * Use nodal point rather than gauss points to determine if the element needs to be splitted or not.
     * This is due to the gauss point is fully within the element (at least for gaussian quadrature point)
     * and only using gauss point to split will miss some element.
     */
    int node_out = 0;
    ZEROPTV elem_node;
    da_->getNodeIndices(localNodeIdx, da_->curr(), true);
    for (int i = 0; i < taly_grid->n_nodes(); i++) {
      taly_grid->GetCoord(elem_node, i);
      if (!point_in_any_geometry(elem_node)) {
        node_out++;
        nodal_inout[localNodeIdx[i]] = inOutInfo::OUT_NODE;
      } else {
        nodal_inout[localNodeIdx[i]] = inOutInfo::IN_NODE;
      }
    }

    /// this is a similar vector holding in/out info using the nodal points
    if (node_out == 0) {
      m_inOutElementNODE[da_->curr()] = inOutInfo::IN_NODE;
    } else if (node_out == taly_grid->n_nodes()) {
      m_inOutElementNODE[da_->curr()] = inOutInfo::OUT_NODE;
    } else {
      m_inOutElementNODE[da_->curr()] = inOutInfo::INTERCEPTED_NODE;
      if (m_triInOctant[da_->curr()].empty() and triangleCheck) {
        PrintWarning("Triangle not found for intercepting element (triangle size maybe too large) ", da_->curr());
        triangleCheck = false;
      }
    }
  }

  /// loop to split elements
  for (da_->init<ot::DA_FLAGS::WRITABLE>();
       da_->curr() < da_->end<ot::DA_FLAGS::WRITABLE>(); da_->next<ot::DA_FLAGS::WRITABLE>()) {

    da_->getElementalCoords(da_->curr(), coords);

    for (int i = 0; i < npe_; i++) {
      convertOctToPhys(&coords[3 * i], m_problemSize_);
    }
    if (eleOrder == 1) {
      sync_.syncCoords<1, 3>(coords, taly_grid);
    } else if (eleOrder == 2) {
      sync_.syncCoords<2, 3>(coords, taly_grid);
    }

    if (m_inOutElementNODE[da_->curr()] == inOutInfo::INTERCEPTED_NODE) {
      int gp_out = 0;
      int maxGP;
      if (isVariableGP_ == VariableGP::RefineBySplitting) {
        std::vector<GPInfo> gaussPoints;
        // todo level
        performPartition(taly_grid, gaussPoints, maxRefineLevel_);

        /// in/out for splitted gauss points
        maxGP = gaussPoints.size();
        for (const auto &gp : gaussPoints) {
          if (!point_in_any_geometry(gp.positionGlobal)) {
            gp_out++;
          }
        }
      } else {
        TALYFEMLIB::FEMElm fe(taly_grid, TALYFEMLIB::BASIS_ALL);
        if (eleOrder == 1) {
          fe.refill(taly_grid->elm_array_[0], TALYFEMLIB::BASIS_LINEAR, isVariableGP_);
        } else if (eleOrder == 2) {
          fe.refill(taly_grid->elm_array_[0], TALYFEMLIB::BASIS_QUADRATIC, isVariableGP_);
        }
        maxGP = fe.n_itg_pts();
        while (fe.next_itg_pt()) {
          if (!point_in_any_geometry(fe.position())) {
            gp_out++;
          }
        }
      }


      /**
       * Check if this element is no_solve or solve
       * This is because some element will be no_solve before the splitting, but they will be solved after
       * the splitting, so we need to fix two vectors: m_inOutElementGP and shouldSolve
       */
      if (gp_out == 0) {
        /// totally inside geometry, do not solve for the element entirely
        setInOutElementGP(da_->curr(), inOutInfo::IN_GP);
      } else if (gp_out == maxGP) {
        /// totally outside geometry, solve
        setInOutElementGP(da_->curr(), inOutInfo::OUT_GP);
      } else {
        /// partially inside geometry, solve
        setInOutElementGP(da_->curr(), inOutInfo::INTERCEPTED_GP);
        if (m_triInOctant[da_->curr()].empty() and triangleCheck) {
          PrintWarning("Triangle not found for intercepting element (triangle size maybe too large) ", da_->curr());
          triangleCheck = false;
        }
      }
    } else if (m_inOutElementNODE[da_->curr()] == inOutInfo::IN_NODE) {
      setInOutElementGP(da_->curr(), inOutInfo::IN_GP);
    } else {
      setInOutElementGP(da_->curr(), inOutInfo::OUT_GP);
    }

  }

  if (not(setInteriorDirichlet_)) {
    /**
     * Use gauss point rather than nodal points to determine if the element needs to be solved or not.
     * This is because nodal points can determine an element to be intercepted but all the gauss points
     * are inside, thus causing the matrix to have a zero entry on diagonal.
     */
    for (da_->init<ot::DA_FLAGS::WRITABLE>();
         da_->curr() < da_->end<ot::DA_FLAGS::WRITABLE>(); da_->next<ot::DA_FLAGS::WRITABLE>()) {
      if (m_inOutElementGP[da_->curr()] == inOutInfo::INVALID) {
        /// check that every m_inOutElement is not invalid
        throw TALYFEMLIB::TALYException() << "m_inOutElementGP invalid!";
      } else if (not(m_inOutElementGP[da_->curr()] == inOutInfo::IN_GP)) {
        da_->getNodeIndices(localNodeIdx, da_->curr(), true);
        for (int i = 0; i < npe_; i++) {
          setShouldSolve(localNodeIdx[i], SOLVE);
        }
      }
    }
  }
  if (setInteriorDirichlet_) {
    for (da_->init<ot::DA_FLAGS::WRITABLE>();
         da_->curr() < da_->end<ot::DA_FLAGS::WRITABLE>(); da_->next<ot::DA_FLAGS::WRITABLE>()) {
      da_->getElementalCoords(da_->curr(),coords);
      if (m_inOutElementGP[da_->curr()] == inOutInfo::INVALID) {
        /// check that every m_inOutElement is not invalid
        throw TALYFEMLIB::TALYException() << "m_inOutElementGP invalid!";
      } else if ((m_inOutElementNODE[da_->curr()] == inOutInfo::OUT_NODE)) {
        da_->getNodeIndices(localNodeIdx, da_->curr(), true);
        for (int i = 0; i < npe_; i++) {
          setShouldSolve(localNodeIdx[i], SOLVE);
        }
      } else if ((m_inOutElementGP[da_->curr()] == inOutInfo::INTERCEPTED_GP)) {
        da_->getNodeIndices(localNodeIdx, da_->curr(), true);
        int counter = 0;
        for (DENDRITE_UINT iz = 0; iz < (eleOrder + 1); iz++) {
          for (DENDRITE_UINT iy = 0; iy < (eleOrder + 1); iy++) {
            for (DENDRITE_UINT ix = 0; ix < (eleOrder + 1); ix++) {
              counter++;
              if (not(da_->getMesh()->isNodeHanging(da_->curr(), ix, iy, iz))) {
                ZEROPTV nodeCoords
                    {coords[counter * m_uiDim + 0], coords[counter * m_uiDim + 1], coords[counter * m_uiDim + 2]};
                if (!(point_in_any_geometry(nodeCoords))) {
//                  setShouldSolve(localNodeIdx[counter], SOLVE);
                }
              }
            }
          }
        }
      }

    }

  }
//  std::vector<double> checkSolve;
//  da_->createVector(checkSolve,false, true,1);
//  for(int i = 0 ; i < checkSolve.size(); i++){
//    checkSolve[i] = shouldSolve[i];
//  }
//  double * localcheckSolve;
//  da_->ghostedNodalToNodalVec(checkSolve.data(),localcheckSolve, false,1);
//  da_->vecTopvtu(localcheckSolve,"checkSolve", nullptr,false, false,1,m_problemSize_);
//  delete[] localcheckSolve;
//  MPI_Barrier(MPI_COMM_WORLD);
//  exit(0);



  /**
   * Till now the should solve buffer is not synchronized . You must call the synchronize function
   * before applying the boundary condition.
   */

  delete[] coords;
  delete[] localNodeIdx;
}
template<typename NodeData>
const inOutInfo IBMSolver<NodeData>::getInOutElementGP(const int elemID) const {
  return m_inOutElementGP[elemID];
}

template<typename NodeData>
void IBMSolver<NodeData>::setInOutElementGP(const int elemID, inOutInfo inOutInfo) {
  m_inOutElementGP[elemID] = inOutInfo;
}

template<typename NodeData>
const inOutInfo IBMSolver<NodeData>::getInOutElementNODE(const int elemID) const {
  return m_inOutElementNODE[elemID];
}

template<typename NodeData>
const vector<triInfo> &IBMSolver<NodeData>::getTriInOctant(const int elemID) const {
  return m_triInOctant[elemID];
}

template<typename NodeData>
const vector<int> &IBMSolver<NodeData>::getShouldSolve() const {
  return shouldSolve;
}

template<typename NodeData>
void IBMSolver<NodeData>::setShouldSolve(int nodeIdx, SolveInfo solveInfo) {
  IBMSolver::shouldSolve[nodeIdx] = solveInfo;
}

template<typename NodeData>
void IBMSolver<NodeData>::syncShouldSolveBuffer() {
  int *should_solve_local;
  da_->ghostedNodalToNodalVec(shouldSolve.data(), should_solve_local, false, 1);

  int *should_solve_buff;
  da_->nodalVecToGhostedNodal(should_solve_local, should_solve_buff, false, 1);
  da_->readFromGhostBegin(should_solve_buff, 1);
  da_->readFromGhostEnd(should_solve_buff, 1);

  for (int i = 0; i < getShouldSolve().size(); i++) {
    if (should_solve_buff[i] == SOLVE) {
      setShouldSolve(i, SOLVE);
    }
  }

  da_->destroyVector(should_solve_local);
  da_->destroyVector(should_solve_buff);
}

template<typename NodeData>
void IBMSolver<NodeData>::performPartition(TALYFEMLIB::GRID *m_grid,
                                           std::vector<GPInfo> &m_gaussPoints,
                                           const DENDRITE_UINT maxRefineLevel) {

  std::queue<eleInfo> RefinedElement;
  const int npe = m_grid->n_nodes();

  ZEROPTV pos1/*{pos1.x(), pos1.y(), pos1.z()}*/;
  ZEROPTV pos2/*{pos2.x(), pos2.y(), pos2.z()}*/;
  m_grid->GetCoord(pos1, 0);
  m_grid->GetCoord(pos2, npe - 1);
  ZEROPTV h(pos2.x() - pos1.x(), pos2.y() - pos1.y(), pos2.z() - pos1.z());
  ZEROPTV root(pos1.x(), pos1.y(), pos1.z());
  eleInfo baseElement;
  baseElement.h = h;
  baseElement.pos = root;
  baseElement.level = 0;
  RefinedElement.push(baseElement);
  while (not(RefinedElement.empty())) {
    doSplitting(RefinedElement.front(), m_grid, maxRefineLevel, RefinedElement, m_gaussPoints);
    RefinedElement.pop();
  }

}

template<typename NodeData>
void IBMSolver<NodeData>::doSplitting(eleInfo &BaseElement, TALYFEMLIB::GRID *m_grid, int maxRefineLevel,
                                      std::queue<eleInfo> &RefinedElement, std::vector<GPInfo> &m_gaussPoints) {
  using namespace TALYFEMLIB;

  DENDRITE_UINT eleOrder = da_->getElementOrder();
  Point pos_(BaseElement.pos.x(), BaseElement.pos.y(), BaseElement.pos.z());
  Point h_(BaseElement.h.x(), BaseElement.h.y(), BaseElement.h.z());
  DENDRITE_UINT npe = da_->getNumNodesPerElement();
  double *coords = new double[npe * m_uiDim];
  build_taly_coordinates(coords, pos_, h_, eleOrder);
  if (eleOrder == 1) {
    sync_.syncCoords<1, 3>(coords, m_grid);
  } else if (eleOrder == 2) {
    sync_.syncCoords<2, 3>(coords, m_grid);
  }

  ZEROPTV elem_node;
  unsigned no_in_points = 0;

  for (int i = 0; i < m_grid->n_nodes(); i++) {
    m_grid->GetCoord(elem_node, i);
    if (this->point_in_any_geometry(elem_node)) {
      no_in_points++;
    }
  }
  bool completelyIn = false;
  bool completelyOut = false;
  if (no_in_points == 0) {
    completelyOut = true;
  } else if (no_in_points == m_grid->n_nodes()) {
    completelyIn = true;
  }

  if (not(completelyIn)) {

    if ((BaseElement.level == maxRefineLevel) or completelyOut) {
      /** Element is already at maximum refine level. No splitting required.
       *  Populate the Gauss point Vector
       * **/

      FEMElm fe(m_grid, TALYFEMLIB::BASIS_ALL);
      fe.refill(0, 0);
      ZEROPTV posl, posg;

      while (fe.next_itg_pt()) {
        posg = fe.position();
        // pass by copy
        GetLocalPtvTempFix(fe, posg, posl, 0, eleOrder);
        GPInfo gauss;
        gauss.positionLocal = posl;
        gauss.positionGlobal = posg;
        gauss.jaccXw = fe.detJxW();
        m_gaussPoints.push_back(gauss);
      }
    } else {
      /** Perform splitting **/
      std::vector<eleInfo> splitElement(8);

      for (auto &ele :splitElement) {
        ele.h = BaseElement.h * 0.5;
        ele.pos = BaseElement.pos;
        ele.level = BaseElement.level + 1;
      }
      /** Element 1 **/
      // Blank

      /** Element 2 **/
      splitElement[1].pos.x() = BaseElement.pos.x() + splitElement[1].h.x();

      /** Element 3 **/
      splitElement[2].pos.y() = BaseElement.pos.y() + splitElement[2].h.y();

      /** Element 4 **/
      splitElement[3].pos.x() = BaseElement.pos.x() + splitElement[3].h.x();
      splitElement[3].pos.y() = BaseElement.pos.y() + splitElement[3].h.y();

      /** Element 5 **/
      splitElement[4].pos.z() = BaseElement.pos.z() + splitElement[4].h.z();

      /** Element 6 **/
      splitElement[5].pos.x() = BaseElement.pos.x() + splitElement[5].h.x();
      splitElement[5].pos.z() = BaseElement.pos.z() + splitElement[5].h.z();

      /** Element 7 **/
      splitElement[6].pos.y() = BaseElement.pos.y() + splitElement[6].h.y();
      splitElement[6].pos.z() = BaseElement.pos.z() + splitElement[6].h.z();

      /** Element 8 **/
      splitElement[7].pos.x() = BaseElement.pos.x() + splitElement[7].h.x();
      splitElement[7].pos.y() = BaseElement.pos.y() + splitElement[7].h.y();
      splitElement[7].pos.z() = BaseElement.pos.z() + splitElement[7].h.z();

      for (auto ele :splitElement) {
        RefinedElement.push(ele);
      }

    }
  }

}

template<typename NodeData>
void IBMSolver<NodeData>::get_freshly_cleared(std::vector<localFreshlyOut> &localFreshlyOutwoNeighbour) {

//  std::vector<double> newNodalInout;
//  da_->createVector(newNodalInout, false, false, 1);
//  std::fill(newNodalInout.begin(), newNodalInout.end(), 0.0);
//  double *localOldNodalInOut;
//  da_->ghostedNodalToNodalVec(nodal_inout.data(), localOldNodalInOut, false, 1);

//  da_->vecTopvtu(localOldNodalInOut, "nodal_inout", nullptr, false, false, 1, m_problemSize_);
//  MPI_Barrier(da_->getCommActive());

  localFreshlyOutwoNeighbour.clear();
  PrintStatus(geometries_[0].geo->kI.center);

  const unsigned int eleOrder = da_->getElementOrder();
  double *coords = new double[npe_ * 3];
  DendroIntL *localNodeIdx = new DendroIntL[npe_];
  double nodalValues[npe_];

  // todo, you need to keep track of all the in_nodes from previous timestep also
  ZEROPTV elem_node;

  for (da_->init<ot::DA_FLAGS::LOCAL_ELEMENTS>();
       da_->curr() < da_->end<ot::DA_FLAGS::LOCAL_ELEMENTS>(); da_->next<ot::DA_FLAGS::LOCAL_ELEMENTS>()) {
    da_->getElementNodalValues(nodal_inout.data(), nodalValues, da_->curr(), 1);
    da_->getElementalCoords(da_->curr(), coords);
    da_->getNodeIndices(localNodeIdx, da_->curr(), true);

    for (int iz = 0; iz <= eleOrder; iz++) {
      for (int iy = 0; iy <= eleOrder; iy++) {
        for (int ix = 0; ix <= eleOrder; ix++) {
          int id = ix + iy * (eleOrder + 1) + iz * (eleOrder + 1) * (eleOrder + 1);

          if ((FEQUALS(nodalValues[id], inOutInfo::IN_NODE))) {
            convertOctToPhys(&coords[3 * id], m_problemSize_);
            elem_node = {coords[3 * id], coords[3 * id + 1], coords[3 * id + 2]};

            if (!point_in_any_geometry(elem_node) and localNodeIdx[id] >= 0) {
              bool ifhanging = da_->getMesh()->isNodeHanging(da_->curr(), ix, iy, iz);
              localFreshlyOutwoNeighbour.push_back(localFreshlyOut(da_->curr(),
                                                                   localNodeIdx[id],
                                                                   elem_node,
                                                                   ifhanging));
            }
          }
        }
      }
    }
  }

  // only sort based on localIndex
  sort(localFreshlyOutwoNeighbour.begin(), localFreshlyOutwoNeighbour.end(), compareBylocalNodeIdx);

  // combine all the same localIndex into one group
  std::vector<localFreshlyOut> out;

  ProcesslocalFreshlyOutNeighbour(localFreshlyOutwoNeighbour, out);
  localFreshlyOutwoNeighbour.clear();
  localFreshlyOutwoNeighbour = out;

  delete[] coords;
  delete[] localNodeIdx;

}

template<typename NodeData>
void IBMSolver<NodeData>::modify_freshly_cleared(std::vector<localFreshlyOut> &localin,
                                                 std::vector<VecInfo> &vec) {
  if (da_->isActive()) {
    DENDRITE_REAL **vec_buff_;
    vec_buff_ = new DENDRITE_REAL *[vec.size()];

    std::vector<VecInfo> solutionVec_;
    solutionVec_.resize(vec.size());

    int totaldof = 0;
    int eleOrder = da_->getElementOrder();

    for (int i = 0; i < vec.size(); i++) {
      totaldof += vec[i].ndof;
      solutionVec_[i].ndof = vec[i].ndof;
      solutionVec_[i].v = vec[i].v;
      solutionVec_[i].nodeDataIndex = vec[i].nodeDataIndex;
    }

    TalyMesh<TALYFEMLIB::NODEData> taly_mesh(eleOrder);
    TALYFEMLIB::GRID *taly_grid = &taly_mesh.grid;
    double *coords = new double[npe_ * 3];
    DendroIntL *localNodeIdx = new DendroIntL[npe_];

    for (int i = 0; i < solutionVec_.size(); i++) {
      VecGetArrayRead(solutionVec_[i].v, &solutionVec_[i].val);
      da_->nodalVecToGhostedNodal(solutionVec_[i].val, vec_buff_[i], false, solutionVec_[i].ndof);
      da_->readFromGhostBegin(vec_buff_[i], solutionVec_[i].ndof);
      da_->readFromGhostEnd(vec_buff_[i], solutionVec_[i].ndof);
    }

    for (auto &freshnode : localin) {
      // loop over elements
      for (const auto &elmid : freshnode.elmID) {
        da_->getElementalCoords(elmid, coords);
        for (int i = 0; i < npe_; i++) {
          convertOctToPhys(&coords[3 * i], m_problemSize_);
        }
        if (eleOrder == 1) {
          sync_.syncCoords<1, 3>(coords, taly_grid);
        } else if (eleOrder == 2) {
          sync_.syncCoords<2, 3>(coords, taly_grid);
        }
        da_->getNodeIndices(localNodeIdx, elmid, false);

        std::vector<ZEROPTV> elem_nodes(npe_);
        std::vector<double> distances(npe_);

        for (int id = 0; id < npe_; id++) {
          if (nodal_inout[localNodeIdx[id]] == inOutInfo::IN_NODE) {
            // use a larger enough distance to make sure that the IN node are excluded
            distances[id] = 100;
            elem_nodes[id] = ZEROPTV{-1, -1, -1};
          } else {
            ZEROPTV elem_node = {coords[3 * id], coords[3 * id + 1], coords[3 * id + 2]};
            distances[id] = (elem_node - freshnode.pos).norm();
            distances[id] = distances[id] < 1e-6 ? 100 : distances[id];
            elem_nodes[id] = elem_node;
          }

        }
        // get values
        std::vector<DENDRITE_REAL> values(totaldof * npe_);
        unsigned int offset = 0;
        for (int i = 0; i < solutionVec_.size(); i++) {
          da_->getElementNodalValues(vec_buff_[i], &values.data()[offset], elmid, solutionVec_[i].ndof);
          offset += solutionVec_[i].ndof * npe_;
        }
        // append values and coordinates into node,

        for (int i = 0; i < distances.size(); i++) {
          neighbour nei;
          nei.neighbour_coord = elem_nodes[i];
          nei.distance = distances[i];
          nei.neighbour_value.resize(totaldof);

          for (int dof = 0; dof < totaldof; dof++) {
            nei.neighbour_value[dof] = values[dof * npe_ + i];
          }
          freshnode.nei.push_back(nei);
        }
      }
    }

    ProcesslocalFreshlyOutData(localin);
  }
};

template<typename NodeData>
void IBMSolver<NodeData>::compute_freshly_cleared(std::vector<localFreshlyOut> &localin,
                                                  std::vector<VecInfo> &vec) {
  if (da_->isActive()) {
    int offset = 0;
    for (auto &v : vec) {
      // get the row index and value to later change the value in Petsc vector
      double *array;
      double *ghostedArray;
      VecGetArray(v.v, &array);
      int ndof = v.ndof;
      da_->nodalVecToGhostedNodal(array, ghostedArray, false, ndof);
      da_->readFromGhostBegin(ghostedArray, ndof);
      da_->readFromGhostEnd(ghostedArray, ndof);
      Vec out;
      da_->petscCreateVector(out, false, true, ndof);
      {
        double *tempArray;
        VecGetArray(out, &tempArray);
        std::memcpy(tempArray, ghostedArray, da_->getTotalNodalSz() * sizeof(double) * ndof);
        VecRestoreArray(out, &tempArray);
      }
      delete[] ghostedArray;

      da_->petscChangeVecToMatBased(out, false, true, ndof);
      VecGetArray(out, &ghostedArray);

      for (const auto &freshnode : localin) {
        std::vector<DENDRITE_REAL> value;
        interpolate(freshnode, value, ndof, offset);
        for (int dof = 0; dof < ndof; dof++) {
          PetscInt row = ndof * freshnode.localNodeIdx + dof;
          ghostedArray[row] = value[dof];
        }
      }
      VecRestoreArray(out, &ghostedArray);
      da_->petscChangeVecToMatFree(out, false, true, ndof);
      {
        VecGetArray(out, &ghostedArray);
        da_->ghostedNodalToNodalVec(ghostedArray, array, true, ndof);
        VecRestoreArray(out, &ghostedArray);
      }
      VecRestoreArray(v.v, &array);
      offset += ndof;
    }
  }
}

template<typename NodeData>
void IBMSolver<NodeData>::interpolate(const localFreshlyOut &lcl,
                                      std::vector<DENDRITE_REAL> &value,
                                      const int dof,
                                      const int offset) {

  bool anyneighbour_fluid = false;
  bool anyneighbour_solid = false;

  value.clear();
  value.resize(dof);
  // Get the fresh node coor
  ZEROPTV loc_coor = lcl.pos;
  auto neighbour = lcl.nei;

  // Set the value array of U/d
  ZEROARRAY<double> A_f, A_s;
  A_f.redim(dof);
  A_f.fill(0.0);
  A_s.redim(dof);
  A_s.fill(0.0);
  // Set the value of 1/d
  double B_f = 0.0, B_s = 0.0;

  // loop over all the neighbor nodes
  for (const auto &nei : neighbour) {
    anyneighbour_fluid = true;
    double df = (nei.neighbour_coord - loc_coor).norm();
    /// add U/d in fluid domain
    for (int i = 0; i < dof; i++) {
      A_f(i) += nei.neighbour_value[i + offset] / df;
    }
    /// add 1/d in fluid domain
    B_f += 1.0 / df;
  }


  /// pull from all geometries
  int max_try = 5;
  int try_no = 0;
  while (not(anyneighbour_solid) and try_no <= max_try) {
    try_no++;
    for (const auto &geom_data : this->geometries_) {
      // todo use maximum length of the mesh, now it uses the x-direction length
      const double delta_h = m_problemSize_[0] / pow(2, geom_data.geo->def_.refine_lvl);
      const double range = delta_h * sqrt(MAX_DIM) * static_cast<double>(try_no);

      for (int i = 0; i < geom_data.geo->surface_mesh.Nnode(); i++) {
        /// calc distance of solid surface point
        double ds = (geom_data.geo->surface_mesh.node_array[i].position - loc_coor).norm();

        /// check if the surface point is in the range of interpolation
        if (ds < range && ds > 1e-12) {
          anyneighbour_solid = true;
          for (int j = 0; j < dof; j++) {
            /// add U/d for surface point
            A_s(j) += geom_data.geo->surface_mesh.node_array[i].data.u[j + offset] / ds;
          }
          /// add 1/d for surface point
          B_s += 1.0 / ds;
        }
      }
    }
  }


  /// compute interpolated value
  for (int i = 0; i < dof; i++) {
    value[i] = (A_f(i) + A_s(i)) / (B_f + B_s);
    if (std::isnan(value[i])) {
      throw TALYFEMLIB::TALYException() << "Nan error for freshly out nodes!";
    }
  }
}
template<typename NodeData>
void IBMSolver<NodeData>::computeSurfaceValue(VecInfo _in, const int index, const int offset) {

  TALYFEMLIB::ZEROPTV ptvl; /// Local point in isoparameteric space
  TALYFEMLIB::ZEROPTV ptvg;
  const int eleOrder = da_->getElementOrder();
  if (geometries_.empty()) {
    return;
  }

  TalyMesh<TALYFEMLIB::NODEData> taly_mesh(eleOrder);
  TALYFEMLIB::GRID *taly_grid = &taly_mesh.grid;
  PetscScalar *ghostedArray;
  const PetscScalar *vArray;
  if (da_->isActive()) {
    VecGetArrayRead(_in.v, &vArray);
    da_->nodalVecToGhostedNodal(vArray, ghostedArray, false, _in.ndof);
    da_->readFromGhostBegin(ghostedArray, _in.ndof);
    da_->readFromGhostEnd(ghostedArray, _in.ndof);
  }
  double *coords = new double[m_uiDim * npe_];
  double *localValues = new double[npe_ * _in.ndof];
  double *localValuesTaly = new double[npe_ * _in.ndof];
  double *valueFEM = new double[_in.ndof];

  /** Looping over the geometries**/
  for (const auto &geom_data : geometries_) {
    std::vector<surfaceExchangeData> localData;
    for (int i = 0; i < geom_data.my_surface_points.size(); i++) {
      if (da_->isActive()) {
        ptvg = {geom_data.my_surface_points[i].values[0],
                geom_data.my_surface_points[i].values[1],
                geom_data.my_surface_points[i].values[2]};
        int backGroundElementID = geom_data.backgroundElementSP[i];

        da_->getElementalCoords(backGroundElementID, coords);
        for (int n = 0; n < npe_; n++) {
          convertOctToPhys(&coords[3 * n], m_problemSize_);
        }
        da_->getElementNodalValues(ghostedArray, localValues, backGroundElementID, _in.ndof);
        if (eleOrder == 1) {
          sync_.syncCoords<1, m_uiDim>(coords, taly_grid);
          sync_.DendroToTalyIn<1, m_uiDim>(localValues, localValuesTaly, _in.ndof);
        } else if (eleOrder == 2) {
          sync_.syncCoords<2, m_uiDim>(coords, taly_grid);
          sync_.DendroToTalyIn<2, m_uiDim>(localValues, localValuesTaly, _in.ndof);
        }

        /// set interpolation point
        FEMElm fe(taly_grid, TALYFEMLIB::BASIS_ALL);

        if (da_->getElementOrder() == 1) {
          fe.refill(taly_grid->elm_array_[0], TALYFEMLIB::BASIS_LINEAR, 0);
        } else if (da_->getElementOrder() == 2) {
          fe.refill(taly_grid->elm_array_[0], TALYFEMLIB::BASIS_QUADRATIC, 0);
        }

        GetLocalPtvTempFix(fe, ptvg, ptvl, 0, eleOrder);
        fe.calc_at(ptvl);
        /// get computed value
        calcValueFEM(fe, _in.ndof, localValuesTaly, valueFEM);
        surfaceExchangeData surfaceLocalData;
        surfaceLocalData.globalID = geom_data.my_surface_points[i].label;
        surfaceLocalData.values = valueFEM[index];

        localData.push_back(surfaceLocalData);
      }
    }
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Datatype surfaceMPIData;
    MPI_Type_contiguous(2, MPI_DOUBLE, &surfaceMPIData);
    MPI_Type_commit(&surfaceMPIData);
    int rank;
    int npes;
    rank = TALYFEMLIB::GetMPIRank(); // get the active ranks
    npes = TALYFEMLIB::GetMPISize(); // get the active processes

    /// find out the number of elements which will be sent to procs
    int numData = localData.size();
    std::vector<int> localEachProcSizeSurfaceNodesSize;
    localEachProcSizeSurfaceNodesSize.resize(npes);


    /// Get the sizes from each process to zeroeth process
    MPI_Allgather(&numData, 1, MPI_INT,
                  localEachProcSizeSurfaceNodesSize.data(),
                  1, MPI_INT, MPI_COMM_WORLD);
    /// Add everything up
    PetscInt totalGlobalSize =
        std::accumulate(localEachProcSizeSurfaceNodesSize.begin(), localEachProcSizeSurfaceNodesSize.end(), 0);

    /// Allocate a global chunk of memory for MPI to push into
    std::vector<surfaceExchangeData> globalSurfaceData;
    globalSurfaceData.resize(totalGlobalSize);
    std::vector<int> displacement(npes, 0);
    for (int i = 1; i < displacement.size(); i++) {
      displacement[i] = displacement[i - 1] + localEachProcSizeSurfaceNodesSize[i - 1];
    }

    MPI_Allgatherv(localData.data(), localData.size(), surfaceMPIData,
                   globalSurfaceData.data(), localEachProcSizeSurfaceNodesSize.data(),
                   displacement.data(), surfaceMPIData, MPI_COMM_WORLD);

    for (int i = 0; i < globalSurfaceData.size(); i++) {
      int label = globalSurfaceData[i].globalID;
      geom_data.geo->surface_mesh.node_array[label].data.u[index + offset] = globalSurfaceData[i].values;
    }

  }

  delete[]coords;
  delete[]localValues;
  delete[]localValuesTaly;
  delete[]valueFEM;

  if (da_->isActive()) {
    delete[]ghostedArray;
    VecRestoreArrayRead(_in.v, &vArray);
  }

}

void check_get_freshly_cleared(ot::DA *octDA,
                               const std::vector<localFreshlyOut> &localCleared,
                               const double scalingFactor[3]) {
  PrintStatus("Check for getting freshly_cleared, write to a file");
  std::vector<double> nodeCheck;
  octDA->createVector(nodeCheck, false, false, 1);
  std::fill(nodeCheck.begin(), nodeCheck.end(), 0);
  DendroIntL *localNodeIdx = new DendroIntL[8];

  for (octDA->init<ot::DA_FLAGS::LOCAL_ELEMENTS>();
       octDA->curr() < octDA->end<ot::DA_FLAGS::LOCAL_ELEMENTS>(); octDA->next<ot::DA_FLAGS::LOCAL_ELEMENTS>()) {
    octDA->getNodeIndices(localNodeIdx, octDA->curr(), false);
    for (const auto &lcl : localCleared) {
      for (int j = 0; j < 8; j++) {
        if (lcl.localNodeIdx == localNodeIdx[j] && nodeCheck[localNodeIdx[j]] != 1) {
          nodeCheck[localNodeIdx[j]] = 1;
        }
      }
    }
  }
  octDA->vecTopvtu(nodeCheck.data(), "freshly", nullptr, false, false, 1, scalingFactor);
  MPI_Barrier(MPI_COMM_WORLD);
}





//void checkNodalinout{
//    double * inOutlocalVec;
//    da_->ghostedNodalToNodalVec(nodal_inout.data(),inOutlocalVec, false,1);
//    da_->vecTopvtu(inOutlocalVec, "nodal_inout", nullptr, false, false, 1, m_problemSize_);
//    delete [] inOutlocalVec;
//    MPI_Barrier(da_->getCommActive());
//};
#endif //DENDRITE2_0_IBMSOLVER_H