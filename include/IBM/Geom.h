//
// Created by maksbh on 6/10/19.
//// This is  the class originally developed by Boshun .
//

#pragma once

/**
 * @author maksbh
 * Changes:
 * 1. Class templated to handle any NodeData.
 * 2. struct NodeData renamed to NodePositionData as it stores both position and values.
 * 3. temperature renamed to dirichlet
 * 4. Constructor of Geom class takes Geomdef and nsd insted of complete input data.
 */


//#include <dendrite/PetscLogging.h>  // for events

#include <talyfem/data_structures/zeroarray.h>
#include <talyfem/data_structures/zeromatrix.h>
#include <talyfem/file_io/tecplot_ascii.h>
#include <talyfem/file_io/gmsh_io.h>
#include <raytracer/ray_tracer.h>
#include <IBM/GeomDef.h>
#include "SixDof.h"

using TALYFEMLIB::ZEROARRAY;
using TALYFEMLIB::ZeroMatrix;
using TALYFEMLIB::TecplotReaderASCII;
using TALYFEMLIB::TecplotHeader;
using TALYFEMLIB::TecplotZone;
using TALYFEMLIB::PrintStatus;

using namespace std;
using namespace TALYFEMLIB;

template<typename NodeData>
class Geom {
 public:
  /// Gauss points info
  struct GaussPointInfo {
    std::vector<ZEROPTV> tri_nodes;
    std::vector<ZEROPTV> gp_position;
    ZEROPTV gp_normal;
    double area = 0.0;
    ZEROPTV gp_velocity;
    int label = 0;
  };

  /// Surface Mesh info
  struct SurfaceMesh {
    RayTracer Raytracer;
    struct NodePositionData {
      ZEROPTV position;
      NodeData data;
    };
    /// Stores the node data
    std::vector<NodePositionData> node_array;

    inline size_t Nnode() const {
      return node_array.size();
    }

    struct ElemData {
      std::vector<int> connectivity{3};
      double area = -1.0;
      ZEROPTV normal;
      int gp_normal_flip = 0;
    };
    /// Stores the element data
    std::vector<ElemData> elem_array;

    inline size_t Nelem() const {
      return elem_array.size();
    }

  };

  /// Kinematic info
  struct KinematicInfo {
    ZEROPTV center;
    ZEROPTV vel;                               ///< linear velocity
    ZEROPTV vel_ang;                           ///< angular velocity
    ZEROPTV LinearDisplacementUpdate;          ///< linear displacement
    std::vector<ZEROPTV> RotationMatrixUpdate; ///< angular displacement
    KinematicInfo() {
      RotationMatrixUpdate.resize(3);
      for (int dim = 0; dim < 3; dim++) {
        RotationMatrixUpdate[dim](dim) = 1.0;
      }
    }
  };

  /// Physical info (density, volume, mass, internia...)
  struct PhysicalInfo {
    double rho = 1.0;
    double volume = 0.0;
    double mass = 0.0;
    ZEROPTV center_of_mass;
    std::vector<ZEROPTV> moment_of_inertia;
    PhysicalInfo() {
      moment_of_inertia.resize(3);
    };
  };

  SurfaceMesh surface_mesh;
  KinematicInfo kI;
  PhysicalInfo pI;
  SixDof sixDof_;

  ///< nsd of background mesh
  unsigned int nsd;
  ///< number of gauss points per surface element
  unsigned int gpno;
  ///< number of nodes per element in surface mesh
  unsigned int nodeno;

  GeomDef def_;

  Geom(const GeomDef &def, const int num_dim = 3) {
    nsd = num_dim;
    nodeno = nsd;
    def_ = def;
    if (def_.gp_level == 0) {
      gpno = 3;
    } else if (def_.gp_level == 1) {
      gpno = 12;
    } else {
      throw TALYFEMLIB::TALYException() << "Recursive not implemted";
    }
    pI.rho = def_.rho;
    /// load geometries
    switch (def_.fileformat) {
      case GeomDef::STL:loadGeom_stl(def_.mesh_path.c_str());
        break;
      case GeomDef::GMSH:loadGeom_gmsh(def_.mesh_path.c_str());
        break;
      default:throw TALYFEMLIB::TALYException() << "Unknown mesh type";
    }

    updateSurfaceMeshLocation(def_.InitialDisplacement, kI.RotationMatrixUpdate, kI.center);
    calcNormals();
    calcPhysicalInfo(&kI, &surface_mesh);


    sixDof_.kp_.mass = pI.mass;
    for (int row = 0; row < 3; row++) {
      for (int col = 0; col < 3; col++) {
        sixDof_.kp_.MOI[row](col) = pI.moment_of_inertia[row](col);
      }
    }
    sixDof_.kp_.InverseMOI = inverse_matrix(sixDof_.kp_.MOI);
    sixDof_.ks_.back().x = pI.center_of_mass;

    sixDof_.ks_.back().P = kI.vel * sixDof_.kp_.mass;
    sixDof_.ks_.back().L = mat_vec_multi(sixDof_.kp_.MOI, kI.vel_ang);
    updateNodeVel();

    checkLoad();

  }

  ~Geom() = default;

  /**
   * load gmsh-msh file, fills in the surface_mesh object of the geo
   * @param fileName
   */
  void loadGeom_gmsh(const char *fileName) {
//    PetscLogEventBegin(geoLoadEvent, 0, 0, 0, 0);
    PrintStatus("Loading ", fileName);
    gmsh::Reader r;
    r.open(fileName);

    surface_mesh.node_array.resize(r.n_nodes());
    /// create nodes
    for (int gmshNodeID = 0; gmshNodeID < r.n_nodes(); gmshNodeID++) {
      const gmsh::Node node = r.read_node();
      for (int dim = 0; dim < nsd; dim++) {
        surface_mesh.node_array[gmshNodeID].position(dim) = node.coords[dim];
      }
    }

    /// read only triangle elements
    for (int gmshElmID = 0; gmshElmID < r.n_elements(); gmshElmID++) {
      gmsh::Element gmshElem = r.read_element();
      /// Only read in triangle elements
      if (gmshElem.connectivity.size() == 3) {
        /// create an ElemData to push back to surface_mesh.elem_array
        typename SurfaceMesh::ElemData elem;
        elem.connectivity.resize(3);
        for (int j = 0; j < nodeno; j++) {
          elem.connectivity.at(j) = gmshElem.connectivity.at(j) - 1;
        }
        surface_mesh.elem_array.push_back(elem);
      }
    }

    /// Create an ray-tracer object from msh file.
    std::vector<stl::Triangle> triangles;
    for (int elmID = 0; elmID < surface_mesh.Nelem(); elmID++) {
      Vector3d v1;
      Vector3d v2;
      Vector3d v3;
      for (int dim = 0; dim < nsd; dim++) {
        v1.data[dim] = surface_mesh.node_array[surface_mesh.elem_array[elmID].connectivity[0]].position[dim];
        v2.data[dim] = surface_mesh.node_array[surface_mesh.elem_array[elmID].connectivity[1]].position[dim];
        v3.data[dim] = surface_mesh.node_array[surface_mesh.elem_array[elmID].connectivity[2]].position[dim];
      }
      triangles.push_back(stl::Triangle(v1, v2, v3));
    }
    surface_mesh.Raytracer.setTriangles(triangles);

    /// calling calcNormalDirectionMpi to calculate whether the normal should be flipped or not,
    /// this is the most numerical intensity part of the loading process
    double stime = MPI_Wtime();
    std::vector<int> gp_normal_flip_global = calcNormalDirectionMpi();
    double etime = MPI_Wtime();
    PrintStatus("Calculating normal_flip using ray_tracer: ", etime - stime);

    /// set normal and area accordingly
    assert(surface_mesh.Nelem() == gp_normal_flip_global.size());
    for (int elmID = 0; elmID < surface_mesh.Nelem(); elmID++) {
      surface_mesh.elem_array[elmID].gp_normal_flip = gp_normal_flip_global[elmID];
      /// fill in elm->normal, elm->gp_normal_flip, and elm->area
      calculate_normal_and_area(&surface_mesh.elem_array[elmID],
                                surface_mesh.node_array.data(),
                                &surface_mesh.Raytracer);
    }
//    PetscLogEventEnd(geoLoadEvent, 0, 0, 0, 0);
  }

  /**
   * loads surface mesh from stl file, fills in the surface_mesh and raytracer object of the geo
   * @param fileName
   */
  void loadGeom_stl(const char *fileName) {
    PrintStatus("Loading ", fileName);
    auto triangles = stl::parse_stl(fileName).triangles;
    surface_mesh.Raytracer.setTriangles(triangles);

    /// initialize node array
    surface_mesh.node_array.resize(3 * triangles.size());
    /// initialize element connectivity matrix
    surface_mesh.elem_array.resize(triangles.size());

    double stime = MPI_Wtime();
    /// each triangle's 3 vertices
    for (int nodeID = 0; nodeID < surface_mesh.Nnode(); nodeID++) {
      /// 3 for (x, y, z), only support 3D coordinates
      for (int dim = 0; dim < nsd; dim++) {
        surface_mesh.node_array[nodeID].position(dim) = triangles[nodeID / 3].v[nodeID % 3].data[dim];
      }
    }
    double etime = MPI_Wtime();
    PrintStatus("Loading nodes: ", etime - stime);

    stime = MPI_Wtime();
    /// read connectivity (0:2, 3:5, ...)
    for (int elmID = 0; elmID < surface_mesh.Nelem(); elmID++) {
      /// each triangle's 3 vertices
      surface_mesh.elem_array.at(elmID).connectivity.resize(3);
      for (int vertex = 0; vertex < 3; vertex++) {
        surface_mesh.elem_array[elmID].connectivity[vertex] = elmID * 3 + vertex;
      }
    }
    etime = MPI_Wtime();
    PrintStatus("Loading elements: ", etime - stime);

  }

  void calcNormals() {
    if (def_.type == GeomDef::MESHOBJECT ||
        def_.type == GeomDef::SPHERE ||
        def_.type == GeomDef::CYLINDER ||
        def_.type == GeomDef::CUBE) {
      /// calling calcNormalDirectionMpi to calculate whether the normal should be flipped or not,
      /// this is the most numerically intensive part of the loading process
      double stime = MPI_Wtime();
      std::vector<int> gp_normal_flip_global = calcNormalDirectionMpi();
      double etime = MPI_Wtime();
      PrintStatus("Calculating normal_flip using ray_tracer: ", etime - stime);

      stime = MPI_Wtime();
      assert(surface_mesh.Nelem() == gp_normal_flip_global.size());
      for (int elmID = 0; elmID < surface_mesh.Nelem(); elmID++) {
        surface_mesh.elem_array[elmID].gp_normal_flip = gp_normal_flip_global[elmID];
        /// fill in elm->normal, elm->gp_normal_flip, and elm->area
        calculate_normal_and_area(&surface_mesh.elem_array[elmID],
                                  surface_mesh.node_array.data(),
                                  &surface_mesh.Raytracer);
      }
      etime = MPI_Wtime();
      PrintStatus("Calculating normal and area: ", etime - stime);
    } else if (def_.type == GeomDef::MESHOBJECT_2D) {
      /// set gp_flip = 1
      std::vector<int> gp_normal_flip_global(surface_mesh.Nelem(), 1);
      assert(surface_mesh.Nelem() == gp_normal_flip_global.size());
      for (int elmID = 0; elmID < surface_mesh.Nelem(); elmID++) {
        surface_mesh.elem_array[elmID].gp_normal_flip = gp_normal_flip_global[elmID];
        /// fill in elm->normal, elm->gp_normal_flip, and elm->area
        calculate_normal_and_area(&surface_mesh.elem_array[elmID],
                                  surface_mesh.node_array.data(),
                                  &surface_mesh.Raytracer);
      }
    }
  }

  void checkLoad() {
    const double eps = 1e-2;
    /// check the normal using calculated center of mass
    if (def_.type == GeomDef::SPHERE || def_.type == GeomDef::CYLINDER || def_.type == GeomDef::CUBE) {
      for (int elmID = 0; elmID < surface_mesh.Nelem(); elmID++) {
        ZEROPTV normal = surface_mesh.elem_array[elmID].normal;
        ZEROPTV center_of_tri = surface_mesh.node_array[surface_mesh.elem_array[elmID].connectivity[0]].position;
        ZEROPTV test_dir = center_of_tri - pI.center_of_mass;
        if (normal.innerProduct(test_dir) > 0) {
          throw TALYFEMLIB::TALYException() << "surface normal pointing outward! Wrong";
        }
      }
    }

    /// for sphere, check if the radius is correct
    if (def_.type == GeomDef::SPHERE) {
      int elmID = 0;
      ZEROPTV center_of_tri = surface_mesh.node_array[surface_mesh.elem_array[elmID].connectivity[0]].position;
      if (fabs(pI.center_of_mass.distanceTo(center_of_tri) - def_.radius) > eps) {
        PrintStatus("Sphere ",
                    def_.name,
                    " radius input: ",
                    def_.radius,
                    " real radius: ",
                    pI.center_of_mass.distanceTo(center_of_tri));
        throw TALYFEMLIB::TALYException();
      }
    }

    /// for cylinder, check if the radius, height (by volume) and bottom_corner is correct
    if (def_.type == GeomDef::CYLINDER) {
      if (fabs(1.0 - pI.volume / (M_PI * def_.radius * def_.radius * def_.height)) > eps) {
        PrintStatus("Cylinder ", def_.name, " volume not matching well enough (check radius and height)");
        throw TALYFEMLIB::TALYException();
      }
      for (int i = 0; i < 3; ++i) {
        if (i != def_.cylinderDir) {
          if (fabs(def_.bottom_center[i] - pI.center_of_mass[i]) > eps) {
            PrintStatus("Cylinder ", def_.name, " center of mass not matching in the ", i, " th direction");
            throw TALYFEMLIB::TALYException();
          }
        } else {
          if (fabs(def_.bottom_center[def_.cylinderDir] + def_.height / 2 - pI.center_of_mass[def_.cylinderDir])
              > eps) {
            PrintStatus("Cylinder ", def_.name, " center of mass not matching in the ", i, " th direction");
            throw TALYFEMLIB::TALYException();
          }
        }
      }
    }

    /// for cube, check if the volume, bottom corner are correct
    if (def_.type == GeomDef::CUBE) {
      if (fabs(1.0 - pI.volume / (def_.cube_dim[1].x() * def_.cube_dim[1].y() * def_.cube_dim[1].z())) > eps) {
        PrintStatus("Cube ", def_.name, " volume not matching well enough (check dimension vector)");
        throw TALYFEMLIB::TALYException();
      }
      ZEROPTV calc_com = def_.cube_dim[0] + def_.cube_dim[1] * 0.5;
      if (fabs(calc_com.distanceTo(pI.center_of_mass)) > eps) {
        PrintStatus("Cube ", def_.name, " center of mass not matching well enough (check corner vector)");
        throw TALYFEMLIB::TALYException();
      }
    }
  }

  /**
   * Move the surface mesh and the corresponding raytracer object (if exists)
   * @param LinearDisplacement
   * @param RotMat
   * @param center
   */
  void updateSurfaceMeshLocation(const ZEROPTV &LinearDisplacement,
                                 const std::vector<ZEROPTV> &RotMat,
                                 const ZEROPTV &center) {
    /// move the surface_mesh, the p/// first rotate than translate
    assert(RotMat.size() == 3);
    // rotate
    if (def_.if_rotation) {
      for (auto &node : surface_mesh.node_array) {
        ZEROPTV newLocation;
        for (int dim = 0; dim < nsd; dim++) {
          for (int k = 0; k < nsd; k++) {
            newLocation(dim) += (node.position(k) - center(k)) * RotMat[dim](k);
          }
        }
        node.position = newLocation + center;
      }
    }

    // translate
    for (auto &node : surface_mesh.node_array) {
      node.position += LinearDisplacement;
    }

    // move the raytracer if exist
    if (!surface_mesh.Raytracer.triangles().empty()) {
      /// move the raytracer if exists
      std::vector<RayTracer::TriangleData> triangles = surface_mesh.Raytracer.triangles();
      for (auto &tri : triangles) {
        // rotate
        if (def_.if_rotation) {
          for (int i = 0; i < 3; i++) {
            ZEROPTV old_vertex_coor = {tri.v[i].data[0], tri.v[i].data[1], tri.v[i].data[2]};
            ZEROPTV newLocation;
            for (int dim = 0; dim < nsd; dim++) {
              for (int k = 0; k < nsd; k++) {
                newLocation(dim) += (old_vertex_coor(k) - center(k)) * RotMat[dim](k);
              }
            }
            ZEROPTV new_vertex_coor = newLocation + center;
            for (int dim = 0; dim < 3; dim++) {
              tri.v[i].data[dim] = new_vertex_coor(dim);
            }
          }
        }
        // translate
        for (int i = 0; i < 3; i++) {
          for (int dim = 0; dim < 3; dim++) {
            tri.v[i].data[dim] += LinearDisplacement(dim);
          }
        }
      }
      surface_mesh.Raytracer.setTriangles(std::move(triangles));

      /// assert that the first node matches
      assert(fabs(surface_mesh.node_array[surface_mesh.elem_array[0].connectivity[0]].position[0]
                      - surface_mesh.Raytracer.triangles().data()->v->data[0])
                 < 1e-10);
      assert(fabs(surface_mesh.node_array[surface_mesh.elem_array[0].connectivity[0]].position[1]
                      - surface_mesh.Raytracer.triangles().data()->v->data[1])
                 < 1e-10);
      assert(fabs(surface_mesh.node_array[surface_mesh.elem_array[0].connectivity[0]].position[2]
                      - surface_mesh.Raytracer.triangles().data()->v->data[2])
                 < 1e-10);
    }
  }


  /**
   * update the surface_mesh node velocity
   */
  void updateNodeVel() {
    /// vel = vel_ang x (node_pos - center) + vel_linear
    if (def_.if_rotation) {
      for (int nodeID = 0; nodeID < surface_mesh.Nnode(); nodeID++) {
        for (int dim = 0; dim < nsd; dim++) {
          surface_mesh.node_array[nodeID].data.u[dim] = (kI.vel_ang((dim + 1) % nsd)
              * (surface_mesh.node_array[nodeID].position[(dim + 2) % nsd] - kI.center((dim + 2) % nsd))
              - kI.vel_ang((dim + 2) % nsd)
                  * (surface_mesh.node_array[nodeID].position[(dim + 1) % nsd]
                      - kI.center((dim + 1) % nsd)))
              + kI.vel(dim);
        }
      }
    } else {
      for (int nodeID = 0; nodeID < surface_mesh.Nnode(); nodeID++) {
        for (int dim = 0; dim < nsd; dim++) {
          surface_mesh.node_array[nodeID].data.u[dim] = kI.vel(dim);
        }
      }
    }

  }



  /**
   * return true if point is inside geometry, return false if point is outside
   * @param coor
   * @return
   */
  bool point_in_geometry(const ZEROPTV &coor) const {
    return !in_out(coor);
  }

  /**
   * return the calculated information of an element, this is the general function that can handle all the geometries
   * @param[in] elmID
   * @param[out] out, struct conatins: position of the gauss point, velocity of the gauss point, area and normal of the element
   * */
  GaussPointInfo gpCoor_len_normal(const int elmID, ZEROPTV thisGPPosition = ZEROPTV({0.0, 0.0, 0.0})) {

    assert (elmID >= 0 && elmID < surface_mesh.Nelem());

    /// pts is the points of the surface mesh, it has three points, each points have three coordinates.
    GaussPointInfo out;
    out.gp_position.resize(gpno);
    out.tri_nodes.resize(3);
    /// elem_node contains the nodes for this element
    const std::vector<int> &connectivity = surface_mesh.elem_array[elmID].connectivity;

    /// copy the coordinates into the tri_nodes vector
    for (int i = 0; i < nodeno; i++) {
      out.tri_nodes[i] = surface_mesh.node_array[connectivity[i]].position;
    }

    // TODO - hardcoded for 3D triangle, 1 gp per side case
    if (gpno == 3) {
      for (int node_id = 0; node_id < nodeno; node_id++) {
          /// take the midpoint of each side of the triangle as the gauss point
          out.gp_position[node_id] = (out.tri_nodes[node_id] + out.tri_nodes[(node_id + 1) % nodeno]) * 0.5;
      }
    } else if (gpno == 12) {
      /// split the triangle into 4 and take the middle point of each smaller triangle
      std::vector<ZEROPTV> total_nodes(6);
      for (int node_id = 0; node_id < nodeno; node_id++) {
        total_nodes[node_id] = out.tri_nodes[node_id];
      }
      for (int node_id = 0; node_id < nodeno; node_id++) {
        total_nodes[node_id + nodeno] = (out.tri_nodes[node_id] + out.tri_nodes[(node_id + 1) % nodeno]) * 0.5;
      }
      for (int t = 0; t < 4; t++) {
        /// one smaller triangle
        std::vector<ZEROPTV> tri_nodes(3);
        if (t == 0) {
          tri_nodes = {total_nodes[1], total_nodes[3], total_nodes[4]};
        } else if (t == 1) {
          tri_nodes = {total_nodes[2], total_nodes[4], total_nodes[5]};
        } else if (t == 2) {
          tri_nodes = {total_nodes[0], total_nodes[3], total_nodes[5]};
        } else if (t == 3) {
          tri_nodes = {total_nodes[3], total_nodes[4], total_nodes[5]};
        }
        for (int node_id = 0; node_id < nodeno; node_id++) {
          /// take the midpoint of each side of the triangle as the gauss point
          out.gp_position[nodeno * t + node_id] = (tri_nodes[node_id] + tri_nodes[(node_id + 1) % nodeno]) * 0.5;
        }
      }
    }

    /// the velocity for rotation is location specific, therefore the gp_current_location is used.
    /// otherwise the gp_velocity is always equal to the velocity of the object.
//    if (def_.if_rotation) {
//      for (int dim = 0; dim < nsd; dim++) {
//        /// calculate the velocity of gauss point using the gp_current_position argument
//        out.gp_velocity(dim) = (kI.vel_ang((dim + 1) % nsd)
//            * (out.gp_position[(dim + 2) % nsd] - kI.center((dim + 2) % nsd))
//            - kI.vel_ang((dim + 2) % nsd)
//                * (out.gp_position[(dim + 1) % nsd] - kI.center((dim + 1) % nsd)))
//            + kI.vel(dim);
//      }
//    } else {
    out.gp_velocity = kI.vel;
//  }
    if (def_.move_type == GeomDef::PrescribedMov::ROTATION) {
      ZEROPTV vel_ang = def_.rotation_axis * def_.rotation_speed;
      for (int dim = 0; dim < nsd; dim++) {
        /// calculate the velocity of gauss point using the gp_current_position argument
        out.gp_velocity(dim) = (vel_ang((dim + 1) % nsd)
            * (thisGPPosition[(dim + 2) % nsd] - kI.center((dim + 2) % nsd))
            - vel_ang((dim + 2) % nsd)
                * (thisGPPosition[(dim + 1) % nsd] - kI.center((dim + 1) % nsd)));
      }
    }
//    PrintStatus("pos:", thisGPPosition, " vel:", out.gp_velocity);

    out.area = surface_mesh.elem_array[elmID].area;
    out.gp_normal = surface_mesh.elem_array[elmID].normal;
    out.label = elmID;
    return out;
  }

  /**
   * This is the function to call after the force and torque is calculated
   * @param force
   * @param torque
   * @param mass
   * @param inertia
   * @param dt
   */
  void updateGeomKinematicInfo(const ZEROPTV &force, const ZEROPTV &torque, const double dt) {
    if (!def_.is_static) {
      ZEROPTV Force = force;
      for (int i = 0; i < 3; ++i) {
        if (!def_.translation_dof[i]) {
          Force(i) = 0.0;
        }
      }
      sixDof_.storePreviousInfo();
      sixDof_.validKinamatic++;
//      sixDof_.forwardEuler(Force, torque, dt);
      sixDof_.BDF6(Force, torque, dt);

      /// copy the sixdof state to kinematic info
      sixDofToKinematicInfo();
      /// Print status to debug
//      sixDof_.PrintStateDebug();
      updateGeoLocation();
    }

  }

  void sixDofToKinematicInfo() {
    // the reason for not updating the center of mass is we need to first rotate the object
    kI.vel = sixDof_.ks_.back().v;
    kI.vel_ang = sixDof_.ks_.back().omega;
    kI.LinearDisplacementUpdate = sixDof_.ks_.back().LinearDisplacementUpdate;
    // prevent large rotation update matrix doing damage (nan error in rotational velocity)
    if (def_.if_rotation) {
      kI.RotationMatrixUpdate = sixDof_.ks_.back().RotationMatrixUpdate;
    }
    

  }

  /**
   * update coordinates and find extreme x, y, z;
   * Meanwhile, this will update the center of kinematie_info
   */
  void updateGeoLocation() {
    /// update mesh (raytracer if exists) location
    updateSurfaceMeshLocation(kI.LinearDisplacementUpdate,
                              kI.RotationMatrixUpdate,
                              kI.center);

    /// update center location
    // todo, too many centers
    kI.center += kI.LinearDisplacementUpdate;
    exitOutOfBound();
  }
  
  /// The following function is hidden from the user as their combination should be called.
  /// Theses function should not be called alone
 private:
  
  void exitOutOfBound() {
    if (kI.center.x() < def_.bounding_min.x()
        || kI.center.y() < def_.bounding_min.y()
        || kI.center.z() < def_.bounding_min.z()) {
      PrintStatus("Geometry out of minimum bound! Exiting with error! ", kI.center, " bound: ", def_.bounding_min);
      exit(1);
    }
    if (kI.center.x() > def_.bounding_max.x()
        || kI.center.y() > def_.bounding_max.y()
        || kI.center.z() > def_.bounding_max.z()) {
      PrintStatus("Geometry out of maximum bound! Exiting with error! ", kI.center, " bound: ", def_.bounding_max);
      exit(1);
    }
  }
  
  /**
   * Sets elm->gp_normal. If elm->gp_normal_flip is zero, this function first uses tracer to calculate
   * if the normal should be flipped. This is done by moving the triangle's center point by its normal
   * and checking if this point is inside the geometry. The result of this check is then cached in
   * elm->gp_normal_flip, and will be used in subsequent calls to this function to avoid the in/out test.
   * @param[inout] elm elm->gp_normal will be set, and elm->gp_normal_flip is set if it is zero
   *               requires elm->connectivity to be set
   *               elm->area is also set (because it's convenient to do here...)
   * @param[in] nodes used to get the node positions of the triangle
   * @param tracer used when elm->gp_normal_flip is zero to check if the normal should be flipped
   */
  void calculate_normal_and_area(typename SurfaceMesh::ElemData *elm,
                                 const typename SurfaceMesh::NodePositionData *nodes,
                                 const RayTracer *tracer) {
    const auto &connectivity = elm->connectivity;

    std::vector<ZEROPTV> pts(nodeno);
    for (int i = 0; i < nodeno; i++) {
      pts[i] = nodes[connectivity[i]].position;
    }

    /// calculate the normal and size of the surface
    ZEROPTV side_1, side_2;
    for (int dim = 0; dim < nsd; dim++) {
      side_1(dim) = pts[1](dim) - pts[0](dim);
      side_2(dim) = pts[2](dim) - pts[0](dim);
    }

    /// this normal is either outside or inside, need to be pointed inside afterwards
    ZEROPTV normal;
    normal.crossProduct(side_1, side_2);
    elm->area = 0.5 * normal.SafeNormalize();  /// note: this line also normalizes normal!
    if (!elm->gp_normal_flip) {
      throw TALYFEMLIB::TALYException()
          << "Must be called after gp_normal_flip is set using the calcNormalDirection function";
    }
    elm->normal = normal * elm->gp_normal_flip;
  }

  /**
   * calcNormalDirection of the entire mesh using Mpi, this makes the loading much faster
   * @return
   */
  std::vector<int> calcNormalDirectionMpi() {
    std::vector<int> gp_normal_flip_global;
    gp_normal_flip_global.resize(surface_mesh.Nelem());
    {
      int rank, size;
      MPI_Comm_rank(MPI_COMM_WORLD, &rank);
      MPI_Comm_size(MPI_COMM_WORLD, &size);
      /// creating a gp_normal_flip_each vector for each process
      std::vector<int> gp_normal_flip_each;
      std::vector<int> gp_normal_offset;
      std::vector<int> gp_normal_sendcounts;
      /// setting up offsets
      for (int i = 0; i < size; i++) {
        gp_normal_offset.push_back(gp_normal_flip_global.size() / size * i);
      }
      /// setting up counts
      for (int i = 0; i < size - 1; i++) {
        gp_normal_sendcounts.push_back(gp_normal_offset[i + 1] - gp_normal_offset[i]);
      }
      gp_normal_sendcounts.push_back(gp_normal_flip_global.size() - gp_normal_offset.back());
      /// resize each vector
      gp_normal_flip_each.resize(gp_normal_sendcounts[rank]);
      /// do the calculation
      for (int elmID = 0; elmID < gp_normal_sendcounts[rank]; elmID++) {
        if (!rank) {
          PrintLoadingStatus("Mesh", gp_normal_sendcounts[rank], elmID);
        }
        gp_normal_flip_each[elmID] = calcNormalDirection(&surface_mesh.elem_array[elmID + gp_normal_offset[rank]],
                                                         surface_mesh.node_array.data(),
                                                         &surface_mesh.Raytracer);
      }
      /// combine all the vectors from all the processes
      MPI_Allgatherv(gp_normal_flip_each.data(),
                     gp_normal_flip_each.size(),
                     MPI_INT,
                     gp_normal_flip_global.data(),
                     gp_normal_sendcounts.data(),
                     gp_normal_offset.data(),
                     MPI_INT,
                     MPI_COMM_WORLD);
    }
    return gp_normal_flip_global;
  }

  /** This function really shouldn't be called alone in large mesh because it is not parallized, use calcNormalDirectionMpi instead!
   * Calculate gp_normal_flip for a single element. If elm->gp_normal_flip is zero, this function first uses tracer to calculate
   * if the normal should be flipped. This is done by moving the triangle's center point by its normal
   * and checking if this point is inside the geometry. The result of this check is then returned,
   * and will be used in subsequent calls to this function to avoid the in/out test.
   * @param[in] elm required information of connectivity
   * @param[in] nodes used to get the node positions of the triangle
   * @param tracer used when gp_normal_flip is zero to check if the normal should be flipped
   * @return return value of gp_normal_flip
   */

  int calcNormalDirection(typename SurfaceMesh::ElemData *elm,
                          const typename SurfaceMesh::NodePositionData *nodes,
                          const RayTracer *tracer) {
    const auto &connectivity = elm->connectivity;

    std::vector<ZEROPTV> pts(nodeno);
    for (int i = 0; i < nodeno; i++) {
      pts[i] = nodes[connectivity[i]].position;
    }

    /// calculate the normal, this normal used for calc normal_flip
    ZEROPTV side_1, side_2;
    for (int dim = 0; dim < nsd; dim++) {
      side_1(dim) = pts[1](dim) - pts[0](dim);
      side_2(dim) = pts[2](dim) - pts[0](dim);
    }

    ZEROPTV normal;
    normal.crossProduct(side_1, side_2);
    normal.SafeNormalize();

    ZEROPTV triangle_centroid = (pts[0] + pts[1] + pts[2]) * (1.0 / 3.0);

    /// create a new point using pts[0] of the triangle and its normal, the factor is chosen with no apparent reason
    ZEROPTV vec = normal * ((side_1.norm() + side_2.norm()) * 0.005);
    ZEROPTV test_coor1 = triangle_centroid + vec;  // in the direction of the normal
    ZEROPTV test_coor2 = triangle_centroid - vec;  // opposite the normal

    InsideType::Type res = tracer->ifInside(test_coor1.data());
    assert (res != InsideType::ON_SURFACE && res != InsideType::UNKNOWN);
    bool inside1 = (res == InsideType::INSIDE);

#ifndef NDEBUG
    // verify that at least one direction of the normal is inside the geometry
    // if this assert fails, the geometry near this point is very very thin,
    // and we can't tell which direction the normal should point by this test
    // if this happens, you can try a smaller constant for vec...
    res = tracer->ifInside(test_coor2.data());
    assert (res != InsideType::ON_SURFACE && res != InsideType::UNKNOWN);
    bool inside2 = (res == InsideType::INSIDE);
    assert (inside1 != inside2);
#endif
    /// normals are expected to point inside the geometry, so flip if inside1 is false
    /// gp_normal_flip is multiplied by normal, so 1 means "don't flip"
    int gp_normal_flip = (inside1 ? 1 : -1);
    return gp_normal_flip;
  }

  /**
   * Returns true if coor is outside the geometry, false if inside.
   */
  bool in_out(const ZEROPTV &coor) const {
    double distance = 0.0;
    switch (def_.type) {
      case GeomDef::SPHERE: {
        distance = getLength(coor, kI.center, nsd);
        return (distance - def_.radius >= 0);
      }
        break;
      case GeomDef::PILLAR: {
        /// cylinder, infinite height
        distance = getLength(coor, pI.center_of_mass, def_.cylinderDir);
        return (distance - def_.radius >= 0);
      }
        break;
      case GeomDef::CYLINDER: {
        int dim = def_.cylinderDir;
        if (fabs(coor[dim] - pI.center_of_mass[dim]) >= def_.height / 2.0) {
          return true;
        } else {
          distance = getLength(coor, pI.center_of_mass, def_.cylinderDir);
        }
        return (distance - def_.radius >= 0);
      }
        break;
      case GeomDef::CUBE: {
        if (coor.x() < def_.cube_dim[0].x() ||
            coor.y() < def_.cube_dim[0].y() ||
            coor.z() < def_.cube_dim[0].z() ||
            coor.x() > def_.cube_dim[0].x() + def_.cube_dim[1].x() ||
            coor.y() > def_.cube_dim[0].y() + def_.cube_dim[1].y() ||
            coor.z() > def_.cube_dim[0].z() + def_.cube_dim[1].z()) {
          return true;
        } else {
          return false;
        }
      }
        break;
      case GeomDef::MESHOBJECT: {
        /// ray-tracing object
        double pt[3]{coor(0), coor(1), coor(2)};
        InsideType::Type res = surface_mesh.Raytracer.ifInside(pt);
        assert(res != InsideType::UNKNOWN);
        if (res == InsideType::INSIDE || res == InsideType::ON_SURFACE) {
          return false;
        } else {
          return true;
        }
      }
        break;
      case GeomDef::MESHOBJECT_2D:return true;
        break;
      default:throw TALYFEMLIB::TALYException() << "Wrong type!";
    }

  }

  /**
   * return distance from two vectors,
   * @param ptv1
   * @param ptv2
   * @param dim
   * @return
   */
  inline double getLength(const ZEROPTV &ptv1, const ZEROPTV &ptv2, const int dim) const {
    double distance = 0.0;
    for (int k = 0; k < 3; k++) {
      distance += (k == dim) ? 0 : pow((ptv1(k) - ptv2(k)), 2);
    }
    distance = sqrt(distance);
    return distance;
  }

  /**
   * Print the loading status of mesh (Probably will be helpful when anyone is impatient)
   * @param fileName
   * @param totalElements
   * @param currentElmID
   * @return
   */
  void PrintLoadingStatus(const char *fileName, const int totalElements, const int currentElmID) {
    if ((totalElements / 10) != 0) {
      if (currentElmID % (totalElements / 10) == 0) {
        PrintStatus("Loading mesh: ",
                    fileName,
                    " at ",
                    currentElmID / (totalElements / 10) * 10,
                    " percent!\t",
                    currentElmID,
                    "\t/\t",
                    totalElements);
      }
    }
  }

  /// calculate physical properties, only changes the member variable physical_info
  void calcPhysicalInfo(KinematicInfo *kI_in,
                        const SurfaceMesh *surface_mesh_in) {
    surface_mesh_in = &this->surface_mesh;
    /// volume, mass and COM
    switch (def_.type) {
      case GeomDef::SPHERE:calcPhysicalInfoMeshObject(surface_mesh_in);
        break;
      case GeomDef::CYLINDER:calcPhysicalInfoMeshObject(surface_mesh_in);
        break;
      case GeomDef::PILLAR: {
        pI.volume = 1e8;
        pI.mass = pI.volume * pI.rho;
        pI.center_of_mass = def_.center_of_mass;
      }
        break;
      case GeomDef::CUBE: {
        calcPhysicalInfoMeshObject(surface_mesh_in);
      }
        break;
      case GeomDef::MESHOBJECT:calcPhysicalInfoMeshObject(surface_mesh_in);
        break;
      default:throw TALYFEMLIB::TALYException() << "Wrong type!";
    }
    kI_in->center = pI.center_of_mass;
    kI_in->vel = def_.ic_vel;
    kI_in->vel_ang = def_.ic_omega;
  };

  /**
   * calculate volume, center_of_mass of a polyhedron
   * https://stackoverflow.com/questions/2083771/a-method-to-calculate-the-centre-of-mass-from-a-stl-stereo-lithography-file
   */
  void calcPhysicalInfoMeshObject(const SurfaceMesh *mesh) {
    assert(pI.moment_of_inertia.size() == 3);
    std::vector<ZEROPTV> moi_prime;
    moi_prime.resize(nsd);

    pI.center_of_mass = {0.0, 0.0, 0.0};
    pI.volume = 0.0;
    pI.mass = 0.0;
    for (int elmID = 0; elmID < mesh->Nelem(); elmID++) {
      /// elem_node contains the nodes for this element
      const std::vector<int> &connectivity = mesh->elem_array[elmID].connectivity;

      /// copy the coordinates into the pts vector, the order of points is flipped by the normal (sounds weird)
      std::vector<ZEROPTV> pts(nodeno);
      if (mesh->elem_array[elmID].gp_normal_flip < 0) {
        /// 0, 1, 2
        for (int i = 0; i < nodeno; i++) {
          pts[i] = mesh->node_array[connectivity[i]].position;
        }
      } else {
        /// 2, 1, 0
        for (int i = 0; i < nodeno; i++) {
          pts[nodeno - i - 1] = mesh->node_array[connectivity[i]].position;
        }
      }

      double currentVolume = (pts[0][0] * pts[1][1] * pts[2][2]
          - pts[0][0] * pts[2][1] * pts[1][2]
          - pts[1][0] * pts[0][1] * pts[2][2]
          + pts[1][0] * pts[2][1] * pts[0][2]
          + pts[2][0] * pts[0][1] * pts[1][2]
          - pts[2][0] * pts[1][1] * pts[0][2]);
      pI.volume += currentVolume;
      // weight the surface element by its size(current volume)

      // Contribution to the centroid
      ZEROPTV contribOfCentroid;
      for (int dim = 0; dim < nsd; dim++) {
        contribOfCentroid(dim) = pts[0][dim] + pts[1][dim] + pts[2][dim];
        pI.center_of_mass(dim) += contribOfCentroid(dim) * currentVolume;
      }

      for (int dim1 = 0; dim1 < nsd; dim1++) {
        for (int dim2 = 0; dim2 < nsd; dim2++) {
          moi_prime[dim1](dim2) += pI.rho * currentVolume
              * ((dim1 == dim2) ? ((pow(pts[0][dim1], 2) + pow(pts[1][dim1], 2) + pow(pts[2][dim1], 2)
                  + pow(contribOfCentroid(dim1), 2)))
                                : (pts[0][dim1] * pts[0][dim2] + pts[1][dim1] * pts[1][dim2]
                      + pts[2][dim1] * pts[2][dim2] + contribOfCentroid(dim1) * contribOfCentroid(dim2)));
        }
      }

    }
    for (int dim = 0; dim < nsd; dim++) {
      pI.center_of_mass(dim) /= (pI.volume * 4.0);
    }
    pI.volume = pI.volume / 6.0;
    pI.mass = pI.volume * pI.rho;

    // transfer I' to I
    for (int dim = 0; dim < nsd; dim++) {
      moi_prime[dim](dim) =
          moi_prime[dim](dim) / 120.0 - pI.mass * pow(pI.center_of_mass(dim), 2);
    }
    for (int dim1 = 0; dim1 < nsd; dim1++) {
      for (int dim2 = 0; dim2 < nsd; dim2++) {
        pI.moment_of_inertia[dim1](dim2) =
            ((dim1 == dim2) ? (moi_prime[((dim1 + 1) % nsd)](((dim1 + 1) % nsd))
                + moi_prime[((dim1 + 2) % nsd)](((dim1 + 2) % nsd)))
                            : (moi_prime[dim1](dim2) / 120.0
                    - pI.mass * pI.center_of_mass(dim1) * pI.center_of_mass(dim2)));
      }
    }
  }

};
