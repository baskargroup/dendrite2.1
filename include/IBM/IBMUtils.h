//
// Created by maksbh on 7/5/19.
//

#ifndef DENDRITE2_0_IBMUTILS_H
#define DENDRITE2_0_IBMUTILS_H

#include <TalyEquation.h>
#include <IBM/IBMLinearSolver.h>
#include <IBM/IBMNonLinearSolver.h>

template<typename Equation, typename NodeData>
IBMNonLinearSolver<Equation,NodeData> *setIBMNonLinearSolver(TalyEquation<Equation, NodeData> *talyEq,
                                                    ot::DA *da,
                                                    const std::vector<Geom<NodeData> *> &geometries,
                                                    IBMSolver<NodeData> *ibmSolver,
                                                    DENDRITE_INT ndof = 1,
                                                    bool mfree = true,
                                                    bool loopOverAllOctantsBC = false) {
  DENDRITE_REAL problem_size[m_uiDim];
  auto solver = new IBMNonLinearSolver<Equation,NodeData>(da, geometries, ibmSolver);
  solver->setTalyEquation(talyEq);
  solver->setMatrix(talyEq->mat);
  solver->setVector(talyEq->vec);
  talyEq->mat->getDimension(problem_size);
  solver->setProblemSize(problem_size);
  solver->setDA(da);
  solver->setDof(ndof);
  solver->set_mfree(mfree);
  solver->loopOctantsBC(loopOverAllOctantsBC);

  if (da->isActive()) {
    solver->init();
  }
  return solver;

}

template<typename Equation, typename NodeData>
IBMLinearSolver<Equation, NodeData> *setIBMLinearSolver(TalyEquation<Equation, NodeData> *talyEq,
                                              ot::DA *da,
                                              const std::vector<Geom<NodeData> *> &geometries,
                                              IBMSolver<NodeData> *ibmSolver,
                                              DENDRITE_INT ndof = 1,
                                              bool mfree = true,
                                              bool loopOverAllOctantsBC = false) {
  DENDRITE_REAL problem_size[m_uiDim];
  auto solver = new IBMLinearSolver<Equation, NodeData>(da, geometries, ibmSolver);
  solver->setTalyEquation(talyEq);
  solver->setMatrix(talyEq->mat);
  solver->setVector(talyEq->vec);
  talyEq->mat->getDimension(problem_size);
  solver->setProblemSize(problem_size);
  solver->setDA(da);
  solver->setDof(ndof);
  solver->set_mfree(mfree);
  solver->loopOctantsBC(loopOverAllOctantsBC);
  if (da->isActive()) {
    solver->init();
  }
  return solver;

}
#endif //DENDRITE2_0_IBMUTILS_H
