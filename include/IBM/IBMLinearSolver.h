//
// Created by maksbh on 7/5/19.
//

#ifndef DENDRITE2_0_IBMLINEARSOLVER_H
#define DENDRITE2_0_IBMLINEARSOLVER_H

#include <PETSc/Solver/LinearSolver.h>
#include "IBMSolver.h"

using namespace PETSc;
template<typename Equation, typename NodeData>
class IBMLinearSolver : public LinearSolver {
  IBMSolver<NodeData> *ibmSolver_;

 protected:
  TalyEquation<Equation, NodeData> *m_talyEq;

 public:
  IBMLinearSolver(ot::DA *da, const std::vector<Geom<NodeData> *> &geometries, IBMSolver<NodeData> *ibmSolver)
      : ibmSolver_(ibmSolver) {
  }

  void setTalyEquation(TalyEquation<Equation, NodeData> *talyEquation) {
    m_talyEq = talyEquation;
  }

  void updateBoundaries(ot::DA *da) override {
    LinearSolver::updateBoundaries(m_octDA);
    auto &bc = getBC();
    int ndof = getDof();
    int npe = m_octDA->getNumNodesPerElement();
    std::vector<DendroIntL> localToGlobalMap = da->getNodeLocalToGlobalMap();
    DendroIntL *localNodeIdx = new DendroIntL[npe];
    ibmSolver_->syncShouldSolveBuffer();
    auto shouldSolveVec = ibmSolver_->getShouldSolve();
    std::vector<double> ibmBC(ndof);
    ibmSolver_->getIBMValues(ibmBC);
    for (m_octDA->init<ot::DA_FLAGS::WRITABLE>(); m_octDA->curr() < m_octDA->end<ot::DA_FLAGS::WRITABLE>();
         m_octDA->next<ot::DA_FLAGS::WRITABLE>()) {

      m_octDA->getNodeIndices(localNodeIdx, m_octDA->curr(), true);
      for (int i = 0; i < npe; i++) {
        if (shouldSolveVec[localNodeIdx[i]] == NOSOLVE) {
          for (int dof = 0; dof < ndof; dof++) {
            PetscInt ibmrows = localToGlobalMap[localNodeIdx[i]] * ndof + dof;
            bc.rows().push_back(ibmrows);
            bc.values().push_back(0);
          }
        }
      }
    }
    delete[] localNodeIdx;

  }

  void surfaceIntegralJac(Mat *mat, Vec sol) override {
    TALYFEMLIB::ZEROPTV ptvl; /// Local point in isoparameteric space
    TALYFEMLIB::ZEROPTV ptvg; /// Gauss point
    const int npe = m_octDA->getNumNodesPerElement();

    auto geometries = ibmSolver_->getGeometries();
    if (geometries.empty()) {
      return;
    }
    auto syncVal = m_talyEq->mat->getSyncValuesVec();
    const ot::Mesh *pMesh = m_octDA->getMesh();
    const ot::TreeNode *allElements = &(*(pMesh->getAllElements().begin()));
    DendroScalar *p2cEleMat = new DendroScalar[npe * npe];
    unsigned int nCount = 0;

    /** PreMat**/
    ZeroMatrix<double> Ae_;
    Ae_.redim(m_uiDof * npe, m_uiDof * npe);

    DENDRITE_REAL **_val_;
    DENDRITE_REAL *coords;
    coords = new DENDRITE_REAL[m_uiDim * npe];
    _val_ = new DENDRITE_REAL *[syncVal.size()];
    for (int i = 0; i < syncVal.size(); i++) {
      _val_[i] = new DENDRITE_REAL[npe * syncVal[i].ndof];
    }
    m_talyEq->mat->preMat();
    int eleOrder = m_octDA->getElementOrder();
    TalyDendroSync sync;

    DendroIntL *node_idx_dendro = new DendroIntL[npe];
    std::vector<ot::MatRecord> records;
    ot::MatRecord mr;

    /** Looping over the geometries**/
    for (const auto &geom_data : geometries) {
      /** Looping over all the background element**/
      for (DENDRITE_UINT gpID = 0; gpID < geom_data.backgroundElementGP.size();) {
        unsigned int currEleId = geom_data.backgroundElementGP[gpID];
        m_octDA->getElementalCoords(currEleId, coords);
        m_talyEq->mat->getSyncValues(_val_, currEleId);

        for (int i = 0; i < npe; i++) {
          convertOctToPhys(&coords[3 * i], m_problemSize);
        }
        if (eleOrder == 1) {
          sync.syncCoords<1, m_uiDim>(coords, m_talyEq->grid());
        } else if (eleOrder == 2) {
          sync.syncCoords<2, m_uiDim>(coords, m_talyEq->grid());
        }
        for (int i = 0; i < syncVal.size(); i++) {
          if (eleOrder == 1) {
            sync.syncValues<NodeData, 1, m_uiDim>(_val_[i], m_talyEq->field(), syncVal[i].ndof,
                                                  syncVal[i].nodeDataIndex);
          } else if (eleOrder == 2) {
            sync.syncValues<NodeData, 2, m_uiDim>(_val_[i], m_talyEq->field(), syncVal[i].ndof,
                                                  syncVal[i].nodeDataIndex);
          }
        }

        Point position(coords[0], coords[1], coords[2]);
        int level = m_octDA->getLevel(currEleId);
        double h1 = m_problemSize[0] / (1u << level);
        double h2 = m_problemSize[1] / (1u << level);
        double h3 = m_problemSize[2] / (1u << level);

        Point h(h1, h2, h3);

        FEMElm fe(m_talyEq->grid(), TALYFEMLIB::BASIS_ALL);

        if (eleOrder == 1) {
          fe.refill(m_talyEq->grid()->elm_array_[0], TALYFEMLIB::BASIS_LINEAR, 0);
        } else if (eleOrder == 2) {
          fe.refill(m_talyEq->grid()->elm_array_[0], TALYFEMLIB::BASIS_QUADRATIC, 0);
        }
        Ae_.fill(0.0);
        while ((geom_data.my_gauss_points[gpID].isLocal == true) and (geom_data.backgroundElementGP[gpID] == currEleId)
        and (gpID < geom_data.backgroundElementGP.size())) {

          ptvg.x() = geom_data.my_gauss_points[gpID].values[0];
          ptvg.y() = geom_data.my_gauss_points[gpID].values[1];
          ptvg.z() = geom_data.my_gauss_points[gpID].values[2];

          // do integration
          GetLocalPtvTempFix(fe, ptvg, ptvl, 0, eleOrder);
          const auto gpinfo = geom_data.geo->gpCoor_len_normal(geom_data.my_gauss_points[gpID].label, ptvg);
          fe.calc_at(ptvl);
          fe.set_jacc_x_w(gpinfo.area * (1.0 / static_cast<double>(geom_data.geo->gpno)));

          m_talyEq->equation()->ibm_Integrands4side_Ae(fe, Ae_, geom_data.geo, gpinfo, position, h);
          gpID++;
        }
        m_octDA->getNodeIndices(node_idx_dendro, currEleId, true);

        /**
        * @author maksbh
        * [Warning:] Do not play around with this ordering of the loop.
        * Dendro5 interpolation assumes that first all enteries of
        * single dof is stored.
        */
        for (int dofi = 0; dofi < m_uiDof; dofi++) {
          for (int dofj = 0; dofj < m_uiDof; dofj++) {
            for (int i = 0; i < npe; i++) {
              for (int j = 0; j < npe; j++) {
                mr.setRowID(node_idx_dendro[i]);
                mr.setColID(node_idx_dendro[j]);
                mr.setColDim(dofj);
                mr.setRowDim(dofi);
                mr.setMatValue(Ae_(m_uiDof * i + dofi, j * m_uiDof + dofj));
                records.push_back(mr);
              }
            }
          }
        }
        pMesh->getElementQMat(currEleId, p2cEleMat, true);
        fem_eMatTogMat(&(*(records.begin() + nCount)), p2cEleMat, eleOrder, m_uiDof);
        nCount += (m_uiDof * npe * npe * m_uiDof);
        if (records.size() > 500) {
          m_octDA->petscSetValuesInMatrix(*mat, records, m_uiDof, ADD_VALUES);
          nCount = 0;
        }
      }
    }
    m_octDA->petscSetValuesInMatrix(*mat, records, m_uiDof, ADD_VALUES);
    /** Post Mat **/
    for (int i = 0; i < syncVal.size(); i++) {
      delete[] _val_[i];
    }
    delete[] node_idx_dendro;
    delete[] coords;
    delete[] p2cEleMat;
    m_talyEq->mat->postMat();
  }

  void surfaceIntegralFunc(Vec in, Vec out) override {

    TALYFEMLIB::ZEROPTV ptvl; /// Local point in isoparameteric space
    TALYFEMLIB::ZEROPTV ptvg; /// Gauss point
    auto geometries = ibmSolver_->getGeometries();
    auto syncVal = m_talyEq->vec->getSyncValuesVec();
    if (geometries.empty()) {
      return;
    }

    const ot::Mesh *pMesh = m_octDA->getMesh();
    const unsigned int npe = pMesh->getNumNodesPerElement();
    double *qMat = new double[npe * npe];
    const unsigned int *e2n_cg = &(*(pMesh->getE2NMapping().begin()));

    /** PreComputeVec**/
    TALYFEMLIB::ZEROARRAY<DENDRITE_REAL> be;
    be.redim(m_uiDof * npe);

    DENDRITE_REAL **_val_;
    DENDRITE_REAL *coords;
    coords = new DENDRITE_REAL[m_uiDim * npe];
    _val_ = new DENDRITE_REAL *[syncVal.size()];
    for (int i = 0; i < syncVal.size(); i++) {
      _val_[i] = new DENDRITE_REAL[npe * syncVal[i].ndof];
    }
    m_talyEq->vec->preComputeVec(NULL, NULL);
    int eleOrder = m_octDA->getElementOrder();
    TalyDendroSync sync;
    DENDRITE_REAL *out_dendro = new DENDRITE_REAL[npe * m_uiDof];
    DENDRITE_REAL *outArray, *_outArray;
    Vec tempIBMvec;
    m_octDA->petscCreateVector(tempIBMvec, false, false, m_uiDof);
    VecSet(tempIBMvec, 0);
    VecGetArray(tempIBMvec, &outArray);

    m_octDA->nodalVecToGhostedNodal(outArray, _outArray, false, m_uiDof);
    m_octDA->readFromGhostBegin(_outArray, m_uiDof);
    m_octDA->readFromGhostEnd(_outArray, m_uiDof);
    const unsigned int eleLocalBegin = pMesh->getElementLocalBegin();
    const unsigned int eleLocalEnd = pMesh->getElementLocalEnd();
    /** Looping over the geometries**/
    for (const auto &geom_data : geometries) {

      /** Looping over all the background element**/
      for (DENDRITE_UINT gpID = 0; gpID < geom_data.backgroundElementGP.size(); ) {


        unsigned int currEleId = geom_data.backgroundElementGP[gpID];
        m_octDA->getElementalCoords(currEleId, coords);
        m_talyEq->vec->getSyncValues(_val_, currEleId);

        for (int i = 0; i < npe; i++) {
          convertOctToPhys(&coords[3 * i], m_problemSize);
        }

        if (eleOrder == 1) {
          sync.syncCoords<1, m_uiDim>(coords, m_talyEq->grid());
        } else if (eleOrder == 2) {
          sync.syncCoords<2, m_uiDim>(coords, m_talyEq->grid());
        }
        for (int i = 0; i < syncVal.size(); i++) {
          if (eleOrder == 1) {
            sync.syncValues<NodeData, 1, m_uiDim>(_val_[i], m_talyEq->field(), syncVal[i].ndof,
                                                  syncVal[i].nodeDataIndex);
          } else if (eleOrder == 2) {
            sync.syncValues<NodeData, 2, m_uiDim>(_val_[i], m_talyEq->field(), syncVal[i].ndof,
                                                  syncVal[i].nodeDataIndex);
          }
        }

        Point position(coords[0], coords[1], coords[2]);
        int level = m_octDA->getLevel(currEleId);
        double h1 = m_problemSize[0] / (1u << level);
        double h2 = m_problemSize[1] / (1u << level);
        double h3 = m_problemSize[2] / (1u << level);

        Point h(h1, h2, h3);

        // do integration
        FEMElm fe(m_talyEq->grid(), TALYFEMLIB::BASIS_ALL);

        if (eleOrder == 1) {
          fe.refill(m_talyEq->grid()->elm_array_[0], TALYFEMLIB::BASIS_LINEAR, 0);
        } else if (eleOrder == 2) {
          fe.refill(m_talyEq->grid()->elm_array_[0], TALYFEMLIB::BASIS_QUADRATIC, 0);
        }
        be.fill(0.0);
        while ((geom_data.my_gauss_points[gpID].isLocal == true) and (geom_data.backgroundElementGP[gpID] == currEleId)
          and(gpID < geom_data.backgroundElementGP.size())) {
          ptvg.x() = geom_data.my_gauss_points[gpID].values[0];
          ptvg.y() = geom_data.my_gauss_points[gpID].values[1];
          ptvg.z() = geom_data.my_gauss_points[gpID].values[2];


          GetLocalPtvTempFix(fe, ptvg, ptvl, 0, eleOrder);
          const auto gpinfo = geom_data.geo->gpCoor_len_normal(geom_data.my_gauss_points[gpID].label, ptvg);
          fe.calc_at(ptvl);
          fe.set_jacc_x_w(gpinfo.area * (1.0 / static_cast<double>(geom_data.geo->gpno)));
          m_talyEq->equation()->ibm_Integrands4side_be(fe, be, geom_data.geo, gpinfo, position, h);
          gpID++;

        }
        for (int i = 0; i < npe; i++) {
          for (int dof = 0; dof < m_uiDof; dof++) {
            out_dendro[npe * dof + i] = be(i * m_uiDof + dof);
          }
        }

        pMesh->getElementQMat(currEleId, qMat, true);
        for (unsigned int dof = 0; dof < m_uiDof; dof++) {
          for (unsigned int i = 0; i < npe; i++) {
            VECType ss = (VECType) 0;
            for (unsigned int j = 0; j < npe; j++) {
              ss += qMat[j * npe + i] * out_dendro[dof * npe + j];
            }
            _outArray[dof * m_octDA->getTotalNodalSz() + e2n_cg[currEleId * npe + i]] += (VECType) ss;
          }
        }
      }
    }
    /** Post ComputeVec **/
    for (int i = 0; i < syncVal.size(); i++) {
      delete[] _val_[i];
    }
    delete[] out_dendro;
    delete[] coords;
    m_octDA->writeToGhostsBegin(_outArray, m_uiDof);
    m_octDA->writeToGhostsEnd(_outArray, ot::DA_FLAGS::WriteMode::ADD_VALUES, m_uiDof);
    m_octDA->ghostedNodalToNodalVec(_outArray, outArray, true, m_uiDof);
    VecRestoreArray(tempIBMvec, &outArray);
    m_octDA->destroyVector(_outArray);
    VecAXPY(out, 1, tempIBMvec);
    VecDestroy(&tempIBMvec);
    m_talyEq->vec->postComputeVec(NULL, NULL);
    delete[] qMat;
  }

};

#endif //DENDRITE2_0_IBMLINEARSOLVER_H
