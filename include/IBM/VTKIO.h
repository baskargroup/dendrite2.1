#pragma once
#include "Geom.h"
#include <iomanip>
#include "TimeInfo.h"
#include <chrono>
#include <ctime>
#include <cmath>
template<typename NodeData>
void stl2vtk(const Geom<NodeData> *Geom,
             const TimeInfo &ti,
             const std::string &file_name = "auto_stl2vtk",
             bool writedata = false,
             int data_index = -1,
             int rank_to_write = 0) {

  int rank, size;

  MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
  MPI_Comm_size(PETSC_COMM_WORLD, &size);
  assert(rank_to_write < size);
  if (rank == rank_to_write) {
    auto m_Start_time = std::chrono::system_clock::now();
    std::cout << "Writing new STL to VTP file: " << file_name << ", Using rank: " << rank << std::endl;

    const auto Nelem = Geom->surface_mesh.Nelem();
    const auto Nnode = Geom->surface_mesh.Nnode();

    char fname[PATH_MAX];
    snprintf(fname, sizeof(fname), "%s.vtp", file_name.c_str());

    // start writing to file
    std::ofstream out;
    out.open(fname);
    // VTK header
    std::string header_info = "<?xml version=\"1.0\"?>\n"
                              "<VTKFile type=\"PolyData\" version=\"0.1\" byte_order=\"LittleEndian\">\n"
                              "<PolyData>\n"
                              "<FieldData>\n"
                              "<Array type=\"String\" Name=\"MeshName\" NumberOfTuples=\"1\" format=\"ascii\">\n"
                              "83 84 76 95 109 101 115 104 0\n"
                              "</Array>\n"
                              "</FieldData>\n"
                              "<Piece NumberOfPoints=\"" + std::to_string(Nnode) + "\" NumberOfPolys=\"" + std::to_string(Nelem) + "\">\n";
    char head[header_info.size()];
    std::strncpy(head, header_info.c_str(), sizeof(head));

    out.write(head, sizeof(head));
    // points
    out << "<Points>\n";
    out << "<DataArray type=\"Float32\" NumberOfComponents=\"3\">\n";

    for (unsigned i = 0; i < Nnode; i++) {
      out << std::setprecision(15)
          << Geom->surface_mesh.node_array[i].position.x() << " "
          << Geom->surface_mesh.node_array[i].position.y() << " "
          << Geom->surface_mesh.node_array[i].position.z() << " \n";
    }
    out << "</DataArray>\n";
    out << "</Points>\n";

    if (writedata) {
      // write data (index)
      out << "<PointData Scalars=\"pressure\">\n";
      out << "<DataArray type=\"Float32\" Name=\"pressure\" format=\"ascii\">\n";
      for (unsigned i = 0; i < Nnode; i++) {
        out << Geom->surface_mesh.node_array[i].data.u[data_index] << " ";
      }
      out << "</DataArray>\n";
      out << "</PointData>\n";
    }


    // write polys
    out << "<Polys>\n";
    out << "<DataArray type=\"Int64\" Name=\"connectivity\">\n";
    for (unsigned i = 0; i < Nelem; i++) {
      out << Geom->surface_mesh.elem_array[i].connectivity[0] << " "
          << Geom->surface_mesh.elem_array[i].connectivity[1] << " "
          << Geom->surface_mesh.elem_array[i].connectivity[2] << " \n";
    }
    out << "</DataArray>\n";


    // write offset (no idea what this means but have to)
    out << "<DataArray type=\"Int64\" Name=\"offsets\" format=\"ascii\">\n";
    for (unsigned i = 0; i < Nelem; i++) {
      out << (i + 1) * 3 << " ";
      if ((i % 6) == 5) {
        out << "\n";
      }
    }
    out << "\n</DataArray>\n</Polys>\n";

    // vtk end
    out << "</Piece>\n</PolyData>\n</VTKFile>";

    out.close();
    auto m_Close_time = std::chrono::system_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(m_Close_time - m_Start_time).count();
//    std::cout << "file writing:" << duration << std::endl;
  }

}