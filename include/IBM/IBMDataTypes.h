//
// Created by maksbh on 6/26/19.
//

#ifndef DENDRITE2_0_IBMDATATYPES_H
#define DENDRITE2_0_IBMDATATYPES_H

#include <talyfem/talyfem.h>
#include <oda.h>
#include <Refinement/RefinementDataTypes.h>
struct triInfo {
  /// Label of Geometry.
  unsigned int geoID = -1;
  /// The surface element id.
  unsigned int id = -1;
  /// The normal corresponding to the surface.
  TALYFEMLIB::ZEROPTV normal = {0, 0, 0};
  /// center of triangle
  ZEROPTV centroid = {0, 0, 0};
};

// Defining the BinaryFunction (compare the two triInfos)
bool triEqual(const triInfo &a, const triInfo &b) {
  if (a.geoID == b.geoID && a.id == b.id) {
    return true;
  } else {
    return false;
  }
}

bool triSort(const triInfo &a, const triInfo &b) {
  if (a.geoID != b.geoID) {
    return a.geoID < b.geoID;
  } else {
    return a.id < b.id;
  }
}
enum inOutInfo {
  INVALID = -1,
  IN_GP = 0,
  OUT_GP = 2,
  INTERCEPTED_GP = 1,
  IN_NODE = 10,
  OUT_NODE = 12,
  INTERCEPTED_NODE = 11
};

enum SolveInfo : int {
  SOLVE = 1,
  NOSOLVE = 0
};

/// Information of element
struct eleInfo {
  // lower bottom point of element
  ZEROPTV pos;
  // element size
  ZEROPTV h;
  // element level of refinement
  DENDRITE_UINT level;
};

void GetLocalPtvTempFix(FEMElm fe, const ZEROPTV &ptvg, ZEROPTV &ptvl, int elm_id, int eleOrder) {

//  FEMElm fe(grid, BASIS_POSITION | BASIS_FIRST_DERIVATIVE);

  // Always use linear basis functions, even for quadratic elements.
  // Nonlinear bases are not guaranteed to converge in the numerical
  // approximation below, and higher order BFs are more expensive to compute.
  // We don't support curved elements, and higher-order elements can be safely reduced
  // to linear (since the node points are in the same order), so this is an acceptable optimization.
  // TODO only use Linear element even for quadratic

//  fe.refill(elm_id, BASIS_LINEAR, 0);
  if (fe.elem()->n_nodes() == 8) {
    fe.refill(elm_id, BASIS_LINEAR, 0);
  } else {
    fe.refill(elm_id, BASIS_QUADRATIC, 0);
  }
  // use (0, 0, 0) as our initial guess
  ptvl = ZEROPTV(0.0, 0.0, 0.0);

  // maximum number of iterations (after which we give up & throw an exception)
  static const int MAX_STEPS = 100;

  // upper limit on L2 error for ptvl
  static const double MIN_ERROR = 1e-7;

  int n_steps = 0;
  double err = 0.0;
  while (n_steps < MAX_STEPS) {
    // calculate the basis function values at our current guess
    fe.calc_at(ptvl);
    // calculate (L2 error)^2 for this guess, and at the same time build
    // the vector from our guess (mapped to physical space) to ptvg
    ZEROPTV DX;
    err = 0.0;
    for (int i = 0; i < fe.nsd(); i++) {
      DX(i) = (ptvg(i) - fe.position()(i));
      err += DX(i) * DX(i);
    }
    // if the error on our guess is acceptable, return it
    // (we use MIN_ERROR^2 here to avoid calculating sqrt(err))
    if (err < MIN_ERROR * MIN_ERROR) {
//      PrintStatus(ptvl, ptvg);
      return;
    }
    // otherwise, move our guess by the inverse of the Jacobian times
    // our delta x vector (calculated above)
    double jaccInv = 1.0 / fe.jacc();
    for (int i = 0; i < fe.nsd(); i++) {
      for (int j = 0; j < fe.nsd(); j++) {
        ptvl(i) += fe.cof(j, i) * DX(j) * jaccInv;
      }
    }
//    PrintStatus("1-\t", ptvl, ptvg);
    n_steps++;
  }

  // if we get here, our loop above failed to converge
  throw TALYException() << "GetLocalPtv did not converge after "
                        << MAX_STEPS << " iterations (final error: "
                        << sqrt(err) << ")";
}

struct neighbour {
  std::vector<DENDRITE_REAL> neighbour_value;
  ZEROPTV neighbour_coord;
  double distance;
};

struct localFreshlyOut {

  std::vector<unsigned int> elmID;
  DendroIntL localNodeIdx;
  ZEROPTV pos;
  bool ifhanging = false;
  std::vector<neighbour> nei;

  localFreshlyOut(unsigned int elmid, DendroIntL idx, ZEROPTV pos_in, bool hanging) {
    elmID.push_back(elmid);
    localNodeIdx = idx;
    pos = pos_in;
    ifhanging = hanging;
  }
};

struct localFreshlyOutNeighbour {
  std::vector<unsigned int> elmID;
  DendroIntL localNodeIdx;
  ZEROPTV pos;
  octNeighbours neighbour;
  void operator=(const localFreshlyOut &base) {
    this->elmID = base.elmID;
    this->localNodeIdx = base.localNodeIdx;
    this->pos = base.pos;
  }
  void resize_neighbour() {
    for (int i = 0; i < NUMFACES; i++) {
      int neighbour_no = neighbour.numNeighbour[i];
      neighbour.neighbourID[i].resize(neighbour_no);
      neighbour.neighbourCoords[i].resize(neighbour.neighbourCoords[i].size() / 4 * neighbour_no);
      neighbour.neighbourValues[i].resize(neighbour.neighbourValues[i].size() / 4 * neighbour_no);
    }
  }

  void print() {
//    PrintStatus("Elem ID: ", elmID, "\nNode Id: ", localNodeIdx, "\nPosition: ", pos);
    for (int numFace = 0; numFace < NUMFACES; numFace++) {
      if (neighbour.numNeighbour[numFace] != 1) {
        PrintStatus("More than 1 faces");
      }
      PrintStatus("Side: ", numFace,
                  "\tNeighbour no: ", neighbour.numNeighbour[numFace],
                  "\tLevel: ", neighbour.neighbourLevel[numFace]);
      std::cout << "Node Index:" << std::endl;
      for (const auto &id:neighbour.neighbourID[numFace]) {
        std::cout << id << "\t";
      }
      std::cout << "\n";
      std::cout << "Coords:" << std::endl;
      for (const auto &coord:neighbour.neighbourCoords[numFace]) {
        std::cout << coord << "\t";
      }
      std::cout << "\n";
      std::cout << "Values:" << std::endl;
      for (const auto &v:neighbour.neighbourValues[numFace]) {
        std::cout << v << "\t";
      }
      std::cout << "\n";
    }
  }
};

void ProcesslocalFreshlyOutNeighbour(const std::vector<localFreshlyOut> &in, std::vector<localFreshlyOut> &out) {
  /*
   * Combine all same nodeIdx into one node
   */
  for (auto it = in.begin(); it != in.end(); ++it) {
    if (out.empty() || it->localNodeIdx != out.back().localNodeIdx) {
      out.push_back(*it);
    } else {
      assert(it->elmID.size() == 1);
      out.back().elmID.push_back(it->elmID[0]);
      if (not(out.back().ifhanging) and (not(it->ifhanging))) {
        assert((out.back().pos - it->pos).norm() < 1e-6);
      }
      if (out.back().ifhanging and (not(it->ifhanging))) {
        out.back().pos = it->pos;
        out.back().ifhanging == false;
      }
    }
  }

}
bool compare_by_coord(const neighbour &first, const neighbour &second) {
  if (fabs(first.neighbour_coord.x() - second.neighbour_coord.x()) < 1e-6) {
    if (fabs(first.neighbour_coord.y() - second.neighbour_coord.y()) < 1e-6) {
      return first.neighbour_coord.z() < second.neighbour_coord.z();
    } else {
      return first.neighbour_coord.y() < second.neighbour_coord.y();
    }
  } else {
    return first.neighbour_coord.x() < second.neighbour_coord.x();
  }
}

bool compare_by_distance(const neighbour &first, const neighbour &second) {
  return first.distance < second.distance;
}

bool sameNeighbour(const neighbour &first, const neighbour &second) {
  if ((first.neighbour_coord - second.neighbour_coord).norm() < 1e-6) {
    return true;
  }
  return false;
}

void ProcesslocalFreshlyOutData(std::vector<localFreshlyOut> &in) {
  /*
   * 1. Sort coordinates and values based on the coordinates
   * 2. remove duplicate
   * 3. sort based on the distance (distance = 0 is placed to the bottom)
   * 4. remove distance >= 100 (node itself or IN node)
   */
  for (auto &freshnode : in) {
    // sort them by coordinates
    std::sort(freshnode.nei.begin(), freshnode.nei.end(), compare_by_coord);
    // remove duplicate
    freshnode.nei.erase(unique(freshnode.nei.begin(), freshnode.nei.end(), sameNeighbour), freshnode.nei.end());
    // sort based on the distance (distance = 100 is placed to the bottom)
    std::sort(freshnode.nei.begin(), freshnode.nei.end(), compare_by_distance);
    // remove distance >= 100 (node itself or IN node)
    freshnode.nei.erase(std::remove_if(freshnode.nei.begin(), freshnode.nei.end(), [](const neighbour &n) {
      return n.distance > 99;
    }), freshnode.nei.end());
  }

}

bool compareBylocalNodeIdx(const localFreshlyOut &first, const localFreshlyOut &second) {
  return first.localNodeIdx < second.localNodeIdx;
}

bool compareBylocalNodeIdx2(const localFreshlyOutNeighbour &first, const localFreshlyOutNeighbour &second) {
  return first.localNodeIdx < second.localNodeIdx;
}

bool samelocalFreshlyOut(const localFreshlyOut &first, const localFreshlyOut &second) {
  return first.localNodeIdx == second.localNodeIdx;
}

void combineFreshlyOut(localFreshlyOut &first, const localFreshlyOut &second) {
  assert(first.localNodeIdx == second.localNodeIdx);
}

#endif //DENDRITE2_0_IBMDATATYPES_H
