//
// Created by maksbh on 11/23/18.
//

/**
 * @author Kumar Saurabh
 * @brief syncs the elemental ordering between Taly and Dendro 5.0
 * */


#ifndef DENDRITE2_0_TALYDENDROSYNC_H
#define DENDRITE2_0_TALYDENDROSYNC_H

#include <iostream>
#include <assert.h>
#include <type_traits>
#include <DataTypes.h>
#include <talyfem/grid/grid_types/grid.h>
#include <talyfem/grid/gridfield.h>

#include <talyfem/grid/grid_types/grid.h>
#include <talyfem/grid/gridfield.h>
#include <talyfem/grid/elem.h>
#include <talyfem/grid/elem_types/elem3dhexahedral.h>
#include <talyfem/grid/femelm.h>
#include <talyfem/data_structures/zeroarray.h>
#include <talyfem/data_structures/zeromatrix.h>


class TalyDendroSync {


public:

    template<const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<
            order == 1 and dim == 3>::type * = nullptr>
    void DendroToTalyIn(const DENDRITE_REAL *in_dendro, DENDRITE_REAL *in_taly, int ndof = 1);

    template<const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<
            order == 2 and dim == 3>::type * = nullptr>
    void DendroToTalyIn(const DENDRITE_REAL *in_dendro, DENDRITE_REAL *in_taly, int ndof = 1);

    template<const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<
            order == 1 and dim == 3>::type * = nullptr>
    void TalyToDendroIn(const DENDRITE_REAL *in_taly, DENDRITE_REAL *in_dendro, int ndof = 1);

    template<const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<
            order == 2 and dim == 3>::type * = nullptr>
    void TalyToDendroIn(const DENDRITE_REAL *in_taly, DENDRITE_REAL *in_dendro, int ndof = 1);

    template<class NodeData, const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<
            order == 1 and dim == 3>::type * = nullptr>
    void DendroToTalyValueAndCoords(const DENDRITE_REAL *in_dendro, DENDRITE_REAL *coords,
                                    TALYFEMLIB::GRID *grid, TALYFEMLIB::GridField<NodeData> *gf,
                                    const DENDRITE_UINT ndof = 1, const DENDRITE_UINT sdof = 0);

    template<class NodeData, const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<
            order == 1 and dim == 3>::type * = nullptr>
    void syncValues(const DENDRITE_REAL *in_dendro, TALYFEMLIB::GridField<NodeData> *gf,
                    const DENDRITE_UINT ndof = 1, const DENDRITE_UINT sdof = 0);

    template<class NodeData, const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<
            order == 2 and dim == 3>::type * = nullptr>
    void syncValues(const DENDRITE_REAL *in_dendro, TALYFEMLIB::GridField<NodeData> *gf,
                    const DENDRITE_UINT ndof = 1, const DENDRITE_UINT sdof = 0);



    template<class NodeData, const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<
            order == 2 and dim == 3>::type * = nullptr>
    void DendroToTalyValueAndCoords(const DENDRITE_REAL *in_dendro, DENDRITE_REAL *coords,
                                    TALYFEMLIB::GRID *grid, TALYFEMLIB::GridField<NodeData> *gf,
                                    const DENDRITE_UINT ndof = 1, const DENDRITE_UINT sdof = 0);

    template<const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<
            order == 1 and dim == 3>::type * = nullptr>
    void DendroToTalyIn(const DENDRITE_UINT *in_dendro, DENDRITE_UINT *in_taly, int ndof = 1);

    template<const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<
            order == 2 and dim == 3>::type * = nullptr>
    void DendroToTalyIn(const DENDRITE_UINT *in_dendro, DENDRITE_UINT *in_taly, int ndof = 1);



    template<const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<
            order == 1 and dim == 3>::type * = nullptr>
    void syncCoords( DENDRITE_REAL * coords_dendro, TALYFEMLIB::GRID *grid);

    template<const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<
            order == 2 and dim == 3>::type * = nullptr>
    void syncCoords( DENDRITE_REAL * coords_dendro, TALYFEMLIB::GRID *grid);



    /**
     * @brief returns the surface flags of which one of the face belongs to the boundary
     * Copied from Dendrite 1.0
     * @param[in] position of the nodes
     * @param[in] problemSize the size of problem
     * @param[in] start : starting point of the problem
     * @return the flags
     */
    unsigned int get_surf_flags(const TALYFEMLIB::ZEROPTV & positon,const  DENDRITE_REAL * problemSize,const DENDRITE_REAL * start);

    /**
     * @brief returns the surface flags of which one of the face belongs to the boundary
     * Copied from Dendrite 1.0
     * @param[in] of the nodes
     * @param[in] problemSize the size of problem
     * @param[default] start : set to (0,0,0)
     * @return the flags
     */
    unsigned int get_surf_flags(const TALYFEMLIB::ZEROPTV & positon, const DENDRITE_REAL * problemSize);


};







template<const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<order == 1 and dim == 3>::type *>
void TalyDendroSync::DendroToTalyIn(const DENDRITE_REAL *in_dendro, DENDRITE_REAL *in_taly, int ndof) {
//    assert(ndof == 1);

    for (int i = 0; i < 8; i++) {
        for (int dof = 0; dof < ndof; dof++) {
            in_taly[i * ndof + dof] = in_dendro[8 * dof + i];
        }

    }

}

template<const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<order == 1 and dim == 3>::type *>
void TalyDendroSync::TalyToDendroIn(const DENDRITE_REAL *in_taly, DENDRITE_REAL *in_dendro, int ndof) {

    for (int dof = 0; dof < ndof; dof++) {
        for (int i = 0; i < 8; i++) {
            in_dendro[8 * dof + i] = in_taly[i * ndof + dof];
        }
    }

}

template<const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<order == 2 and dim == 3>::type *>
void TalyDendroSync::TalyToDendroIn(const DENDRITE_REAL *in_taly, DENDRITE_REAL *in_dendro, int ndof) {

    for (int dof = 0; dof < ndof; dof++) {
        for (int i = 0; i < 27; i++) {
            in_dendro[27 * dof + i] = in_taly[i * ndof + dof];
        }
    }
}


template<const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<order == 2 and dim == 3>::type *>
void TalyDendroSync::DendroToTalyIn(const DENDRITE_REAL *in_dendro, DENDRITE_REAL *in_taly, int ndof) {

    for (int dof = 0; dof < ndof; dof++) {
        for (int i = 0; i < 27; i++) {
            in_taly[i * ndof + dof] = in_dendro[27 * dof + i];
        }
    }
}

template<class NodeData, const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<
        order == 1 and dim == 3>::type *>
void TalyDendroSync::DendroToTalyValueAndCoords(const DENDRITE_REAL *in_dendro, DENDRITE_REAL *coords,
                                                TALYFEMLIB::GRID *grid, TALYFEMLIB::GridField<NodeData> *gf,
                                                const DENDRITE_UINT ndof, const DENDRITE_UINT sdof) {


    DENDRITE_REAL *in_taly = new DENDRITE_REAL[8 * ndof];
    DendroToTalyIn<1, 3>(in_dendro, in_taly, ndof);
    for (unsigned int i = 0; i < 8; i++) {
        grid->node_array_[i]->setCoor(coords[i * 3], coords[i * 3 + 1], coords[i * 3 + 2]);
    }
    for (unsigned int dof = 0; dof < ndof; dof++) {
        gf->NodeDataFromArray(&in_taly[dof], sdof + dof, ndof);
    }

    delete[] in_taly;
}


template<class NodeData, const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<
        order == 2 and dim == 3>::type *>
void TalyDendroSync::DendroToTalyValueAndCoords(const DENDRITE_REAL *in_dendro, DENDRITE_REAL *coords,
                                                TALYFEMLIB::GRID *grid, TALYFEMLIB::GridField<NodeData> *gf,
                                                const DENDRITE_UINT ndof, const DENDRITE_UINT sdof) {


    DENDRITE_REAL *in_taly = new DENDRITE_REAL[27 * ndof];

    DendroToTalyIn<2, 3>(in_dendro, in_taly, ndof);
    for (unsigned int i = 0; i < 27; i++) {
        grid->node_array_[i]->setCoor(coords[i * 3], coords[i * 3 + 1], coords[i * 3 + 2]);

    }
    for (unsigned int dof = 0; dof < ndof; dof++) {
        gf->NodeDataFromArray(&in_taly[dof], sdof + dof, ndof);
    }

    delete[] in_taly;

}


template<class NodeData, const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<
        order == 1 and dim == 3>::type *>
void TalyDendroSync::syncValues(const DENDRITE_REAL *in_dendro, TALYFEMLIB::GridField<NodeData> *gf,
                                const DENDRITE_UINT ndof, const DENDRITE_UINT sdof) {


    DENDRITE_REAL *in_taly = new DENDRITE_REAL[8 * ndof];

    DendroToTalyIn<1, 3>(in_dendro, in_taly, ndof);

    for (unsigned int dof = 0; dof < ndof; dof++) {
        gf->NodeDataFromArray(&in_taly[dof], sdof + dof, ndof);
    }


    delete[] in_taly;
}


template<class NodeData, const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<
        order == 2 and dim == 3>::type *>
void TalyDendroSync::syncValues(const DENDRITE_REAL *in_dendro, TALYFEMLIB::GridField<NodeData> *gf,
                                const DENDRITE_UINT ndof, const DENDRITE_UINT sdof) {


    DENDRITE_REAL *in_taly = new DENDRITE_REAL[27 * ndof];
    DendroToTalyIn<2, 3>(in_dendro, in_taly, ndof);

    for (unsigned int dof = 0; dof < ndof; dof++) {
        gf->NodeDataFromArray(&in_taly[dof], sdof + dof, ndof);
    }

    delete[] in_taly;

}
template< const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<
        order == 1 and dim == 3>::type *>
void TalyDendroSync::syncCoords(DENDRITE_REAL * coords_dendro, TALYFEMLIB::GRID *grid) {

    for (unsigned int i = 0; i < 8; i++) {
        grid->node_array_[i]->setCoor(coords_dendro[i * 3], coords_dendro[i * 3 + 1], coords_dendro[i * 3 + 2]);
    }

}

template< const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<
        order == 2 and dim == 3>::type *>
void TalyDendroSync::syncCoords( DENDRITE_REAL * coords_dendro, TALYFEMLIB::GRID *grid) {

    for (unsigned int i = 0; i < 27; i++) {
        grid->node_array_[i]->setCoor(coords_dendro[i * 3], coords_dendro[i * 3 + 1], coords_dendro[i * 3 + 2]);
    }


}

template<const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<order == 1 and dim == 3>::type *>
void TalyDendroSync::DendroToTalyIn(const DENDRITE_UINT *in_dendro, DENDRITE_UINT *in_taly, int ndof) {
    for (int dof = 0; dof < ndof; dof++) {
        for (int i = 0; i < 8; i++) {
            in_taly[i * ndof + dof] = in_dendro[8 * dof + i];
        }
    }
}

template<const DENDRITE_UINT order, const DENDRITE_UINT dim, typename std::enable_if<order == 2 and dim == 3>::type *>
void TalyDendroSync::DendroToTalyIn(const DENDRITE_UINT *in_dendro, DENDRITE_UINT *in_taly, int ndof) {
    for (int dof = 0; dof < ndof; dof++) {
        for (int i = 0; i < 27; i++) {
            in_taly[i * ndof + dof] = in_dendro[27 * dof + i];
        }
    }
}




#endif //DENDRITE2_0_TALYDENDROSYNC_H
