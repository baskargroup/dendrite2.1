//
// Created by maksbh on 11/26/18.
//

#ifndef DENDRITE2_0_TALYEQUATION_H
#define DENDRITE2_0_TALYEQUATION_H

#include "TalyMesh.h"

template<typename Equation, typename NodeData>
class TalyEquation {
  Point m_domainMin, m_domainMax;
 protected:
  bool m_surfAssembly;
  TalyMesh<NodeData> *mesh_ = NULL;
  bool owned_mesh_;
  Equation taly_eq_;

 public:
  TalyMat<Equation, NodeData> *mat = NULL;
  TalyVec<Equation, NodeData> *vec = NULL;

  inline Equation *equation() { return &taly_eq_; }

  /**
   * Arguments past the first two are passed to the Equation constructor.
   * @param da : the DA for the octree.
   * @param ndof number of degrees of freedom
   * @param surfAssembly to enable Integrands4Side.
   * @param args passed to Equation constructor
   * @param domain_min minimum of domain.
   * @param domain_max maximum of domain.
   * Domain min and Domain max determines the bounding box.
   */
  template<typename... Args>
  TalyEquation(ot::DA *da, Point &domain_min, Point &domain_max, int ndof = 1, bool surfAssembly = false,
               Args... args)
      : taly_eq_(args...) {

    mesh_ = new TalyMesh<NodeData>(da->getElementOrder());
    m_surfAssembly = surfAssembly;

    owned_mesh_ = true;

    taly_eq_.p_grid_ = grid();
    taly_eq_.p_data_ = field();

    mat = new TalyMat<Equation, NodeData>(da, &taly_eq_, grid(), field(), ndof, m_surfAssembly);
    vec = new TalyVec<Equation, NodeData>(da, &taly_eq_, grid(), field(), ndof, m_surfAssembly);

    mat->setDimension(domain_min, domain_max);
    vec->setDimension(domain_min, domain_max);

    m_domainMax = domain_max;
    m_domainMin = domain_min;

  }

  /**
     * Same constructor as before, but now user passes the mesh object.
     * Arguments past the first two are passed to the Equation constructor.
     * @param da : the DA for the octree.
     * @param ndof number of degrees of freedom
     * @param surfAssembly to enable Integrands4Side.
     * @param args passed to Equation constructor
     * @param domain_min minimum of domain.
     * @param domain_max maximum of domain.
     * Domain min and Domain max determines the bounding box.
  */

  template<typename... Args>
  TalyEquation(TalyMesh<NodeData> *mesh, ot::DA *da, Point &domain_min, Point &domain_max, int ndof = 1, bool
  surfAssembly = false,
               Args... args)
      : taly_eq_(args...) {

    mesh_ = mesh;
    m_surfAssembly = surfAssembly;

    owned_mesh_ = false;

    taly_eq_.p_grid_ = grid();
    taly_eq_.p_data_ = field();

    mat = new TalyMat<Equation, NodeData>(da, &taly_eq_, grid(), field(), ndof, m_surfAssembly);
    vec = new TalyVec<Equation, NodeData>(da, &taly_eq_, grid(), field(), ndof, m_surfAssembly);

    mat->setDimension(domain_min, domain_max);
    vec->setDimension(domain_min, domain_max);

    m_domainMax = domain_max;
    m_domainMin = domain_min;

  }

  inline Point getDomainMin() {
    return m_domainMin;
  }

  inline bool checkSurfAssembly() {
    return m_surfAssembly;
  }

  inline Point getDomainMax() {
    return m_domainMax;
  }

  void setGaussPointVector(std::vector<DENDRITE_UINT> &vGP) {
    mat->setGaussPointVector(vGP);
    vec->setGaussPointVector(vGP);
  }

  virtual ~TalyEquation();

  TALYFEMLIB::GRID *grid();

  TALYFEMLIB::GridField<NodeData> *field();

};

template<typename Equation, typename NodeData>
TalyEquation<Equation, NodeData>::~TalyEquation() {
  delete mat;
  delete vec;
  mat = NULL;
  vec = NULL;

  if (owned_mesh_) {
    delete mesh_;
    mesh_ = NULL;
  }
}

template<typename Equation, typename NodeData>
TALYFEMLIB::GRID *TalyEquation<Equation, NodeData>::grid() {
  return &mesh_->grid;
}

template<typename Equation, typename NodeData>
TALYFEMLIB::GridField<NodeData> *TalyEquation<Equation, NodeData>::field() {
  return &mesh_->field;
}

#endif //DENDRITE2_0_TALYEQUATION_H
