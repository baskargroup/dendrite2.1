//
// Created by maksbh on 11/25/18.
//

#ifndef DENDRITE2_0_GLOBALS_H
#define DENDRITE2_0_GLOBALS_H


class Globals{
    int relOrder_;
    unsigned int bf_;
    unsigned int nsd_;
    bool set_;
public:
    Globals();
    void setGlobals();
    const int getrelOrder();
    const unsigned int getBasisFunction();
    const unsigned int nsd();
    void setrelOrder( const int relOrder);
    void setBasisFunction( const int bf);
    const unsigned int ndof();

};


#endif //DENDRITE2_0_GLOBALS_H
