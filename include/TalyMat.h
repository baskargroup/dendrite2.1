//
// Created by milinda on 11/22/18.
//

#ifndef DENDRITE2_0_TALLYMAT_H
#define DENDRITE2_0_TALLYMAT_H

#include "oda.h"
#include "feMatrix.h"
#include <point.h>
#include "BasisCache.h"
#include "TalyDendroSync.h"
#include <talyfem/grid/grid_types/grid.h>
#include <talyfem/grid/gridfield.h>

#include <talyfem/grid/grid_types/grid.h>
#include <talyfem/grid/gridfield.h>
#include <talyfem/grid/elem.h>
#include <talyfem/grid/elem_types/elem3dhexahedral.h>
#include <talyfem/grid/femelm.h>
#include <talyfem/data_structures/zeroarray.h>
#include <talyfem/data_structures/zeromatrix.h>
#include <TimeInfo.h>
#include <PETSc/VecInfo.h>
#include <FEUtils.h>

#include <TensorOP/Mat.h>

/**@brief Dendrite basic interface class which combines dendro and taly*/
template<class Equation, class NodeData>
class TalyMat : public feMatrix<TalyMat<Equation, NodeData> > {
  TalyDendroSync sync_;
  BasisCache basis_cache_;
  TALYFEMLIB::GRID *taly_grid_;
  TALYFEMLIB::GridField<NodeData> *taly_gf_;
  TALYFEMLIB::ELEM *taly_elem_;

  Equation *taly_eq;
  TALYFEMLIB::FEMElm taly_fe_;
  TALYFEMLIB::ZeroMatrix<DENDRITE_REAL> Ae_;
  DENDRITE_UINT numTotalnodes_;
  DENDRITE_UINT ele_order_;
  DENDRITE_UINT nodal_size_;
  DENDRITE_REAL problemSize_[3];
  DENDRITE_REAL **vec_buff_;
  std::vector<VecInfo> syncValues_;
  TimeInfo *ti_;
  ot::MatRecord mr_;
  bool timeStepping = false;
  DENDRITE_REAL **_val_;
  bool surfaceAssembly_ = false;
  VECType *_out_taly_;
  VECType *_in_taly_;
  FEUtils<Equation, NodeData> *utils_ = new FEUtils<Equation, NodeData>();
  double timer_ = 0;

  /**
   * Linear TalyElement TalyMesh  for linear interpolation of quadratic element
   */

  bool initLinearMap_ = false;
  TALYFEMLIB::ELEM *LinearTalyElem_;
  TALYFEMLIB::GRID LinearTalyGrid_;
  TALYFEMLIB::GridField<NodeData> LinearTalyGridField_;
  double **LinearVal_;
  double *LinearCoords_;
  TALYFEMLIB::ZeroMatrix<DENDRITE_REAL> LinearAe_;

  const DENDRITE_UINT QuadToLinearMap[8][8]
      {{0, 1, 3, 4, 9, 10, 12, 13},
       {1, 2, 4, 5, 10, 11, 13, 14},
       {3, 4, 6, 7, 12, 13, 15, 16},
       {4, 5, 7, 8, 13, 14, 16, 17},
       {9, 10, 12, 13, 18, 19, 21, 22},
       {10, 11, 13, 14, 19, 20, 22, 23},
       {12, 13, 15, 16, 21, 22, 24, 25},
       {13, 14, 16, 17, 22, 23, 25, 26}};
#ifdef TENSOR
    TensorMat::Mat * tensorMat;
    const RefElement * refElement;
    DENDRITE_REAL *mat;
    DENDRITE_REAL * nodalValues_;
#endif
 protected:
  typedef feMatrix<TalyMat<Equation, NodeData> > Parent;
  using Parent::m_uiOctDA;
  using Parent::m_uiDof;
  using Parent::m_uiPtMin;
  using Parent::m_uiPtMax;
  using Parent::setProblemDimensions;
  std::vector<DENDRITE_UINT> m_relOrder;

 public:
  /**@brief: TallyMat constructot */
  TalyMat(ot::DA *da, Equation *eq, TALYFEMLIB::GRID *grid,
          TALYFEMLIB::GridField<NodeData> *gf, unsigned int dof = 1, bool surfAssembly = false);

  /**@biref : TallyMat destructor*/
  ~TalyMat();

  /**@biref elemental matvec*/
  virtual void elementalMatVec(const VECType *in, VECType *out, double *coords = NULL, double scale = 1.0);

  /**@brief things need to be performed before matvec (i.e. coords transform)*/
  bool preMatVec(const VECType *in_dendro, VECType *out_dendro, double scale = 1.0);

  /**@brief things need to be performed after matvec (i.e. coords transform)*/
  bool postMatVec(const VECType *in_dendro, VECType *out_dendro, double scale = 1.0);

  void setDimension(Point &start, Point &end);

  void getDimension(double *dim);

  void getSolution();

  virtual void setPlaceholder(const Vec &v);

  void setVector(const std::vector<VecInfo> &v);

  void getElementalMatrix(unsigned int elemID, std::vector<ot::MatRecord> &records, DENDRITE_REAL *coords_dendro);

  void setTime(TimeInfo *t);

  bool preMat();

  bool postMat();

  void performSurfaceMatAssembly(TALYFEMLIB::FEMElm &fe);

  void getSyncValues(DENDRITE_REAL **out, unsigned int ele, bool isAllocated = true);

  const std::vector<VecInfo> &getSyncValuesVec() const;

  void setGaussPointVector(std::vector<DENDRITE_UINT> &vGP);

  void setFEUtils(FEUtils<Equation, NodeData> *utilsn) {
    utils_ = utilsn;
  }

  void performLinarElementIntegrationForHigherOrderElement(const DENDRITE_REAL *coords);

};

template<class Equation, class NodeData>
TalyMat<Equation, NodeData>::TalyMat(ot::DA *da, Equation *eq, TALYFEMLIB::GRID *grid,
                                     TALYFEMLIB::GridField<NodeData> *gf, unsigned int dof, bool surfAssembly)
    : feMatrix<TalyMat<Equation, NodeData> >(da, dof), taly_eq(eq),
      taly_grid_(grid), taly_gf_(gf), taly_fe_(grid, TALYFEMLIB::BASIS_DEFAULT) {

  taly_elem_ = taly_grid_->elm_array_[0];

  ele_order_ = m_uiOctDA->getElementOrder();
  nodal_size_ = (ele_order_ + 1) * (ele_order_ + 1) * (ele_order_ + 1);

  surfaceAssembly_ = surfAssembly;

  /** Variable number of Gauss Point **/
  m_relOrder.clear();
  m_uiOctDA->createVector(m_relOrder, true, true, 1);
  std::fill(m_relOrder.begin(), m_relOrder.end(), 0);

}
template<class Equation, class NodeData>
void TalyMat<Equation, NodeData>::performLinarElementIntegrationForHigherOrderElement(const DENDRITE_REAL *coords) {
  if (not initLinearMap_) {
    initLinearMap_ = true;
    LinearAe_.redim(8 * m_uiDof, 8 * m_uiDof);
    int node_id_array[8];
    LinearTalyGrid_.redimArrays(8, 1);
    for (int i = 0; i < 8; i++) {
      LinearTalyGrid_.node_array_[i] = new TALYFEMLIB::NODE();
      node_id_array[i] = i;
    }

    LinearTalyElem_ = new TALYFEMLIB::ELEM3dHexahedral();
    LinearTalyGrid_.elm_array_[0] = LinearTalyElem_;

    LinearTalyElem_->redim(8, node_id_array);

    LinearTalyGridField_.redimGrid(&LinearTalyGrid_);
    LinearTalyGridField_.redimNodeData();
    LinearVal_ = new DENDRITE_REAL *[syncValues_.size()];
    for (int i = 0; i < syncValues_.size(); i++) {
      LinearVal_[i] = new DENDRITE_REAL[8 * syncValues_[i].ndof];
    }
    LinearCoords_ = new DENDRITE_REAL[8 * m_uiDim];
  }
  if (ele_order_ == 2) {
    taly_eq->p_grid_ = &LinearTalyGrid_;
    taly_eq->p_data_ = &LinearTalyGridField_;
    int numberOfElements = 8;
    for (DENDRITE_UINT numEle = 0; numEle < numberOfElements; numEle++) {
      LinearAe_.fill(0.0);
      /// Copy Values to linear element
      for (DENDRITE_UINT j = 0; j < syncValues_.size(); j++) {
        for (DENDRITE_UINT i = 0; i < 8; i++) {
          LinearVal_[j][i] = _val_[j][QuadToLinearMap[numEle][i]];
        }
      }
      for (DENDRITE_UINT i = 0; i < 8; i++) {
        LinearCoords_[i * m_uiDim + 0] = coords[QuadToLinearMap[numEle][i] * m_uiDim + 0];
        LinearCoords_[i * m_uiDim + 1] = coords[QuadToLinearMap[numEle][i] * m_uiDim + 1];
        LinearCoords_[i * m_uiDim + 2] = coords[QuadToLinearMap[numEle][i] * m_uiDim + 2];
      }

      sync_.syncCoords<1, m_uiDim>(LinearCoords_, &LinearTalyGrid_);
      for (DENDRITE_UINT i = 0; i < syncValues_.size(); i++) {
        sync_.syncValues<NodeData, 1, m_uiDim>(LinearVal_[i],
                                               &LinearTalyGridField_,
                                               syncValues_[i].ndof,
                                               syncValues_[i].nodeDataIndex);
      }
      TALYFEMLIB::FEMElm fe(&LinearTalyGrid_, TALYFEMLIB::BASIS_ALL);
      fe.refill(0, 0);
      while (fe.next_itg_pt()) {
        taly_eq->Integrands_Ae(fe, LinearAe_);
      }
      for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
          for (int dofi = 0; dofi < m_uiDof; dofi++) {
            for (int dofj = 0; dofj < m_uiDof; dofj++) {
              Ae_(QuadToLinearMap[numEle][i] * m_uiDof + dofi, QuadToLinearMap[numEle][j] * m_uiDof + dofj) +=
                  LinearAe_(i * m_uiDof + dofi, j * m_uiDof + dofj);
            }
          }
        }
      }
    }
  }

}

template<class Equation, class NodeData>
TalyMat<Equation, NodeData>::~TalyMat() {
  double ttime;
  MPI_Reduce(&timer_,&ttime,1,MPI_DOUBLE,MPI_SUM,0,m_uiOctDA->getCommActive());
  TALYFEMLIB::PrintStatus("Time for assembly ", ttime );
  if (initLinearMap_) {
    for (int i = 0; i < 8; i++) {
      delete[] LinearVal_[i];
    }
    delete[] LinearVal_;
    delete[] LinearCoords_;

  }
#ifdef TENSOR
  taly_eq->destroy();
  delete tensorMat;
  delete mat;
#endif
}

template<class Equation, class NodeData>
void TalyMat<Equation, NodeData>::getDimension(DENDRITE_REAL *dim) {
  dim[0] = problemSize_[0];
  dim[1] = problemSize_[1];
  dim[2] = problemSize_[2];

}

template<class Equation, class NodeData>
void TalyMat<Equation, NodeData>::setDimension(Point &start, Point &end) {
  setProblemDimensions(start, end);
  problemSize_[0] = (end.x() - start.x());
  problemSize_[1] = (end.y() - start.y());
  problemSize_[2] = (end.z() - start.z());

}

template<class Equation, class NodeData>
void TalyMat<Equation, NodeData>::setVector(const std::vector<VecInfo> &vecs) {
  if (m_uiOctDA->isActive()) {
    for (unsigned int i = 0; i < vecs.size(); i++) {
      assert ((vecs[i].v != NULL && vecs[i].placeholder == PLACEHOLDER_NONE)
                  || (vecs[i].v == NULL && vecs[i].placeholder != PLACEHOLDER_NONE));
    }

    syncValues_ = vecs;

  }
}

template<class Equation, class NodeData>
void TalyMat<Equation, NodeData>::setTime(TimeInfo *t) {
  ti_ = t;
  timeStepping = true;
}

template<class Equation, class NodeData>
const std::vector<VecInfo> &TalyMat<Equation, NodeData>::getSyncValuesVec() const {
  return syncValues_;
}

template<class Equation, class NodeData>
void TalyMat<Equation, NodeData>::getSyncValues(DENDRITE_REAL **out, unsigned int ele, bool isAllocated) {
  for (int i = 0; i < syncValues_.size(); i++) {
    m_uiOctDA->getElementNodalValues(vec_buff_[i], out[i], ele, syncValues_[i].ndof);
  }
}

template<class Equation, class NodeData>
void TalyMat<Equation, NodeData>::setGaussPointVector(std::vector<DENDRITE_UINT> &vGP) {

  if (vGP.size() != m_relOrder.size()) {
    TALYFEMLIB::TALYException() << "The size of the input and output vector doesn't match\n";
  }
  std::copy(vGP.begin(), vGP.end(), m_relOrder.begin());
}

/****************************************** Matrix free implementation**********************************/



template<class Equation, class NodeData>
bool TalyMat<Equation, NodeData>::preMatVec(const VECType *in_dendro, VECType *out_dendro, double scale) {

  problemSize_[0] = (m_uiPtMax.x() - m_uiPtMin.x());
  problemSize_[1] = (m_uiPtMax.y() - m_uiPtMin.y());
  problemSize_[2] = (m_uiPtMax.z() - m_uiPtMin.z());
  basis_cache_.init(problemSize_, taly_grid_, m_uiMaxDepth, TALYFEMLIB::BASIS_ALL, ele_order_);

  Ae_.redim(nodal_size_ * m_uiDof, nodal_size_ * m_uiDof);
  if (timeStepping == true) {
    taly_eq->set_t(ti_->getCurrentTime());
    taly_eq->set_dt(ti_->getCurrentStep());
  }
  _val_ = new DENDRITE_REAL *[syncValues_.size()];
  for (int i = 0; i < syncValues_.size(); i++) {
    _val_[i] = new DENDRITE_REAL[nodal_size_ * syncValues_[i].ndof];
  }

  vec_buff_ = new DENDRITE_REAL *[syncValues_.size()];

  for (int i = 0; i < syncValues_.size(); i++) {
    m_uiOctDA->nodalVecToGhostedNodal(syncValues_[i].val, vec_buff_[i], false, syncValues_[i].ndof);
    m_uiOctDA->readFromGhostBegin(vec_buff_[i], syncValues_[i].ndof);
    m_uiOctDA->readFromGhostEnd(vec_buff_[i], syncValues_[i].ndof);
  }
  numTotalnodes_ = m_uiOctDA->getTotalNodalSz();

  _in_taly_ = new VECType[nodal_size_ * m_uiDof];
  _out_taly_ = new VECType[nodal_size_ * m_uiDof];

  return (true);
}

template<class Equation, class NodeData>
void TalyMat<Equation, NodeData>::elementalMatVec(const VECType *in_dendro, VECType *out_dendro, double *coords_dendro,
                                                  double scale) {
#ifdef TENSOR
  TALYFEMLIB::TALYException() << " Not supported\n";
#else
  Ae_.fill(0.0);
  taly_elem_->set_elm_id(m_uiOctDA->curr());
  for (int i = 0; i < nodal_size_; i++) {
    convertOctToPhys(&coords_dendro[3 * i], problemSize_);
  }

  if (ele_order_ == 1) {
    sync_.syncCoords<1, m_uiDim>(coords_dendro, taly_grid_);
  } else if (ele_order_ == 2) {
    sync_.syncCoords<2, m_uiDim>(coords_dendro, taly_grid_);
  }
  //TODO: To be optimized with MiniTaly
  for (int i = 0; i < syncValues_.size(); i++) {
    m_uiOctDA->getElementNodalValues(vec_buff_[i], _val_[i], m_uiOctDA->curr(), syncValues_[i].ndof);
    if (ele_order_ == 1) {
      sync_.syncValues<NodeData, 1, m_uiDim>(_val_[i], taly_gf_, syncValues_[i].ndof, syncValues_[i].nodeDataIndex);
    } else if (ele_order_ == 2) {
      sync_.syncValues<NodeData, 2, m_uiDim>(_val_[i], taly_gf_, syncValues_[i].ndof, syncValues_[i].nodeDataIndex);
    }
  }

  for (unsigned int i = 0; i < basis_cache_.n_itg_pts(); i++) {
    const TALYFEMLIB::FEMElm *fe = basis_cache_.get(m_uiOctDA->getLevel(m_uiOctDA->curr()), i,
                                                    taly_grid_->GetNode(0)->location());
    taly_eq->Integrands_Ae(*fe, Ae_);

  }
  if (surfaceAssembly_) {
    performSurfaceMatAssembly(taly_fe_);
  }

  if (ele_order_ == 1) {
    sync_.DendroToTalyIn<1, 3>(in_dendro, _in_taly_, m_uiDof);
  } else if (ele_order_ == 2) {
    sync_.DendroToTalyIn<2, 3>(in_dendro, _in_taly_, m_uiDof);
  }

  for (int i = 0; i < nodal_size_ * m_uiDof; i++) {
    _out_taly_[i] = 0;
  }

  for (int i = 0; i < nodal_size_ * m_uiDof; i++) {
    for (int j = 0; j < nodal_size_ * m_uiDof; j++) {
      _out_taly_[i] += _in_taly_[j] * Ae_(i, j);
    }
  }

  if (ele_order_ == 1) {
    sync_.TalyToDendroIn<1, m_uiDim>(_out_taly_, out_dendro, m_uiDof);
  } else if (ele_order_ == 2) {
    sync_.TalyToDendroIn<2, m_uiDim>(_out_taly_, out_dendro, m_uiDof);
  }
#endif
}

template<class Eqaution, class NodeData>
bool TalyMat<Eqaution, NodeData>::postMatVec(const VECType *in_dendro, VECType *out_dendro, double scale) {
  for (int i = 0; i < syncValues_.size(); i++) {
    delete[] vec_buff_[i];
    delete[] _val_[i];
  }
  delete[] vec_buff_;
  delete[] _in_taly_;
  delete[] _out_taly_;
  delete _val_;
}

/************************* Matrix implementation goes here ***************************************************/

template<class Equation, class NodeData>
bool TalyMat<Equation, NodeData>::preMat() {

  problemSize_[0] = (m_uiPtMax.x() - m_uiPtMin.x());
  problemSize_[1] = (m_uiPtMax.y() - m_uiPtMin.y());
  problemSize_[2] = (m_uiPtMax.z() - m_uiPtMin.z());
  basis_cache_.init(problemSize_, taly_grid_, m_uiMaxDepth, TALYFEMLIB::BASIS_ALL, ele_order_, 0);

  Ae_.redim(nodal_size_ * m_uiDof, nodal_size_ * m_uiDof);
  if (m_uiOctDA->isActive()) {
    for (unsigned int i = 0; i < syncValues_.size(); i++) {
      if (syncValues_[i].placeholder == PLACEHOLDER_NONE) {
        VecGetArrayRead(syncValues_[i].v, &syncValues_[i].val);
      }
    }
  }
  if (timeStepping == true) {
    taly_eq->set_t(ti_->getCurrentTime());
    taly_eq->set_dt(ti_->getCurrentStep());
  }
  _val_ = new DENDRITE_REAL *[syncValues_.size()];
  for (int i = 0; i < syncValues_.size(); i++) {
    _val_[i] = new DENDRITE_REAL[nodal_size_ * syncValues_[i].ndof];
  }

  vec_buff_ = new DENDRITE_REAL *[syncValues_.size()];

  for (int i = 0; i < syncValues_.size(); i++) {
    m_uiOctDA->nodalVecToGhostedNodal(syncValues_[i].val, vec_buff_[i], false, syncValues_[i].ndof);
    m_uiOctDA->readFromGhostBegin(vec_buff_[i], syncValues_[i].ndof);
    m_uiOctDA->readFromGhostEnd(vec_buff_[i], syncValues_[i].ndof);
  }
  numTotalnodes_ = m_uiOctDA->getTotalNodalSz();

  return (true);
}

template<class Equation, class NodeData>
void TalyMat<Equation, NodeData>::getElementalMatrix(unsigned int elemID, std::vector<ot::MatRecord> &records,
                                                     DENDRITE_REAL *coords_dendro) {
  PetscLogEvent elementalMat;
  PetscLogEventRegister("elementalMatAssembly",0,&elementalMat);
  PetscLogEventBegin(elementalMat,0,0,0,0);
#ifdef TENSOR
    auto start = std::chrono::high_resolution_clock::now();

  memset(mat,0, sizeof(DENDRITE_REAL)*nodal_size_*nodal_size_*m_uiDof*m_uiDof);
    for(int i = 0; i < nodal_size_; i++) {
      convertOctToPhys(&coords_dendro[m_uiDim*i], problemSize_);
    }
    tensorMat->setScale(coords_dendro,ele_order_);
    if(syncValues_.size() == 0) {
      taly_eq->IntegrandsAe(tensorMat, mat, nullptr, coords_dendro);
    }
    else{
      for(int i = 0; i < syncValues_.size(); i++) {
        m_uiOctDA->getElementNodalValues(vec_buff_[i], &nodalValues_[nodal_size_*syncValues_[i].nodeDataIndex], m_uiOctDA->curr(), syncValues_[i].ndof);
      }
      taly_eq->IntegrandsAe(tensorMat, mat, nodalValues_, coords_dendro);

    }

  DendroIntL *node_idx_dendro = new DendroIntL[nodal_size_];
  m_uiOctDA->getNodeIndices(node_idx_dendro, m_uiOctDA->curr(), true);
  /**
   * @author maksbh
   * [Warning:] Do not play around with this ordering of the loop.
   * Dendro5 interpolation assumes that first all enteries of
   * single dof is stored.
   */
  for (int dofi = 0; dofi < m_uiDof; dofi++) {
    for (int dofj = 0; dofj < m_uiDof; dofj++) {
      for (int i = 0; i < nodal_size_; i++) {
        for (int j = 0; j < nodal_size_; j++) {
          mr_.setRowID(node_idx_dendro[i]);
          mr_.setColID(node_idx_dendro[j]);
          mr_.setColDim(dofj);
          mr_.setRowDim(dofi);
          const int dof = dofi*m_uiDof+dofj;
          mr_.setMatValue(mat[dof*nodal_size_*nodal_size_+i*nodal_size_+j]);
          records.push_back(mr_);
        }
      }
    }
  }
  auto finish = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(finish - start);
  timer_+= time_span.count();
  delete [] node_idx_dendro;

#else
    auto start = std::chrono::high_resolution_clock::now();
    Ae_.fill(0.0);
    taly_elem_->set_elm_id(m_uiOctDA->curr());

    for (int i = 0; i < nodal_size_; i++) {
      convertOctToPhys(&coords_dendro[3 * i], problemSize_);
    }
    DendroIntL *node_idx_dendro = new DendroIntL[nodal_size_];


    m_uiOctDA->getNodeIndices(node_idx_dendro, m_uiOctDA->curr(), true);


    if(ele_order_ == 1) {
      sync_.syncCoords<1,m_uiDim>(coords_dendro,taly_grid_);
    } else if(ele_order_ == 2){
      sync_.syncCoords<2,m_uiDim>(coords_dendro,taly_grid_);
    }
    //TODO: To be optimized with MiniTaly

    for (int i = 0; i < syncValues_.size(); i++) {
      m_uiOctDA->getElementNodalValues(vec_buff_[i], _val_[i], m_uiOctDA->curr(), syncValues_[i].ndof);
      if (ele_order_ == 1) {
        sync_.syncValues<NodeData, 1, m_uiDim>(_val_[i], taly_gf_, syncValues_[i].ndof, syncValues_[i].nodeDataIndex);
      } else if (ele_order_ == 2) {
        sync_.syncValues<NodeData, 2, m_uiDim>(_val_[i], taly_gf_, syncValues_[i].ndof, syncValues_[i].nodeDataIndex);
      }
    }
    DENDRITE_UINT relativeOrder = m_relOrder[m_uiOctDA->curr()];
    if(relativeOrder == VariableGP::RefineBySplitting){
      utils_->init(taly_grid_,taly_gf_,taly_eq,coords_dendro,ele_order_);
      utils_->performPartition();
      utils_->performIntegrationAe(Ae_);
    }
    else {
#ifdef CLENSHAW_CURTIS
      TALYFEMLIB::FEMElm fe(taly_grid_, TALYFEMLIB::BASIS_ALL);
        fe.refill(0, relativeOrder);
        while (fe.next_itg_pt()) {
            taly_eq->Integrands_Ae(fe, Ae_);
        }
#else
      if(relativeOrder == 0) {

        for (unsigned int i = 0; i < basis_cache_.n_itg_pts(); i++) {
          const TALYFEMLIB::FEMElm *fe = basis_cache_.get(m_uiOctDA->getLevel(m_uiOctDA->curr()), i,
                                                          taly_grid_->GetNode(0)->location());
          taly_eq->Integrands_Ae(*fe, Ae_);
        }
      }
      else {
        TALYFEMLIB::FEMElm fe(taly_grid_, TALYFEMLIB::BASIS_ALL);
        fe.refill(0, relativeOrder);
        while (fe.next_itg_pt()){
          taly_eq->Integrands_Ae(fe, Ae_);
        }
      }
#endif
    }
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(finish - start);
    timer_+= time_span.count();
    if(surfaceAssembly_){
      performSurfaceMatAssembly(taly_fe_);
    }

    /**
     * @author maksbh
     * [Warning:] Do not play around with this ordering of the loop.
     * Dendro5 interpolation assumes that first all enteries of
     * single dof is stored.
     */
    for (int dofi = 0; dofi < m_uiDof; dofi++) {
      for (int dofj = 0; dofj < m_uiDof; dofj++) {
        for (int i = 0; i < nodal_size_; i++) {
          for (int j = 0; j < nodal_size_; j++) {
            mr_.setRowID(node_idx_dendro[i]);
            mr_.setColID(node_idx_dendro[j]);
            mr_.setColDim(dofj);
            mr_.setRowDim(dofi);
            mr_.setMatValue(Ae_(m_uiDof*i + dofi, j*m_uiDof + dofj ));

            records.push_back(mr_);

          }
        }
      }
    }

    delete[] node_idx_dendro;

#endif
    PetscLogEventEnd(elementalMat,0,0,0,0);

}

template<class Eqaution, class NodeData>
bool TalyMat<Eqaution, NodeData>::postMat() {
  for (int i = 0; i < syncValues_.size(); i++) {
    delete[] vec_buff_[i];
    delete[] _val_[i];
  }
  delete[] vec_buff_;
  delete _val_;
#ifdef TENSOR
  delete [] nodalValues_;
#endif
  return (true);
}

/**************************** Extra Utils for Surface Assembly ************************************/
template<class Eqaution, class NodeData>
void TalyMat<Eqaution, NodeData>::performSurfaceMatAssembly(TALYFEMLIB::FEMElm &fe) {

  const int *surf_arr = taly_elem_->GetSurfaceCheckArray();
  const int surf_row_len = taly_elem_->GetSurfaceCheckArrayRowLength();
  const int nodes_per_surf = taly_elem_->GetNodesPerSurface();
  for (int i = 0; i < taly_elem_->GetSurfaceCount(); i++) {
    int surf_id = surf_arr[i * surf_row_len];  // id determines which nodes on the surface

    // note: loop assumes elements have 8 nodes and use an identity map, so we skip using elem->ElemToLocalNodeID
    unsigned int flags = ~0u;  // initialize to all true
    TALYFEMLIB::ZEROPTV position;

    for (unsigned int n = 0; n < nodes_per_surf && flags != 0; n++) {
      int elem_node_id = surf_arr[i * surf_row_len + 1 + n];
      // remove any surface flags that all nodes do not have in common
      // skipping elem->ElemToLocalNodeID because identity map!
      position = taly_grid_->GetNode(elem_node_id)->location();
      flags = flags & sync_.get_surf_flags(position, problemSize_);

    }

    if (flags != 0) {
      for (unsigned int side_idx = 1; side_idx <= 6; side_idx++) {
        if (flags & (1u << side_idx)) {

          // TODO set up basis cache for surfaces (should also cache normals...)
          TALYFEMLIB::SurfaceIndicator surf(surf_id);
          surf.set_normal(taly_elem_->CalculateNormal(taly_grid_, surf_id));

          if (ele_order_ == 1) {
            fe.refill_surface(taly_elem_, &surf, TALYFEMLIB::BASIS_LINEAR, 0);
          } else if (ele_order_ == 2) {
            fe.refill_surface(taly_elem_, &surf, TALYFEMLIB::BASIS_QUADRATIC, 0);
          }

          // loop over surface gauss points
          while (fe.next_itg_pt()) {
            taly_eq->Integrands4side_Ae(taly_fe_, side_idx, Ae_);
          }
        }
      }
    }
  }
}
/************************* Petsc dependent utilities *******************************************/

template<class Equation, class NodeData>
void TalyMat<Equation, NodeData>::setPlaceholder(const Vec &v) {
  if (v == NULL) {
    return;
  }
  for (unsigned int i = 0; i < syncValues_.size(); i++) {
    if (syncValues_[i].placeholder == PLACEHOLDER_GUESS) {
      VecGetArrayRead(v, &syncValues_[i].val);

    }
  }
}

#endif //DENDRITE2_0_TALLYMAT_H
