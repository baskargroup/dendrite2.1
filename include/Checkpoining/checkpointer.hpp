//
// Created by makrand on 10/4/19.
//


#pragma once

#include <string>
#include <checkPoint.h>
#include "petscvec.h"
#include "TimeInfo.h"
#include <oda.h>
#include "PETSc/VecInfo.h"
#include <sys/stat.h>
#include <ftw.h>
#include <IBM/Geom.h>

class checkpointer {

 private:
  int n_backups_ = 1;
  int frequency_ = 10;
  int current_checkpoint_ = 0;
  std::string file_prefix_ = "Checkpoint";
  int eleOrder_ = 1;
  /** Number of active processor */
  int r_numActiveProc_;
  int r_numVecs;
  std::vector<int> dof_; // Dof corresponding to each vecs.
  std::vector<int> nodeDataIndex_;

  /**
	 * Write a PETSc Vec to a file. Unlike PETSc's built-in VecView routines, the per-process row counts are also
	 * included in the file to ensure the structure exactly matches when loaded. This also means that this file can
	 * only be read by the same number of MPI tasks that created it.
	 * @param filename file to write to (will be overwritten if it exists)
	 * @param vecs list of vecs to save
	 * @param comm MPI communicator (usually PETSC_COMM_WORLD)
	 */
  void write_values(const ot::DA *da, std::vector<VecInfo> &vecs, const std::string &filename);

  /**
	 *
	 * @param filename filename
	 * @param treeNodes treeNodes that octree is built
	 * @param comm global Comm
	 */
  void read_octree(const std::string &filename, std::vector<ot::TreeNode> &treeNodes);

  /**
   * Called automatically when string checkpointing
   * @param da octDA
   * @param filename filename = "oct_+ activeProcId
  */
  void write_octree(const ot::DA *da, const std::string &filename);

  /**
   * Called automatically when loading checkpoint
   * @param da The octDA
   * @param filename filename
   * @param vec  vectrors
   * @param comm only active Comm calls this.
   */

  void read_values(ot::DA *da, const std::string &filename, std::vector<VecInfo> &vec);

  /**
   * Called automatically when storing checkpoint
   * @param da DA
   * @param filename filename = 'cfg_backup'
   * @param numVecs number of ves
   * @param ti timeInfo
   */
  void write_cfg(const ot::DA *da, const std::string &filename, const int numVecs, const TimeInfo *ti);
  /**
 * Called automatically when loading checkpoint
 * @param filename filename = 'cfg_backup'
 * @param numVecs number of ves
 * @param ti timeInfo
 */
  void read_cfg(const std::string &filename, TimeInfo *ti);

 public:
  /**
   * How often calls to checkpoint() should actually write data.
   * Default is 10 (every 10 calls, write a checkpoint file).
   * @param freq how often to write checkpoint data
   */
  inline void set_checkpoint_frequency(int freq) { frequency_ = freq; }

  /**
   * Number of backup checkpoint files to keep.
   * Backups are named as prefix.bak1, prefix.bak2, etc. (higher number = further into the past).
   * You should keep at least one backup, as checkpointing is not atomic (i.e. if you are
   * using no backups and checkpoint() call is interrupted, the old data has already been partially overwritten).
   * The default is 1.
   * @param n_backups number of backups to keep
   */
  inline void set_n_backups(int n_backups) { n_backups_ = n_backups; }

  inline void set_filename_prefix(const std::string &prefix) { file_prefix_ = prefix; }

  /**
   * Save the octree shape and some accompanying data vectors.
   * Only actually writes checkpoint data every frequency calls.
   * @param[in] da  The DA you want to write for CheckPointing
   * @param[in] vecs The vecs you want to sync
   * @param[in] ti TimeInfo context
   */
  void storeCheckpoint(const ot::DA *da, std::vector<VecInfo> &vecs, TimeInfo *ti);

  /**
   * Store extra geometries info (for IBM)
   */
  template<typename NodeData>
  void storeCheckpoint(const ot::DA *da,
                       std::vector<VecInfo> &vecs,
                       TimeInfo *ti,
                       const std::vector<Geom<NodeData> *> &geoms);

  /**
   *
   * @param da Octda
   * @param vecs_out Vectors loaded from checkpointing. The allocation happens inside the function
   * @param ti_out TimeInfo
   * @param comm Global Comm
   */

  void loadFromCheckPointing(ot::DA *&da, std::vector<VecInfo> &vecs_out, TimeInfo *ti_out);

  /**
   * load extra info for geometry (for IBM)
   */

  template<typename NodeData>
  void loadFromCheckPointing(ot::DA *&da,
                             vector<VecInfo> &vecs_out,
                             TimeInfo *ti_out,
                             std::vector<Geom<NodeData> *> geoms);

  /**
   * Rename files for backups to ensure atomic updates.
   * Remove old folder and rename new folder.
   */
  void shift_backups();

  /**
   * Return "/Checkpoint_{i}" folder name, create "/Checkpoint" if not exist
   */
  std::string get_backup_folder_name(int i) const;

  /**
   * save geometry state (for IBM)
   * @tparam NodeData
   * @param filename
   * @param geoms
   */
  template<typename NodeData>
  void write_geom(const string &filename, const std::vector<Geom<NodeData> *> geoms);

  /**
   * load geometry state (for IBM)
   * @tparam NodeData
   * @param filename
   * @param geoms
   */
  template<typename NodeData>
  void read_geom(const string &filename, std::vector<Geom<NodeData> *> geoms);

};

/**
 * Store extra geometries info (for IBM)
 */
template<typename NodeData>
void checkpointer::storeCheckpoint(const ot::DA *da,
                                   std::vector<VecInfo> &vecs,
                                   TimeInfo *ti,
                                   const std::vector<Geom<NodeData> *> &geoms) {
  // original checkpoint
  checkpointer::storeCheckpoint(da, vecs, ti);
  if (current_checkpoint_ % frequency_ != 0) {
    return;
  }
  // additional checkpoint
  MPI_Barrier(da->getCommActive());
  auto foldername = get_backup_folder_name(0);
  std::string filename_geom = foldername + "/" + file_prefix_ + "_geom_backup";
  write_geom(filename_geom, geoms);

};

/** String Utils **/
template<typename NodeData>
void checkpointer::loadFromCheckPointing(ot::DA *&da,
                                         std::vector<VecInfo> &vecs_out,
                                         TimeInfo *ti_out,
                                         std::vector<Geom<NodeData> *> geoms) {
  // original load
  loadFromCheckPointing(da, vecs_out, ti_out);
  // additional load
  std::string foldername = file_prefix_;
  std::string filename_geom = foldername + "/" + file_prefix_ + "_geom_backup";
  read_geom(filename_geom, geoms);
};

template<typename NodeData>
void checkpointer::write_geom(const std::string &filename, const std::vector<Geom<NodeData> *> geoms) {

  if (TALYFEMLIB::GetMPIRank() == 0) {
    libconfig::Config cfg;
    auto &cfgGeomList = cfg.getRoot().add("geom_state", libconfig::Setting::TypeList);
    for (unsigned i = 0; i < geoms.size(); i++) {
      auto &cfgGeom = cfgGeomList.add(libconfig::Setting::TypeGroup);
      libconfig::Setting &com_array = cfgGeom.add("center_of_mass", libconfig::Setting::TypeArray);
      libconfig::Setting &rot_row_1 = cfgGeom.add("rot_row_1", libconfig::Setting::TypeArray);
      libconfig::Setting &rot_row_2 = cfgGeom.add("rot_row_2", libconfig::Setting::TypeArray);
      libconfig::Setting &rot_row_3 = cfgGeom.add("rot_row_3", libconfig::Setting::TypeArray);
      libconfig::Setting &L = cfgGeom.add("L", libconfig::Setting::TypeArray);
      libconfig::Setting &P = cfgGeom.add("P", libconfig::Setting::TypeArray);
      libconfig::Setting &v = cfgGeom.add("v", libconfig::Setting::TypeArray);
      libconfig::Setting &omega = cfgGeom.add("omega", libconfig::Setting::TypeArray);
      for (int dim = 0; dim < 3; dim++) {
        com_array.add(libconfig::Setting::TypeFloat) = geoms[i]->sixDof_.ks_.back().x[dim];
        rot_row_1.add(libconfig::Setting::TypeFloat) = geoms[i]->sixDof_.ks_.back().R[0](dim);
        rot_row_2.add(libconfig::Setting::TypeFloat) = geoms[i]->sixDof_.ks_.back().R[1](dim);
        rot_row_3.add(libconfig::Setting::TypeFloat) = geoms[i]->sixDof_.ks_.back().R[2](dim);
        L.add(libconfig::Setting::TypeFloat) = geoms[i]->sixDof_.ks_.back().L[dim];
        P.add(libconfig::Setting::TypeFloat) = geoms[i]->sixDof_.ks_.back().P[dim];
        v.add(libconfig::Setting::TypeFloat) = geoms[i]->sixDof_.ks_.back().v[dim];
        omega.add(libconfig::Setting::TypeFloat) = geoms[i]->sixDof_.ks_.back().omega[dim];
      }
    }
    cfg.writeFile(filename.c_str());
  }
}

template<typename NodeData>
void checkpointer::read_geom(const std::string &filename, std::vector<Geom<NodeData> *> geoms) {
  libconfig::Config cfg;
  cfg.readFile(filename.c_str());
  const auto &geometries = cfg.getRoot()["geom_state"];
  assert(geometries.getLength() == geoms.size());
  for (unsigned int i = 0; i < geoms.size(); i++) {
    geoms[i]->sixDof_.ks_.back().x = ZEROPTV((double) geometries[i]["center_of_mass"][0],
                                      (double) geometries[i]["center_of_mass"][1],
                                      (double) geometries[i]["center_of_mass"][2]);
    geoms[i]->sixDof_.ks_.back().R[0] = ZEROPTV((double) geometries[i]["rot_row_1"][0],
                                         (double) geometries[i]["rot_row_1"][1],
                                         (double) geometries[i]["rot_row_1"][2]);
    geoms[i]->sixDof_.ks_.back().R[1] = ZEROPTV((double) geometries[i]["rot_row_2"][0],
                                         (double) geometries[i]["rot_row_2"][1],
                                         (double) geometries[i]["rot_row_2"][2]);
    geoms[i]->sixDof_.ks_.back().R[2] = ZEROPTV((double) geometries[i]["rot_row_3"][0],
                                         (double) geometries[i]["rot_row_3"][1],
                                         (double) geometries[i]["rot_row_3"][2]);
    geoms[i]->sixDof_.ks_.back().L = ZEROPTV((double) geometries[i]["L"][0],
                                      (double) geometries[i]["L"][1],
                                      (double) geometries[i]["L"][2]);
    geoms[i]->sixDof_.ks_.back().P = ZEROPTV((double) geometries[i]["P"][0],
                                      (double) geometries[i]["P"][1],
                                      (double) geometries[i]["P"][2]);
    geoms[i]->sixDof_.ks_.back().v = ZEROPTV((double) geometries[i]["v"][0],
                                      (double) geometries[i]["v"][1],
                                      (double) geometries[i]["v"][2]);
    geoms[i]->sixDof_.ks_.back().omega = ZEROPTV((double) geometries[i]["omega"][0],
                                          (double) geometries[i]["omega"][1],
                                          (double) geometries[i]["omega"][2]);
    geoms[i]->sixDof_.ks_.back().LinearDisplacementUpdate = geoms[i]->sixDof_.ks_.back().x - geoms[i]->kI.center;
    std::vector<ZEROPTV> RotationMatrixUpdate = geoms[i]->sixDof_.ks_.back().R;
    geoms[i]->sixDof_.ks_.back().RotationMatrixUpdate = RotationMatrixUpdate;
    if (!geoms[i]->def_.is_static) {
      geoms[i]->sixDofToKinematicInfo();
      geoms[i]->updateGeoLocation();
      geoms[i]->updateNodeVel();
    }

  }
}
