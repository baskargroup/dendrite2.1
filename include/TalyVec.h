//
// Created by maksbh on 11/26/18.
//

#ifndef DENDRITE2_0_TALYVEC_H
#define DENDRITE2_0_TALYVEC_H

#include "oda.h"
#include "feVec.h"
#include <point.h>
#include "BasisCache.h"
#include "TalyDendroSync.h"
#include <talyfem/grid/grid_types/grid.h>
#include <talyfem/grid/gridfield.h>

#include <talyfem/grid/grid_types/grid.h>
#include <talyfem/grid/gridfield.h>
#include <talyfem/grid/elem.h>
#include <talyfem/grid/elem_types/elem3dhexahedral.h>
#include <talyfem/grid/femelm.h>
#include <talyfem/data_structures/zeroarray.h>
#include <talyfem/data_structures/zeromatrix.h>
#include <feVector.h>
#include <TimeInfo.h>
#include <PETSc/VecInfo.h>
#include <FEUtils.h>

template<class Equation, class NodeData>
class TalyVec : public feVector<TalyVec<Equation, NodeData> > {
  TalyDendroSync sync_;
  BasisCache basis_cache_;
  TALYFEMLIB::GRID *taly_grid_;
  TALYFEMLIB::GridField<NodeData> *taly_gf_;
  TALYFEMLIB::ELEM *taly_elem_;

  Equation *taly_eq;
  TALYFEMLIB::FEMElm taly_fe_;
  TALYFEMLIB::ZEROARRAY<DENDRITE_REAL> be_;
  DENDRITE_UINT ele_order_;
  DENDRITE_REAL problemSize_[3];
  DENDRITE_UINT nodal_size_;
  DENDRITE_UINT numTotalnodes_;
  std::vector<VecInfo> syncValues_;
  DENDRITE_REAL **vec_buff_;
  TimeInfo *ti_;
  bool timeStepping = false;
  DENDRITE_REAL **_val_;

  bool surfaceAssembly_ = false;
  FEUtils<Equation, NodeData> *utils_ = new FEUtils<Equation, NodeData>();

  const DENDRITE_UINT QuadToLinearMap[8][8]
      {{0, 1, 3, 4, 9, 10, 12, 13},
       {1, 2, 4, 5, 10, 11, 13, 14},
       {3, 4, 6, 7, 12, 13, 15, 16},
       {4, 5, 7, 8, 13, 14, 16, 17},
       {9, 10, 12, 13, 18, 19, 21, 22},
       {10, 11, 13, 14, 19, 20, 22, 23},
       {12, 13, 15, 16, 21, 22, 24, 25},
       {13, 14, 16, 17, 22, 23, 25, 26}};

  /**
  * Linear TalyElement TalyMesh  for linear interpolation of quadratic element
  */

  bool initLinearMap_ = false;
  TALYFEMLIB::ELEM *LinearTalyElem_;
  TALYFEMLIB::GRID LinearTalyGrid_;
  TALYFEMLIB::GridField<NodeData> LinearTalyGridField_;
  double **LinearVal_;
  double *LinearCoords_;
  TALYFEMLIB::ZEROARRAY<DENDRITE_REAL> LinearBe_;

 protected:
  typedef feVector<TalyVec<Equation, NodeData> > Parent;
  using Parent::m_uiOctDA;
  using Parent::m_uiDof;
  using Parent::m_uiPtMin;
  using Parent::m_uiPtMax;
  using Parent::setProblemDimensions;
  std::vector<DENDRITE_UINT> m_relOrder;

 public:

  TalyVec(ot::DA *da, Equation *eq, TALYFEMLIB::GRID *grid,
          TALYFEMLIB::GridField<NodeData> *gf, unsigned int dof = 1, bool surfAssembly = false);

  ~TalyVec();

  virtual void elementalComputVec(const VECType *in, VECType *out, double *coords = NULL, double scale = 1.0);

  bool preComputeVec(const VECType *in_dendro, VECType *out_dendro, double scale = 1.0);

  bool postComputeVec(const VECType *in_dendro, VECType *out_dendro, double scale = 1.0);

  void setDimension(Point &start, Point &end);

  virtual void setPlaceholder(const Vec &v);

  void setVector(const std::vector<VecInfo> &v);

  void setTime(TimeInfo *ti);

  void performSurfaceMatAssembly(TALYFEMLIB::FEMElm &fe);

  void getSyncValues(DENDRITE_REAL **out, unsigned int ele, bool isAllocated = true);

  const std::vector<VecInfo> &getSyncValuesVec() const;

  void setGaussPointVector(std::vector<DENDRITE_UINT> &vGP);

  void setFEUtils(FEUtils<Equation, NodeData> *utilsn);

  void performLinarElementIntegrationForHigherOrderElement(const DENDRITE_REAL *coords);

};

template<class Equation, class NodeData>
TalyVec<Equation, NodeData>::TalyVec(ot::DA *da, Equation *eq, TALYFEMLIB::GRID *grid,
                                     TALYFEMLIB::GridField<NodeData> *gf, unsigned int dof, bool surfAssembly)
    :feVector<TalyVec<Equation, NodeData> >(da, dof), taly_eq(eq),
     taly_grid_(grid), taly_gf_(gf), taly_fe_(grid, TALYFEMLIB::BASIS_DEFAULT) {
  taly_elem_ = taly_grid_->elm_array_[0];
  ele_order_ = m_uiOctDA->getElementOrder();
  nodal_size_ = (ele_order_ + 1) * (ele_order_ + 1) * (ele_order_ + 1);
  problemSize_[0] = (m_uiPtMax.x() - m_uiPtMin.x());
  problemSize_[1] = (m_uiPtMax.y() - m_uiPtMin.y());
  problemSize_[2] = (m_uiPtMax.z() - m_uiPtMin.z());

  surfaceAssembly_ = surfAssembly;
  /** Variable number of Gauss Point **/
  m_relOrder.clear();
  m_uiOctDA->createVector(m_relOrder, true, true, 1);
  std::fill(m_relOrder.begin(), m_relOrder.end(), 0);

}
template<class Equation, class NodeData>
void TalyVec<Equation, NodeData>::performLinarElementIntegrationForHigherOrderElement(const DENDRITE_REAL *coords) {
  if (not initLinearMap_) {
    initLinearMap_ = true;
    LinearBe_.redim(8 * m_uiDof);
    int node_id_array[8];
    LinearTalyGrid_.redimArrays(8, 1);
    for (int i = 0; i < 8; i++) {
      LinearTalyGrid_.node_array_[i] = new TALYFEMLIB::NODE();
      node_id_array[i] = i;
    }

    LinearTalyElem_ = new TALYFEMLIB::ELEM3dHexahedral();
    LinearTalyGrid_.elm_array_[0] = LinearTalyElem_;

    LinearTalyElem_->redim(8, node_id_array);


    LinearTalyGridField_.redimGrid(&LinearTalyGrid_);
    LinearTalyGridField_.redimNodeData();
    LinearVal_ = new DENDRITE_REAL *[syncValues_.size()];
    for (int i = 0; i < syncValues_.size(); i++) {
      LinearVal_[i] = new DENDRITE_REAL[8 * syncValues_[i].ndof];
    }
    LinearCoords_ = new DENDRITE_REAL[8 * m_uiDim];
  }
  if (ele_order_ == 2) {
    taly_eq->p_grid_ = &LinearTalyGrid_;
    taly_eq->p_data_ = &LinearTalyGridField_;
    int numberOfElements = 8;
    for (DENDRITE_UINT numEle = 0; numEle < numberOfElements; numEle++) {
      LinearBe_.fill(0.0);
      /// Copy Values to linear element
      for (DENDRITE_UINT j = 0; j < syncValues_.size(); j++) {
        for (DENDRITE_UINT i = 0; i < 8; i++) {
          LinearVal_[j][i] = _val_[j][QuadToLinearMap[numEle][i]];
        }
      }
      for (DENDRITE_UINT i = 0; i < 8; i++) {
        LinearCoords_[i * m_uiDim + 0] = coords[QuadToLinearMap[numEle][i] * m_uiDim + 0];
        LinearCoords_[i * m_uiDim + 1] = coords[QuadToLinearMap[numEle][i] * m_uiDim + 1];
        LinearCoords_[i * m_uiDim + 2] = coords[QuadToLinearMap[numEle][i] * m_uiDim + 2];
      }

      sync_.syncCoords<1, m_uiDim>(LinearCoords_, &LinearTalyGrid_);
      for (DENDRITE_UINT i = 0; i < syncValues_.size(); i++) {
        sync_.syncValues<NodeData, 1, m_uiDim>(LinearVal_[i],
                                               &LinearTalyGridField_,
                                               syncValues_[i].ndof,
                                               syncValues_[i].nodeDataIndex);
      }
      TALYFEMLIB::FEMElm fe(&LinearTalyGrid_, TALYFEMLIB::BASIS_ALL);
      fe.refill(0, 0);
      while (fe.next_itg_pt()) {
        taly_eq->Integrands_be(fe, LinearBe_);
      }
      for (int i = 0; i < 8; i++) {
        for (int dofi = 0; dofi < m_uiDof; dofi++) {
          be_(QuadToLinearMap[numEle][i] * m_uiDof + dofi) += LinearBe_(i * m_uiDof + dofi);
        }
      }
    }
  }

}
template<class Equation, class NodeData>
TalyVec<Equation, NodeData>::~TalyVec() {
  if(initLinearMap_){
    for(int i = 0; i < 8; i++){
      delete[] LinearVal_[i];
    }
    delete [] LinearVal_;
    delete [] LinearCoords_;

  }
}

template<class Equation, class NodeData>
const std::vector<VecInfo> &TalyVec<Equation, NodeData>::getSyncValuesVec() const {
  return syncValues_;
}

template<class Equation, class NodeData>
void TalyVec<Equation, NodeData>::getSyncValues(DENDRITE_REAL **out, unsigned int ele, bool isAllocated) {
  for (int i = 0; i < syncValues_.size(); i++) {
    m_uiOctDA->getElementNodalValues(vec_buff_[i], out[i], ele, syncValues_[i].ndof);
  }
}
template<class Equation, class NodeData>
void TalyVec<Equation, NodeData>::setFEUtils(FEUtils<Equation, NodeData> *utilsn) {
  utils_ = utilsn;
}

template<class Equation, class NodeData>
bool TalyVec<Equation, NodeData>::preComputeVec(const VECType *in_dendro, VECType *out_dendro, double scale) {

  problemSize_[0] = (m_uiPtMax.x() - m_uiPtMin.x());
  problemSize_[1] = (m_uiPtMax.y() - m_uiPtMin.y());
  problemSize_[2] = (m_uiPtMax.z() - m_uiPtMin.z());

  basis_cache_.init(problemSize_, taly_grid_, m_uiMaxDepth, TALYFEMLIB::BASIS_ALL, ele_order_, 0);

  be_.redim(nodal_size_ * m_uiDof);

  if (timeStepping == true) {
    taly_eq->set_t(ti_->getCurrentTime());
    taly_eq->set_dt(ti_->getCurrentStep());
  }
  if (m_uiOctDA->isActive()) {
    for (unsigned int i = 0; i < syncValues_.size(); i++) {
      if (syncValues_[i].placeholder == PLACEHOLDER_NONE) {
        VecGetArrayRead(syncValues_[i].v, &syncValues_[i].val);
      }
    }
  }
  _val_ = new DENDRITE_REAL *[syncValues_.size()];
  for (int i = 0; i < syncValues_.size(); i++) {
    _val_[i] = new DENDRITE_REAL[nodal_size_ * syncValues_[i].ndof];
  }

  vec_buff_ = new DENDRITE_REAL *[syncValues_.size()];

  for (int i = 0; i < syncValues_.size(); i++) {
    m_uiOctDA->nodalVecToGhostedNodal(syncValues_[i].val, vec_buff_[i], false, syncValues_[i].ndof);
    m_uiOctDA->readFromGhostBegin(vec_buff_[i], syncValues_[i].ndof);
    m_uiOctDA->readFromGhostEnd(vec_buff_[i], syncValues_[i].ndof);
  }

  numTotalnodes_ = m_uiOctDA->getTotalNodalSz();

  return (true);
}

template<class Equation, class NodeData>
void
TalyVec<Equation, NodeData>::elementalComputVec(const VECType *in_dendro, VECType *out_dendro, double *coords_dendro,
                                                double scale) {
  taly_elem_->set_elm_id(m_uiOctDA->curr());
  be_.fill(0.0);

  for (int i = 0; i < nodal_size_; i++) {
    convertOctToPhys(&coords_dendro[3 * i], problemSize_);
  }

  if (ele_order_ == 1) {
    sync_.syncCoords<1, m_uiDim>(coords_dendro, taly_grid_);
  } else if (ele_order_ == 2) {
    sync_.syncCoords<2, m_uiDim>(coords_dendro, taly_grid_);
  }

  //TODO: To be optimized with MiniTaly
  for (int i = 0; i < syncValues_.size(); i++) {
    m_uiOctDA->getElementNodalValues(vec_buff_[i], _val_[i], m_uiOctDA->curr(), syncValues_[i].ndof);
    if (ele_order_ == 1) {
      sync_.syncValues<NodeData, 1, m_uiDim>(_val_[i], taly_gf_, syncValues_[i].ndof,
                                             syncValues_[i].nodeDataIndex);
    } else if (ele_order_ == 2) {
      sync_.syncValues<NodeData, 2, m_uiDim>(_val_[i], taly_gf_, syncValues_[i].ndof,
                                             syncValues_[i].nodeDataIndex);
    }
  }

  DENDRITE_UINT relativeOrder = m_relOrder[m_uiOctDA->curr()];
  if((relativeOrder == VariableGP::QuadraticByLinear) and (ele_order_ > 1) ){
    if(ele_order_ == 1){
      throw TALYFEMLIB::TALYException() << "This operation is not supported for linear basis function" << "\n";
      MPI_Abort(MPI_COMM_WORLD,EXIT_FAILURE);
    }
    performLinarElementIntegrationForHigherOrderElement(coords_dendro);
    taly_eq->p_grid_ = taly_grid_;
    taly_eq->p_data_ = taly_gf_;
  }
  else if (relativeOrder == VariableGP::RefineBySplitting) {
    utils_->init(taly_grid_, taly_gf_, taly_eq, coords_dendro, ele_order_);
    utils_->performPartition();
    utils_->performIntegrationBe(be_);
  } else {
#ifdef CLENSHAW_CURTIS
    TALYFEMLIB::FEMElm fe(taly_grid_, TALYFEMLIB::BASIS_ALL);
    fe.refill(0, relativeOrder);
    while (fe.next_itg_pt()) {
        taly_eq->Integrands_be(fe, be_);
    }
#else
    if (relativeOrder == 0) {
      for (unsigned int i = 0; i < basis_cache_.n_itg_pts(); i++) {
        const TALYFEMLIB::FEMElm *fe = basis_cache_.get(m_uiOctDA->getLevel(m_uiOctDA->curr()), i,
                                                        taly_grid_->GetNode(0)->location());
        taly_eq->Integrands_be(*fe, be_);

      }
    } else {
      TALYFEMLIB::FEMElm fe(taly_grid_, TALYFEMLIB::BASIS_ALL);
      fe.refill(0, relativeOrder);
      while (fe.next_itg_pt()) {
        taly_eq->Integrands_be(fe, be_);
      }

    }
#endif
  }
  if (surfaceAssembly_) {
    performSurfaceMatAssembly(taly_fe_);
  }

  for (int i = 0; i < nodal_size_; i++) {
    for (int dof = 0; dof < m_uiDof; dof++) {
      out_dendro[nodal_size_ * dof + i] = be_(i * m_uiDof + dof);
    }

  }

}

template<class Equation, class NodeData>
bool TalyVec<Equation, NodeData>::postComputeVec(const VECType *in_dendro, VECType *out_dendro, double scale) {

  for (int i = 0; i < syncValues_.size(); i++) {
    delete[] vec_buff_[i];
    delete[] _val_[i];
  }
  delete[] vec_buff_;
  delete _val_;
}

template<class Equation, class NodeData>
void TalyVec<Equation, NodeData>::setDimension(Point &start, Point &end) {
  setProblemDimensions(start, end);
}

template<class Equation, class NodeData>
void TalyVec<Equation, NodeData>::setVector(const std::vector<VecInfo> &vecs) {
  for (unsigned int i = 0; i < vecs.size(); i++) {
    assert ((vecs[i].v != NULL && vecs[i].placeholder == PLACEHOLDER_NONE)
                || (vecs[i].v == NULL && vecs[i].placeholder != PLACEHOLDER_NONE));
  }

  syncValues_ = vecs;
}

template<class Equation, class NodeData>
void TalyVec<Equation, NodeData>::setTime(TimeInfo *t) {
  ti_ = t;
  timeStepping = true;
}

template<class Equation, class NodeData>
void TalyVec<Equation, NodeData>::setGaussPointVector(std::vector<DENDRITE_UINT> &vGP) {

  if (vGP.size() != m_relOrder.size()) {
    TALYFEMLIB::TALYException() << "The size of the input and output vector doesn't match\n";
  }
  std::copy(vGP.begin(), vGP.end(), m_relOrder.begin());
}

/**************************** Extra Utils for Surface Assembly ************************************/
template<class Eqaution, class NodeData>
void TalyVec<Eqaution, NodeData>::performSurfaceMatAssembly(TALYFEMLIB::FEMElm &fe) {

  const int *surf_arr = taly_elem_->GetSurfaceCheckArray();
  const int surf_row_len = taly_elem_->GetSurfaceCheckArrayRowLength();
  const int nodes_per_surf = taly_elem_->GetNodesPerSurface();
  for (int i = 0; i < taly_elem_->GetSurfaceCount(); i++) {
    int surf_id = surf_arr[i * surf_row_len];  // id determines which nodes on the surface

    // note: loop assumes elements have 8 nodes and use an identity map, so we skip using elem->ElemToLocalNodeID
    unsigned int flags = ~0u;  // initialize to all true
    TALYFEMLIB::ZEROPTV position;

    for (unsigned int n = 0; n < nodes_per_surf && flags != 0; n++) {
      int elem_node_id = surf_arr[i * surf_row_len + 1 + n];
      // remove any surface flags that all nodes do not have in common
      // skipping elem->ElemToLocalNodeID because identity map!
      position = taly_grid_->GetNode(elem_node_id)->location();
      flags = flags & sync_.get_surf_flags(position, problemSize_);

    }

    if (flags != 0) {
      for (unsigned int side_idx = 1; side_idx <= 6; side_idx++) {
        if (flags & (1u << side_idx)) {

          // TODO set up basis cache for surfaces (should also cache normals...)
          TALYFEMLIB::SurfaceIndicator surf(surf_id);
          surf.set_normal(taly_elem_->CalculateNormal(taly_grid_, surf_id));
          if (ele_order_ == 1) {
            fe.refill_surface(taly_elem_, &surf, TALYFEMLIB::BASIS_LINEAR, 0);
          } else if (ele_order_ == 2) {
            fe.refill_surface(taly_elem_, &surf, TALYFEMLIB::BASIS_QUADRATIC, 0);
          }
          // loop over surface gauss points
          while (fe.next_itg_pt()) {
            taly_eq->Integrands4side_be(taly_fe_, side_idx, be_);
          }
        }
      }
    }
  }
}
/************************* Petsc dependent utilities *******************************************/
#ifdef BUILD_WITH_PETSC

template<class Equation, class NodeData>
void TalyVec<Equation, NodeData>::setPlaceholder(const Vec &v) {
  if (v == NULL) {
    return;
  }
  for (unsigned int i = 0; i < syncValues_.size(); i++) {
    if (syncValues_[i].placeholder == PLACEHOLDER_GUESS) {
      VecGetArrayRead(v, &syncValues_[i].val);

    }
  }
}

#endif
#endif //DENDRITE2_0_TALYVEC_H
