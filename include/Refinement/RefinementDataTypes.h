//
// Created by maksbh on 6/20/19.
//

#ifndef DENDRITE2_0_REFINEMENTDATATYPES_H
#define DENDRITE2_0_REFINEMENTDATATYPES_H

#define MAXNEIGHBOURSIZE 4
#define NUMFACES 6


#include <DataTypes.h>
#include <talyfem/talyfem.h>
class octVolume{
	bool init_ = false;
 public:
	/** Soln values in the order [0,1,...dof,0,1,...dof]**/
	DENDRITE_REAL * soln;

	DENDRITE_REAL * coords;

	void init(const unsigned int nodalSize, const unsigned int ndof)
	{
		init_ = true;
		soln = new DENDRITE_REAL[nodalSize*ndof];
		coords = new DENDRITE_REAL[nodalSize*m_uiDim];
	}
	~octVolume(){
		if(init_) {
			delete[] soln;
			delete[] coords;
		}
	}
};
/**
 * This class contains the information about the
 * level of neighbour faces and the neighbour values.
 */
class octNeighbours{
	bool init_ = false;

 public:
	/// Level of neighbour cells.
	std::vector<DENDRITE_UINT> neighbourLevel;
	/// A book keeping to how many of the real values to be checked.
	std::vector<DENDRITE_UINT> numNeighbour;
	/// The actual neighbour values
	std::vector<std::vector<DENDRITE_REAL> > neighbourValues;
	std::vector<std::vector<DENDRITE_REAL> > neighbourCoords;
	std::vector<std::vector<DENDRITE_UINT > > neighbourID;

	void init(const unsigned int nodalSize, const unsigned int ndof){

		if(not(init_)) {
			neighbourLevel.resize(NUMFACES);
			numNeighbour.resize(NUMFACES);
			neighbourValues.resize(NUMFACES);
			neighbourCoords.resize(NUMFACES);
			neighbourID.resize(NUMFACES);
			for (int i = 0; i < NUMFACES; i++) {
				neighbourValues[i].resize(nodalSize * ndof * MAXNEIGHBOURSIZE);
				neighbourCoords[i].resize(nodalSize * MAXNEIGHBOURSIZE * 3);
				neighbourID[i].resize(MAXNEIGHBOURSIZE);
			}
		}

		init_ = true;
	}

	octNeighbours & operator = (const octNeighbours & neighbours) {
		assert(neighbours.init_== true);
		this->init_= neighbours.init_;
		this->neighbourLevel = neighbours.neighbourLevel;
		this->numNeighbour = neighbours.numNeighbour;
		for(int i = 0; i < NUMFACES; i++){
			this->neighbourValues[i] = neighbours.neighbourValues[i];
			this->neighbourCoords[i] = neighbours.neighbourCoords[i];
			this->neighbourID[i] =neighbours.neighbourID[i];
		}
		return *this;
	}

};

class octSurface{
	bool init_ = false;
 public:

	/** Soln in the order [0,1,...dof,0,1,...dof]**/
	DENDRITE_REAL * soln;

	/** ValueFEM [0,1,...dof]**/
	DENDRITE_REAL * valueFEM;

	DENDRITE_REAL * valueDerivativeFEM;

	DENDRITE_REAL * coords;

	TALYFEMLIB::ZEROPTV normal;

	void init(const unsigned int nodalSize, const unsigned int ndof, const int numSurfGaussPoint = 4)
	{
		init_ = true;
		soln = new DENDRITE_REAL[nodalSize*ndof];
		valueFEM = new DENDRITE_REAL[ndof];
		valueDerivativeFEM = new DENDRITE_REAL[ndof*nodalSize*m_uiDim];
		coords = new DENDRITE_REAL[nodalSize*m_uiDim];
	}
	~octSurface(){
		if(init_) {
			delete[] soln;
			delete[] valueFEM;
			delete[] valueDerivativeFEM;
			delete[] coords;
		}
	}
};


#endif //DENDRITE2_0_REFINEMENTDATATYPES_H
