//
// Created by maksbh on 3/11/19.
//

#ifndef DENDRITE2_0_REFINEMENT_H
#define DENDRITE2_0_REFINEMENT_H

#include <oda.h>
#include <DataTypes.h>
#include <OctToPhysical.h>
#include <TalyDendroSync.h>
#include <TalyEquation.h>
#include <PETSc/Solver/LinearSolver.h>
#include <DendriteUtils.h>
#include <PETSc/Solver/NonLinearSolver.h>
#include <PETSc/PetscUtils.h>
#include <Refinement/RefinementDataTypes.h>
using namespace PETSc;

/**
 * @brief The side ids, used in Neighbour calculation
 */
enum SIDE : short {
  DLEFT = 0,
  DRIGHT = 1,
  DTOP = 2,
  DBOTTOM = 3,
  DFRONT = 4,
  DBACK = 5,
};
/**
 * @brief Three types of refinement:
 * 1. Position based . Ex: IBM -> refine near the walls
 * 2. Value Based . Ex: Cahn - Hillard : refine near the interface based on the value of \phi
 * 3. Value_Neigbour_based: Value of the current octant along with the neighbour values. Used for block adaptivity
 * 3. Aposteriori error estimate based.
 */
enum RefineType : short {
  POSITION_BASED = 0,
  VALUE_BASED = 1,
  VALUE_NEIGHBOUR_BASED = 2,
  APOSTERIORI = 3

};

static unsigned int getEqualNodeWeight(const ot::TreeNode * t) {
  return 1;
}

class Refinement {

 private:

  /**
   * @ brief  interpNeighbourCoords interpolates the coordinates of neighbour grid.
   * This is used with RefineType = APOSTERIORI and surfacecalc = true.
   * The instantiaiton is done through template values.
   */
  template<const DENDRITE_UINT order, const DENDRITE_UINT dim, const SIDE side, typename std::enable_if<
      order == 1 and dim == 3 and side == SIDE::DLEFT>::type * = nullptr>
  void interpNeighbourCoords(const int sz);

  template<const DENDRITE_UINT order, const DENDRITE_UINT dim, const SIDE side, typename std::enable_if<
      order == 1 and dim == 3 and side == SIDE::DRIGHT>::type * = nullptr>
  void interpNeighbourCoords(const int sz);

  template<const DENDRITE_UINT order, const DENDRITE_UINT dim, const SIDE side, typename std::enable_if<
      order == 1 and dim == 3 and side == SIDE::DTOP>::type * = nullptr>
  void interpNeighbourCoords(const int sz);

  template<const DENDRITE_UINT order, const DENDRITE_UINT dim, const SIDE side, typename std::enable_if<
      order == 1 and dim == 3 and side == SIDE::DBOTTOM>::type * = nullptr>
  void interpNeighbourCoords(const int sz);

  template<const DENDRITE_UINT order, const DENDRITE_UINT dim, const SIDE side, typename std::enable_if<
      order == 1 and dim == 3 and side == SIDE::DFRONT>::type * = nullptr>
  void interpNeighbourCoords(const int sz);

  template<const DENDRITE_UINT order, const DENDRITE_UINT dim, const SIDE side, typename std::enable_if<
      order == 1 and dim == 3 and side == SIDE::DBACK>::type * = nullptr>
  void interpNeighbourCoords(const int sz);

  /** refine type: POSITION_BASED, VALUE_BASED or APOSTERIORI**/
  RefineType refineType_;

  /** @brief: This loops through localElement of each of the processor
   *           and calculates the refinement flags based on aposteriori
   *           error estimates. This function is called based on constructor
   *           arguments.
  **/

  void calcAposterioriRefinementVector();

  /** @brief: This loops through localElement of each of the processor
   *           and calculates the refinement flags based on position.
  **/

  void calcPositionBasedRefinementVector();
  /** @brief: This loops through localElement of each of the processor
    *           and calculates the refinement flags based on the values of
    *           the solution at the given location.
  **/
  void calcValueBasedRefinementVector();

  /** Temporary variable to store vector buffer **/
  DENDRITE_REAL **vec_buff_;
  DENDRITE_REAL *soln_;

  TalyDendroSync sync_;

  octSurface thisSurface_;
  octSurface neighbourSurface_;
  octVolume thisVolume_;
  octNeighbours octNeighbourValues_;
  /***
   *  Temp variables for neighbour valFEM and ValDerivativeFEM calculation
   */
  DENDRITE_REAL *_valFEMCurrent_;
  DENDRITE_REAL *_valFEMNeighbour_;

  DENDRITE_REAL **_valDFEMCurrent_;
  DENDRITE_REAL **_valDFEMNeighbour_;

  /** The mapping for surfaces **/
  const int NeighbourLeftMapOrder1[8]{1, -1, 3, -1, 5, -1, 6, -1};
  const int NeighbourRightMapOrder1[8]{-1, 0, -1, 2, -1, 4, -1, 6};
  const int NeighbourBottomMapOrder1[8]{2, 3, -1, -1, 6, 7, -1, -1};
  const int NeighbourTopMapOrder1[8]{-1, -1, 0, 1, -1, -1, 4, 5};
  const int NeighbourFrontMapOrder1[8]{-1, -1, -1, -1, 0, 1, 2, 3};
  const int NeighbourBackMapOrder1[8]{4, 5, 6, 7, -1, -1, -1, -1};

  /// The Neighbour list so  that the Gauss point surface matches.
  const int NeighbourTransfer[6][4]{{1, 0, 3, 2},
                                    {1, 0, 3, 2},
                                    {2, 3, 0, 1},
                                    {2, 3, 0, 1},
                                    {0, 1, 2, 3},
                                    {0, 1, 2, 3}};

  const int TalySurfIndicator[6]{-1, 1, 2, -2, 3, -3}; /// Normal to left, Right, Top, Bottom, Front, Back

  /** Neighbour to TalySurfIndicator **/
  const int NeighbourSurIndicator[6]{SIDE::DRIGHT, SIDE::DLEFT, SIDE::DBOTTOM, SIDE::DTOP, SIDE::DBACK, SIDE::DFRONT};

  /**
   * @ brief : Performs cleanup operation.
   */
  void cleanup();

  /**
   * This is the fix for coarsening.
   * Basically if all the childrens are marked for coarsening, then only coarsen.
   * Thanks Milinda.
   */

  void checkForCoarsening();

 protected:
  /**The old DA which we want to refine**/
  ot::DA *baseDA_;

  /**The vector of elemental length of baseDA. This will hold the refinement flag**/
  ot::DA_FLAGS::Refine *refinementVector_;

  std::vector<VecInfo> solutionVec_;

  /**The number of degree of freedom **/
  DENDRITE_UINT ndof_ = 1;

  /**The problem size**/
  DENDRITE_REAL problemSize_[m_uiDim];

  /**Number of nodes per element**/
  DENDRITE_UINT nPe_;
  /**Element order**/
  int eleOrder_;

  bool surfaceErrorCalc_ = false;

  double totalElementError_ = 0.0;
  double totalSurfaceError_ = 0.0;




  /**
    * @brief: The user has to specify the refinement strategy only on the values.
    * The values can be accesed by using "octVol.soln" and coordinates using "octVol.coords"
    * Used in Value Based Refinement
    * @return the elemental error which is accumulated in "totalElementalError"
  **/
  virtual DENDRITE_REAL calcElementalError(const octVolume &octVol) {
    throw TALYFEMLIB::TALYException() << "You need to override " << __func__ << "\n";
  }

  /**
   * @brief: ElementalError that one need to override to fill the refinementVector
   * The user has to specify the refinement strategy only on individual Gauss point.
   * Used in Aposterirori Error estimates
   * @return the elemental error which is accumulated in "totalElementalError"
  **/
  virtual DENDRITE_REAL calcElementalError(TALYFEMLIB::FEMElm &fe, const octVolume &octVol) {
    throw TALYFEMLIB::TALYException() << "You need to override " << __func__ << "\n";
  }

  /**
    * @brief: The user has to specify the refinement strategy based on the value of Gauss point
    * of the surface.
    * Used in Aposterirori Error estimates
    * @return the surface error which is accumulated in "totalSurfaceError"
  **/
  virtual DENDRITE_REAL calcSurfaceError(const TALYFEMLIB::ZEROPTV & pos,
                                         const octSurface &current, const octSurface &neighbour) {
    throw TALYFEMLIB::TALYException() << "You need to override " << __func__ << "\n";
  }

  /**
   * Pure virtual function.
   * Need to override in for all Refine Type
   * * @return Refine flags: COARSEN, NO_CHANGE or REFINE.
   */

  virtual ot::DA_FLAGS::Refine calcRefineFlags() = 0;



  /**
   *
   * @param fe:        fe surface object of the current grid.
   * @param fe_neigh : fe neighbour surface object of the  neighbour grid.
   * @param elem     : Current Taly element
   * @param elem_neigh : Neighbour TalyElement
   * @param grid    : Current grid object
   * @param grid_neigh : Neighbour grid object
   * @return  : surface error after calling calcSurfaceError(const TALYFEMLIB::FEMElm &fe,
                                         const octSurface &current, const octSurface &neighbour).
   */

  DENDRITE_REAL calcSurfError(TALYFEMLIB::FEMElm &fe, TALYFEMLIB::FEMElm &fe_neigh, const TALYFEMLIB::ELEM *elem,
                              const TALYFEMLIB::ELEM *elem_neigh, const TALYFEMLIB::GRID &grid,
                              const TALYFEMLIB::GRID &grid_neigh, const SIDE);

  /**
   *
   * @return The total error: totalElementalError + totalSurfaceError
   */
  DENDRITE_REAL getTotalError();

 public:

  /**
   *
   * @param da The base DA
   * @param in Vectors to use during refinement.
   * @param problemSize The problem size
   * @param refineType Can be Value Based or Aposteriori based
   * @param surfaceError true or false, depending on weather to include the surface error or
   */
  Refinement(ot::DA *da,
             std::vector<VecInfo> &in,
             const DENDRITE_REAL *problemSize,
             const RefineType refineType = RefineType::VALUE_BASED,
             const bool surfaceError = false);

  /**
   * @brief: This constructor is used for position based refinement
   * @param da The base DA
   * @param problemSize The problem size
   *
   */
  Refinement(ot::DA *da, const DENDRITE_REAL *problemSize);

  ~Refinement();

  /** @brief: This loops through localElement of each of the processor
   *           and calculates the refinement flags
   *
  **/
  void calcRefinementVector();

  /**
   * @return the newDA after refinement.
   */

  ot::DA *getRefineDA(unsigned int (*getWeight)(const ot::TreeNode *) = NULL,const DENDRITE_UINT grainSz = 100) {
    ot::DA *newDA = NULL;
    if(getWeight == NULL){
      newDA = baseDA_->remesh(refinementVector_, baseDA_->getLocalElemSz(),grainSz);
    }
    else{
      newDA = baseDA_->remesh(refinementVector_, baseDA_->getLocalElemSz(),grainSz,0.3,2,getWeight);
    }

    if (newDA == NULL) {
      newDA = baseDA_;
    }
    return newDA;
  }

	/**
	 *  @brief Returns the type of refinement being used
	 *  @return: Refinetype used for refinement
	 */
	RefineType getRefineType() {
		return refineType_;
	}

	/**
	 * This function is to give the neighbour values and level of the neighbour.
	 * @param octNeighbours The class to get the neighbour values
	 * of the octant
	 */
	void getNeigbourValues(octNeighbours & octNeighbours);


  /**
   *
   * @param newDA new refined DA
   * @param in The old Vector
   * @param out The
   * @param ndof
   */
  void performIntergridTransfer(ot::DA *newDA, const Vec &in, Vec &out, int ndof);

  void finalizeAMR(ot::DA *newDA);

  void interploateNeighboursLeft(const double *u, const double *coords, const NeighbourLevel &level);

  void interploateNeighboursRight(const double *u, const double *coords, const NeighbourLevel &level);

  void interploateNeighboursBottom(const double *u, const double *coords, const NeighbourLevel &level);

  void interploateNeighboursTop(const double *u, const double *coords, const NeighbourLevel &level);

  void interploateNeighboursFront(const double *u, const double *coords, const NeighbourLevel &level);

  void interploateNeighboursBack(const double *u, const double *coords, const NeighbourLevel &level);

  void performVectorExchange(const ot::DA *newDA, Vec & oldDAvec, Vec &newDAvec, const DENDRITE_UINT ndof, const bool destroyAllocatedOutVec = true);

  void writeRefineDatatoPvtu(const char * fname = NULL);


};

template<const DENDRITE_UINT order, const DENDRITE_UINT dim, const SIDE side, typename std::enable_if<
    order == 1 and dim == 3 and side == SIDE::DLEFT>::type *>
void Refinement::interpNeighbourCoords(const int sz1) {
  neighbourSurface_.coords[0] = thisVolume_.coords[0] - sz1;
  neighbourSurface_.coords[1] = thisVolume_.coords[1];
  neighbourSurface_.coords[2] = thisVolume_.coords[2];
  neighbourSurface_.coords[3] = thisVolume_.coords[0];
  neighbourSurface_.coords[4] = thisVolume_.coords[1];
  neighbourSurface_.coords[5] = thisVolume_.coords[2];
  neighbourSurface_.coords[6] = thisVolume_.coords[6] - sz1;
  neighbourSurface_.coords[7] = thisVolume_.coords[7];
  neighbourSurface_.coords[8] = thisVolume_.coords[8];
  neighbourSurface_.coords[9] = thisVolume_.coords[6];
  neighbourSurface_.coords[10] = thisVolume_.coords[7];
  neighbourSurface_.coords[11] = thisVolume_.coords[8];
  neighbourSurface_.coords[12] = thisVolume_.coords[12] - sz1;
  neighbourSurface_.coords[13] = thisVolume_.coords[13];
  neighbourSurface_.coords[14] = thisVolume_.coords[14];
  neighbourSurface_.coords[15] = thisVolume_.coords[12];
  neighbourSurface_.coords[16] = thisVolume_.coords[13];
  neighbourSurface_.coords[17] = thisVolume_.coords[14];
  neighbourSurface_.coords[18] = thisVolume_.coords[18] - sz1;
  neighbourSurface_.coords[19] = thisVolume_.coords[19];
  neighbourSurface_.coords[20] = thisVolume_.coords[20];
  neighbourSurface_.coords[21] = thisVolume_.coords[18];
  neighbourSurface_.coords[22] = thisVolume_.coords[19];
  neighbourSurface_.coords[23] = thisVolume_.coords[20];
}

template<const DENDRITE_UINT order, const DENDRITE_UINT dim, const SIDE side, typename std::enable_if<
    order == 1 and dim == 3 and side == SIDE::DRIGHT>::type *>
void Refinement::interpNeighbourCoords(const int sz1) {
  neighbourSurface_.coords[0] = thisVolume_.coords[3];
  neighbourSurface_.coords[1] = thisVolume_.coords[4];
  neighbourSurface_.coords[2] = thisVolume_.coords[5];
  neighbourSurface_.coords[3] = thisVolume_.coords[3] + sz1;
  neighbourSurface_.coords[4] = thisVolume_.coords[4];
  neighbourSurface_.coords[5] = thisVolume_.coords[5];
  neighbourSurface_.coords[6] = thisVolume_.coords[9];
  neighbourSurface_.coords[7] = thisVolume_.coords[10];
  neighbourSurface_.coords[8] = thisVolume_.coords[11];
  neighbourSurface_.coords[9] = thisVolume_.coords[9] + sz1;
  neighbourSurface_.coords[10] = thisVolume_.coords[10];
  neighbourSurface_.coords[11] = thisVolume_.coords[11];
  neighbourSurface_.coords[12] = thisVolume_.coords[15];
  neighbourSurface_.coords[13] = thisVolume_.coords[16];
  neighbourSurface_.coords[14] = thisVolume_.coords[17];
  neighbourSurface_.coords[15] = thisVolume_.coords[15] + sz1;
  neighbourSurface_.coords[16] = thisVolume_.coords[16];
  neighbourSurface_.coords[17] = thisVolume_.coords[17];
  neighbourSurface_.coords[18] = thisVolume_.coords[21];
  neighbourSurface_.coords[19] = thisVolume_.coords[22];
  neighbourSurface_.coords[20] = thisVolume_.coords[23];
  neighbourSurface_.coords[21] = thisVolume_.coords[21] + sz1;
  neighbourSurface_.coords[22] = thisVolume_.coords[22];
  neighbourSurface_.coords[23] = thisVolume_.coords[23];
}

template<const DENDRITE_UINT order, const DENDRITE_UINT dim, const SIDE side, typename std::enable_if<
    order == 1 and dim == 3 and side == SIDE::DTOP>::type *>
void Refinement::interpNeighbourCoords(const int sz1) {
  neighbourSurface_.coords[0] = thisVolume_.coords[6];
  neighbourSurface_.coords[1] = thisVolume_.coords[7];
  neighbourSurface_.coords[2] = thisVolume_.coords[8];
  neighbourSurface_.coords[3] = thisVolume_.coords[9];
  neighbourSurface_.coords[4] = thisVolume_.coords[10];
  neighbourSurface_.coords[5] = thisVolume_.coords[11];
  neighbourSurface_.coords[6] = thisVolume_.coords[6];
  neighbourSurface_.coords[7] = thisVolume_.coords[7] + sz1;
  neighbourSurface_.coords[8] = thisVolume_.coords[8];
  neighbourSurface_.coords[9] = thisVolume_.coords[9];
  neighbourSurface_.coords[10] = thisVolume_.coords[10] + sz1;
  neighbourSurface_.coords[11] = thisVolume_.coords[11];
  neighbourSurface_.coords[12] = thisVolume_.coords[18];
  neighbourSurface_.coords[13] = thisVolume_.coords[19];
  neighbourSurface_.coords[14] = thisVolume_.coords[20];
  neighbourSurface_.coords[15] = thisVolume_.coords[21];
  neighbourSurface_.coords[16] = thisVolume_.coords[22];
  neighbourSurface_.coords[17] = thisVolume_.coords[23];
  neighbourSurface_.coords[18] = thisVolume_.coords[18];
  neighbourSurface_.coords[19] = thisVolume_.coords[19] + sz1;
  neighbourSurface_.coords[20] = thisVolume_.coords[20];
  neighbourSurface_.coords[21] = thisVolume_.coords[21];
  neighbourSurface_.coords[22] = thisVolume_.coords[22] + sz1;
  neighbourSurface_.coords[23] = thisVolume_.coords[23];

}

template<const DENDRITE_UINT order, const DENDRITE_UINT dim, const SIDE side, typename std::enable_if<
    order == 1 and dim == 3 and side == SIDE::DBOTTOM>::type *>
void Refinement::interpNeighbourCoords(const int sz1) {
  neighbourSurface_.coords[0] = thisVolume_.coords[0];
  neighbourSurface_.coords[1] = thisVolume_.coords[1] - sz1;
  neighbourSurface_.coords[2] = thisVolume_.coords[2];
  neighbourSurface_.coords[3] = thisVolume_.coords[3];
  neighbourSurface_.coords[4] = thisVolume_.coords[4] - sz1;
  neighbourSurface_.coords[5] = thisVolume_.coords[5];
  neighbourSurface_.coords[6] = thisVolume_.coords[0];
  neighbourSurface_.coords[7] = thisVolume_.coords[1];
  neighbourSurface_.coords[8] = thisVolume_.coords[2];
  neighbourSurface_.coords[9] = thisVolume_.coords[3];
  neighbourSurface_.coords[10] = thisVolume_.coords[4];
  neighbourSurface_.coords[11] = thisVolume_.coords[5];
  neighbourSurface_.coords[12] = thisVolume_.coords[12];
  neighbourSurface_.coords[13] = thisVolume_.coords[13] - sz1;
  neighbourSurface_.coords[14] = thisVolume_.coords[14];
  neighbourSurface_.coords[15] = thisVolume_.coords[15];
  neighbourSurface_.coords[16] = thisVolume_.coords[16] - sz1;
  neighbourSurface_.coords[17] = thisVolume_.coords[17];
  neighbourSurface_.coords[18] = thisVolume_.coords[12];
  neighbourSurface_.coords[19] = thisVolume_.coords[13];
  neighbourSurface_.coords[20] = thisVolume_.coords[14];
  neighbourSurface_.coords[21] = thisVolume_.coords[15];
  neighbourSurface_.coords[22] = thisVolume_.coords[16];
  neighbourSurface_.coords[23] = thisVolume_.coords[17];

}
template<const DENDRITE_UINT order, const DENDRITE_UINT dim, const SIDE side, typename std::enable_if<
    order == 1 and dim == 3 and side == SIDE::DFRONT>::type *>
void Refinement::interpNeighbourCoords(const int sz1) {
  neighbourSurface_.coords[0] = thisVolume_.coords[12];
  neighbourSurface_.coords[1] = thisVolume_.coords[13];
  neighbourSurface_.coords[2] = thisVolume_.coords[14];
  neighbourSurface_.coords[3] = thisVolume_.coords[15];
  neighbourSurface_.coords[4] = thisVolume_.coords[16];
  neighbourSurface_.coords[5] = thisVolume_.coords[17];
  neighbourSurface_.coords[6] = thisVolume_.coords[18];
  neighbourSurface_.coords[7] = thisVolume_.coords[19];
  neighbourSurface_.coords[8] = thisVolume_.coords[20];
  neighbourSurface_.coords[9] = thisVolume_.coords[21];
  neighbourSurface_.coords[10] = thisVolume_.coords[22];
  neighbourSurface_.coords[11] = thisVolume_.coords[23];
  neighbourSurface_.coords[12] = thisVolume_.coords[12];
  neighbourSurface_.coords[13] = thisVolume_.coords[13];
  neighbourSurface_.coords[14] = thisVolume_.coords[14] + sz1;
  neighbourSurface_.coords[15] = thisVolume_.coords[15];
  neighbourSurface_.coords[16] = thisVolume_.coords[16];
  neighbourSurface_.coords[17] = thisVolume_.coords[17] + sz1;
  neighbourSurface_.coords[18] = thisVolume_.coords[18];
  neighbourSurface_.coords[19] = thisVolume_.coords[19];
  neighbourSurface_.coords[20] = thisVolume_.coords[20] + sz1;
  neighbourSurface_.coords[21] = thisVolume_.coords[21];
  neighbourSurface_.coords[22] = thisVolume_.coords[22];
  neighbourSurface_.coords[23] = thisVolume_.coords[23] + sz1;

}

template<const DENDRITE_UINT order, const DENDRITE_UINT dim, const SIDE side, typename std::enable_if<
    order == 1 and dim == 3 and side == SIDE::DBACK>::type *>
void Refinement::interpNeighbourCoords(const int sz1) {

  neighbourSurface_.coords[0] = thisVolume_.coords[12];
  neighbourSurface_.coords[1] = thisVolume_.coords[13];
  neighbourSurface_.coords[2] = thisVolume_.coords[14] - sz1;
  neighbourSurface_.coords[3] = thisVolume_.coords[15];
  neighbourSurface_.coords[4] = thisVolume_.coords[16];
  neighbourSurface_.coords[5] = thisVolume_.coords[17] - sz1;
  neighbourSurface_.coords[6] = thisVolume_.coords[18];
  neighbourSurface_.coords[7] = thisVolume_.coords[19];
  neighbourSurface_.coords[8] = thisVolume_.coords[20] - sz1;
  neighbourSurface_.coords[9] = thisVolume_.coords[21];
  neighbourSurface_.coords[10] = thisVolume_.coords[22];
  neighbourSurface_.coords[11] = thisVolume_.coords[23] - sz1;
  neighbourSurface_.coords[12] = thisVolume_.coords[12];
  neighbourSurface_.coords[13] = thisVolume_.coords[13];
  neighbourSurface_.coords[14] = thisVolume_.coords[14];
  neighbourSurface_.coords[15] = thisVolume_.coords[15];
  neighbourSurface_.coords[16] = thisVolume_.coords[16];
  neighbourSurface_.coords[17] = thisVolume_.coords[17];
  neighbourSurface_.coords[18] = thisVolume_.coords[18];
  neighbourSurface_.coords[19] = thisVolume_.coords[19];
  neighbourSurface_.coords[20] = thisVolume_.coords[20];
  neighbourSurface_.coords[21] = thisVolume_.coords[21];
  neighbourSurface_.coords[22] = thisVolume_.coords[22];
  neighbourSurface_.coords[23] = thisVolume_.coords[23];

}

#endif //DENDRITE2_0_REFINEMENT_H
