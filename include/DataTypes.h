//
// Created by maksbh on 11/23/18.
//

#ifndef DENDRITE2_0_DATATYPES_H
#define DENDRITE2_0_DATATYPES_H

#include "TreeNode.h"
#include <talyfem/talyfem.h>
typedef double DENDRITE_REAL;
typedef unsigned int DENDRITE_UINT;
typedef int DENDRITE_INT;

#define FEQUALS(x, y) fabs((x) - (y)) < 1E-10 ? true : false

enum VariableGP : int {
  QuadraticByLinear = 64,
  RefineBySplitting = 32,
  IncreasedRelativeOrder = 0,
  NoChange = 0
};

struct GPInfo {
  TALYFEMLIB::ZEROPTV positionLocal;
  TALYFEMLIB::ZEROPTV positionGlobal;
  DENDRITE_REAL jaccXw;
};

enum Timers:DENDRITE_UINT {
  MatrixAssembly=0,
  VectorAssembly=1,
  IBM_MatrixAssembly=2,
  IBM_VectorAssembly=3,
  TotalMatrixTime = 4,
  TotalVectorTime = 5,
  Solve = 6,
  MAX_TIMERS = 7
};

/**
   @brief A small helper class that pairs an octant with some values.
   The class is templated on the length and type of the array. This class has an
   MPI_Datatype associated with it and it can be communicated between processors.
   @author Rahul Sampath
   @author maksbh: Copied from Dendro4
   */
template<typename T, unsigned int ARR_LEN>
class NodeAndValues {

 public:
  int label;
  bool isLocal = true;
  ot::TreeNode node; /**< The octant */
  T values[ARR_LEN]; /**< The values */

  /** @name Overload operators */
  //@{
  bool Equals(NodeAndValues<T, ARR_LEN> const &other) const {
    bool result = ((this->node) == other.node);

    for (unsigned int i = 0; (i < ARR_LEN) && result; i++) {
      result = ((this->values[i]) == (other.values[i]));
    }

    return result;
  }

  bool operator==(NodeAndValues<T, ARR_LEN> const &other) const {
    return ((this->node) == other.node);
  }//end fn.

  bool operator!=(NodeAndValues<T, ARR_LEN> const &other) const {
    return ((this->node) != other.node);
  }//end fn.

  bool operator<(NodeAndValues<T, ARR_LEN> const &other) const {
    return ((this->node) < other.node);
  }//end function

  bool operator<=(NodeAndValues<T, ARR_LEN> const &other) const {
    return ((this->node) <= other.node);
  }//end fn.

  bool operator>(NodeAndValues<T, ARR_LEN> const &other) const {
    return ((this->node) > other.node);
  }//end fn.

  bool operator>=(NodeAndValues<T, ARR_LEN> const &other) const {
    return ((this->node) >= other.node);
  }//end fn.

  //Asignment Operator
  NodeAndValues<T, ARR_LEN> &operator=(NodeAndValues<T, ARR_LEN> const &other) {
    if (this == (&other)) { return *this; }
    this->node = other.node;
    for (unsigned int i = 0; i < ARR_LEN; i++) {
      this->values[i] = other.values[i];
    }
    this->label = other.label;
    this->isLocal = other.isLocal;
    return *this;
  }//end fn.



  friend std::ostream &operator<<(std::ostream &os, NodeAndValues<T, ARR_LEN> const &other) {
    return (os << other.node.getX() << " " << other.node.getY() << " " << other.node.getZ() << " "
               << other.node.getLevel());
  } //end fn.


  //@}

  /** @name Constructors */
  //@{
  NodeAndValues() {}

  //copy constructor
  NodeAndValues(const NodeAndValues<T, ARR_LEN> &other) {
    this->node = other.node;
    for (unsigned int i = 0; i < ARR_LEN; i++) {
      this->values[i] = other.values[i];
    }
    this->label = other.label;
    this->isLocal = other.isLocal;
  }
  //@}

};//end class definition

#endif //DENDRITE2_0_DATATYPES_H
