//
// Created by maksbh on 7/31/19.
//

#ifndef DENDRITE2_0_FEUTILS_H
#define DENDRITE2_0_FEUTILS_H

#include <talyfem/grid/grid_types/grid.h>
#include <talyfem/grid/gridfield.h>

#include <talyfem/grid/grid_types/grid.h>
#include <talyfem/grid/gridfield.h>
#include <talyfem/grid/elem.h>
#include <talyfem/grid/elem_types/elem3dhexahedral.h>
#include <talyfem/grid/femelm.h>
#include <talyfem/data_structures/zeromatrix.h>
#include "DataTypes.h"

template<class Equation, class NodeData>
class FEUtils {
  TalyDendroSync sync;
  void GetLocalPtv(FEMElm  fe, const ZEROPTV &ptvg, ZEROPTV &ptvl, int elm_id){
    if (fe.elem()->n_nodes() == 8) {
      fe.refill(elm_id, BASIS_LINEAR, 0);
    } else {
      fe.refill(elm_id, BASIS_QUADRATIC, 0);
    }
    // use (0, 0, 0) as our initial guess
    ptvl = ZEROPTV(0.0, 0.0, 0.0);

    // maximum number of iterations (after which we give up & throw an exception)
    static const int MAX_STEPS = 100;

    // upper limit on L2 error for ptvl
    static const double MIN_ERROR = 1e-7;

    int n_steps = 0;
    double err = 0.0;
    while (n_steps < MAX_STEPS) {
      // calculate the basis function values at our current guess
      fe.calc_at(ptvl);
      // calculate (L2 error)^2 for this guess, and at the same time build
      // the vector from our guess (mapped to physical space) to ptvg
      ZEROPTV DX;
      err = 0.0;
      for (int i = 0; i < fe.nsd(); i++) {
        DX(i) = (ptvg(i) - fe.position()(i));
        err += DX(i) * DX(i);
      }
      // if the error on our guess is acceptable, return it
      // (we use MIN_ERROR^2 here to avoid calculating sqrt(err))
      if (err < MIN_ERROR*MIN_ERROR) {
//      PrintStatus(ptvl, ptvg);
        return;
      }
      // otherwise, move our guess by the inverse of the Jacobian times
      // our delta x vector (calculated above)
      double jaccInv = 1.0 / fe.jacc();
      for (int i = 0; i < fe.nsd(); i++) {
        for (int j = 0; j < fe.nsd(); j++) {
          ptvl(i) += fe.cof(j, i) * DX(j) * jaccInv;
        }
      }
//    PrintStatus("1-\t", ptvl, ptvg);
      n_steps++;
    }

    // if we get here, our loop above failed to converge
    throw TALYException() << "GetLocalPtv did not converge after "
                          << MAX_STEPS << " iterations (final error: "
                          << sqrt(err) << ")";
  }
 protected:
  TALYFEMLIB::GridField<NodeData> *m_gridField;
  TALYFEMLIB::GRID *m_grid;
  Equation *m_talyEq;
  std::vector<GPInfo> m_gaussPoints;
  DENDRITE_UINT eleOrder_;
  DENDRITE_REAL *m_coords;
 public:
  void init(TALYFEMLIB::GRID *grid, TALYFEMLIB::GridField<NodeData> *gridField,
            Equation *talyEq, const DENDRITE_REAL *coords, const DENDRITE_UINT eleOrder) {
    m_gaussPoints.clear();
    m_gridField = gridField;
    m_grid = grid;
    m_talyEq = talyEq;
    eleOrder_ = eleOrder;
    m_coords = new DENDRITE_REAL[m_grid->n_nodes() * m_uiDim];
    for (int i = 0; i < m_grid->n_nodes() * m_uiDim; i++) {
      m_coords[i] = coords[i];
    }
  }

  virtual void performPartition() {
    throw TALYFEMLIB::TALYException() << "Thou need to override this function \n";
  }

  virtual void performIntegrationAe(TALYFEMLIB::ZeroMatrix<double> &Ae) {
    if (eleOrder_ == 1) {
      sync.syncCoords<1, 3>(m_coords, m_grid);
    } else if (eleOrder_ == 2) {
      sync.syncCoords<2, 3>(m_coords, m_grid);
    }
    FEMElm fe(m_grid, TALYFEMLIB::BASIS_ALL);
    fe.refill(0, 0);
    for (int i = 0; i < m_gaussPoints.size(); i++) {
      ZEROPTV posl;
      this->GetLocalPtv(fe, m_gaussPoints[i].positionGlobal, posl, 0);
      fe.calc_at(posl);
      fe.set_jacc_x_w(m_gaussPoints[i].jaccXw);
      m_talyEq->Integrands_Ae(fe, Ae);
    }
  }

  virtual void performIntegrationBe(TALYFEMLIB::ZEROARRAY<DENDRITE_REAL> &be) {
    if (eleOrder_ == 1) {
      sync.syncCoords<1, 3>(m_coords, m_grid);
    } else if (eleOrder_ == 2) {
      sync.syncCoords<2, 3>(m_coords, m_grid);
    }
    FEMElm fe(m_grid, TALYFEMLIB::BASIS_ALL);
    fe.refill(0, 0);
    for (int i = 0; i < m_gaussPoints.size(); i++) {
      ZEROPTV posl;
      this->GetLocalPtv(fe, m_gaussPoints[i].positionGlobal, posl, 0);
      fe.calc_at(posl);
      fe.set_jacc_x_w(m_gaussPoints[i].jaccXw);
      m_talyEq->Integrands_be(fe, be);
    }
  }

};
#endif //DENDRITE2_0_FEUTILS_H
