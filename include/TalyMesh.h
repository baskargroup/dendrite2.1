//
// Created by maksbh on 11/26/18.
//

#ifndef DENDRITE2_0_TALYMESH_H
#define DENDRITE2_0_TALYMESH_H

#include "TalyMat.h"
#include "TalyVec.h"
#include <type_traits>
template <typename NodeData>
class TalyMesh {
public:
    TALYFEMLIB::ELEM *taly_elem;
    TALYFEMLIB::GRID grid;
    TALYFEMLIB::GridField<NodeData> field;

    TalyMesh(int order);

    virtual ~TalyMesh();
};


template <typename NodeData>
TalyMesh<NodeData>::TalyMesh(int order){
    if(order == 1) {
        int node_id_array[8];
        grid.redimArrays(8, 1);
        for (int i = 0; i < 8; i++) {
            grid.node_array_[i] = new TALYFEMLIB::NODE();
            node_id_array[i] = i;
        }

        taly_elem = new TALYFEMLIB::ELEM3dHexahedral();
        grid.elm_array_[0] = taly_elem;


        taly_elem->redim(8, node_id_array);

        field.redimGrid(&grid);
        field.redimNodeData();
    }
    else if(order == 2){
        int node_id_array[27];
        grid.redimArrays(27, 1);
        for (int i = 0; i < 27; i++) {
            grid.node_array_[i] = new TALYFEMLIB::NODE();
            node_id_array[i] = i;
        }

        taly_elem = new TALYFEMLIB::ELEM3dHexahedral();
        grid.elm_array_[0] = taly_elem;


        taly_elem->redim(27, node_id_array);

        field.redimGrid(&grid);
        field.redimNodeData();
    }
}


template <typename NodeData>
TalyMesh<NodeData>::~TalyMesh() {
}


#endif //DENDRITE2_0_TALYMESH_H
