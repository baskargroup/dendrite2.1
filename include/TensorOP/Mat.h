//
// Created by maksbh on 2/24/20.
//

#ifndef DENDRITE2_0_MAT_H
#define DENDRITE2_0_MAT_H

#include <refel.h>
#include <DataTypes.h>
#include "Basis.h"

namespace TensorMat {
extern "C" void dgemm_(char *,
                       char *,
                       int *,
                       int *,
                       int *,
                       double *,
                       double *,
                       int *,
                       double *,
                       int *,
                       double *,
                       double *,
                       int *);

extern "C" void dgemv_(char* TRANS, const int* M, const int* N,
            double* alpha, double* A, const int* LDA, double* X,
            const int* INCX, double* beta, double* C, const int* INCY);

extern "C" void daxpy_(int* n, double* alpha, double* x, int *incx, double* y, int *incy);

class Mat {
  DENDRITE_UINT npe_;
  const double *Q1d;
  const double *QT1d;
  const double *Dg;
  const double *DgT;
  const double *W1d;
  DENDRITE_UINT eleOrder_;

  DENDRITE_REAL *imMat1;
  DENDRITE_REAL *imMat2;
  DENDRITE_REAL *imMat3;
  DENDRITE_REAL *Kx;
  DENDRITE_REAL *Ky;
  DENDRITE_REAL *Kz;
  DENDRITE_REAL *Nx;
  DENDRITE_REAL *UgradW;
  DENDRITE_REAL *UgradV;
  DENDRITE_UINT numEnteriesPerDof_; /// number of enteries in the matrix for each dof
  DENDRITE_UINT ndof_;
  DENDRITE_REAL scale_[m_uiDim];

  DENDRITE_REAL * kernel_W_IP_V;
  DENDRITE_REAL * kernel_gradW_IP_gradV;
  DENDRITE_REAL * kernel_W_IP_gradV;
  DENDRITE_REAL * kernel_gradW_IP_V; /// (\grad w,v)
  DENDRITE_REAL * kernel_gradWx_IP_V;
  DENDRITE_REAL * kernel_gradWy_IP_V;
  DENDRITE_REAL * kernel_gradWz_IP_V;

  DENDRITE_REAL * kernel_W_IP_gradVx; /// (w,vx)
  DENDRITE_REAL * kernel_W_IP_gradVy; /// (w,vy)
  DENDRITE_REAL * kernel_W_IP_gradVz; /// (w,vy)


  /**
   * matVec = \alpha * mat*vec + \beta * matVec
   * @param mat
   * @param vec
   * @param matVec
   * @param alpha
   */
  void DGEMV(DENDRITE_REAL *mat, DENDRITE_REAL *vec, DENDRITE_REAL *matVec,const DENDRITE_UINT dof, DENDRITE_REAL alpha, DENDRITE_REAL beta = 0 ){

    char TRANSA = 'N';
    int M = npe_;
    int N = npe_;
    int LDA = npe_;
    int LDB = npe_;
    int LDC = npe_;
    int INCX = 1;
    double BETA = 0;
    dgemv_(&TRANSA,&M,&N,&alpha,mat,&LDA,&vec[dof*npe_],&INCX,&beta,matVec,&INCX);
  }



  /**
   * mat = \alpha * mat1'mat2 + \beta * mat
   * @param mat1
   * @param mat2
   * @param mat
   * @param id_a
   * @param id_b
   * @param alpha
   * @param beta
   */
  void DGEMM(DENDRITE_REAL * mat1,DENDRITE_REAL *mat2,DENDRITE_REAL *mat,const DENDRITE_UINT id_a,
      const DENDRITE_UINT id_b, DENDRITE_REAL alpha, DENDRITE_REAL beta){

    const DENDRITE_UINT matOffset = (id_a*ndof_ + id_b)*numEnteriesPerDof_;
    char TRANSA = 'T';
    char TRANSB = 'N';
    int M = npe_;
    int K = npe_;
    int LDA = npe_;
    int LDB = npe_;
    int LDC = npe_;

    dgemm_(&TRANSA, &TRANSB, &M, &M, &M, &alpha, mat1, &LDA, mat2, &LDA, &beta, &mat[matOffset], &LDC);


  }


  void AXPY(DENDRITE_REAL * outputVec,DENDRITE_REAL * inputVec, DENDRITE_REAL alpha){
    int INC = 1;
    int N = npe_*npe_;
    daxpy_(&N,&alpha,inputVec,&INC,outputVec,&INC);
  }

  inline void performQuadratureLoop(DENDRITE_REAL * mat, const DENDRITE_REAL scale){
    for (unsigned int npe = 0; npe < npe_; npe++) {
      int startid = npe_ * npe;
      for (unsigned int k = 0; k < (eleOrder_ + 1); k++) {
        for (unsigned int j = 0; j < (eleOrder_ + 1); j++) {
          for (unsigned int i = 0; i < (eleOrder_ + 1); i++) {
            mat[startid + k * (eleOrder_ + 1) * (eleOrder_ + 1) + j * (eleOrder_ + 1) + i] *=
                scale*(W1d[i] * W1d[j] * W1d[k]);
          }
        }
      }
    }
  }

  inline void performQuadratureLoop(DENDRITE_REAL * mat, const DENDRITE_REAL * quadValues, const DENDRITE_REAL scale){

    for (unsigned int npe = 0; npe < npe_; npe++) {
      DENDRITE_UINT  counter = 0;
      int startid = npe_ * npe;
      for (unsigned int k = 0; k < (eleOrder_ + 1); k++) {
        for (unsigned int j = 0; j < (eleOrder_ + 1); j++) {
          for (unsigned int i = 0; i < (eleOrder_ + 1); i++) {
            mat[startid + k * (eleOrder_ + 1) * (eleOrder_ + 1) + j * (eleOrder_ + 1) + i] *=
                scale*(W1d[i] * W1d[j] * W1d[k])*quadValues[counter++];
          }
        }
      }
    }
  }

  inline void performQuadratureLoopValOnly(DENDRITE_REAL * mat, const DENDRITE_REAL * quadValues, const DENDRITE_REAL scale){

    for (unsigned int npe = 0; npe < npe_; npe++) {
      DENDRITE_UINT  counter = 0;
      int startid = npe_ * npe;
      for (unsigned int k = 0; k < (eleOrder_ + 1); k++) {
        for (unsigned int j = 0; j < (eleOrder_ + 1); j++) {
          for (unsigned int i = 0; i < (eleOrder_ + 1); i++) {
            mat[startid + k * (eleOrder_ + 1) * (eleOrder_ + 1) + j * (eleOrder_ + 1) + i] *=
                scale*quadValues[counter++];
          }
        }
      }
    }
  }

  inline void preCompute_w_IP_v() {
    std::memcpy(imMat1,Nx, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,1.0);
    DGEMM(Nx,imMat1,kernel_W_IP_V,0,0,1,0);
  }

  inline void preCompute_gradW_IP_gradV() {
    std::memcpy(imMat1,Kx, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,1.0);
    DGEMM(Kx,imMat1,kernel_gradW_IP_gradV,0,0,1,0);

    std::memcpy(imMat2,Ky, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat2,1);
    DGEMM(Ky,imMat2,kernel_gradW_IP_gradV,0,0,1,1);

    std::memcpy(imMat1,Kz, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,1);
    DGEMM(Kz,imMat1,kernel_gradW_IP_gradV,0,0,1,1);
  }

  inline void preCompute_gradW_IP_V() {
    std::memcpy(imMat1,Kx, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,1);
    DGEMM(Nx,imMat1,kernel_gradWx_IP_V,0,0,1,0);

    std::memcpy(imMat1,Ky, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,1);
    DGEMM(Nx,imMat1,kernel_gradWy_IP_V,0,0,1,0);

    std::memcpy(imMat1,Kz, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,1);
    DGEMM(Nx,imMat1,kernel_gradWz_IP_V,0,0,1,0);

    memset(kernel_gradW_IP_V,0, sizeof(DENDRITE_REAL)*npe_*npe_);
    AXPY(kernel_gradW_IP_V,kernel_gradWx_IP_V,1);
    AXPY(kernel_gradW_IP_V,kernel_gradWy_IP_V,1);
    AXPY(kernel_gradW_IP_V,kernel_gradWz_IP_V,1);


    std::memcpy(imMat1,Nx, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,1);
    DGEMM(Kx,imMat1,kernel_W_IP_gradVx,0,0,1,0);
    DGEMM(Ky,imMat1,kernel_W_IP_gradVy,0,0,1,0);
    DGEMM(Kz,imMat1,kernel_W_IP_gradVz,0,0,1,0);


    memset(kernel_W_IP_gradV,0, sizeof(DENDRITE_REAL)*npe_*npe_);

    AXPY(kernel_W_IP_gradV,kernel_W_IP_gradVx,1);
    AXPY(kernel_W_IP_gradV,kernel_W_IP_gradVy,1);
    AXPY(kernel_W_IP_gradV,kernel_W_IP_gradVz,1);

  }

  void preComputeLinearKernel(){
    preCompute_w_IP_v();
    preCompute_gradW_IP_gradV();
    preCompute_gradW_IP_V();
  }
 public:
  Mat(const DENDRITE_UINT eleOrder, const DENDRITE_UINT nPe, const DENDRITE_UINT ndof, const RefElement *refElement) {
    Q1d = refElement->getQ1d();
    QT1d = refElement->getQT1d();
    Dg = refElement->getDg1d();
    DgT = refElement->getDgT1d();
    W1d = refElement->getWgq();
    npe_ = nPe;
    eleOrder_ = eleOrder;
    imMat1 = new DENDRITE_REAL[nPe * nPe];
    imMat2 = new DENDRITE_REAL[nPe * nPe];
    imMat3 = new DENDRITE_REAL[nPe * nPe];
    Kx = new DENDRITE_REAL[nPe * nPe];
    Ky = new DENDRITE_REAL[nPe * nPe];
    Kz = new DENDRITE_REAL[nPe * nPe];
    Nx = new DENDRITE_REAL[nPe * nPe];

    kernel_gradW_IP_gradV = new DENDRITE_REAL[nPe*nPe];
    kernel_W_IP_gradV = new DENDRITE_REAL[nPe*nPe];
    kernel_W_IP_V = new DENDRITE_REAL[nPe*nPe];
    kernel_gradW_IP_V = new DENDRITE_REAL[nPe*nPe];
    kernel_gradWx_IP_V = new DENDRITE_REAL[nPe*nPe];
    kernel_gradWy_IP_V = new DENDRITE_REAL[nPe*nPe];
    kernel_gradWz_IP_V = new DENDRITE_REAL[nPe*nPe];

    kernel_W_IP_gradVx = new DENDRITE_REAL[nPe*nPe];
    kernel_W_IP_gradVy = new DENDRITE_REAL[nPe*nPe];
    kernel_W_IP_gradVz = new DENDRITE_REAL[nPe*nPe];

    UgradW = new DENDRITE_REAL[nPe*nPe];
    UgradV = new DENDRITE_REAL[nPe*nPe];

    ndof_ = ndof;
    numEnteriesPerDof_ = nPe*nPe;

    TENSOROP::DENDRO_TENSOR_AAAX_APPLY_ELEM(eleOrder,Q1d,Q1d,Dg,Kx);
    TENSOROP::DENDRO_TENSOR_AAAX_APPLY_ELEM(eleOrder,Q1d,Dg,Q1d,Ky);
    TENSOROP::DENDRO_TENSOR_AAAX_APPLY_ELEM(eleOrder,Dg,Q1d,Q1d,Kz);
    TENSOROP::DENDRO_TENSOR_AAAX_APPLY_ELEM(eleOrder,Q1d,Q1d,Q1d,Nx);

    preComputeLinearKernel();
  }

  ~Mat() {
    delete[] imMat1;
    delete[] imMat2;
    delete[] Kx;
    delete[] Ky;
    delete[] Kz;
    delete[] Nx;
    delete[] kernel_W_IP_V;
    delete[] kernel_gradW_IP_gradV;
    delete[] kernel_W_IP_gradV;
    delete[] kernel_gradW_IP_V;
    delete[] kernel_gradWx_IP_V;
    delete[] kernel_gradWy_IP_V;
    delete[] kernel_gradWz_IP_V;
    delete[] kernel_W_IP_gradVx;
    delete[] kernel_W_IP_gradVy;
    delete[] kernel_W_IP_gradVz;
    delete[] imMat3;
    delete[] UgradW;
    delete[] UgradV;


  }

  inline DENDRITE_REAL getScale(){
    return scale_[0]/0.5;
  }
  inline const DENDRITE_UINT getEleOrder() {
    return eleOrder_;
  }

  inline void calcValueFEM(DENDRITE_REAL * val,DENDRITE_REAL * quad, const DENDRITE_UINT dof, const DENDRITE_REAL prefactor = 1){
    DGEMV(Nx, val, quad,dof, prefactor,0);
  }

  inline void setScale(DENDRITE_REAL * coords,int eleOrder){

    scale_[0] = (coords[m_uiDim*eleOrder] - coords[0])*0.5;
    scale_[1] = (coords[m_uiDim*eleOrder] - coords[1])*0.5;
    scale_[2] = (coords[m_uiDim*eleOrder] - coords[2])*0.5;
  }

  inline void calcValueDerivativeFEMX(DENDRITE_REAL * val,DENDRITE_REAL * quad, const DENDRITE_UINT dof,const DENDRITE_REAL prefactor = 1){
    DENDRITE_REAL alpha = prefactor/scale_[0];
    DGEMV(Kx, val, quad,dof, alpha,0);
  }

  inline void calcValueDerivativeFEMY(DENDRITE_REAL * val,DENDRITE_REAL * quad, const DENDRITE_UINT dof,const DENDRITE_REAL prefactor = 1){
    DENDRITE_REAL alpha = prefactor/scale_[0];
    DGEMV(Ky, val, quad,dof,alpha,0);
  }

  inline void calcValueDerivativeFEMZ(DENDRITE_REAL * val,DENDRITE_REAL * quad,const DENDRITE_UINT dof, const DENDRITE_REAL prefactor = 1){
    DENDRITE_REAL alpha = prefactor/scale_[0];
    DGEMV(Kz, val, quad,dof,alpha,0);
  }

  inline void gradW_IP_V(DENDRITE_REAL * mat,const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    const DENDRITE_UINT matOffset = (id_a*ndof_ + id_b)*numEnteriesPerDof_;
    DENDRITE_REAL scale = scale_[0]*scale_[0]*alpha;
    AXPY(&mat[matOffset],kernel_gradW_IP_V,scale);
  }

  inline void W_IP_gradVx(DENDRITE_REAL * mat,const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    const DENDRITE_UINT matOffset = (id_a*ndof_ + id_b)*numEnteriesPerDof_;
    DENDRITE_REAL scale = scale_[0]*scale_[0]*alpha;
    AXPY(&mat[matOffset],kernel_W_IP_gradVx,scale);
  }

  inline void W_IP_gradVy(DENDRITE_REAL * mat,const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    const DENDRITE_UINT matOffset = (id_a*ndof_ + id_b)*numEnteriesPerDof_;
    DENDRITE_REAL scale = scale_[0]*scale_[0]*alpha;
    AXPY(&mat[matOffset],kernel_W_IP_gradVy,scale);
  }

  inline void W_IP_gradVz(DENDRITE_REAL * mat,const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    const DENDRITE_UINT matOffset = (id_a*ndof_ + id_b)*numEnteriesPerDof_;
    DENDRITE_REAL scale = scale_[0]*scale_[0]*alpha;
    AXPY(&mat[matOffset],kernel_W_IP_gradVz,scale);
  }

  inline void gradWx_IP_V(DENDRITE_REAL * mat,const DENDRITE_REAL * quad,const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    DENDRITE_REAL scale = scale_[0]*scale_[0];
    std::memcpy(imMat1,Kx, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quad,scale);
    DGEMM(Nx,imMat1,mat,id_a,id_b,alpha,beta);
  }

  inline void gradWy_IP_V(DENDRITE_REAL * mat,const DENDRITE_REAL * quad,const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    DENDRITE_REAL scale = scale_[0]*scale_[0];
    std::memcpy(imMat1,Ky, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quad,scale);
    DGEMM(Nx,imMat1,mat,id_a,id_b,alpha,beta);
  }

  inline void gradWz_IP_V(DENDRITE_REAL * mat,const DENDRITE_REAL * quad,const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    DENDRITE_REAL scale = scale_[0]*scale_[0];
    std::memcpy(imMat1,Kz, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quad,scale);
    DGEMM(Nx,imMat1,mat,id_a,id_b,alpha,beta);
  }


  inline void gradWx_IP_V(DENDRITE_REAL * mat,const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    const DENDRITE_UINT matOffset = (id_a*ndof_ + id_b)*numEnteriesPerDof_;
    DENDRITE_REAL scale = scale_[0]*scale_[0]*alpha;
    AXPY(&mat[matOffset],kernel_gradWx_IP_V,scale);
  }



  inline void gradWy_IP_V(DENDRITE_REAL * mat,const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    const DENDRITE_UINT matOffset = (id_a*ndof_ + id_b)*numEnteriesPerDof_;
    DENDRITE_REAL scale = scale_[0]*scale_[0]*alpha;
    AXPY(&mat[matOffset],kernel_gradWy_IP_V,scale);
  }

  inline void gradWz_IP_V(DENDRITE_REAL * mat,const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    const DENDRITE_UINT matOffset = (id_a*ndof_ + id_b)*numEnteriesPerDof_;
    DENDRITE_REAL scale = scale_[0]*scale_[0]*alpha;
    AXPY(&mat[matOffset],kernel_gradWz_IP_V,scale);
  }

  inline void computeUGradW(const DENDRITE_REAL * velx,const DENDRITE_REAL * vely,const DENDRITE_REAL * velz){
    std::memcpy(UgradW,Kx, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoopValOnly(UgradW,velx,1);
    std::memcpy(imMat2,Ky, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoopValOnly(imMat2,vely,1);
    AXPY(UgradW,imMat2,1);
    std::memcpy(imMat3,Kz, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoopValOnly(imMat3,velz,1);
    AXPY(UgradW,imMat3,1);
  }

  inline void computeUGradV(const DENDRITE_REAL * velx,const DENDRITE_REAL * vely,const DENDRITE_REAL * velz){
    std::memcpy(UgradV,Kx, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoopValOnly(UgradV,velx,1);
    std::memcpy(imMat2,Ky, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoopValOnly(imMat2,vely,1);
    AXPY(UgradV,imMat2,1);
    std::memcpy(imMat3,Kz, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoopValOnly(imMat3,velz,1);
    AXPY(UgradV,imMat3,1);
  }





  /**
   * Linear version
   * @param [Out] mat The output matrix
   * @param alpha The prefactor
   * @param id_a
   * @param id_b
   * @param scale
   */
  inline void w_IP_v(DENDRITE_REAL *mat, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1) {
    const DENDRITE_UINT matOffset = (id_a*ndof_ + id_b)*numEnteriesPerDof_;
    DENDRITE_REAL scale = scale_[0]*scale_[0]*scale_[0]*alpha;
    AXPY(&mat[matOffset],kernel_W_IP_V,scale);
  }

  /**
   * Non - linear version
   * @param [Out] mat The output matrix
   * @param alpha The prefactor
   * @param id_a
   * @param id_b
   * @param scale
   */
  inline void w_IP_v(DENDRITE_REAL *mat,DENDRITE_REAL *quadValues, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1) {

    std::memcpy(imMat1,Nx, sizeof(DENDRITE_REAL)*npe_*npe_);
    const DENDRITE_REAL scale = scale_[0]*scale_[0]*scale_[0];
    performQuadratureLoop(imMat1,quadValues,scale);
    DGEMM(Nx,imMat1,mat,id_a,id_b,alpha,beta);
  }


  /**
   * Linear version
   * @param mat
   * @param alpha
   * @param id_a
   * @param id_b
   * @param scale
   */

  inline void gradW_IP_gradV(DENDRITE_REAL *mat, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1) {
    DENDRITE_REAL  scale = scale_[0]*alpha;
    const DENDRITE_UINT matOffset = (id_a*ndof_ + id_b)*numEnteriesPerDof_;
    AXPY(&mat[matOffset],kernel_gradW_IP_gradV,scale);
  }

  /**
   * Non - linear version
   * @param mat
   * @param quadVal
   * @param alpha
   * @param id_a
   * @param id_b
   * @param scale
   */
  inline void gradW_IP_gradV(DENDRITE_REAL *mat, const DENDRITE_REAL * quadx, const DENDRITE_REAL * quady,const DENDRITE_REAL * quadz, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1) {
    const DENDRITE_REAL  scale = scale_[0];


    std::memcpy(imMat1,Kx, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quadx,scale);
    DGEMM(Kx,imMat1,mat,id_a,id_b,alpha,beta);

    std::memcpy(imMat2,Ky, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat2,quady,scale);
    DGEMM(Ky,imMat2,mat,id_a,id_b,alpha,beta);

    std::memcpy(imMat1,Kz, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quadz,scale);
    DGEMM(Kz,imMat1,mat,id_a,id_b,alpha,beta);
  }

  inline void uDotGradW_IP_uDotGradV(DENDRITE_REAL * mat,const DENDRITE_REAL * velx, const DENDRITE_REAL * vely,const DENDRITE_REAL * velz,DENDRITE_REAL * quad, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    const DENDRITE_REAL  scale = scale_[0];
    std::memcpy(imMat1,Kx, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,velx,1);
    std::memcpy(imMat2,Ky, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat2,vely,1);
    AXPY(imMat1,imMat2,1);
    std::memcpy(imMat2,Kz, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat2,velz,1);
    AXPY(imMat1,imMat2,1);

    std::memcpy(imMat3,imMat1, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quad,1.0);
    DGEMM(imMat1,imMat3,imMat2,0,0,alpha,0);
    const DENDRITE_UINT matOffset = (id_a*ndof_ + id_b)*numEnteriesPerDof_;
    AXPY(&mat[matOffset],imMat2,scale);


  }

  inline void uDotGradW_IP_uDotGradV(DENDRITE_REAL * mat,DENDRITE_REAL * quad, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    const DENDRITE_REAL  scale = scale_[0];
    std::memcpy(imMat1,UgradW, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quad,1.0);
    DGEMM(imMat1,UgradW,imMat2,0,0,alpha,0);
    const DENDRITE_UINT matOffset = (id_a*ndof_ + id_b)*numEnteriesPerDof_;
    AXPY(&mat[matOffset],imMat2,scale);
  }
  inline void U1DotGradW_IP_U2DotGradV(DENDRITE_REAL * mat,const DENDRITE_REAL * u2gradW, const DENDRITE_REAL * quad, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    const DENDRITE_REAL  scale = scale_[0];
    std::memcpy(imMat1,UgradW, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quad,scale);
    DGEMM(UgradV,imMat1,mat,id_a,id_b,alpha,beta);
//    std::memcpy(imMat1,u2gradW, sizeof(DENDRITE_REAL)*npe_*npe_);
//    performQuadratureLoop(imMat1,quad,1.0);
//    DGEMM(imMat1,UgradW,imMat2,0,0,alpha,0);
//    const DENDRITE_UINT matOffset = (id_a*ndof_ + id_b)*numEnteriesPerDof_;
//    AXPY(&mat[matOffset],imMat2,scale);
  }

  inline void uDotGradW_IP_V(DENDRITE_REAL * mat, DENDRITE_REAL * quad,const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    const DENDRITE_REAL  scale = scale_[0]*scale_[0];
    std::memcpy(imMat1,Nx, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quad,scale);
    DGEMM(imMat1,UgradW,mat,id_a,id_b,alpha,beta);

  }
  inline void uDotGradW_IP_V(DENDRITE_REAL * mat, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    const DENDRITE_REAL  scale = scale_[0]*scale_[0];
    std::memcpy(imMat1,Nx, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,scale);
    DGEMM(imMat1,UgradW,mat,id_a,id_b,alpha,beta);

  }
  inline void uDotGradW_IP_gradWx(DENDRITE_REAL * mat, DENDRITE_REAL * quad,const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    const DENDRITE_REAL  scale = scale_[0];
    std::memcpy(imMat1,Kx, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quad,scale);
    DGEMM(imMat1,UgradW,mat,id_a,id_b,alpha,beta);
//    const DENDRITE_REAL  scale = scale_[0];
//    std::memcpy(imMat1,UgradW, sizeof(DENDRITE_REAL)*npe_*npe_);
//    performQuadratureLoop(imMat1,quad,1.0);
//    DGEMM(Kx,imMat1,imMat2,0,0,alpha,0);
//    const DENDRITE_UINT matOffset = (id_a*ndof_ + id_b)*numEnteriesPerDof_;
//    AXPY(&mat[matOffset],imMat2,scale);
  }

  inline void uDotGradW_IP_gradWy(DENDRITE_REAL * mat, DENDRITE_REAL * quad,const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    const DENDRITE_REAL  scale = scale_[0];
    std::memcpy(imMat1,Ky, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quad,scale);
    DGEMM(imMat1,UgradW,mat,id_a,id_b,alpha,beta);
//    const DENDRITE_REAL  scale = scale_[0];
//    std::memcpy(imMat1,UgradW, sizeof(DENDRITE_REAL)*npe_*npe_);
//    performQuadratureLoop(imMat1,quad,1.0);
//    DGEMM(Ky,imMat1,imMat2,0,0,alpha,0);
//    const DENDRITE_UINT matOffset = (id_a*ndof_ + id_b)*numEnteriesPerDof_;
//    AXPY(&mat[matOffset],imMat2,scale);
  }

  inline void uDotGradW_IP_gradWz(DENDRITE_REAL * mat, DENDRITE_REAL * quad,const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    const DENDRITE_REAL  scale = scale_[0];
    std::memcpy(imMat1,Kz, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quad,scale);
    DGEMM(imMat1,UgradW,mat,id_a,id_b,alpha,beta);
//    const DENDRITE_REAL  scale = scale_[0];
//    std::memcpy(imMat1,UgradW, sizeof(DENDRITE_REAL)*npe_*npe_);
//    performQuadratureLoop(imMat1,quad,1.0);
//    DGEMM(Kz,imMat1,imMat2,0,0,alpha,0);
//    const DENDRITE_UINT matOffset = (id_a*ndof_ + id_b)*numEnteriesPerDof_;
//    AXPY(&mat[matOffset],imMat2,scale);
  }

  inline void gradWx_IP_UDotgradV(DENDRITE_REAL * mat, DENDRITE_REAL * quad,const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    const DENDRITE_REAL  scale = scale_[0];
    std::memcpy(imMat1,Kx, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quad,scale);
    DGEMM(UgradV,imMat1,mat,id_a,id_b,alpha,beta);

  }

  inline void gradWy_IP_UDotgradV(DENDRITE_REAL * mat, DENDRITE_REAL * quad,const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    const DENDRITE_REAL  scale = scale_[0];
    std::memcpy(imMat1,Ky, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quad,scale);
    DGEMM(UgradV,imMat1,mat,id_a,id_b,alpha,beta);
  }
  inline void gradWz_IP_UDotgradV(DENDRITE_REAL * mat, DENDRITE_REAL * quad,const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1){
    const DENDRITE_REAL  scale = scale_[0];
    std::memcpy(imMat1,Kz, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quad,scale);
    DGEMM(UgradV,imMat1,mat,id_a,id_b,alpha,beta);
  }


  inline void gradWx_IP_gradVx(DENDRITE_REAL *mat, const DENDRITE_REAL * quadx, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1) {
    const DENDRITE_REAL  scale = scale_[0];
    std::memcpy(imMat1,Kx, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quadx,scale);
    DGEMM(Kx,imMat1,mat,id_a,id_b,alpha,beta);
  }

  inline void gradWx_IP_gradVy(DENDRITE_REAL *mat, const DENDRITE_REAL * quad, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1) {
    const DENDRITE_REAL  scale = scale_[0];
    std::memcpy(imMat1,Kx, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quad,scale);
    DGEMM(Ky,imMat1,mat,id_a,id_b,alpha,beta);
  }

  inline void gradWx_IP_gradVz(DENDRITE_REAL *mat, const DENDRITE_REAL * quad, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1) {
    const DENDRITE_REAL  scale = scale_[0];
    std::memcpy(imMat1,Kx, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quad,scale);
    DGEMM(Kz,imMat1,mat,id_a,id_b,alpha,beta);
  }

  inline void gradWy_IP_gradVx(DENDRITE_REAL *mat, const DENDRITE_REAL * quad, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1) {
    const DENDRITE_REAL  scale = scale_[0];
    std::memcpy(imMat1,Ky, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quad,scale);
    DGEMM(Kx,imMat1,mat,id_a,id_b,alpha,beta);
  }

  inline void gradWy_IP_gradVy(DENDRITE_REAL *mat, const DENDRITE_REAL * quady, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1) {
    const DENDRITE_REAL  scale = scale_[0];
    std::memcpy(imMat1,Ky, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quady,scale);
    DGEMM(Ky,imMat1,mat,id_a,id_b,alpha,beta);
  }
  inline void gradWy_IP_gradVz(DENDRITE_REAL *mat, const DENDRITE_REAL * quady, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1) {
    const DENDRITE_REAL  scale = scale_[0];
    std::memcpy(imMat1,Ky, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quady,scale);
    DGEMM(Kz,imMat1,mat,id_a,id_b,alpha,beta);
  }
  inline void gradWz_IP_gradVx(DENDRITE_REAL *mat, const DENDRITE_REAL * quadz, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1) {
    const DENDRITE_REAL  scale = scale_[0];
    std::memcpy(imMat1,Kz, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quadz,scale);
    DGEMM(Kx,imMat1,mat,id_a,id_b,alpha,beta);
  }

  inline void gradWz_IP_gradVy(DENDRITE_REAL *mat, const DENDRITE_REAL * quadz, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1) {
    const DENDRITE_REAL  scale = scale_[0];
    std::memcpy(imMat1,Kz, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quadz,scale);
    DGEMM(Ky,imMat1,mat,id_a,id_b,alpha,beta);
  }


  inline void gradWz_IP_gradVz(DENDRITE_REAL *mat, const DENDRITE_REAL * quadz, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1) {
    const DENDRITE_REAL  scale = scale_[0];
    std::memcpy(imMat1,Kz, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quadz,scale);
    DGEMM(Kz,imMat1,mat,id_a,id_b,alpha,beta);
  }




  inline void W_IP_gradV(DENDRITE_REAL *mat, const DENDRITE_REAL * quadx,const DENDRITE_REAL * quady,const DENDRITE_REAL * quadz, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1) {
    const DENDRITE_REAL  scale = scale_[0]*scale_[0];

    std::memcpy(imMat1,UgradV, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,scale);
//    performQuadratureLoop(imMat1,quadx,scale);
    DGEMM(imMat1,Nx,mat,id_a,id_b,alpha,beta);

//    std::memcpy(imMat1,Kx, sizeof(DENDRITE_REAL)*npe_*npe_);
//    performQuadratureLoop(imMat1,quadx,scale);
//    DGEMM(imMat1,Nx,mat,id_a,id_b,alpha,beta);
//
//    std::memcpy(imMat2,Nx, sizeof(DENDRITE_REAL)*npe_*npe_);
//    performQuadratureLoop(imMat2,quady,scale);
//    DGEMM(Ky,imMat2,mat,id_a,id_b,alpha,beta);
//
//    std::memcpy(imMat1,Nx, sizeof(DENDRITE_REAL)*npe_*npe_);
//    performQuadratureLoop(imMat1,quadz,scale);
//    DGEMM(Kz,imMat1,mat,id_a,id_b,alpha,beta);
  }

  inline void W_IP_UdotgradV(DENDRITE_REAL *mat, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1) {
    const DENDRITE_REAL  scale = scale_[0]*scale_[0];
    std::memcpy(imMat1,Nx, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,scale);
    DGEMM(UgradV,imMat1,mat,id_a,id_b,alpha,beta);
  }


  inline void W_IP_UdotgradV(DENDRITE_REAL *mat, const DENDRITE_REAL * quad, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1) {
    const DENDRITE_REAL  scale = scale_[0]*scale_[0];
    std::memcpy(imMat1,Nx, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,quad,scale);
    DGEMM(UgradV,imMat1,mat,id_a,id_b,alpha,beta);
  }


  inline void gradW_IP_V(DENDRITE_REAL *mat, const DENDRITE_REAL * quadx,const DENDRITE_REAL * quady,const DENDRITE_REAL * quadz, const DENDRITE_UINT id_a, const DENDRITE_UINT id_b, DENDRITE_REAL alpha = 1, DENDRITE_REAL  beta = 1) {
    const DENDRITE_REAL  scale = scale_[0]*scale_[0];

    this->computeUGradW(quadx,quady,quadz);
    std::memcpy(imMat1,UgradW, sizeof(DENDRITE_REAL)*npe_*npe_);
    performQuadratureLoop(imMat1,scale);
    DGEMM(Nx,imMat1,mat,id_a,id_b,alpha,beta);
//    std::memcpy(imMat1,Kx, sizeof(DENDRITE_REAL)*npe_*npe_);
//    performQuadratureLoop(imMat1,quadx,scale);
//    DGEMM(Nx,imMat1,mat,id_a,id_b,alpha,beta);
//
//    std::memcpy(imMat2,Ky, sizeof(DENDRITE_REAL)*npe_*npe_);
//    performQuadratureLoop(imMat2,quady,scale);
//    DGEMM(Nx,imMat2,mat,id_a,id_b,alpha,beta);
//
//    std::memcpy(imMat1,Kz, sizeof(DENDRITE_REAL)*npe_*npe_);
//    performQuadratureLoop(imMat1,quadz,scale);
//    DGEMM(Nx,imMat1,mat,id_a,id_b,alpha,beta);
  }

  void copyUGradW(DENDRITE_REAL * ugradWVec){
    std::memcpy(ugradWVec,UgradW, sizeof(DENDRITE_REAL)*npe_*npe_);
  }
  void setUGradW(const DENDRITE_REAL * ugradWVec){
    std::memcpy(UgradW,ugradWVec, sizeof(DENDRITE_REAL)*npe_*npe_);
  }




  void printMat(const DENDRITE_REAL *mat){
    std::ofstream fout("T.dat");
    for(int i = 0; i < npe_; i++){
      for(int dofi = 0;dofi < ndof_; dofi++) {
      for(int j = 0; j < npe_; j++){

          for(int dofj = 0;dofj < ndof_; dofj++){
            const int dof = dofi*ndof_ + dofj;
            if(fabs(mat[(npe_*npe_*dof+i*npe_+j)]) > 0) {
              fout << std::setprecision(7)  << mat[(npe_ * npe_ * dof + i * npe_ + j)] << " ";
            } else{
              fout << 0 << " ";
            }
          }
        }
        fout << "\n";
      }

    }
    fout.close();
//    std::ofstream fout("T.dat");
//    for(int i = 0; i < npe_*npe_; i++){
//      fout << mat[i] << "\n";
//      fout<< "\n";
//    }

  }

};
};

#endif

