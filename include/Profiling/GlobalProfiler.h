#pragma once

#if BUILD_WITH_PETSC
#ifdef PETSC_LOGGING_IMPL
#define Event  int
#else
#define Event extern int
#endif


Event gcalcRefinementVecValueBasedEvent;
Event gcalcRefinementVecPositionBasedEvent;
Event gcalcRefinementVecAposterioriErrorBasedEvent;
Event gRemeshEvent;
Event gCoarseningCheckEvent;

Event gUpdateBoundaryConditionEvent;
Event gSolverInitEvent;
Event gSolverVectorAssemblyEvent;
Event gApplyVecBCEvent;
Event gLinearSolveEvent;
Event gNonLinearSolveEvent;
Event gVecVolumeGaussPointEvent;
Event gVecSurfaceGaussPointEvent;

/** Matrix Version **/
Event gMatAssemblyEvent;
Event gSolverJacobianAssemblyEvent;
Event gSolverMatBCEvent;
Event gSolverJacobianBCEvent;
Event gMatVolumeGaussPointEvent;
Event gMatSurfaceGaussPointEvent;

/** Matrix free Version **/
Event gMatMultEvent;
Event gSolverJacobianMatMultEvent;
Event gSolverMatfreeBCEvent;
Event gSolverJacobianMatFreeBCEvent;

Event totalEvent;

/** Buffer event **/
Event gBufferEvent1;
Event gBufferEvent2;
Event gBufferEvent3;
Event gBufferEvent4;
Event gBufferEvent5;


#ifdef PETSC_LOGGING_IMPL
void registerglobalProfiler()
{
  PetscLogEventRegister("Dendrite-UpdateBoundaryCondition", 0, &gUpdateBoundaryConditionEvent);
  PetscLogEventRegister("Dendrite-Remesh", 0, &gRemeshEvent);
  PetscLogEventRegister("Dendrite-ValueBased", 0, &gcalcRefinementVecValueBasedEvent);
  PetscLogEventRegister("Dendrite-PositionBased", 0, &gcalcRefinementVecPositionBasedEvent);
  PetscLogEventRegister("Dendrite-AposterioriBased", 0, &gcalcRefinementVecAposterioriErrorBasedEvent);
  PetscLogEventRegister("Dendrite-CoarseningCheck", 0, &gCoarseningCheckEvent);

  PetscLogEventRegister("Dendrite-SolverInit", 0, &gSolverInitEvent);
  PetscLogEventRegister("Dendrite-VecAssembly", 0, &gSolverVectorAssemblyEvent);
  PetscLogEventRegister("Dendrite-VecBC", 0, &gApplyVecBCEvent);
  PetscLogEventRegister("Dendrite-VecBC", 0, &gApplyVecBCEvent);
  PetscLogEventRegister("Dendrite-LinSolve", 0, &gLinearSolveEvent);
  PetscLogEventRegister("Dendrite-NonLinSolve", 0, &gNonLinearSolveEvent);
  PetscLogEventRegister("Dendrite-VecVolumeGaussPoint", 0, &gVecVolumeGaussPointEvent);
  PetscLogEventRegister("Dendrite-VecSurfaceGaussPoint", 0, &gVecSurfaceGaussPointEvent);

/** Matrix Version **/
  PetscLogEventRegister("Dendrite-MatAssembly", 0, &gMatAssemblyEvent);
  PetscLogEventRegister("Dendrite-MatAssemblyBC", 0, &gSolverMatBCEvent);

  PetscLogEventRegister("Dendrite-JacobianAssembly", 0, &gSolverJacobianAssemblyEvent);
  PetscLogEventRegister("Dendrite-JacobianBC", 0, &gSolverJacobianBCEvent);

  PetscLogEventRegister("Dendrite-MatVolumeGaussEvent", 0, &gMatVolumeGaussPointEvent);
  PetscLogEventRegister("Dendrite-MatSurfaceGaussEvent", 0, &gMatSurfaceGaussPointEvent);

/** Matrix free Version **/
  PetscLogEventRegister("Dendrite-MatMult", 0, &gMatMultEvent);
  PetscLogEventRegister("Dendrite-MatFreeBC", 0, &gSolverMatfreeBCEvent);

  PetscLogEventRegister("Dendrite-JacobianMatMult", 0, &gSolverJacobianMatMultEvent);
  PetscLogEventRegister("Dendrite-JacobianMatFreeBC", 0, &gSolverJacobianMatFreeBCEvent);
  PetscLogEventRegister("Dendrite-total", 0, &totalEvent);

  /** Buffer Event **/
  PetscLogEventRegister("Dendrite-BufferEvent1", 0, &gBufferEvent1);
  PetscLogEventRegister("Dendrite-BufferEvent2", 0, &gBufferEvent2);
  PetscLogEventRegister("Dendrite-BufferEvent3", 0, &gBufferEvent3);
  PetscLogEventRegister("Dendrite-BufferEvent4", 0, &gBufferEvent4);
  PetscLogEventRegister("Dendrite-BufferEvent5", 0, &gBufferEvent5);

}
#endif
#undef Event
#endif