//
// Created by maksbh on 11/28/18.
//

#ifndef DENDRITE2_0_LINEARSOLVER_H
#define DENDRITE2_0_LINEARSOLVER_H
#pragma once

#include <sstream>

#include <petscksp.h>

#include <PETSc/Solver/Solver.h>
#include <PETSc/PetscTimings.h>

namespace PETSc {
    class LinearSolver : public Solver {



    public:


        virtual ~LinearSolver();

        PetscErrorCode init() override;

        PetscErrorCode solve() override;

        PetscErrorCode cleanup();

        // matrix-free shell operations
        PetscErrorCode jacobianMatMult(Vec In, Vec Out) ;

        PetscErrorCode matGetBlockDiagonal(Mat M, Mat * Out);

        void set_mfree(bool mfree);

        PetscErrorCode matGetDiagonal(Mat M, Vec Out);

        static PetscErrorCode PreKSP(KSP, Vec in, Vec out, void *prectx);
        static PetscErrorCode PostKSP(KSP, Vec in, Vec out, void *postctx);

        inline Vec VecGetResidual(){
            return  m_vecRHS;
        }

        inline KSP & ksp() {
          return m_ksp;
        }
        inline int getDof(){
            return m_uiDof;
        }
        bool get_mfree();
    protected:
        using Solver::m_Mat;
        using Solver::m_Vec;
        using Solver::ShellMatMult;
        using Solver::m_octDA;
        using Solver::m_uiDof;
        KSP m_ksp = NULL;
        bool m_matrixFree = true;
    };




}
#endif //DENDRITE2_0_LINEARSOLVER_H
