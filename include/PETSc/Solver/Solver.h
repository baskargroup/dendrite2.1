//
// Created by maksbh on 11/28/18.
//

#ifndef DENDRITE2_0_SOLVER_H
#define DENDRITE2_0_SOLVER_H
#include <vector>
#include <string>

#include "petscdmda.h"
#include "petscts.h"
#include <TalyEquation.h>
#include <TalyMat.h>
#include <TalyVec.h>

#include <PETSc/BoundaryConditions.h>
#include <Profiling/GlobalProfiler.h>
#include <PETSc/PetscTimings.h>

namespace PETSc {

class Solver {
  //  PetscTimings gBoundaryConditionTimer;
 protected:
  ot::DA *m_octDA;
  DM m_da;
  feMat *m_Mat = NULL;

  feVec *m_Vec = NULL;

  Mat m_matJacobian;
  Mat preConditionedMatrix;

  Vec m_vecRHS;
  Vec m_vecSolution;
  double m_problemSize[3];

  unsigned int m_uiDof;
  BoundaryConditions boundary_conditions_;

  bool loopOverAllOctants = false;

  std::vector<double> m_timers;

  // for generating dirichlet bc
  std::function<Boundary(double, double, double, unsigned int)> m_boundaryCondition;

 public:

  Solver();

  virtual ~Solver() {}

  /**
   * Initializes solver data structures (working vectors, SNES/KSP objects).
   * Should be called once after construction or a call to cleanup().
   * @return
   */
  virtual PetscErrorCode init() = 0;

  virtual PetscErrorCode solve() = 0;

  void setMatrix(feMat *mat);

  void setVector(feVec *vec);

  void setBoundaryCondition(const std::function<Boundary(double, double, double, unsigned int)> &f);

  inline Vec getCurrentSolution() const {
    if (m_octDA->isActive()) {
      assert(m_vecSolution != NULL);  // only valid after init()
    }
    return m_vecSolution;
  }

  inline BoundaryConditions &getBC() {
    return boundary_conditions_;
  }

  inline std::vector<double> & getTimers(){
    return m_timers;
  }

  /**
     *	@brief The Jacobian Matmult operation done a matrix free fashion
     *  @param _jac PETSC Matrix which is of shell type used in the time stepping
     *  @param _in  PETSC Vector which is the input vector
     *  @param _out PETSC Vector which is the output vector _jac*_in
     *  @return bool true if successful, false otherwise
     *
     *
     **/

  virtual PetscErrorCode jacobianMatMult(Vec _in, Vec _out) { throw std::runtime_error("Not implemented"); };

  virtual PetscErrorCode matGetBlockDiagonal(Mat in, Mat *out) { throw std::runtime_error("Not implemented"); };

  virtual PetscErrorCode matGetDiagonal(Mat in, Vec out) { throw std::runtime_error("Not implemented"); };

  void setProblemSize(const double *size);


  static PetscErrorCode ShellMatMult(Mat M, Vec In, Vec Out) {

    Solver *contxt;
    MatShellGetContext(M, &contxt);
    return contxt->jacobianMatMult(In, Out);
  }

  /**
   * @brief: The routine to extract diagonal enteries from matrix
   * @param M
   * @param Out The vector returning diagonal entries.
   * @return 0 if successful
   */
  static PetscErrorCode MatShellGetDiagonal(Mat M, Vec Out) {
    Solver *contxt;
    MatShellGetContext(M, &contxt);
    return contxt->matGetDiagonal(M,Out);
  }

  /**
   * @brief The routine for extracting the block diagonal
   * @param M
   * @param [out] Out The matrix with block diagonal
   * @return
   */

  static PetscErrorCode MatShellGetBlockDiagonal(Mat M, Mat *Out) {
    Solver *contxt;
    MatShellGetContext(M, &contxt);
    return contxt->matGetBlockDiagonal(M, Out);
  }



  void loopOctantsBC(bool loopAllOctantsBC);

  void setDA(ot::DA *da);

  void setDof(unsigned int dof);

  virtual void updateBoundaries(ot::DA *da);
  const std::function<Boundary(double, double, double, unsigned int)> &getBoundaryCondition();

  /** Thsese functionality is used for IBM **/
  virtual void surfaceIntegralJac(Mat *jac, Vec sol) {}
  virtual void surfaceIntegralJacMfree(Vec mfree_in, Vec mfree_out) {}
  virtual void surfaceIntegralFunc(Vec in, Vec out) {}

};

}
#endif //DENDRITE2_0_SOLVER_H
