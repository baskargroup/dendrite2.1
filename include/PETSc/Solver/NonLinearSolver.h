//
// Created by maksbh on 12/3/18.
//

#include <sstream>

#include "Solver.h"

#ifndef DENDRITE2_0_NONLINEARSOLVER_H
#define DENDRITE2_0_NONLINEARSOLVER_H
namespace PETSc {
    class NonlinearSolver : public Solver {
    std::chrono::high_resolution_clock::time_point t1, t2;

    public:
        ~NonlinearSolver() override;
        PetscErrorCode init() override;
        PetscErrorCode solve() override;
        virtual PetscErrorCode cleanup();

        PetscErrorCode jacobianMatMult(Vec in, Vec out) override;


        void set_mfree(bool mfree);

        inline SNES & snes() {
          return m_snes;
        }
        inline bool get_mfree(){
            return m_matrixFree;
        }
        inline int getDof(){
            return m_uiDof;
        }

    protected:
        using Solver::m_Mat;
        using Solver::m_Vec;
        using Solver::ShellMatMult;
        using Solver::m_octDA;
        using Solver::m_uiDof;
        using Solver::m_timers;
        static PetscErrorCode FormJacobian(SNES snes, Vec sol, Mat jac, Mat precond_matrix, void *ctx);
        static PetscErrorCode FormFunction(SNES snes, Vec in, Vec out, void *ctx);
        static PetscErrorCode PreKSP(KSP, Vec in, Vec out, void *prectx);
        static PetscErrorCode PostKSP(KSP, Vec in, Vec out, void *postctx);

        SNES m_snes = NULL;
        Vec m_guess = NULL;  // last guess passed into FormJacobian
        bool m_matrixFree = true;

    };

}
#endif //DENDRITE2_0_NONLINEARSOLVER_H
