//
// Created by maksbh on 12/4/18.
//

#ifndef DENDRITE2_0_PETSCUTILS_H
#define DENDRITE2_0_PETSCUTILS_H

#include <oda.h>
#include <DendriteUtils.h>
#include <PETSc/Solver/LinearSolver.h>
#include <PETSc/Solver/NonLinearSolver.h>

#include <TalyEquation.h>

namespace PETSc {

void petscSetInitialCondition(ot::DA *da, Vec vec, int dof,
                              const std::function<double(double, double, double, int)> &f,
                              const Point &domain_min, const Point &domain_max);

PetscScalar *petscCalcL2Error(ot::DA *da, const double *problem_size, Vec vec, int dof,
                              const AnalyticFunction &f);
void petscEvalFunction(ot::DA *da, const double *problemSize, Vec u, int ndof,
                       const std::vector<EvalFunction> &f, double *func_val);

void petscDumpFilesforRegressionTest(ot::DA *octDA, const Vec &v, const std::string file_prefix = "vec");

template<typename Equation, typename NodeData>
LinearSolver *setLinearSolver(TalyEquation<Equation, NodeData> *talyEq,
                              ot::DA *da,
                              DENDRITE_INT ndof = 1,
                              bool mfree = true,
                              bool loopOverAllOctantsBC = false) {
  DENDRITE_REAL problem_size[m_uiDim];
  LinearSolver *solver = new LinearSolver();
  solver->setMatrix(talyEq->mat);
  solver->setVector(talyEq->vec);
  talyEq->mat->getDimension(problem_size);
  solver->setProblemSize(problem_size);
  solver->setDA(da);
  solver->setDof(ndof);
  solver->set_mfree(mfree);
  solver->loopOctantsBC(loopOverAllOctantsBC);
  if (da->isActive()) {
    solver->init();
  }
  return solver;

}

template<typename Equation, typename NodeData>
NonlinearSolver *setNonLinearSolver(TalyEquation<Equation, NodeData> *talyEq,
                                    ot::DA *da,
                                    DENDRITE_INT ndof = 1,
                                    bool mfree = true,
                                    bool loopOverAllOctantsBC = false) {
  DENDRITE_REAL problem_size[m_uiDim];
  NonlinearSolver *solver = new NonlinearSolver();
  solver->setMatrix(talyEq->mat);
  solver->setVector(talyEq->vec);
  talyEq->mat->getDimension(problem_size);
  solver->setProblemSize(problem_size);
  solver->setDA(da);
  solver->setDof(ndof);
  solver->set_mfree(mfree);
  solver->loopOctantsBC(loopOverAllOctantsBC);
  if (da->isActive()) {
    solver->init();
  }
  return solver;

}

/**
* @ brief update Equation and linear solver class after refinement.
* @tparam Equation Equation class
* @tparam NodeData NodeData class
* @tparam Args args passed to the constructor class
* @param newDA the new DA
* @param talyeq the talyEq pointer to the current DA.
* @param solver the solver pointer to the current DA.
* @param args the argument for the constructor of talyEq.
* @return talyeq the talyEq pointer to the new DA.
* @return solver the solver pointer to the new DA.
*
*/
template<typename Equation, typename NodeData, typename ... Args>
void updateEquationAndSolver(ot::DA *newDA,
                             TalyEquation<Equation, NodeData> *&talyeq,
                             LinearSolver *&solver,
                             Args ... args) {
  Point domain_max = talyeq->getDomainMax();
  Point domain_min = talyeq->getDomainMin();
  auto bnd = solver->getBoundaryCondition();
  bool mfree = solver->get_mfree();
  bool surfaceAssembly = talyeq->checkSurfAssembly();
  int ndof = solver->getDof();

  delete talyeq;
  delete solver;

  talyeq = new TalyEquation<Equation, NodeData>(newDA, domain_min, domain_max, ndof, surfaceAssembly, args...);

  solver = setLinearSolver(talyeq, newDA, ndof, mfree);
  solver->setBoundaryCondition(bnd);
}

/**
  * @ brief update Equation and non linear solver class after refinement.
  * @tparam Equation Equation class
  * @tparam NodeData NodeData class
  * @tparam Args args passed to the constructor class
  * @param newDA the new DA
  * @param talyeq the talyEq pointer to the current DA.
  * @param solver the solver pointer to the current DA.
  * @param args the argument for the constructor of talyEq.
  * @return talyeq the talyEq pointer to the new DA.
  * @return solver the solver pointer to the new DA.
*/

template<typename Equation, typename NodeData, typename ... Args>
void updateEquationAndSolver(ot::DA *newDA,
                             TalyEquation<Equation, NodeData> *&talyeq,
                             NonlinearSolver *&solver,
                             Args ... args) {
  Point domain_max = talyeq->getDomainMax();
  Point domain_min = talyeq->getDomainMin();
  auto bnd = solver->getBoundaryCondition();
  bool mfree = solver->get_mfree();
  bool surfaceAssembly = talyeq->checkSurfAssembly();
  int ndof = solver->getDof();
  delete talyeq;
  delete solver;

  talyeq = new TalyEquation<Equation, NodeData>(newDA, domain_min, domain_max, ndof, surfaceAssembly, args...);

  solver = setNonLinearSolver(talyeq, newDA, ndof, mfree);
  solver->setBoundaryCondition(bnd);
}

/**
* @ brief update Equation and linear solver class after refinement.
* @tparam Equation Equation class
* @tparam NodeData NodeData class
* @tparam Args args passed to the constructor class
* @param newDA the new DA
* @param mesh TalyMesh
* @param talyeq the talyEq pointer to the current DA.
* @param solver the solver pointer to the current DA.
* @param args the argument for the constructor of talyEq.
* @return talyeq the talyEq pointer to the new DA.
* @return solver the solver pointer to the new DA.
*
*/
template<typename Equation, typename NodeData, typename ... Args>
void updateEquationAndSolver(TalyMesh<NodeData> *mesh, ot::DA *newDA,
                             TalyEquation<Equation, NodeData> *&talyeq,
                             LinearSolver *&solver,
                             Args ... args) {
  Point domain_max = talyeq->getDomainMax();
  Point domain_min = talyeq->getDomainMin();
  auto bnd = solver->getBoundaryCondition();
  bool mfree = solver->get_mfree();
  bool surfaceAssembly = talyeq->checkSurfAssembly();
  int ndof = solver->getDof();

  delete talyeq;
  delete solver;

  talyeq = new TalyEquation<Equation, NodeData>(mesh, newDA, domain_min, domain_max, ndof, surfaceAssembly, args...);

  solver = setLinearSolver(talyeq, newDA, ndof, mfree);
  solver->setBoundaryCondition(bnd);
}

/**
* @ brief update Equation and nonLinear solver class after refinement.
* @tparam Equation Equation class
* @tparam NodeData NodeData class
* @tparam Args args passed to the constructor class
* @param newDA the new DA
* @param mesh TalyMesh
* @param talyeq the talyEq pointer to the current DA.
* @param solver the solver pointer to the current DA.
* @param args the argument for the constructor of talyEq.
* @return talyeq the talyEq pointer to the new DA.
* @return solver the solver pointer to the new DA.
*
*/
template<typename Equation, typename NodeData, typename ... Args>
void updateEquationAndSolver(TalyMesh<NodeData> *mesh, ot::DA *newDA,
                             TalyEquation<Equation, NodeData> *&talyeq,
                             NonlinearSolver *&solver,
                             Args ... args) {
  Point domain_max = talyeq->getDomainMax();
  Point domain_min = talyeq->getDomainMin();
  auto bnd = solver->getBoundaryCondition();
  bool mfree = solver->get_mfree();
  bool surfaceAssembly = talyeq->checkSurfAssembly();
  int ndof = solver->getDof();

  delete talyeq;
  delete solver;

  talyeq = new TalyEquation<Equation, NodeData>(mesh, newDA, domain_min, domain_max, ndof, surfaceAssembly, args...);

  solver = setNonLinearSolver(talyeq, newDA, ndof, mfree);
  solver->setBoundaryCondition(bnd);
}

}
#endif //DENDRITE2_0_INITIALCONDITION_H
