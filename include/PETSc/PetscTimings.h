//
// Created by maksbh on 12/1/19.
//

#ifndef DENDRITE2_0_PETSCTIMINGS_H
#define DENDRITE2_0_PETSCTIMINGS_H

#include <petsc.h>

namespace PETSc {

class PetscTimings {
  PetscLogEvent event_; // Event ID
  std::string name_; // name


 public:
  /**
   * @brief Default constructor
   */
  PetscTimings() {
    event_ = 0;
  }
  /**
   * @brief Constructor
   * @param event Event ID
   * @param name nameof the event
   */
  PetscTimings(PetscLogEvent &event, std::string name) {
    event_ = event;
    name_ = name;
    PetscLogEventRegister(name_.c_str(), 0, &event_);

  }
  /**
   * @brief initialize PetscEvent for logging. Not required if constructor (not default) is used.
   * @param event Event ID
   * @param name name of the event
   */
  inline void init(PetscLogEvent &event, std::string name) {
    event_ = event;
    name_ = name;
    PetscLogEventRegister(name_.c_str(), 0, &event_);
  }

  /**
   * @brief Init the timer.
   */
  inline void start() {
    PetscLogEventBegin(event_, 0, 0, 0, 0);
  }

  /**
   * @brief Stop the timers.
   */
  inline void stop() {
    PetscLogEventEnd(event_, 0, 0, 0, 0);
  }
};
}

#endif //DENDRITE2_0_PETSCTIMINGS_H
