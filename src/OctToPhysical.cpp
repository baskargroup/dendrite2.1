//
// Created by maksbh on 11/23/18.
//

#include "OctToPhysical.h"
#include <iostream>
#include <TreeNode.h>


Point convertOctToPhys( Point & oct){
    const DENDRITE_REAL problem_size[m_uiDim] {1.0,1.0,1.0};
    const DENDRITE_REAL offset[m_uiDim] {0.0,0.0,0.0};
    Point phys = convertOctToPhys(oct,problem_size,offset);
    return (phys);
}

Point  convertOctToPhys( Point & oct, const DENDRITE_REAL * problem_size){
    const DENDRITE_REAL offset[m_uiDim] {0.0,0.0,0.0};
    Point phys = convertOctToPhys(oct,problem_size,offset);
    return (phys);
}

Point  convertOctToPhys( Point & oct,const  DENDRITE_REAL * problem_size,const  DENDRITE_REAL * offset){
    const DENDRITE_REAL xFac = problem_size[0]/((DENDRITE_REAL)(1<<(m_uiMaxDepth))) ;
    const DENDRITE_REAL yFac = problem_size[1]/((DENDRITE_REAL)(1<<(m_uiMaxDepth))) ;
    const DENDRITE_REAL zFac = problem_size[2]/((DENDRITE_REAL)(1<<(m_uiMaxDepth))) ;
    Point phys(oct.x()*xFac + offset[0],oct.y()*yFac + offset[1],oct.z()*zFac + offset[2]);
    return (phys);
}

Point  convertOctToPhys( Point & oct,const Point & domain_min,const Point & domain_max){
    const DENDRITE_REAL problem_size[3] {
        domain_max.x() - domain_min.x(),
        domain_max.y() - domain_min.y(),
        domain_max.z() - domain_min.z()};
    const DENDRITE_REAL xFac = problem_size[0]/((DENDRITE_REAL)(1<<(m_uiMaxDepth))) ;
    const DENDRITE_REAL yFac = problem_size[1]/((DENDRITE_REAL)(1<<(m_uiMaxDepth))) ;
    const DENDRITE_REAL zFac = problem_size[2]/((DENDRITE_REAL)(1<<(m_uiMaxDepth))) ;
    Point phys(oct.x()*xFac + domain_min.x(),oct.y()*yFac + domain_min.y(),oct.z()*zFac + domain_min.z());
    return (phys);
}

void  convertOctToPhys(DENDRITE_REAL * oct ){
    const DENDRITE_REAL xFac = 1.0/((DENDRITE_REAL)(1<<(m_uiMaxDepth))) ;
    const DENDRITE_REAL yFac = 1.0/((DENDRITE_REAL)(1<<(m_uiMaxDepth))) ;
    const DENDRITE_REAL zFac = 1.0/((DENDRITE_REAL)(1<<(m_uiMaxDepth))) ;
    oct[0] = oct[0]*xFac;
    oct[1] = oct[1]*yFac;
    oct[2] = oct[2]*zFac;
}

void  convertOctToPhys(DENDRITE_REAL * oct, const DENDRITE_REAL * problem_size){
    const DENDRITE_REAL xFac = problem_size[0]/((DENDRITE_REAL)(1<<(m_uiMaxDepth))) ;
    const DENDRITE_REAL yFac = problem_size[1]/((DENDRITE_REAL)(1<<(m_uiMaxDepth))) ;
    const DENDRITE_REAL zFac = problem_size[2]/((DENDRITE_REAL)(1<<(m_uiMaxDepth))) ;
    oct[0] = oct[0]*xFac;
    oct[1] = oct[1]*yFac;
    oct[2] = oct[2]*zFac;
}

void build_taly_coordinates(double *coords , const Point &pt, const Point &h, const DENDRITE_UINT ele_order) {
    const double &hx = h.x();
    const double &hy = h.y();
    const double &hz = h.z();

    if( ele_order == 1) {

        coords[0] = pt.x();            coords[1]  = pt.y();          coords[2] = pt.z();
        coords[3] = coords[0] + hx;    coords[4]  = coords[1];       coords[5] = coords[2];
        coords[6] = coords[0];         coords[7]  = coords[1] + hy;  coords[8] = coords[2];
        coords[9] = coords[0] + hx;    coords[10] = coords[1] + hy;  coords[11] = coords[2];

        coords[12] = coords[0];        coords[13] = coords[1];       coords[14] = coords[2] + hz;
        coords[15] = coords[0] + hx;   coords[16] = coords[1];       coords[17] = coords[2] + hz;
        coords[18] = coords[0] ;       coords[19] = coords[1] + hy;  coords[20] = coords[2] + hz;
        coords[21] = coords[0]+ hx;    coords[22] = coords[1] + hy;  coords[23] = coords[2] + hz;
    }
    if(ele_order == 2){
        coords[0] = pt.x();                coords[1]  = pt.y();          coords[2] = pt.z();
        coords[3] = coords[0] + 0.5*hx;    coords[4]  = coords[1];       coords[5] = coords[2];
        coords[6] = coords[0] + hx;        coords[7]  = coords[1];       coords[8] = coords[2];

        coords[9]  = coords[0];             coords[10]  = coords[1] + 0.5*hy;      coords[11] = coords[2];
        coords[12] = coords[0] + 0.5*hx;    coords[13]  = coords[1] + 0.5*hy;      coords[14] = coords[2];
        coords[15] = coords[0] + hx;        coords[16]  = coords[1] + 0.5*hy;      coords[17] = coords[2];

        coords[18]  = coords[0];            coords[19]  = coords[1] + hy;      coords[20] = coords[2];
        coords[21] = coords[0] + 0.5*hx;    coords[22]  = coords[1] + hy;      coords[23] = coords[2];
        coords[24] = coords[0] + hx;        coords[25]  = coords[1] + hy;      coords[26] = coords[2];

        coords[27] = coords[0];             coords[28]  = coords[1] ;      coords[29] = coords[2] + 0.5*hz;
        coords[30] = coords[0] + 0.5*hx;    coords[31]  = coords[1] ;      coords[32] = coords[2] + 0.5*hz;
        coords[33] = coords[0] + hx;        coords[34]  = coords[1] ;      coords[35] = coords[2] + 0.5*hz;

        coords[36] = coords[0];             coords[37]  = coords[1] + 0.5*hy;      coords[38] = coords[2] + 0.5*hz;
        coords[39] = coords[0] + 0.5*hx;    coords[40]  = coords[1] + 0.5*hy;      coords[41] = coords[2] + 0.5*hz;
        coords[42] = coords[0] + hx;        coords[43]  = coords[1] + 0.5*hy;      coords[44] = coords[2] + 0.5*hz;

        coords[45] = coords[0];             coords[46]  = coords[1] + hy;      coords[47] = coords[2] + 0.5*hz;
        coords[48] = coords[0] + 0.5*hx;    coords[49]  = coords[1] + hy;      coords[50] = coords[2] + 0.5*hz;
        coords[51] = coords[0] + hx;        coords[52]  = coords[1] + hy;      coords[53] = coords[2] + 0.5*hz;

        coords[54] = coords[0];             coords[55]  = coords[1] ;      coords[56] = coords[2] + hz;
        coords[57] = coords[0] + 0.5*hx;    coords[58]  = coords[1] ;      coords[59] = coords[2] + hz;
        coords[60] = coords[0] + hx;        coords[61]  = coords[1] ;      coords[62] = coords[2] + hz;

        coords[63] = coords[0];             coords[64]  = coords[1] + 0.5*hy ;      coords[65] = coords[2] + hz;
        coords[66] = coords[0] + 0.5*hx;    coords[67]  = coords[1] + 0.5*hy ;      coords[68] = coords[2] + hz;
        coords[69] = coords[0] + hx;        coords[70]  = coords[1] + 0.5*hy ;      coords[71] = coords[2] + hz;

        coords[72] = coords[0];             coords[73]  = coords[1] + hy;           coords[74] = coords[2] + hz;
        coords[75] = coords[0] + 0.5*hx;    coords[76]  = coords[1] + hy;           coords[77] = coords[2] + hz;
        coords[78] = coords[0] + hx;        coords[79]  = coords[1] + hy;           coords[80] = coords[2] + hz;


    }
}

void build_taly_coordinates(double *coords , const ZEROPTV &pt, const ZEROPTV &h, const DENDRITE_UINT ele_order) {
    const double &hx = h.x();
    const double &hy = h.y();
    const double &hz = h.z();

    if( ele_order == 1) {

        coords[0] = pt.x();            coords[1]  = pt.y();          coords[2] = pt.z();
        coords[3] = coords[0] + hx;    coords[4]  = coords[1];       coords[5] = coords[2];
        coords[6] = coords[0];         coords[7]  = coords[1] + hy;  coords[8] = coords[2];
        coords[9] = coords[0] + hx;    coords[10] = coords[1] + hy;  coords[11] = coords[2];

        coords[12] = coords[0];        coords[13] = coords[1];       coords[14] = coords[2] + hz;
        coords[15] = coords[0] + hx;   coords[16] = coords[1];       coords[17] = coords[2] + hz;
        coords[18] = coords[0] ;       coords[19] = coords[1] + hy;  coords[20] = coords[2] + hz;
        coords[21] = coords[0]+ hx;    coords[22] = coords[1] + hy;  coords[23] = coords[2] + hz;
    }
    if(ele_order == 2){
        coords[0] = pt.x();                coords[1]  = pt.y();          coords[2] = pt.z();
        coords[3] = coords[0] + 0.5*hx;    coords[4]  = coords[1];       coords[5] = coords[2];
        coords[6] = coords[0] + hx;        coords[7]  = coords[1];       coords[8] = coords[2];

        coords[9]  = coords[0];             coords[10]  = coords[1] + 0.5*hy;      coords[11] = coords[2];
        coords[12] = coords[0] + 0.5*hx;    coords[13]  = coords[1] + 0.5*hy;      coords[14] = coords[2];
        coords[15] = coords[0] + hx;        coords[16]  = coords[1] + 0.5*hy;      coords[17] = coords[2];

        coords[18]  = coords[0];            coords[19]  = coords[1] + hy;      coords[20] = coords[2];
        coords[21] = coords[0] + 0.5*hx;    coords[22]  = coords[1] + hy;      coords[23] = coords[2];
        coords[24] = coords[0] + hx;        coords[25]  = coords[1] + hy;      coords[26] = coords[2];

        coords[27] = coords[0];             coords[28]  = coords[1] ;      coords[29] = coords[2] + 0.5*hz;
        coords[30] = coords[0] + 0.5*hx;    coords[31]  = coords[1] ;      coords[32] = coords[2] + 0.5*hz;
        coords[33] = coords[0] + hx;        coords[34]  = coords[1] ;      coords[35] = coords[2] + 0.5*hz;

        coords[36] = coords[0];             coords[37]  = coords[1] + 0.5*hy;      coords[38] = coords[2] + 0.5*hz;
        coords[39] = coords[0] + 0.5*hx;    coords[40]  = coords[1] + 0.5*hy;      coords[41] = coords[2] + 0.5*hz;
        coords[42] = coords[0] + hx;        coords[43]  = coords[1] + 0.5*hy;      coords[44] = coords[2] + 0.5*hz;

        coords[45] = coords[0];             coords[46]  = coords[1] + hy;      coords[47] = coords[2] + 0.5*hz;
        coords[48] = coords[0] + 0.5*hx;    coords[49]  = coords[1] + hy;      coords[50] = coords[2] + 0.5*hz;
        coords[51] = coords[0] + hx;        coords[52]  = coords[1] + hy;      coords[53] = coords[2] + 0.5*hz;

        coords[54] = coords[0];             coords[55]  = coords[1] ;      coords[56] = coords[2] + hz;
        coords[57] = coords[0] + 0.5*hx;    coords[58]  = coords[1] ;      coords[59] = coords[2] + hz;
        coords[60] = coords[0] + hx;        coords[61]  = coords[1] ;      coords[62] = coords[2] + hz;

        coords[63] = coords[0];             coords[64]  = coords[1] + 0.5*hy ;      coords[65] = coords[2] + hz;
        coords[66] = coords[0] + 0.5*hx;    coords[67]  = coords[1] + 0.5*hy ;      coords[68] = coords[2] + hz;
        coords[69] = coords[0] + hx;        coords[70]  = coords[1] + 0.5*hy ;      coords[71] = coords[2] + hz;

        coords[72] = coords[0];             coords[73]  = coords[1] + hy;           coords[74] = coords[2] + hz;
        coords[75] = coords[0] + 0.5*hx;    coords[76]  = coords[1] + hy;           coords[77] = coords[2] + hz;
        coords[78] = coords[0] + hx;        coords[79]  = coords[1] + hy;           coords[80] = coords[2] + hz;


    }
}

/**
  @brief builds coordinates in ZEROPTV vector.
  @param [in] coords: octant coordinates
  @param [out] coords: physical coordinates
  @param [in] ele_order element order
  @param [in] problemSize : problem size / scaling factor
  @param [out] node: ZEROPTV vector of node position. The order is first x, then y and then z.
  @param [in] isAllocated : true if size of node is allocated, false otherwise
 **/

void coordsToZeroptv(double *coords , std::vector<ZEROPTV >&node, const double * problemSize, const DENDRITE_UINT ele_order, bool isAllocated){
  DENDRITE_UINT nnodes;
  if(ele_order == 1){
    nnodes = 8;
  }
  else if (ele_order == 2){
    nnodes = 27;
  } else{
    throw TALYFEMLIB::TALYException() << "Wrong elemental order \n";
  }
  for(int i = 0; i < nnodes; i++){
    convertOctToPhys(&coords[3*i],problemSize);
  }
  if(not(isAllocated)){
    node.resize(nnodes);
  }
  for(int i = 0; i < nnodes; i++){
    node[i] = {coords[3*i], coords[3*i + 1], coords[3*i+2]};
  }

}

void treeNodesToZEROPTV(const ot::TreeNode *treeNode, std::vector<ZEROPTV> &coords, const DENDRITE_REAL *problem_size) {

    DENDRITE_UINT x = treeNode->getX();
    DENDRITE_UINT y = treeNode->getY();
    DENDRITE_UINT z = treeNode->getZ();
    DENDRITE_UINT len = (1u << (m_uiMaxDepth - ((treeNode->getLevel() & ot::TreeNode::MAX_LEVEL) + 1)));

    {
        Point oct(x, y, z);
        Point phys = convertOctToPhys(oct);
        coords[0] = TALYFEMLIB::ZEROPTV{phys.x(), phys.y(), phys.z()};
    }

    {
        Point oct(x + len, y, z);
        Point phys = convertOctToPhys(oct);
        coords[1] = TALYFEMLIB::ZEROPTV{phys.x(), phys.y(), phys.z()};
    }

    {
        Point oct(x, y + len, z);
        Point phys = convertOctToPhys(oct);
        coords[2] = TALYFEMLIB::ZEROPTV{phys.x(), phys.y(), phys.z()};
    }

    {
        Point oct(x + len, y + len, z);
        Point phys = convertOctToPhys(oct);
        coords[3] = TALYFEMLIB::ZEROPTV{phys.x(), phys.y(), phys.z()};
    }

    {
        Point oct(x, y, z + len);
        Point phys = convertOctToPhys(oct);
        coords[4] = TALYFEMLIB::ZEROPTV{phys.x(), phys.y(), phys.z()};
    }

    {
        Point oct(x + len, y, z + len);
        Point phys = convertOctToPhys(oct);
        coords[5] = TALYFEMLIB::ZEROPTV{phys.x(), phys.y(), phys.z()};
    }

    {
        Point oct(x, y + len, z + len);
        Point phys = convertOctToPhys(oct);
        coords[6] = TALYFEMLIB::ZEROPTV{phys.x(), phys.y(), phys.z()};
    }

    {
        Point oct(x + len, y + len, z + len);
        Point phys = convertOctToPhys(oct);
        coords[7] = TALYFEMLIB::ZEROPTV{phys.x(), phys.y(), phys.z()};
    }




}
