//
// Created by makrand on 10/4/19.
//
#include <oda.h>
#include "Checkpoining/checkpointer.hpp"
#include <TimeInfo.h>

#include <libconfig.h++>

// used for remove folder, reference:
// https://stackoverflow.com/questions/5467725/how-to-delete-a-directory-and-its-contents-in-posix-c
int unlink_cb(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf) {
  int rv = remove(fpath);
  if (rv) {
    perror(fpath);
  }
  return rv;
}

// used for remove folder
int rmrf(char *path) {
  return nftw(path, unlink_cb, 64, FTW_DEPTH | FTW_PHYS);
}

/** String Utils **/
void checkpointer::storeCheckpoint(const ot::DA *da,
                                   std::vector<VecInfo> &vecs, TimeInfo *ti) {
  current_checkpoint_++;

  // only write files every frequency_ calls to checkpoint()
  if (current_checkpoint_ % frequency_ != 0) {
    return;
  }

  // remove and rename backup folder
  shift_backups();
  MPI_Barrier(da->getCommActive());
  auto foldername = get_backup_folder_name(0);
  std::string filename_oct = foldername + "/" + file_prefix_ + "_oct_rank_" + std::to_string(da->getRankActive());
  std::string filename_vec = foldername + "/" + file_prefix_ + "_val_rank_" + std::to_string(da->getRankActive());
  std::string filename_cfg = foldername + "/" + file_prefix_ + "_cfg_backup";

  write_octree(da, filename_oct);

  write_values(da, vecs, filename_vec);

  write_cfg(da, filename_cfg, vecs.size(), ti);
}



void checkpointer::write_octree(const ot::DA *da, const std::string &filename) {

  if (da->isActive()) {
    unsigned int rank = da->getMesh()->getMPIRank();
    unsigned int npes = da->getMesh()->getMPICommSize();

    const ot::TreeNode *pNodes = &(*(da->getMesh()->getAllElements().begin()
        + da->getMesh()->getElementLocalBegin()));

    io::checkpoint::writeOctToFile(filename.c_str(), pNodes, da->getMesh()->getNumLocalMeshElements());

  }

}

void checkpointer::write_values(const ot::DA *da, std::vector<VecInfo> &vecs, const std::string &filename) {
  const ot::Mesh *pMesh = da->getMesh();

  if (pMesh->isActive()) {
    unsigned int numNodes = pMesh->getNumLocalMeshNodes() + pMesh->getNumPreMeshNodes() + pMesh->getNumPostMeshNodes();
    unsigned int nLocalBegin = pMesh->getNodeLocalBegin();
    unsigned int nLocalEnd = pMesh->getNodeLocalEnd();
    if (TALYFEMLIB::GetMPIRank() == 0) {
      dof_.resize(vecs.size());
      nodeDataIndex_.resize(vecs.size());
    }
    for (int i = 0; i < vecs.size(); i++) {

      if (TALYFEMLIB::GetMPIRank() == 0) {
        dof_[i] = vecs[i].ndof;
        nodeDataIndex_[i] = vecs[i].nodeDataIndex;
      }
      std::string fname = filename + "_" + std::to_string(i);

      FILE *outfile = fopen(fname.c_str(), "w");
      VecGetArrayRead(vecs[i].v, &vecs[i].val);
      if (outfile == NULL) { std::cout << fname << " file open failed " << std::endl; }

      fwrite(&numNodes, sizeof(unsigned int), 1, outfile);
      fwrite(&nLocalBegin, sizeof(unsigned int), 1, outfile);
      fwrite(&nLocalEnd, sizeof(unsigned int), 1, outfile);

      if (numNodes > 0) {
        fwrite((vecs[i].val), sizeof(DENDRITE_REAL), pMesh->getNumLocalMeshNodes() * vecs[i].ndof, outfile);
      }
      VecRestoreArrayRead(vecs[i].v, &vecs[i].val);
      fclose(outfile);
    }

  }
}

void checkpointer::read_octree(const std::string &filename, std::vector<ot::TreeNode> &pNodes) {

  io::checkpoint::readOctFromFile(filename.c_str(), pNodes);

}

void checkpointer::loadFromCheckPointing(ot::DA *&da, std::vector<VecInfo> &vecs_out, TimeInfo *ti_out) {
  std::string foldername = file_prefix_;
  std::string filename_oct = foldername + "/" + file_prefix_ + "_oct_rank_" + std::to_string(TALYFEMLIB::GetMPIRank());
  std::string filename_vec = foldername + "/" + file_prefix_ + "_val_rank_" + std::to_string(TALYFEMLIB::GetMPIRank());
  std::string filename_cfg = foldername + "/" + file_prefix_ + "_cfg_backup";
  read_cfg(filename_cfg, ti_out);
  bool isActive = (TALYFEMLIB::GetMPIRank() < r_numActiveProc_);
  std::vector<ot::TreeNode> treeNodes;
  if (isActive) {
    read_octree(filename_oct.c_str(), treeNodes);
  }

  ot::Mesh *mesh = new ot::Mesh(treeNodes, 1, static_cast<DENDRITE_UINT >(eleOrder_), static_cast<DENDRITE_UINT >
  (r_numActiveProc_), MPI_COMM_WORLD, true, ot::SM_TYPE::FEM_CG);
  da = new ot::DA(mesh);
  if (isActive) {
    read_values(da, filename_vec, vecs_out);
  }

}


/**
 * Delete folder of old checkpoint and rename the new folder
 */
void checkpointer::shift_backups() {
  int ierr;

  if (TALYFEMLIB::GetMPIRank() == 0 && n_backups_ > 0) {
    for (int i = n_backups_ - 1; i > 0; i--) {
      auto new_foldername = get_backup_folder_name(i - 1);
      auto old_foldername = get_backup_folder_name(i);

      // check if the old folder exists
      ierr = access(old_foldername.c_str(), F_OK);
      if (ierr == 0 || errno != ENOENT) {
        // remove old folder
        char folder[PATH_MAX];
        snprintf(folder, sizeof(folder), old_foldername.c_str());
        ierr = rmrf(folder);
        if (ierr != 0) {
          throw TALYFEMLIB::TALYException() << "Folder cannot be removed";
        }
      }
      // check if the new folder exists
      ierr = access(new_foldername.c_str(), F_OK);
      if (ierr == 0 || errno != ENOENT) {
        // rename new folder
        ierr = rename(new_foldername.c_str(), old_foldername.c_str());
        if (ierr != 0) {
          // there was an error moving the backup (and the error isn't that old filename doesn't exist)
          throw TALYFEMLIB::TALYException() << "Could not rename backup '" << old_foldername
                                            << "' to '" << new_foldername << "' (errno: " << errno << ")";
        }
      }
    }
  }

}

std::string checkpointer::get_backup_folder_name(int i) const {
  if (i == 0) {
    char folder[PATH_MAX];
    snprintf(folder, sizeof(folder), file_prefix_.c_str());
    int ierr = mkdir(folder, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (ierr != 0 && errno != EEXIST) {
      PrintError("Could not create folder for storing results (", strerror(errno), ").");
    }
    return file_prefix_;
  } else {
    return file_prefix_ + "_" + std::to_string(i);
  }
}

void checkpointer::read_values(ot::DA *da, const std::string &filename, std::vector<VecInfo> &vec) {

  vec.resize(r_numVecs);

  const ot::Mesh *pMesh = da->getMesh();

  unsigned int numNodes, nLocalBegin, nLocalEnd;

  for (unsigned int i = 0; i < vec.size(); i++) {
    std::string fname = filename + "_" + std::to_string(i);
    FILE *infile = fopen(fname.c_str(), "r");
    if (infile == NULL) { std::cout << fname << " file open failed " << std::endl; }
    fread(&numNodes, sizeof(unsigned int), 1, infile);
    fread(&nLocalBegin, sizeof(unsigned int), 1, infile);
    fread(&nLocalEnd, sizeof(unsigned int), 1, infile);
    if (numNodes != (pMesh->getNumPreMeshNodes() + pMesh->getNumLocalMeshNodes() + pMesh->getNumPostMeshNodes())) {
      std::cout << fname << " file number of total node mismatched with the mesh. " << std::endl;
    }
    if (nLocalBegin != pMesh->getNodeLocalBegin()) {
      std::cout << fname << " file local node begin location mismatched with the mesh. " << std::endl;
    }
    if (nLocalEnd != pMesh->getNodeLocalEnd()) {
      std::cout << fname << " file local node end location mismatched with the mesh. " << std::endl;
    }
    da->petscCreateVector(vec[i].v, false, false, static_cast<unsigned int>(dof_[i]));
    double *array;
    vec[i].nodeDataIndex = static_cast<DENDRITE_UINT >(nodeDataIndex_[i]);
    vec[i].ndof = static_cast<DENDRITE_UINT >(dof_[i]);
    if (da->isActive()) {
      VecGetArray(vec[i].v, &array);
      fread((array), sizeof(DENDRITE_REAL), dof_[i] * pMesh->getNumLocalMeshNodes(), infile);
      VecRestoreArray(vec[i].v, &array);
    }
    fclose(infile);
  }

}

void checkpointer::write_cfg(const ot::DA *da, const std::string &filename, const int numVecs, const TimeInfo *ti) {

  if (TALYFEMLIB::GetMPIRank() == 0) {
    libconfig::Config cfg;
    auto &cfgGlobal = cfg.getRoot().add("global_info", libconfig::Setting::TypeGroup);
    cfgGlobal.add("Active_Comm", libconfig::Setting::TypeInt) = static_cast<int>(da->getNpesActive());
    cfgGlobal.add("MaxDepth", libconfig::Setting::TypeInt) = static_cast<int>(m_uiMaxDepth);
    cfgGlobal.add("Num_Vecs", libconfig::Setting::TypeInt) = numVecs;
    cfgGlobal.add("EleOrder", libconfig::Setting::TypeInt) = static_cast<int>(da->getElementOrder());
    auto &dof = cfgGlobal.add("Dof", libconfig::Setting::TypeArray);
    auto &nodeDataIndex = cfgGlobal.add("NodeDataIndex", libconfig::Setting::TypeArray);
    for (int i = 0; i < dof_.size(); i++) {
      dof.add(libconfig::Setting::TypeInt) = dof_[i];
      nodeDataIndex.add(libconfig::Setting::TypeInt) = nodeDataIndex_[i];
    }

    if (ti != nullptr) {
      auto &cfgTi = cfg.getRoot().add("time_info", libconfig::Setting::TypeGroup);
      cfgTi.add("current_time", libconfig::Setting::TypeFloat) = ti->getCurrentTime();
      cfgTi.add("current_step_num", libconfig::Setting::TypeInt) = (int) ti->getTimeStepNumber();

    }
    cfg.writeFile(filename.c_str());
  }
}

void checkpointer::read_cfg(const std::string &filename, TimeInfo *ti) {
  DENDRITE_REAL current_time;
  DENDRITE_UINT current_step_number;
  DENDRITE_UINT maxDepth;
  if (TALYFEMLIB::GetMPIRank() == 0) {
    libconfig::Config cfg;
    cfg.readFile(filename.c_str());

    const auto &global = cfg.getRoot()["global_info"];
    r_numActiveProc_ = global["Active_Comm"];
    r_numVecs = global["Num_Vecs"];
    dof_.clear();

    dof_.resize(r_numVecs);
    nodeDataIndex_.resize(r_numVecs);
    maxDepth = global["MaxDepth"];
    for (int i = 0; i < r_numVecs; i++) {
      nodeDataIndex_[i] = (int) global["NodeDataIndex"][i];
      dof_[i] = (int) global["Dof"][i];
    }

    eleOrder_ = global["EleOrder"];

    PrintStatus("Start reading");
    if (ti != nullptr) {
      const auto &cfgTi = cfg.getRoot()["time_info"];
      current_time = cfgTi["current_time"];
      current_step_number = cfgTi["current_step_num"];
    }
  }
  MPI_Bcast(&r_numActiveProc_, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&r_numVecs, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&maxDepth, 1, MPI_INT, 0, MPI_COMM_WORLD);
  m_uiMaxDepth = maxDepth;
  if (TALYFEMLIB::GetMPIRank()) {
    dof_.resize(r_numVecs);
    nodeDataIndex_.resize(r_numVecs);
  }
  MPI_Bcast(&dof_[0], dof_.size(), MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&nodeDataIndex_[0], nodeDataIndex_.size(), MPI_INT, 0, MPI_COMM_WORLD);
  if (ti != nullptr) {
    MPI_Bcast(&current_time, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&current_step_number, 1, MPI_UINT32_T, 0, MPI_COMM_WORLD);
    ti->setTimeStepNumber(current_step_number);
    ti->setCurrentTime(current_time);
  }
  MPI_Barrier(MPI_COMM_WORLD);
  PrintStatus("Done reading");
}

