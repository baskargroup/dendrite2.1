//
// Created by maksbh on 11/25/18.
//

#include <BasisCache.h>
#include "DataTypes.h"

BasisCache::BasisCache() : cache_ {} {
    Globals globals;
    relOrder = globals.getrelOrder();
    bf = globals.getBasisFunction();
    N_GAUSS_POINTS = (bf + relOrder + 1)*(bf + relOrder +1 )*(bf + relOrder +1 );
    num_coords = (bf + 1)*(bf + 1 )*(bf  + 1 );
    cache_.resize(MAX_LEVELS);
    positions_.resize(MAX_LEVELS);
    for (unsigned int i = 0; i < MAX_LEVELS; i++) {
        cache_[i].resize(N_GAUSS_POINTS);
        positions_[i].resize(N_GAUSS_POINTS);
    }
    for (unsigned int i = 0; i < MAX_LEVELS; i++) {
        for (unsigned int j = 0; j < N_GAUSS_POINTS; j++) {
            cache_[i][j] = new TALYFEMLIB::FEMElm(nullptr);
        }
    }
}


BasisCache::~BasisCache() {
    for (unsigned int i = 0; i < MAX_LEVELS; i++) {
        for (unsigned int j = 0; j < N_GAUSS_POINTS; j++) {

            delete cache_[i][j];
        }
    }
}

void BasisCache::init(const DENDRITE_REAL * problem_size, TALYFEMLIB::GRID* grid,unsigned int max_depth,
        unsigned int basis_flags, int ele_order, int rel_order) {


    oct_to_phys_[0] = problem_size[0] / ((PetscScalar) (1 << (max_depth )));
    oct_to_phys_[1] = problem_size[1] / ((PetscScalar) (1 << (max_depth )));
    oct_to_phys_[2] = problem_size[2] / ((PetscScalar) (1 << (max_depth )));

    for (unsigned int i = 1; i < max_depth; i++) {
        double coords[num_coords*3];
        Point origin(0.0, 0.0, 0.0);
        Point h(oct_to_phys_[0] * (1 << (max_depth - i)),
                oct_to_phys_[1] * (1 << (max_depth - i)),
                oct_to_phys_[2] * (1 << (max_depth - i)));
        build_taly_coordinates(coords, origin, h,bf);

        assert(grid->n_nodes() == num_coords);
        for (unsigned int j = 0; j < num_coords; j++) {
            grid->GetNode(j)->location() = TALYFEMLIB::ZEROPTV(coords[j*3 + 0], coords[j*3+1], coords[j*3+2]);
        }

        for (unsigned int j = 0; j < N_GAUSS_POINTS; j++) {
            *cache_[i][j] = TALYFEMLIB::FEMElm(grid, basis_flags);
            cache_[i][j]->refill(0, rel_order);
            for (unsigned int k = 0; k <= j; k++)
                cache_[i][j]->next_itg_pt();
            positions_[i][j] = cache_[i][j]->position();
        }
    }


}

const TALYFEMLIB::FEMElm* BasisCache::get(unsigned char level, unsigned int gp_idx, const TALYFEMLIB::ZEROPTV& offset) {
    assert(level >= 0 && level < MAX_LEVELS);
    assert(gp_idx >= 0 && gp_idx < N_GAUSS_POINTS);

    TALYFEMLIB::FEMElm* fe = cache_[level][gp_idx];
    /*TALYFEMLIB::ZEROPTV p(offset.x() * oct_to_phys_[0],
                          offset.y() * oct_to_phys_[1],
                          offset.z() * oct_to_phys_[2]);*/
    fe->set_position(positions_[level][gp_idx] + offset);

    return fe;
}