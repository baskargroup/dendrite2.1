////
//// Created by maksbh on 11/28/18.
////
//
#include <PETSc/Solver/Solver.h>
#include <Profiling/GlobalProfiler.h>

namespace PETSc {
//int gBoundaryCondition;
    Solver::Solver() {
        //gBoundaryConditionTimer.init(gBoundaryCondition,"gBoundaryCondition");
        m_octDA = NULL;
        m_da = NULL;

        m_Mat = NULL;
        m_Vec = NULL;

        // Matrix
        m_matJacobian = NULL;

        m_vecRHS = NULL;
        m_vecSolution = NULL;

        m_uiDof = 0;


    }

    void Solver::setMatrix(feMat *taly) {
        m_Mat = taly;
    }

    void Solver::setVector(feVec *taly) {
        m_Vec = taly;
    }

    void Solver::setDof(const unsigned int dof) {
        m_uiDof = dof;
        boundary_conditions_.setDof(dof);
    }
    void Solver::setDA(ot::DA *da) {
        m_octDA = da;
    }

    void Solver::setProblemSize(const double *size) {
        m_problemSize[0] = size[0];
        m_problemSize[1] = size[1];
        m_problemSize[2] = size[2];
    }

    void Solver::setBoundaryCondition(const std::function<Boundary(double, double, double, unsigned int)> &f) {
        m_boundaryCondition = f;
    }

    const std::function<Boundary(double, double, double, unsigned int)> & Solver::getBoundaryCondition() {
        return m_boundaryCondition;
    }
    void Solver::loopOctantsBC(bool loopOverAllOctantsBC) {
        loopOverAllOctants  = loopOverAllOctantsBC;
    }
    void Solver::updateBoundaries(ot::DA *da) {

        PetscLogEventBegin(gUpdateBoundaryConditionEvent,0,0,0,0);
        if (m_boundaryCondition) {
            boundary_conditions_.clear();
            boundary_conditions_.addByNodalFunction(da, m_problemSize, m_uiDof, m_boundaryCondition,loopOverAllOctants);
        }
        PetscLogEventEnd(gUpdateBoundaryConditionEvent,0,0,0,0);

    }



}