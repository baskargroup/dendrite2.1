//
// Created by maksbh on 12/3/18.
//

#include <PETSc/Solver/NonLinearSolver.h>

namespace PETSc{
    PetscErrorCode NonlinearSolver::init() {

        PetscLogEventBegin(gSolverInitEvent,0,0,0,0);
        assert(m_vecSolution == NULL);
        assert(m_vecRHS == NULL);
        assert(m_uiDof != 0);

        int ierr;

        // Allocate memory for working vectors
        ierr = m_octDA->petscCreateVector(m_vecSolution, false, false, m_uiDof); CHKERRQ(ierr);
        ierr = m_octDA->petscCreateVector(m_vecRHS, false, false, m_uiDof); CHKERRQ(ierr);

        m_timers.resize(Timers::MAX_TIMERS,0.0);


        if(m_matrixFree) {
            //Matrix free
            int matsize;
            ierr = VecGetLocalSize(m_vecRHS, &matsize);
            CHKERRQ(ierr);
            ierr = MatCreateShell(PETSC_COMM_WORLD, matsize, matsize, PETSC_DETERMINE, PETSC_DETERMINE, this,
                                  &m_matJacobian);
            CHKERRQ(ierr);
            ierr = MatShellSetOperation(m_matJacobian, MATOP_MULT, (void (*)()) (ShellMatMult));
            CHKERRQ(ierr);
        }
        else{
            ierr =  m_octDA->createMatrix(m_matJacobian, MATAIJ, m_uiDof); CHKERRQ(ierr);
        }

        // avoids new nonzero errors when boundary conditions are used
        ierr = MatSetOption(m_matJacobian, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE); CHKERRQ(ierr);

        // Create a KSP context to solve  @ every timestep
        ierr = SNESCreate(m_octDA->getCommActive(), &m_snes); CHKERRQ(ierr);
        ierr = SNESSetFunction(m_snes, m_vecRHS, FormFunction, this); CHKERRQ(ierr);
        ierr = SNESSetJacobian(m_snes, m_matJacobian, m_matJacobian, FormJacobian, this); CHKERRQ(ierr);

        KSP ksp;
        SNESGetKSP(m_snes,&ksp);
        if(!m_matrixFree) {
            ierr = KSPSetPreSolve(ksp, PreKSP, this);  CHKERRQ(ierr);
            ierr = KSPSetPostSolve(ksp, PostKSP, this);  CHKERRQ(ierr);
        }


        ierr = SNESSetFromOptions(m_snes); CHKERRQ(ierr);
        PetscLogEventEnd(gSolverInitEvent,0,0,0,0);
        return 0;
    }

    PetscErrorCode NonlinearSolver::solve() {
        if (m_octDA->isActive()) {
            int ierr;
            updateBoundaries(m_octDA);

            PetscLogEventBegin(gApplyVecBCEvent,0,0,0,0);
            ierr = boundary_conditions_.applyVecBC(m_octDA, m_vecSolution);
            CHKERRQ(ierr);



            ierr = VecAssemblyBegin(m_vecSolution);
            CHKERRQ(ierr);
            ierr = VecAssemblyEnd(m_vecSolution);
            CHKERRQ(ierr);
            PetscLogEventEnd(gApplyVecBCEvent,0,0,0,0);


            PetscLogEventBegin(gNonLinearSolveEvent,0,0,0,0);
            t1 = std::chrono::high_resolution_clock::now();
            ierr = SNESSolve(m_snes, PETSC_NULL, m_vecSolution);
            t2 = std::chrono::high_resolution_clock::now();
            m_timers[Timers::Solve] +=  std::chrono::duration<double>(t2 - t1).count();
            PetscLogEventEnd(gNonLinearSolveEvent,0,0,0,0);

            CHKERRQ(ierr);
            SNESConvergedReason converged_reason;
            SNESGetConvergedReason(m_snes, &converged_reason);

            if (converged_reason < 0) {
                // diverged
                TALYFEMLIB::PrintWarning("Non-linear solve diverged.");

            }
        }

    }

    PetscErrorCode NonlinearSolver::jacobianMatMult(Vec in, Vec out) {
        assert(m_matrixFree);
        assert(m_Mat != NULL);
        PetscLogEventBegin(gSolverJacobianMatMultEvent,0,0,0,0);
        int ierr;
        ierr = VecZeroEntries(out); CHKERRQ(ierr);

        m_Mat->setPlaceholder(m_guess);
        m_Mat->matVec(in, out);
        PetscLogEventEnd(gSolverJacobianMatMultEvent,0,0,0,0);

        PetscLogEventBegin(gSolverJacobianMatFreeBCEvent,0,0,0,0);
        ierr = getBC().applyMatrixFreeBC(m_octDA, in, out); CHKERRQ(ierr);
        PetscLogEventEnd(gSolverJacobianMatFreeBCEvent,0,0,0,0);

        return 0;
    }

    PetscErrorCode NonlinearSolver::FormJacobian(SNES snes, Vec sol, Mat jac, Mat precond_matrix, void *ctx) {

        int ierr;
        NonlinearSolver *nl = (NonlinearSolver *) ctx;

        // note: jacobianMatMult depends on this staying set
        nl->m_guess = sol;

        ierr = MatSetOption(jac, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE);
        if(!nl->m_matrixFree){
            PetscLogEventBegin(gSolverJacobianAssemblyEvent,0,0,0,0);
            ierr = MatZeroEntries(jac); CHKERRQ(ierr);

            nl->m_Mat->setPlaceholder(nl->m_guess);
            std::chrono::high_resolution_clock::time_point tmatStart = std::chrono::high_resolution_clock::now();
            PetscLogEventBegin(gMatVolumeGaussPointEvent,0,0,0,0);
            nl->t1 = std::chrono::high_resolution_clock::now();
            nl->m_Mat->getAssembledMatrix(&jac, 0);
            nl->t2 = std::chrono::high_resolution_clock::now();
            nl->m_timers[Timers::MatrixAssembly] += std::chrono::duration<double>(nl->t2 - nl->t1).count();
            PetscLogEventEnd(gMatVolumeGaussPointEvent,0,0,0,0);
            /// Surface integral for IBM
            PetscLogEventBegin(gMatSurfaceGaussPointEvent,0,0,0,0);
            nl->t1 = std::chrono::high_resolution_clock::now();
            nl->surfaceIntegralJac(&jac,sol);
            nl->t2 = std::chrono::high_resolution_clock::now();
            nl->m_timers[Timers::IBM_MatrixAssembly] += std::chrono::duration<double>(nl->t2 - nl->t1).count();
            PetscLogEventEnd(gMatSurfaceGaussPointEvent,0,0,0,0);
            std::chrono::high_resolution_clock::time_point tmatEnd = std::chrono::high_resolution_clock::now();
            nl->m_timers[Timers::TotalMatrixTime] += std::chrono::duration<double>(tmatEnd - tmatStart).count();
            ierr = MatAssemblyBegin(jac, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
            ierr = MatAssemblyEnd(jac, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
            PetscLogEventEnd(gSolverJacobianAssemblyEvent,0,0,0,0);

            PetscLogEventBegin(gSolverJacobianBCEvent,0,0,0,0);
            ierr = nl->getBC().applyMatBC(nl->m_octDA, jac); CHKERRQ(ierr);
            PetscLogEventEnd(gSolverJacobianBCEvent,0,0,0,0);

        }

        nl->m_Mat->setPlaceholder(NULL);

        return 0;
    }
    PetscErrorCode NonlinearSolver::FormFunction(SNES snes, Vec in, Vec out, void *ctx) {

        int ierr;
        NonlinearSolver *nl = (NonlinearSolver *) ctx;

        nl->m_guess = in;

        PetscLogEventBegin(gSolverVectorAssemblyEvent,0,0,0,0);
        nl->m_Vec->setPlaceholder(nl->m_guess);
        VecZeroEntries(out);
        PetscLogEventBegin(gVecVolumeGaussPointEvent,0,0,0,0);
        nl->t1 = std::chrono::high_resolution_clock::now();
        nl->m_Vec->computeVec(in,out);
        nl->t2 = std::chrono::high_resolution_clock::now();
        nl->m_timers[Timers::VectorAssembly] += std::chrono::duration<double>(nl->t2 - nl->t1).count();
        PetscLogEventEnd(gVecVolumeGaussPointEvent,0,0,0,0);

        PetscLogEventBegin(gVecSurfaceGaussPointEvent,0,0,0,0);
        nl->t1 = std::chrono::high_resolution_clock::now();
        nl->surfaceIntegralFunc(in, out);
        nl->t2 = std::chrono::high_resolution_clock::now();
        nl->m_timers[Timers::IBM_VectorAssembly] += std::chrono::duration<double>(nl->t2 - nl->t1).count();
        PetscLogEventEnd(gVecSurfaceGaussPointEvent,0,0,0,0);
        ierr = nl->getBC().applyResidualBC(nl->m_octDA, out); CHKERRQ(ierr);
        ierr = VecAssemblyBegin(out); CHKERRQ(ierr);
        ierr = VecAssemblyEnd(out); CHKERRQ(ierr);
        PetscLogEventEnd(gSolverVectorAssemblyEvent,0,0,0,0);
        nl->m_Vec->setPlaceholder( NULL);
        return 0;
    }

    NonlinearSolver::~NonlinearSolver() {
        cleanup();
    }

    PetscErrorCode NonlinearSolver::cleanup() {
        int ierr;
        ierr = SNESDestroy(&m_snes); CHKERRQ(ierr);
        ierr = MatDestroy(&m_matJacobian); CHKERRQ(ierr);
        ierr = VecDestroy(&m_vecSolution); CHKERRQ(ierr);
        ierr = VecDestroy(&m_vecRHS); CHKERRQ(ierr);
        ierr = PetscObjectRegisterDestroyAll(); CHKERRQ(ierr);

        m_guess = NULL;  // managed by petsc
        return 0;
    }

    void NonlinearSolver::set_mfree(bool mfree) {
        m_matrixFree = mfree;
    }

    PetscErrorCode NonlinearSolver::PreKSP(KSP ksp, Vec in, Vec out, void *prectx){
        NonlinearSolver *nl = (NonlinearSolver *) prectx;
        int ierr;
        ierr = nl->m_octDA->petscChangeVecToMatBased(in, false, false, nl->m_uiDof); CHKERRQ(ierr);
        ierr = nl->m_octDA->petscChangeVecToMatBased(out, false, false, nl->m_uiDof); CHKERRQ(ierr);
        return (0);

    }

    PetscErrorCode NonlinearSolver::PostKSP(KSP ksp, Vec in, Vec out, void *prectx){
        NonlinearSolver *nl = (NonlinearSolver *) prectx;
        int ierr;
        ierr = nl->m_octDA->petscChangeVecToMatFree(in, false, false, nl->m_uiDof); CHKERRQ(ierr);
        ierr = nl->m_octDA->petscChangeVecToMatFree(out, false, false, nl->m_uiDof);  CHKERRQ(ierr);
        return 0;
    }



}


