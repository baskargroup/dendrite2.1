//
// Created by maksbh on 11/28/18.
//

#include <PETSc/Solver/LinearSolver.h>
#include <Profiling/GlobalProfiler.h>
#include <petscksp.h>
//#include <PETSc/PetscGlobalProfiler.h>
//#include <PETSc/PetscTimings.h>
namespace PETSc {
PetscErrorCode LinearSolver::init() {
  PetscLogEventBegin(gSolverInitEvent, 0, 0, 0, 0);
  int ierr;  // for petsc error codes
  ierr = m_octDA->petscCreateVector(m_vecSolution, false, false, m_uiDof);
  CHKERRQ(ierr);
  ierr = m_octDA->petscCreateVector(m_vecRHS, false, false, m_uiDof);
  CHKERRQ(ierr);

  if (m_matrixFree) {
    int matsize;
    ierr = VecGetLocalSize(m_vecSolution, &matsize);
    CHKERRQ(ierr);
    ierr = MatCreateShell(m_octDA->getCommActive(), matsize, matsize, PETSC_DETERMINE, PETSC_DETERMINE, this,
                          &m_matJacobian);
    CHKERRQ(ierr);

    ierr = MatShellSetOperation(m_matJacobian, MATOP_MULT, (void (*)()) (ShellMatMult));
    CHKERRQ(ierr);
    ierr = MatShellSetOperation(m_matJacobian, MATOP_GET_DIAGONAL, (void (*)(void)) MatShellGetDiagonal);
    CHKERRQ(ierr);
#if ((PETSC_VERSION_MAJOR >= 3) and(PETSC_VERSION_MINOR >= 8))
    ierr = MatShellSetOperation(m_matJacobian, MATOP_GET_DIAGONAL_BLOCK, (void (*)(void)) MatShellGetBlockDiagonal);
    CHKERRQ(ierr);
#endif

    ierr = MatCreate(m_octDA->getCommActive(), &preConditionedMatrix);
    CHKERRQ(ierr);
    MatSetSizes(preConditionedMatrix, matsize, matsize, PETSC_DECIDE, PETSC_DECIDE);
    MatSetFromOptions(preConditionedMatrix);
    MatSetUp(preConditionedMatrix);
    MatZeroEntries(preConditionedMatrix);
  } else {
    ierr = m_octDA->createMatrix(m_matJacobian, MATAIJ, m_uiDof);
    CHKERRQ(ierr);
    // avoids new nonzero errors when boundary conditions are used
    ierr = MatSetOption(m_matJacobian, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
    CHKERRQ(ierr);
    ierr = MatSetOption(m_matJacobian, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE);
    CHKERRQ(ierr);
  }


  // create a KSP context
  ierr = KSPCreate(m_octDA->getCommActive(), &m_ksp);
  CHKERRQ(ierr);
  ierr = KSPSetOperators(m_ksp, m_matJacobian, m_matJacobian);
  CHKERRQ(ierr);
  if (!m_matrixFree) {
    ierr = KSPSetPreSolve(m_ksp, PreKSP, this);
    CHKERRQ(ierr);
    ierr = KSPSetPostSolve(m_ksp, PostKSP, this);
    CHKERRQ(ierr);
  }
  ierr = KSPSetFromOptions(m_ksp);

  CHKERRQ(ierr);
  PetscLogEventEnd(gSolverInitEvent, 0, 0, 0, 0);

}

PetscErrorCode LinearSolver::matGetBlockDiagonal(Mat M, Mat *Out) {
  if(m_octDA->isActive()){
    PetscErrorCode ierr;
    m_Mat->petscGetMatBlockDiagonal(preConditionedMatrix);
    MatAssemblyBegin(preConditionedMatrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(preConditionedMatrix, MAT_FINAL_ASSEMBLY);
    ierr = getBC().applyMatBC(m_octDA, preConditionedMatrix); CHKERRQ(ierr);
    MatGetDiagonalBlock(preConditionedMatrix, Out);
  }
  return 0;
}


PetscErrorCode LinearSolver::matGetDiagonal(Mat M, Vec Out){
  if(m_octDA->isActive()){
    m_Mat->petscGetMatDiagonal(Out);
  }
  return 0;
}
PetscErrorCode LinearSolver::solve() {
  if (m_octDA->isActive()) {
    int ierr;
    // update Boundaries
    updateBoundaries(m_octDA);
    ierr = VecZeroEntries(m_vecRHS);
    CHKERRQ(ierr);

    PetscLogEventBegin(gSolverVectorAssemblyEvent, 0, 0, 0, 0);
    m_Vec->computeVec(m_vecSolution, m_vecRHS);
    surfaceIntegralFunc(m_vecSolution, m_vecRHS);
    ierr = getBC().applyVecBC(m_octDA, m_vecRHS);
    CHKERRQ(ierr);
    ierr = VecAssemblyBegin(m_vecRHS);
    CHKERRQ(ierr);
    ierr = VecAssemblyEnd(m_vecRHS);
    CHKERRQ(ierr);
    PetscLogEventEnd(gSolverVectorAssemblyEvent, 0, 0, 0, 0);

    if (!m_matrixFree) {
      PetscLogEventBegin(gMatAssemblyEvent, 0, 0, 0, 0);
      ierr = MatZeroEntries(m_matJacobian);
      CHKERRQ(ierr);

      m_Mat->getAssembledMatrix(&m_matJacobian, MATAIJ);
      /// surface integral for IBM
      surfaceIntegralJac(&m_matJacobian, m_vecSolution);

      ierr = MatAssemblyBegin(m_matJacobian, MAT_FINAL_ASSEMBLY);
      CHKERRQ(ierr);
      ierr = MatAssemblyEnd(m_matJacobian, MAT_FINAL_ASSEMBLY);
      CHKERRQ(ierr);
      PetscLogEventEnd(gMatAssemblyEvent, 0, 0, 0, 0);

      PetscLogEventBegin(gSolverMatBCEvent, 0, 0, 0, 0);
      ierr = getBC().applyMatBC(m_octDA, m_matJacobian);
      CHKERRQ(ierr);
      PetscLogEventEnd(gSolverMatBCEvent, 0, 0, 0, 0);
    }

    KSPSetInitialGuessNonzero(m_ksp, PETSC_TRUE);
    ierr = KSPSolve(m_ksp, m_vecRHS, m_vecSolution);
    CHKERRQ(ierr);
  }

}

PetscErrorCode LinearSolver::jacobianMatMult(Vec In, Vec Out) {

  PetscLogEventBegin(gMatMultEvent, 0, 0, 0, 0);
  assert(m_matrixFree);
  int ierr;
  ierr = VecZeroEntries(Out);
  CHKERRQ(ierr);
  m_Mat->matVec(In, Out);
  ierr = getBC().applyMatrixFreeBC(m_octDA, In, Out);
  CHKERRQ(ierr);
  PetscLogEventEnd(gMatMultEvent, 0, 0, 0, 0);
  return 0;
}

LinearSolver::~LinearSolver() {
  cleanup();
}

PetscErrorCode LinearSolver::cleanup() {
  int ierr;
  ierr = KSPDestroy(&m_ksp);
  CHKERRQ(ierr);
  ierr = MatDestroy(&m_matJacobian);
  CHKERRQ(ierr);
  ierr = VecDestroy(&m_vecSolution);
  CHKERRQ(ierr);
  ierr = VecDestroy(&m_vecRHS);
  CHKERRQ(ierr);
  if(m_matrixFree){
    ierr = MatDestroy(&preConditionedMatrix);
    CHKERRQ(ierr);
  }
  ierr = PetscObjectRegisterDestroyAll();
  CHKERRQ(ierr);
  return 0;
}

void LinearSolver::set_mfree(bool mfree) {
  m_matrixFree = mfree;
}

bool LinearSolver::get_mfree() {
  return (m_matrixFree);
}

PetscErrorCode LinearSolver::PreKSP(KSP ksp, Vec in, Vec out, void *prectx) {

  LinearSolver *ls = (LinearSolver *) prectx;
  ls->m_octDA->petscChangeVecToMatBased(in, false, false, ls->m_uiDof);
  ls->m_octDA->petscChangeVecToMatBased(out, false, false, ls->m_uiDof);

  return (0);

}

PetscErrorCode LinearSolver::PostKSP(KSP ksp, Vec in, Vec out, void *prectx) {
  LinearSolver *ls = (LinearSolver *) prectx;
  ls->m_octDA->petscChangeVecToMatFree(in, false, false, ls->m_uiDof);
  ls->m_octDA->petscChangeVecToMatFree(out, false, false, ls->m_uiDof);
  return 0;
}
}