//
// Created by maksbh on 12/4/18.
//

#include <PETSc/PetscUtils.h>
#include <DendriteUtils.h>

namespace PETSc {
    void petscSetInitialCondition(ot::DA *da, Vec vec, int ndof,
                                  const std::function<double(double, double, double, int)> &f,
                                  const Point &domain_min, const Point &domain_max) {
      if(da->isActive()) {
        PetscScalar *array;
        VecGetArray(vec, &array);

        setScalarByFunction(da, array, ndof, f, domain_min, domain_max);

        VecRestoreArray(vec, &array);
      }
    }

    PetscScalar *petscCalcL2Error(ot::DA *da, const double *problem_size, Vec vec, int ndof,
                                  const AnalyticFunction &f) {
        if(not(da->isActive())){
            return NULL;
        }
        double *error;

        const PetscScalar *array;
        VecGetArrayRead(vec, &array);
        error = calcL2Errors(da, problem_size, array, ndof, f);
        VecRestoreArrayRead(vec, &array);


        return error;
    }

    void petscEvalFunction(ot::DA *da, const double *problemSize, Vec u, int ndof,
                           const std::vector<EvalFunction> &f, double *func_val) {
      if(not(da->isActive())){
        return ;
      }
        const PetscScalar *array;
        VecGetArrayRead(u, &array);
        evalFunction(da, problemSize, array, ndof, f, func_val);
        VecRestoreArrayRead(u, &array);
    }

void petscDumpFilesforRegressionTest(ot::DA *octDA, const Vec &v, const std::string file_prefix ){
      PetscViewer viewer;
      int ierr = PetscViewerBinaryOpen(octDA->getCommActive(), file_prefix.c_str(), FILE_MODE_WRITE, &viewer);
      assert(ierr == 0);
      ierr = VecView(v, viewer);
      assert(ierr == 0);
      ierr = PetscViewerDestroy(&viewer);
      assert(ierr == 0);
    }

}