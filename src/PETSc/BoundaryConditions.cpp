//
// Created by maksbh on 11/28/18.
//

#include <PETSc/BoundaryConditions.h>
#include <OctToPhysical.h>
namespace PETSc {

    void BoundaryConditions::clear() {
        m_boundaryRows.clear();
        m_boundaryValues.clear();
    }

    void BoundaryConditions::addByNodalFunction(ot::DA *da, const double * problem_size, const  int ndof,
                       const std::function<Boundary(double, double, double, unsigned int)> &f, const bool loopOverAllOctants) {

        if(!(da->isActive())){
            return;
        }

        std::map<unsigned int, Boundary> bdy;


//        PetscInt totalNodalSize = da->getTotalNodalSz();

        std::vector<DendroIntL> localToGlobalMap = da->getNodeLocalToGlobalMap();
        if(loopOverAllOctants){
            int npe = da->getNumNodesPerElement();
            double * coords = new double[npe*m_uiDim];
            DendroIntL *localNodeIdx = new DendroIntL[npe];
            int eleOrder = da->getElementOrder();
            for (da->init<ot::DA_FLAGS::WRITABLE>();
                 da->curr() < da->end<ot::DA_FLAGS::WRITABLE>(); da->next<ot::DA_FLAGS::WRITABLE>()) {
                da->getElementalCoords(da->curr(),coords);
                da->getNodeIndices(localNodeIdx,da->curr(),true);
                for(int i = 0; i < npe; i++) {
                    convertOctToPhys(&coords[3 * i], problem_size);
                }
                for(int i = 0; i < npe; i++) {
                    for (DENDRITE_UINT iz = 0; iz < (eleOrder + 1); iz++) {
                        for (DENDRITE_UINT iy = 0; iy < (eleOrder + 1); iy++) {
                            for (DENDRITE_UINT ix = 0; ix < (eleOrder + 1); ix++) {
                                if (not(da->getMesh()->isNodeHanging(da->curr(), ix, iy, iz))){
                                    auto boundary = f(coords[3 * i + 0], coords[3 * i + 1], coords[3 * i + 2],
                                                      static_cast<DENDRITE_UINT >(localNodeIdx[i]) );
                                    bdy[localNodeIdx[i]] = boundary;
                                }
                            }
                        }
                    }
                }
            }
            delete[] localNodeIdx;
            delete[] coords;
        }
        else {
            std::vector<DENDRITE_UINT> bdy_index;
            std::vector<DENDRITE_REAL> coords;
            da->getOctreeBoundaryNodeIndices(bdy_index, coords, true);


            // Looping over the outer boundaries to check if the dirichlet BC needs to be specified

            for (int i = 0; i < bdy_index.size(); i++) {
                convertOctToPhys(&coords[3 * i], problem_size);
                auto boundary = f(coords[3 * i + 0], coords[3 * i + 1], coords[3 * i + 2], bdy_index[i]);
                bdy[bdy_index[i]] = boundary;
            }
        }

        for (const auto &it : bdy) {
            for (const auto &direchlet : it.second.dirichlets) {
                PetscInt row = ndof * localToGlobalMap[it.first] + direchlet.first;
                m_boundaryRows.push_back(row);
                m_boundaryValues.push_back(direchlet.second);
            }

        }
    }



    PetscErrorCode BoundaryConditions::applyMatBC(ot::DA *da, Mat mat) {


        int ierr;
        // TODO cache this if it works
        IS is;
        ierr = ISCreateGeneral(da->getCommActive(), m_boundaryRows.size(), m_boundaryRows.data(), PETSC_COPY_VALUES, &is);
        CHKERRQ(ierr);
        ierr = ISSortRemoveDups(is);CHKERRQ(ierr);
        ierr = MatZeroRowsIS(mat, is, 1.0, NULL, NULL); CHKERRQ(ierr);
        ISDestroy(&is);

        return 0;
    }

    PetscErrorCode BoundaryConditions::applyVecBC(ot::DA* da, Vec rhs) {
        da->petscChangeVecToMatBased(rhs, false, false,ndof_);
        int ierr = VecSetValues(rhs, m_boundaryRows.size(), m_boundaryRows.data(), m_boundaryValues.data(), INSERT_VALUES);
        VecAssemblyBegin(rhs);
        VecAssemblyEnd(rhs);
        da->petscChangeVecToMatFree(rhs, false, false,ndof_);
        return ierr;
    }

    PetscErrorCode BoundaryConditions::applyResidualBC(ot::DA *da, Vec residual) {
        da->petscChangeVecToMatBased(residual, false, false,ndof_);
        std::vector<double> zeros(m_boundaryRows.size(), 0.0);
        int ierr = VecSetValues(residual, m_boundaryRows.size(), m_boundaryRows.data(), zeros.data(), INSERT_VALUES);
        VecAssemblyBegin(residual);
        VecAssemblyEnd(residual);
        da->petscChangeVecToMatFree(residual, false, false,ndof_);
        return ierr;
    }

    PetscErrorCode BoundaryConditions::applyMatrixFreeBC(ot::DA *da, Vec in, Vec out) {
        int ierr;
        IS is;
        int state;
        VecLockGet(in,&state);
        VecLockPop(in);
        da->petscChangeVecToMatBased(in, false, false,ndof_);
        da->petscChangeVecToMatBased(out,false, false,ndof_);
        VecLockPush(in);
        ierr = ISCreateGeneral(da->getCommActive(), m_boundaryRows.size(), m_boundaryRows.data(), PETSC_USE_POINTER, &is);
        CHKERRQ(ierr);

        VecScatter scatter;
        ierr = VecScatterCreate(in, is, out, is, &scatter);
        CHKERRQ(ierr);

        ierr = VecScatterBegin(scatter, in, out, INSERT_VALUES, SCATTER_FORWARD);
        CHKERRQ(ierr);
        ierr = VecScatterEnd(scatter, in, out, INSERT_VALUES, SCATTER_FORWARD);
        CHKERRQ(ierr);

        ierr = VecScatterDestroy(&scatter);
        CHKERRQ(ierr);
        ierr = ISDestroy(&is);
        CHKERRQ(ierr);

        VecLockGet(in,&state);
        VecLockPop(in);
        da->petscChangeVecToMatFree(in, false, false,ndof_);
        da->petscChangeVecToMatFree(out,false, false,ndof_);
        VecLockPush(in);
        return 0;
    }


}