//
// Created by maksbh on 11/25/18.
//

#include "Globals.h"
#include <libconfig.h++>
#include <iostream>
#include <assert.h>
#include <math.h>
#include <talyfem/talyfem.h>

Globals::Globals() {
    bf_ = 1;
    relOrder_ = 0;
    set_ = false;
    nsd_ = 3;
    setGlobals();
}
//
const  int Globals::getrelOrder(){
    assert(set_ == true);
    return relOrder_;
}

void Globals::setGlobals() {

    if(set_ == false) {
        std::string config_file = "config.txt";
        libconfig::Config cfg;
        try {
            cfg.readFile(config_file.c_str());
        }
        catch (const libconfig::FileIOException &fioex)
        {
          if(TALYFEMLIB::GetMPIRank() == 0) {
            TALYFEMLIB::PrintWarning("\033[1;31m Config.txt not found. Setting to default \033[0m");
          }
            set_ = true;
            return;
        }
        cfg.lookupValue("basisRelOrder", relOrder_);
        cfg.lookupValue("BasisFunction", bf_);
        cfg.lookupValue("nsd", nsd_);
        set_ = true;
    }
}
const unsigned int Globals::getBasisFunction(){
    assert(set_ == true);
    return bf_;
}

void Globals::setBasisFunction(const int bf){

    bf_ = bf;
    relOrder_ = 0;
}
void Globals::setrelOrder(const int relOrder) {
    relOrder_ = relOrder;
}

const unsigned int Globals::nsd() {
    assert(set_ == true);
    return nsd_;
}

const unsigned int Globals::ndof() {
    assert(set_ == true);
    return (pow((bf_ + 1),nsd_));
}


