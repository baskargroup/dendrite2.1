////
//// Created by maksbh on 3/11/19.
////
//
#include <Refinement/Refinement.h>
#include <PETSc/Solver/LinearSolver.h>
#include <DendriteUtils.h>
#include <Profiling/GlobalProfiler.h>

Refinement::Refinement(ot::DA *da, std::vector<VecInfo> &vec, const DENDRITE_REAL *problemSize,
                       const RefineType refineType, const bool surfaceError) {
	refineType_ = refineType;
	baseDA_ = da;


	eleOrder_ = baseDA_->getElementOrder();
	nPe_ = baseDA_->getNumNodesPerElement();
	refinementVector_ = new ot::DA_FLAGS::Refine[da->getLocalElemSz()];
	solutionVec_.resize(vec.size());
	ndof_ = 0;
	vec_buff_ = new DENDRITE_REAL *[solutionVec_.size()];

	for (int i = 0; i < vec.size(); i++) {
		ndof_ += vec[i].ndof;
		solutionVec_[i].ndof = vec[i].ndof;
		solutionVec_[i].v = vec[i].v;
		solutionVec_[i].nodeDataIndex = vec[i].nodeDataIndex;
	}

	thisVolume_.init(nPe_, ndof_);
	if(refineType_ == RefineType::VALUE_NEIGHBOUR_BASED) {
		octNeighbourValues_.init(nPe_, ndof_);
	}
	if (refineType_ == RefineType::APOSTERIORI) {
		thisSurface_.init(nPe_, ndof_);
		neighbourSurface_.init(nPe_, ndof_);
	}

	soln_ = new DENDRITE_REAL[ndof_];
	problemSize_[0] = problemSize[0];
	problemSize_[1] = problemSize[1];
	problemSize_[2] = problemSize[2];
	surfaceErrorCalc_ = surfaceError;

}

Refinement::Refinement(ot::DA *da, const DENDRITE_REAL *problemSize) {

	refineType_ = RefineType::POSITION_BASED;
	baseDA_ = da;
	refinementVector_ = new ot::DA_FLAGS::Refine[da->getLocalElemSz()];
	problemSize_[0] = problemSize[0];
	problemSize_[1] = problemSize[1];
	problemSize_[2] = problemSize[2];
}

Refinement::~Refinement() {
	delete[] refinementVector_;
}

void Refinement::cleanup() {
	if ((refineType_ == APOSTERIORI) or (refineType_ == VALUE_BASED)) {
		if (baseDA_->isActive()) {
			for (int i = 0; i < solutionVec_.size(); i++) {
				delete[] vec_buff_[i];
			}
			delete[] vec_buff_;
			delete[] soln_;
		}
	}

}
void Refinement::calcRefinementVector() {

	if (refineType_ == APOSTERIORI) {
		PetscLogEventBegin(gcalcRefinementVecAposterioriErrorBasedEvent,0,0,0,0);
		calcAposterioriRefinementVector();
		PetscLogEventEnd(gcalcRefinementVecAposterioriErrorBasedEvent,0,0,0,0);
	} else if (refineType_ == POSITION_BASED) {
		PetscLogEventBegin(gcalcRefinementVecPositionBasedEvent,0,0,0,0);
		calcPositionBasedRefinementVector();
		PetscLogEventEnd(gcalcRefinementVecPositionBasedEvent,0,0,0,0);
	} else if ((refineType_ == VALUE_BASED) or (refineType_ == VALUE_NEIGHBOUR_BASED)) {
		PetscLogEventBegin(gcalcRefinementVecValueBasedEvent,0,0,0,0);
		calcValueBasedRefinementVector();
		PetscLogEventEnd(gcalcRefinementVecValueBasedEvent,0,0,0,0);
	}
	PetscLogEventBegin(gCoarseningCheckEvent,0,0,0,0);
	checkForCoarsening();
	PetscLogEventEnd(gCoarseningCheckEvent,0,0,0,0);
	cleanup();

}

void Refinement::calcValueBasedRefinementVector() {
	using namespace TALYFEMLIB;

	if (not(baseDA_->isActive())) {
		return;
	}
	DENDRITE_REAL * in_dendro;
	in_dendro = new DENDRITE_REAL[nPe_*ndof_];

	for (int i = 0; i < solutionVec_.size(); i++) {
		VecGetArrayRead(solutionVec_[i].v, &solutionVec_[i].val);
		baseDA_->nodalVecToGhostedNodal(solutionVec_[i].val, vec_buff_[i], false, solutionVec_[i].ndof);
		baseDA_->readFromGhostBegin(vec_buff_[i], solutionVec_[i].ndof);
		baseDA_->readFromGhostEnd(vec_buff_[i], solutionVec_[i].ndof);
	}
	int num_neighbours;
	NeighbourLevel neighLevel;
	DENDRITE_UINT  currLevel;
	int counter = 0;
	for (baseDA_->init<ot::DA_FLAGS::LOCAL_ELEMENTS>();
	     baseDA_->curr() < baseDA_->end<ot::DA_FLAGS::LOCAL_ELEMENTS>();
	     baseDA_->next<ot::DA_FLAGS::LOCAL_ELEMENTS>()) {

		baseDA_->getElementalCoords(baseDA_->curr(), thisVolume_.coords);

		for (int i = 0; i < solutionVec_.size(); i++) {
			baseDA_->getElementNodalValues(vec_buff_[i], in_dendro,
			                               baseDA_->curr(), solutionVec_[i].ndof);

			if (eleOrder_ == 1){
				sync_.DendroToTalyIn<1,3>(in_dendro,&thisVolume_.soln[solutionVec_[i].nodeDataIndex],solutionVec_[i].ndof);
			}
			else if (eleOrder_ == 2) {
				sync_.DendroToTalyIn<2, 3> (in_dendro, &thisVolume_.soln[solutionVec_[i].nodeDataIndex], solutionVec_[i].ndof);
			}
			if(refineType_ == VALUE_NEIGHBOUR_BASED){

				currLevel = baseDA_->getLevel(baseDA_->curr());
				for(int numFace = 0; numFace < NUMFACES; numFace++) {
					num_neighbours = baseDA_->getFaceNeighborValues(baseDA_->curr(), vec_buff_[i],
					                                                octNeighbourValues_.neighbourValues[numFace].data(),
					                                                octNeighbourValues_.neighbourCoords[numFace].data(),
					                                                octNeighbourValues_.neighbourID[numFace].data(),
					                                                numFace, neighLevel, solutionVec_[i].ndof);


					octNeighbourValues_.numNeighbour[numFace] = num_neighbours;
					if (neighLevel == NeighbourLevel::SAME) {
						octNeighbourValues_.neighbourLevel[numFace] = currLevel;
					}
					if (neighLevel == NeighbourLevel::COARSE) {
						octNeighbourValues_.neighbourLevel[numFace] = currLevel - 1;
					}
					if (neighLevel == NeighbourLevel::REFINE) {
						octNeighbourValues_.neighbourLevel[numFace] = currLevel + 1;
					}
					for(int coord = 0; coord < (octNeighbourValues_.neighbourCoords[numFace].size()/3) ; coord++){
						convertOctToPhys(&octNeighbourValues_.neighbourCoords[numFace][3*coord]);
					}

				}
			}
		}

		totalElementError_ = calcElementalError(thisVolume_);

		refinementVector_[counter++] = calcRefineFlags();

	}
	for (int i = 0; i < solutionVec_.size(); i++) {
		VecRestoreArrayRead(solutionVec_[i].v, &solutionVec_[i].val);
	}
	delete [] in_dendro;

}

void Refinement::calcPositionBasedRefinementVector() {
	if (not(baseDA_->isActive())) {
		return;
	}

	int counter = 0;

	for (baseDA_->init<ot::DA_FLAGS::LOCAL_ELEMENTS>();
	     baseDA_->curr() < baseDA_->end<ot::DA_FLAGS::LOCAL_ELEMENTS>();
	     baseDA_->next<ot::DA_FLAGS::LOCAL_ELEMENTS>()) {
		refinementVector_[counter++] = calcRefineFlags();
	}

}

void Refinement::calcAposterioriRefinementVector() {

	using namespace TALYFEMLIB;

	if (not(baseDA_->isActive())) {
		return;
	}

	for (int i = 0; i < solutionVec_.size(); i++) {
		VecGetArrayRead(solutionVec_[i].v, &solutionVec_[i].val);
		baseDA_->nodalVecToGhostedNodal(solutionVec_[i].val, vec_buff_[i], false, solutionVec_[i].ndof);
		baseDA_->readFromGhostBegin(vec_buff_[i], solutionVec_[i].ndof);
		baseDA_->readFromGhostEnd(vec_buff_[i], solutionVec_[i].ndof);
	}

	GRID grid;
	GRID grid_neigh;

	grid.redimArrays(nPe_, 1);
	grid_neigh.redimArrays(nPe_, 1);
	for (DENDRITE_INT i = 0; i < nPe_; i++) {
		grid.node_array_[i] = new NODE();
		grid_neigh.node_array_[i] = new NODE();
	}

	ELEM *elem = new ELEM3dHexahedral();
	ELEM *elem_neigh = new ELEM3dHexahedral();
	grid.elm_array_[0] = elem;
	grid_neigh.elm_array_[0] = elem_neigh;

	DENDRITE_INT *node_id_array = new DENDRITE_INT[nPe_];
	for (int i = 0; i < nPe_; i++) {
		node_id_array[i] = i;
	}
	elem->redim(nPe_, node_id_array);
	elem_neigh->redim(nPe_, node_id_array);

	FEMElm fe(&grid, BASIS_ALL);

	/** For surface error. The space is allocated only for one of the face.
	 *
	 * **/
	DENDRITE_REAL *solnNeighbour_ = new double[MAXNEIGHBOURSIZE * nPe_ * ndof_];
	DENDRITE_REAL *coordsNeighbour = new double[MAXNEIGHBOURSIZE* nPe_ * m_uiDim];
	DENDRITE_UINT *neighID = new unsigned int[MAXNEIGHBOURSIZE];

	/// Array for the Values at the surface gauss points
	_valFEMCurrent_ = new DENDRITE_REAL[MAXNEIGHBOURSIZE * ndof_];
	_valFEMNeighbour_ = new DENDRITE_REAL[MAXNEIGHBOURSIZE * ndof_];

	/// Array for the derivatives at the surface gauss points
	/// from the point of view of the neighboring elements
	_valDFEMCurrent_ = new DENDRITE_REAL *[MAXNEIGHBOURSIZE];
	_valDFEMNeighbour_ = new DENDRITE_REAL *[MAXNEIGHBOURSIZE];

	for (int i = 0; i < MAXNEIGHBOURSIZE; i++) {
		_valDFEMCurrent_[i] = new DENDRITE_REAL[m_uiDim * ndof_];
		_valDFEMNeighbour_[i] = new DENDRITE_REAL[m_uiDim * ndof_];
	}

	TALYFEMLIB::FEMElm *fe_neigh = NULL;

	int num_neighbours;
	NeighbourLevel level;

	int counter = 0;

	for (baseDA_->init<ot::DA_FLAGS::LOCAL_ELEMENTS>();
	     baseDA_->curr() < baseDA_->end<ot::DA_FLAGS::LOCAL_ELEMENTS>();
	     baseDA_->next<ot::DA_FLAGS::LOCAL_ELEMENTS>()) {

		baseDA_->getElementalCoords(baseDA_->curr(), thisVolume_.coords);
		for (int i = 0; i < solutionVec_.size(); i++) {
			baseDA_->getElementNodalValues(vec_buff_[i], &thisVolume_.soln[solutionVec_[i].nodeDataIndex],
			                               baseDA_->curr(), solutionVec_[i].ndof);
		}

		for (int i = 0; i < nPe_; i++) {
			convertOctToPhys(&thisVolume_.coords[3 * i], problemSize_);
			grid.node_array_[i]->setCoor(thisVolume_.coords[3 * i], thisVolume_.coords[3 * i + 1],
			                             thisVolume_.coords[3 * i + 2]);
		}

		if (eleOrder_ == 1) {
			fe.refill(elem, BASIS_LINEAR, 0);
		} else if (eleOrder_ == 2) {
			fe.refill(elem, BASIS_QUADRATIC, 0);
		}

		totalElementError_ = 0.0;
		totalSurfaceError_ = 0.0;
		while (fe.next_itg_pt()) {
			totalElementError_ += calcElementalError(fe, thisVolume_);
		}

		if (surfaceErrorCalc_) {

			std::memcpy(thisSurface_.soln, thisVolume_.soln, sizeof(DENDRITE_REAL) * ndof_ * nPe_);

			std::memcpy(thisSurface_.coords, thisVolume_.coords, sizeof(DENDRITE_REAL) * m_uiDim * nPe_);

			for (int i = 0; i < solutionVec_.size(); i++) {
				num_neighbours = baseDA_->getFaceNeighborValues(baseDA_->curr(), vec_buff_[i],
				                                                &solnNeighbour_[solutionVec_[i].nodeDataIndex],
				                                                coordsNeighbour, neighID, OCT_DIR_LEFT,
				                                                level, solutionVec_[i].ndof);
			}
			if (num_neighbours > 0) {
				interploateNeighboursLeft(solnNeighbour_, coordsNeighbour, level);

				for (int i = 0; i < nPe_; i++) {
					convertOctToPhys(&neighbourSurface_.coords[3 * i], problemSize_);
				}
				for (unsigned int i = 0; i < nPe_; i++) {
					grid_neigh.node_array_[i]->setCoor(neighbourSurface_.coords[i * 3],
					                                   neighbourSurface_.coords[i * 3 + 1],
					                                   neighbourSurface_.coords[i * 3 + 2]);
				}

				fe_neigh = new FEMElm(&grid_neigh, BASIS_ALL);
				fe_neigh->refill(elem_neigh, BASIS_LINEAR, 0);
				totalSurfaceError_ += calcSurfError(fe, *fe_neigh, elem, elem_neigh, grid, grid_neigh, SIDE::DLEFT);
				delete fe_neigh;
			}

			for (int i = 0; i < solutionVec_.size(); i++) {
				num_neighbours = baseDA_->getFaceNeighborValues(baseDA_->curr(), vec_buff_[i],
				                                                &solnNeighbour_[solutionVec_[i].nodeDataIndex],
				                                                coordsNeighbour, neighID, OCT_DIR_RIGHT,
				                                                level, solutionVec_[i].ndof);
			}

			if (num_neighbours > 0) {
				interploateNeighboursRight(solnNeighbour_, coordsNeighbour, level);

				for (int i = 0; i < nPe_; i++) {
					convertOctToPhys(&neighbourSurface_.coords[3 * i], problemSize_);
				}
				for (unsigned int i = 0; i < 8; i++) {
					grid_neigh.node_array_[i]->setCoor(neighbourSurface_.coords[i * 3],
					                                   neighbourSurface_.coords[i * 3 + 1],
					                                   neighbourSurface_.coords[i * 3 + 2]);
				}
				fe_neigh = new FEMElm(&grid_neigh, BASIS_ALL);
				fe_neigh->refill(elem_neigh, BASIS_LINEAR, 0);
				totalSurfaceError_ += calcSurfError(fe, *fe_neigh, elem, elem_neigh, grid, grid_neigh, SIDE::DRIGHT);
				delete fe_neigh;
			}

			for (int i = 0; i < solutionVec_.size(); i++) {
				num_neighbours = baseDA_->getFaceNeighborValues(baseDA_->curr(), vec_buff_[i],
				                                                &solnNeighbour_[solutionVec_[i].nodeDataIndex],
				                                                coordsNeighbour, neighID, OCT_DIR_DOWN,
				                                                level, solutionVec_[i].ndof);
			}

			if (num_neighbours > 0) {
				interploateNeighboursBottom(solnNeighbour_, coordsNeighbour, level);

				for (int i = 0; i < nPe_; i++) {
					convertOctToPhys(&neighbourSurface_.coords[3 * i], problemSize_);
				}
				for (unsigned int i = 0; i < 8; i++) {
					grid_neigh.node_array_[i]->setCoor(neighbourSurface_.coords[i * 3],
					                                   neighbourSurface_.coords[i * 3 + 1],
					                                   neighbourSurface_.coords[i * 3 + 2]);
				}
				fe_neigh = new FEMElm(&grid_neigh, BASIS_ALL);
				fe_neigh->refill(elem_neigh, BASIS_LINEAR, 0);
				totalSurfaceError_ += calcSurfError(fe, *fe_neigh, elem, elem_neigh, grid, grid_neigh, SIDE::DBOTTOM);
				delete fe_neigh;
			}

			for (int i = 0; i < solutionVec_.size(); i++) {
				num_neighbours = baseDA_->getFaceNeighborValues(baseDA_->curr(), vec_buff_[i],
				                                                &solnNeighbour_[solutionVec_[i].nodeDataIndex],
				                                                coordsNeighbour, neighID, OCT_DIR_UP,
				                                                level, solutionVec_[i].ndof);
			}

			if (num_neighbours > 0) {
				interploateNeighboursTop(solnNeighbour_, coordsNeighbour, level);

				for (int i = 0; i < nPe_; i++) {
					convertOctToPhys(&neighbourSurface_.coords[3 * i], problemSize_);
				}
				for (unsigned int i = 0; i < 8; i++) {
					grid_neigh.node_array_[i]->setCoor(neighbourSurface_.coords[i * 3],
					                                   neighbourSurface_.coords[i * 3 + 1],
					                                   neighbourSurface_.coords[i * 3 + 2]);
				}

				fe_neigh = new FEMElm(&grid_neigh, BASIS_ALL);
				fe_neigh->refill(elem_neigh, BASIS_LINEAR, 0);
				totalSurfaceError_ += calcSurfError(fe, *fe_neigh, elem, elem_neigh, grid, grid_neigh, SIDE::DTOP);
				delete fe_neigh;
			}
			for (int i = 0; i < solutionVec_.size(); i++) {
				num_neighbours = baseDA_->getFaceNeighborValues(baseDA_->curr(), vec_buff_[i],
				                                                &solnNeighbour_[solutionVec_[i].nodeDataIndex],
				                                                coordsNeighbour, neighID, OCT_DIR_FRONT,
				                                                level, solutionVec_[i].ndof);
			}

			if (num_neighbours > 0) {
				interploateNeighboursFront(solnNeighbour_, coordsNeighbour, level);

				for (int i = 0; i < nPe_; i++) {
					convertOctToPhys(&neighbourSurface_.coords[3 * i], problemSize_);
				}
				for (unsigned int i = 0; i < 8; i++) {
					grid_neigh.node_array_[i]->setCoor(neighbourSurface_.coords[i * 3],
					                                   neighbourSurface_.coords[i * 3 + 1],
					                                   neighbourSurface_.coords[i * 3 + 2]);
				}
				fe_neigh = new FEMElm(&grid_neigh, BASIS_ALL);
				fe_neigh->refill(elem_neigh, BASIS_LINEAR, 0);
				totalSurfaceError_ += calcSurfError(fe, *fe_neigh, elem, elem_neigh, grid, grid_neigh, SIDE::DFRONT);
				delete fe_neigh;
			}
			for (int i = 0; i < solutionVec_.size(); i++) {
				num_neighbours = baseDA_->getFaceNeighborValues(baseDA_->curr(), vec_buff_[i],
				                                                &solnNeighbour_[solutionVec_[i].nodeDataIndex],
				                                                coordsNeighbour, neighID, OCT_DIR_BACK,
				                                                level, solutionVec_[i].ndof);
			}
			if (num_neighbours > 0) {
				interploateNeighboursBack(solnNeighbour_, coordsNeighbour, level);

				for (int i = 0; i < nPe_; i++) {
					convertOctToPhys(&neighbourSurface_.coords[3 * i], problemSize_);
				}
				for (unsigned int i = 0; i < 8; i++) {
					grid_neigh.node_array_[i]->setCoor(neighbourSurface_.coords[i * 3],
					                                   neighbourSurface_.coords[i * 3 + 1],
					                                   neighbourSurface_.coords[i * 3 + 2]);

				}
				fe_neigh = new FEMElm(&grid_neigh, BASIS_ALL);
				fe_neigh->refill(elem_neigh, BASIS_LINEAR, 0);
				totalSurfaceError_ += calcSurfError(fe, *fe_neigh, elem, elem_neigh, grid, grid_neigh, SIDE::DBACK);
				delete fe_neigh;
			}

		}
		refinementVector_[counter++] = calcRefineFlags();
	}

	for (int i = 0; i < solutionVec_.size(); i++) {
		VecRestoreArrayRead(solutionVec_[i].v, &solutionVec_[i].val);
	}

	delete[] solnNeighbour_;
	delete[] coordsNeighbour;
	delete[] neighID;
	delete[] node_id_array;
	delete[] _valFEMNeighbour_;
	delete[] _valFEMCurrent_;
	for (int i = 0; i < 4; i++) {
		delete[] _valDFEMCurrent_[i];
		delete[] _valDFEMNeighbour_[i];

	}
	delete[] _valDFEMCurrent_;
	delete[] _valDFEMNeighbour_;

}

void Refinement::interploateNeighboursLeft(const double *u, const double *coords, const NeighbourLevel &level) {

	const int eleOrder = baseDA_->getElementOrder();
	if (eleOrder > 1) {
		std::cout << "Currently only supports Linear Basis function \n";
		exit(0);
	}
	switch (level) {
	case NeighbourLevel::SAME: {
		std::memcpy(neighbourSurface_.soln, u, sizeof(double) * ndof_ * 8);
		std::memcpy(neighbourSurface_.coords, coords, sizeof(double) * 24);
		break;
	}
	case NeighbourLevel::REFINE: {
		unsigned int sz1 = 1u << (m_uiMaxDepth - baseDA_->getLevel(baseDA_->curr()) - 1);

		interpNeighbourCoords<1, 3, DLEFT>(sz1);

		for (int dof = 0; dof < ndof_; dof++) {
			neighbourSurface_.soln[0 * ndof_ + dof] = u[32 * dof + 0];
			neighbourSurface_.soln[1 * ndof_ + dof] = u[32 * dof + 1];
			neighbourSurface_.soln[2 * ndof_ + dof] = u[32 * dof + 10];
			neighbourSurface_.soln[3 * ndof_ + dof] = u[32 * dof + 11];
			neighbourSurface_.soln[4 * ndof_ + dof] = u[32 * dof + 20];
			neighbourSurface_.soln[5 * ndof_ + dof] = u[32 * dof + 21];
			neighbourSurface_.soln[6 * ndof_ + dof] = u[32 * dof + 30];
			neighbourSurface_.soln[7 * ndof_ + dof] = u[32 * dof + 31];
		}
		break;
	}
	case NeighbourLevel::COARSE: {
		/** This is currently in the original Dendro ordering **/
		unsigned int cnum = baseDA_->getMortonChildNum(baseDA_->curr());

		double *interpU_taly = new double[8 * ndof_];
		RefElement refEl(m_uiDim, eleOrder);
		for (int dof = 0; dof < ndof_; dof++) {
			refEl.I3D_Parent2Child(&u[8 * dof], &interpU_taly[8 * dof], NeighbourLeftMapOrder1[cnum]);
		}
		sync_.DendroToTalyIn<1, 3>(interpU_taly, neighbourSurface_.soln, ndof_);
		delete[] interpU_taly;
		unsigned int sz1 = 1u << (m_uiMaxDepth - baseDA_->getLevel(baseDA_->curr()));
		interpNeighbourCoords<1, 3, DLEFT>(sz1);

		break;
	}
	default:std::cout << "Wrong neighbour level in " << __func__ << "\n";
		exit(0);
	}

}

void Refinement::interploateNeighboursRight(const double *u, const double *coords, const NeighbourLevel &level) {
	const int eleOrder = baseDA_->getElementOrder();
	if (eleOrder > 1) {
		std::cout << "Currently only supports Linear Basis function \n";
		exit(0);
	}

	switch (level) {
	case NeighbourLevel::SAME: {
		std::memcpy(neighbourSurface_.soln, u, sizeof(double) * ndof_ * 8);
		std::memcpy(neighbourSurface_.coords, coords, sizeof(double) * 24);
		break;
	}
	case NeighbourLevel::REFINE: {
		unsigned int sz1 = 1u << (m_uiMaxDepth - baseDA_->getLevel(baseDA_->curr()) - 1);
		interpNeighbourCoords<1, 3, DRIGHT>(sz1);
		for (int dof = 0; dof < ndof_; dof++) {
			neighbourSurface_.soln[0 * ndof_ + dof] = u[32 * dof + 0];
			neighbourSurface_.soln[1 * ndof_ + dof] = u[32 * dof + 1];
			neighbourSurface_.soln[2 * ndof_ + dof] = u[32 * dof + 10];
			neighbourSurface_.soln[3 * ndof_ + dof] = u[32 * dof + 11];
			neighbourSurface_.soln[4 * ndof_ + dof] = u[32 * dof + 20];
			neighbourSurface_.soln[5 * ndof_ + dof] = u[32 * dof + 21];
			neighbourSurface_.soln[6 * ndof_ + dof] = u[32 * dof + 30];
			neighbourSurface_.soln[7 * ndof_ + dof] = u[32 * dof + 31];
		}

		break;
	}
	case NeighbourLevel::COARSE: {
		/** This is currently in the original Dendro ordering **/
		unsigned int cnum = baseDA_->getMortonChildNum(baseDA_->curr());
		double *interpU_taly = new double[8 * ndof_];
		RefElement refEl(m_uiDim, eleOrder);
		for (int dof = 0; dof < ndof_; dof++) {
			refEl.I3D_Parent2Child(&u[8 * dof], &interpU_taly[8 * dof], NeighbourRightMapOrder1[cnum]);
		}
		sync_.DendroToTalyIn<1, 3>(interpU_taly, neighbourSurface_.soln, ndof_);
		delete[] interpU_taly;

		unsigned int sz1 = 1u << (m_uiMaxDepth - baseDA_->getLevel(baseDA_->curr()));
		interpNeighbourCoords<1, 3, DRIGHT>(sz1);

		break;
	}
	default:std::cout << "Wrong neighbour level in " << __func__ << "\n";
		exit(0);
	}
}

void Refinement::interploateNeighboursBottom(const double *u, const double *coords, const NeighbourLevel &level) {
	const int eleOrder = baseDA_->getElementOrder();
	if (eleOrder > 1) {
		std::cout << "Currently only supports Linear Basis function \n";
		exit(0);
	}

	switch (level) {
	case NeighbourLevel::SAME: {
		std::memcpy(neighbourSurface_.soln, u, sizeof(double) * ndof_ * 8);
		std::memcpy(neighbourSurface_.coords, coords, sizeof(double) * 24);
		break;

	}
	case NeighbourLevel::REFINE: {
		unsigned int sz1 = 1u << (m_uiMaxDepth - baseDA_->getLevel(baseDA_->curr()) - 1);
		interpNeighbourCoords<1, 3, DBOTTOM>(sz1);

		for (int dof = 0; dof < ndof_; dof++) {
			neighbourSurface_.soln[ndof_ * 0 + dof] = u[32 * dof + 0];
			neighbourSurface_.soln[ndof_ * 1 + dof] = u[32 * dof + 2];
			neighbourSurface_.soln[ndof_ * 2 + dof] = u[32 * dof + 9];
			neighbourSurface_.soln[ndof_ * 3 + dof] = u[32 * dof + 11];
			neighbourSurface_.soln[ndof_ * 4 + dof] = u[32 * dof + 20];
			neighbourSurface_.soln[ndof_ * 5 + dof] = u[32 * dof + 22];
			neighbourSurface_.soln[ndof_ * 6 + dof] = u[32 * dof + 29];
			neighbourSurface_.soln[ndof_ * 7 + dof] = u[32 * dof + 31];
		}

		break;
	}
	case NeighbourLevel::COARSE: {
		/** This is currently in the original Dendro ordering **/
		unsigned int cnum = baseDA_->getMortonChildNum(baseDA_->curr());
		double *interpU_taly = new double[8 * ndof_];
		RefElement refEl(m_uiDim, eleOrder);
		for (int dof = 0; dof < ndof_; dof++) {
			refEl.I3D_Parent2Child(&u[8 * dof], &interpU_taly[8 * dof], NeighbourBottomMapOrder1[cnum]);
		}
		sync_.DendroToTalyIn<1, 3>(interpU_taly, neighbourSurface_.soln, ndof_);
		delete[] interpU_taly;

		unsigned int sz1 = 1u << (m_uiMaxDepth - baseDA_->getLevel(baseDA_->curr()));

		interpNeighbourCoords<1, 3, DBOTTOM>(sz1);

		break;
	}
	default:std::cout << "Wrong neighbour level in " << __func__ << "\n";
		exit(0);
	}

}

void Refinement::interploateNeighboursTop(const double *u, const double *coords, const NeighbourLevel &level) {

	const int eleOrder = baseDA_->getElementOrder();
	if (eleOrder > 1) {
		std::cout << "Currently only supports Linear Basis function \n";
		exit(0);
	}

	switch (level) {
	case NeighbourLevel::SAME: {
		std::memcpy(neighbourSurface_.soln, u, sizeof(double) * ndof_ * 8);
		std::memcpy(neighbourSurface_.coords, coords, sizeof(double) * 24);
		break;
	}
	case NeighbourLevel::REFINE: {
		unsigned int sz1 = 1u << (m_uiMaxDepth - baseDA_->getLevel(baseDA_->curr()) - 1);
		/** This is currently in the original Dendro ordering **/
		interpNeighbourCoords<1, 3, DTOP>(sz1);

		for (int dof = 0; dof < ndof_; dof++) {
			neighbourSurface_.soln[ndof_ * 0 + dof] = u[32 * dof + 0];
			neighbourSurface_.soln[ndof_ * 1 + dof] = u[32 * dof + 2];
			neighbourSurface_.soln[ndof_ * 2 + dof] = u[32 * dof + 9];
			neighbourSurface_.soln[ndof_ * 3 + dof] = u[32 * dof + 11];
			neighbourSurface_.soln[ndof_ * 4 + dof] = u[32 * dof + 20];
			neighbourSurface_.soln[ndof_ * 5 + dof] = u[32 * dof + 22];
			neighbourSurface_.soln[ndof_ * 6 + dof] = u[32 * dof + 29];
			neighbourSurface_.soln[ndof_ * 7 + dof] = u[32 * dof + 31];
		}

		break;
	}
	case NeighbourLevel::COARSE: {
		/** This is currently in the original Dendro ordering **/
		unsigned int cnum = baseDA_->getMortonChildNum(baseDA_->curr());
		double *interpU_taly = new double[8 * ndof_];
		RefElement refEl(m_uiDim, eleOrder);
		for (int dof = 0; dof < ndof_; dof++) {
			refEl.I3D_Parent2Child(&u[8 * dof], &interpU_taly[8 * dof], NeighbourTopMapOrder1[cnum]);
		}
		sync_.DendroToTalyIn<1, 3>(interpU_taly, neighbourSurface_.soln, ndof_);
		delete[] interpU_taly;
		unsigned int sz1 = 1u << (m_uiMaxDepth - baseDA_->getLevel(baseDA_->curr()));

		interpNeighbourCoords<1, 3, DTOP>(sz1);

		break;
	}
	default:std::cout << "Wrong neighbour level in " << __func__ << "\n";
		exit(0);
	}
}

void Refinement::interploateNeighboursFront(const double *u, const double *coords, const NeighbourLevel &level) {
	const int eleOrder = baseDA_->getElementOrder();
	if (eleOrder > 1) {
		std::cout << "Currently only supports Linear Basis function \n";
		exit(0);
	}

	switch (level) {
	case NeighbourLevel::SAME: {
		std::memcpy(neighbourSurface_.soln, u, sizeof(double) * ndof_ * 8);
		std::memcpy(neighbourSurface_.coords, coords, sizeof(double) * 24);
		break;
	}
	case NeighbourLevel::REFINE: {
		unsigned int sz1 = 1u << (m_uiMaxDepth - baseDA_->getLevel(baseDA_->curr()) - 1);
		interpNeighbourCoords<1, 3, DFRONT>(sz1);

		for (int dof = 0; dof < ndof_; dof++) {
			neighbourSurface_.coords[0 * ndof_ + dof] = u[32 * dof + 0];
			neighbourSurface_.coords[4 * ndof_ + dof] = u[32 * dof + 4];
			neighbourSurface_.coords[1 * ndof_ + dof] = u[32 * dof + 9];
			neighbourSurface_.coords[5 * ndof_ + dof] = u[32 * dof + 13];
			neighbourSurface_.coords[2 * ndof_ + dof] = u[32 * dof + 18];
			neighbourSurface_.coords[6 * ndof_ + dof] = u[32 * dof + 22];
			neighbourSurface_.coords[3 * ndof_ + dof] = u[32 * dof + 27];
			neighbourSurface_.coords[7 * ndof_ + dof] = u[32 * dof + 31];

		}

		break;
	}
	case NeighbourLevel::COARSE: {
		/** This is currently in the original Dendro ordering **/
		unsigned int cnum = baseDA_->getMortonChildNum(baseDA_->curr());

		double *interpU_taly = new double[8 * ndof_];
		RefElement refEl(m_uiDim, eleOrder);
		for (int dof = 0; dof < ndof_; dof++) {
			refEl.I3D_Parent2Child(&u[8 * dof], &interpU_taly[8 * dof], NeighbourFrontMapOrder1[cnum]);
		}
		sync_.DendroToTalyIn<1, 3>(interpU_taly, neighbourSurface_.soln, ndof_);
		delete[] interpU_taly;
		unsigned int sz1 = 1u << (m_uiMaxDepth - baseDA_->getLevel(baseDA_->curr()));

		interpNeighbourCoords<1, 3, DFRONT>(sz1);

		break;
	}
	default:std::cout << "Wrong neighbour level in " << __func__ << "\n";
		exit(0);
	}
}

void Refinement::interploateNeighboursBack(const double *u, const double *coords, const NeighbourLevel &level) {
	const int eleOrder = baseDA_->getElementOrder();
	if (eleOrder > 1) {
		std::cout << "Currently only supports Linear Basis function \n";
		exit(0);
	}

	switch (level) {
	case NeighbourLevel::SAME: {
		std::memcpy(neighbourSurface_.soln, u, sizeof(double) * ndof_ * 8);
		std::memcpy(neighbourSurface_.coords, coords, sizeof(double) * 24);
		break;
	}
	case NeighbourLevel::REFINE: {
		unsigned int sz1 = 1u << (m_uiMaxDepth - baseDA_->getLevel(baseDA_->curr()) - 1);
		interpNeighbourCoords<1, 3, DBACK>(sz1);

		for (int dof = 0; dof < ndof_; dof++) {
			neighbourSurface_.soln[0 * ndof_ + dof] = u[32 * dof + 0];
			neighbourSurface_.soln[4 * ndof_ + dof] = u[32 * dof + 4];
			neighbourSurface_.soln[1 * ndof_ + dof] = u[32 * dof + 9];
			neighbourSurface_.soln[5 * ndof_ + dof] = u[32 * dof + 13];
			neighbourSurface_.soln[2 * ndof_ + dof] = u[32 * dof + 18];
			neighbourSurface_.soln[6 * ndof_ + dof] = u[32 * dof + 22];
			neighbourSurface_.soln[3 * ndof_ + dof] = u[32 * dof + 27];
			neighbourSurface_.soln[7 * ndof_ + dof] = u[32 * dof + 31];
		}

		break;
	}
	case NeighbourLevel::COARSE: {
		/** This is currently in the original Dendro ordering **/
		unsigned int cnum = baseDA_->getMortonChildNum(baseDA_->curr());
		double *interpU_taly = new double[8 * ndof_];
		RefElement refEl(m_uiDim, eleOrder);
		for (int dof = 0; dof < ndof_; dof++) {
			refEl.I3D_Parent2Child(&u[8 * dof], &interpU_taly[8 * dof], NeighbourBackMapOrder1[cnum]);
		}
		sync_.DendroToTalyIn<1, 3>(interpU_taly, neighbourSurface_.soln, ndof_);
		delete[] interpU_taly;
		unsigned int sz1 = 1u << (m_uiMaxDepth - baseDA_->getLevel(baseDA_->curr()));

		interpNeighbourCoords<1, 3, DBACK>(sz1);

		break;
	}
	default:std::cout << "Wrong neighbour level in " << __func__ << "\n";
		exit(0);
	}

}

DENDRITE_REAL
Refinement::calcSurfError(TALYFEMLIB::FEMElm &fe, TALYFEMLIB::FEMElm &fe_neigh, const TALYFEMLIB::ELEM *elem,
                          const TALYFEMLIB::ELEM *elem_neigh, const TALYFEMLIB::GRID &grid,
                          const TALYFEMLIB::GRID &grid_neigh, const SIDE side) {


	/// Set the surface indicator and the normal of the current element
	TALYFEMLIB::SurfaceIndicator surf(TalySurfIndicator[side]);
	surf.set_normal(elem->CalculateNormal(&grid, TalySurfIndicator[side]));
	/// Set the surface indicator and the normal of the neighbor element
	TALYFEMLIB::SurfaceIndicator surf_neigh(TalySurfIndicator[NeighbourSurIndicator[side]]);
	surf_neigh.set_normal(
			elem->CalculateNormal(&grid_neigh, TalySurfIndicator[NeighbourSurIndicator[side]]));

	fe.refill_surface(elem, &surf, TALYFEMLIB::BASIS_LINEAR, 0);
	fe_neigh.refill_surface(elem_neigh, &surf_neigh, TALYFEMLIB::BASIS_LINEAR, 0);

	double surface_error = 0;

	for (int i = 0; i < fe.n_itg_pts(); i++) {
		fe.next_itg_pt();
		fe_neigh.next_itg_pt();

		calcValueFEM(fe, ndof_, thisSurface_.soln, &_valFEMCurrent_[i * ndof_]);
		calcValueFEM(fe_neigh, ndof_, neighbourSurface_.soln, &_valFEMNeighbour_[i * ndof_]);
		calcValueDerivativeFEM(fe, ndof_, thisSurface_.soln, _valDFEMCurrent_[i]);
		calcValueDerivativeFEM(fe_neigh, ndof_, neighbourSurface_.soln, _valDFEMNeighbour_[i]);
	}

	fe.refill_surface(elem, &surf, TALYFEMLIB::BASIS_LINEAR, 0);

	for (int nitg = 0; nitg < fe.n_itg_pts(); nitg++) {
		fe.next_itg_pt();
		std::memcpy(thisSurface_.valueFEM, &_valFEMCurrent_[nitg * ndof_], sizeof(DENDRITE_REAL) * ndof_);
		std::memcpy(neighbourSurface_.valueFEM, &_valFEMNeighbour_[NeighbourTransfer[side][nitg] * ndof_],
		            sizeof(DENDRITE_REAL) * ndof_);

		std::memcpy(thisSurface_.valueDerivativeFEM, _valDFEMCurrent_[nitg], sizeof(DENDRITE_REAL) * ndof_ * m_uiDim);
		std::memcpy(neighbourSurface_.valueDerivativeFEM, _valDFEMNeighbour_[NeighbourTransfer[side][nitg]],
		            sizeof(DENDRITE_REAL) * ndof_ * m_uiDim);

		thisSurface_.normal = surf.normal();
		neighbourSurface_.normal = surf_neigh.normal();
		surface_error += calcSurfaceError(fe.position(), thisSurface_, neighbourSurface_);

	}
	return surface_error;
}

DENDRITE_REAL Refinement::getTotalError() {
	return (totalElementError_ + totalSurfaceError_);
}

void Refinement::performIntergridTransfer(ot::DA *newDA, const Vec &in, Vec &out, int ndof) {
	baseDA_->petscIntergridTransfer<DENDRITE_REAL>(in, out, newDA, false, false, ndof);
}

void Refinement::finalizeAMR(ot::DA *newDA){
	if(not(baseDA_ == newDA)){
		delete baseDA_;
	}
}

void Refinement::performVectorExchange(const ot::DA*newDA,Vec & outVec, Vec & inVec, const DENDRITE_UINT ndof, const bool destroyAllocatedOutVec){
	// Delete the outgoing Vec.
	if(baseDA_->isActive() and destroyAllocatedOutVec) {
		VecDestroy(&outVec);
	}
	if(newDA->isActive()){
		newDA->petscCreateVector(outVec,false, false,ndof);
		VecCopy(inVec,outVec);
		VecDestroy(&inVec);
	}

}

void Refinement::writeRefineDatatoPvtu(const char * filename) {
	std::string fname;
	if(filename == NULL){
		fname = "Refine";
	}
	else{
		fname = filename;
	}

	writeCellDatatoPvtu(baseDA_,refinementVector_,fname.c_str(),problemSize_,true);

}

void Refinement::checkForCoarsening() {
	DENDRITE_UINT localElemSz = baseDA_->getLocalElemSz();
	auto pNodes = baseDA_->getMesh()->getAllElements();
	const DENDRITE_UINT localElementBegin = baseDA_->getMesh()->getElementLocalBegin();
	const DENDRITE_UINT localElementEnd = baseDA_->getMesh()->getElementLocalEnd();

	for(DENDRITE_UINT ele = 0; ele < localElemSz; ele++){
		if ((ele + localElementBegin + NUM_CHILDREN < pNodes.size())
		    and (pNodes[ele + localElementBegin].getParent() == pNodes[ele + localElementBegin + NUM_CHILDREN - 1].getParent())) {

			bool allCoarse = true;
			for(int i = 0; i < NUM_CHILDREN; i++){
				if(refinementVector_[ele + i] != ot::DA_FLAGS::Refine::DA_COARSEN){
					allCoarse = false;
					break;
				}
			}
			if(allCoarse) {

			}
			else{
				for(int i = 0; i < NUM_CHILDREN; i++) {
					if (refinementVector_[ele + i] == ot::DA_FLAGS::Refine::DA_COARSEN){
						refinementVector_[ele + i] = ot::DA_FLAGS::Refine::DA_NO_CHANGE;
					}
				}
			}
			ele = ele + NUM_CHILDREN - 1;
		}
		else if(refinementVector_[ele] == ot::DA_FLAGS::Refine::DA_COARSEN){
			refinementVector_[ele] = ot::DA_FLAGS::Refine::DA_NO_CHANGE;
		}
	}
}

void Refinement::getNeigbourValues(octNeighbours & octNeighbours){
	octNeighbours.init(nPe_,ndof_);
	octNeighbours = octNeighbourValues_;
}