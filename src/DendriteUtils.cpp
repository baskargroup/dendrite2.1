//
// Created by maksbh on 11/30/18.
//

#include <DendriteUtils.h>
#include <talyfem/grid/elem-types.h>
#include <talyfem/grid/grid_types/grid.h>
#include <talyfem/grid/femelm.h>
#include <Globals.h>
#include <OctToPhysical.h>
#include <TalyDendroSync.h>
#define PETSC_LOGGING_IMPL
#include <Profiling/GlobalProfiler.h>
#undef PETSC_LOGGING_IMPL


double *
calcL2Errors(ot::DA *da, const double *problemSize, const double *u, int ndof, const AnalyticFunction &f) {
    using namespace TALYFEMLIB;

    const unsigned int element_order = da->getElementOrder();
    static const unsigned int nodal_size = (element_order + 1) * (element_order + 1) * (element_order + 1);


    TalyDendroSync sync;
    GRID grid;
    grid.redimArrays(nodal_size, 1);
    for (DENDRITE_INT i = 0; i < nodal_size; i++) {
        grid.node_array_[i] = new NODE();
    }

    ELEM *elem = new ELEM3dHexahedral();
    grid.elm_array_[0] = elem;


    DENDRITE_INT *node_id_array = new DENDRITE_INT[nodal_size];
    for (int i = 0; i < nodal_size; i++) {
        node_id_array[i] = i;
    }
    elem->redim(nodal_size, node_id_array);

    FEMElm fe(&grid, BASIS_ALL);
    DENDRITE_REAL *coords_dendro = new DENDRITE_REAL[nodal_size * 3];


    DENDRITE_REAL val_dendro[nodal_size * ndof];
    DENDRITE_REAL val_taly[nodal_size * ndof];
    DENDRITE_REAL *val_c = new DENDRITE_REAL[ndof];


    DENDRITE_REAL *l2_errors = new double[ndof];
    for (int i = 0; i < ndof; i++) {
        l2_errors[i] = 0;
    }

    DENDRITE_REAL *u_vec;
    da->nodalVecToGhostedNodal(u, u_vec, false, ndof);
    da->readFromGhostBegin(u_vec,ndof);
    da->readFromGhostEnd(u_vec,ndof);


    for (da->init<ot::DA_FLAGS::LOCAL_ELEMENTS>();
         da->curr() < da->end<ot::DA_FLAGS::LOCAL_ELEMENTS>(); da->next<ot::DA_FLAGS::LOCAL_ELEMENTS>()) {

        da->getElementalCoords(da->curr(), coords_dendro);
        da->getElementNodalValues(u_vec, val_dendro, da->curr(), ndof);

        for (int i = 0; i < nodal_size; i++) {
            convertOctToPhys(&coords_dendro[3 * i], problemSize);
            grid.node_array_[i]->setCoor(coords_dendro[i * 3], coords_dendro[i * 3 + 1], coords_dendro[i * 3 + 2]);

        }


        if (element_order == 1) {
            sync.DendroToTalyIn<1,3>(val_dendro,val_taly,ndof);
            fe.refill(elem, BASIS_LINEAR, 0);
        } else if (element_order == 2) {
            sync.DendroToTalyIn<2,3>(val_dendro,val_taly,ndof);
            fe.refill(elem, BASIS_QUADRATIC, 0);
        }

        while (fe.next_itg_pt()) {
            calcValueFEM(fe, ndof, val_taly, val_c);
            for (unsigned int i = 0; i < ndof; i++) {
                double val_a = f(fe.position().x(), fe.position().y(), fe.position().z(), i);
                l2_errors[i] += (val_c[i] - val_a) * (val_c[i] - val_a) * fe.detJxW();
            }
        }
    }


    delete[] u_vec;
    delete[] val_c;
    delete[] coords_dendro;
    delete[] node_id_array;

    DENDRITE_REAL *all_err = new DENDRITE_REAL[ndof];
    MPI_Reduce(l2_errors, all_err, ndof, MPI_DOUBLE, MPI_SUM, 0, da->getCommActive());
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (rank == 0) {
        for (int i = 0; i < ndof; i++) {
            all_err[i] = sqrt(all_err[i]);
        }
    }
    delete[] l2_errors;
    return all_err;
}

void
evalFunction(ot::DA *da, const double *problemSize, const double *u, int ndof,
             const std::vector<EvalFunction> &f, double *func_val) {

    if(not(da->isActive())){
        return;
    }

    /*** NOt tested properly. Use at your own risk. */
    using namespace TALYFEMLIB;

    const unsigned int element_order = da->getElementOrder();
    static const unsigned int nodal_size = (element_order + 1) * (element_order + 1) * (element_order + 1);



    TalyDendroSync sync;
    GRID grid;
    grid.redimArrays(nodal_size, 1);
    for (DENDRITE_INT i = 0; i < nodal_size; i++) {
        grid.node_array_[i] = new NODE();
    }

    ELEM *elem = new ELEM3dHexahedral();
    grid.elm_array_[0] = elem;

    DENDRITE_INT *node_id_array = new DENDRITE_INT[nodal_size];
    for (int i = 0; i < nodal_size; i++) {
        node_id_array[i] = i;
    }
    elem->redim(nodal_size, node_id_array);

    FEMElm fe(&grid, BASIS_ALL);
    DENDRITE_REAL *coords_dendro = new DENDRITE_REAL[nodal_size * 3];

    DENDRITE_REAL val_dendro[nodal_size * ndof];
    DENDRITE_REAL val_taly[nodal_size * ndof];

    DENDRITE_REAL *val_c = new DENDRITE_REAL[ndof];
    DENDRITE_REAL *val_d = new DENDRITE_REAL[ndof * m_uiDim];


    DENDRITE_REAL *u_vec;

    da->nodalVecToGhostedNodal(u, u_vec, false, ndof);
    da->readFromGhostBegin(u_vec,ndof);
    da->readFromGhostEnd(u_vec,ndof);

    std::vector<DENDRITE_REAL> localFuncVal(f.size(),0.0);


  for (int i = 0; i < f.size(); i++) {
        func_val[i] = 0.;
    }



    for (da->init<ot::DA_FLAGS::LOCAL_ELEMENTS>();
         da->curr() < da->end<ot::DA_FLAGS::LOCAL_ELEMENTS>(); da->next<ot::DA_FLAGS::LOCAL_ELEMENTS>()) {

        da->getElementalCoords(da->curr(), coords_dendro);
        da->getElementNodalValues(u_vec, val_dendro, da->curr(), ndof);

        for (int i = 0; i < nodal_size; i++) {
            convertOctToPhys(&coords_dendro[3 * i], problemSize);
            grid.node_array_[i]->setCoor(coords_dendro[i * 3], coords_dendro[i * 3 + 1], coords_dendro[i * 3 + 2]);

        }


        if (element_order == 1) {
            sync.DendroToTalyIn<1,3>(val_dendro,val_taly,ndof);
            fe.refill(elem, BASIS_LINEAR, 0);
        } else if (element_order == 2) {
            sync.DendroToTalyIn<2,3>(val_dendro,val_taly,ndof);
            fe.refill(elem, BASIS_QUADRATIC, 0);
        }
        while (fe.next_itg_pt()) {
            calcValueFEM(fe, ndof, val_taly, val_c);
            calcValueDerivativeFEM(fe, ndof, val_taly, val_d);
            for (int i = 0; i < f.size(); i++) {
              localFuncVal[i] +=
                  f[i](fe.position().x(), fe.position().y(), fe.position().z(), val_c, val_d) * fe.detJxW();
            }


        }

    }

    MPI_Reduce(localFuncVal.data(),func_val,f.size(),MPI_DOUBLE,MPI_SUM,0,da->getCommActive());
    delete[] u_vec;
    delete[] val_c;
    delete[] val_d;
    delete[] coords_dendro;
    delete[] node_id_array;


}


void calcValueDerivativeFEM(TALYFEMLIB::FEMElm &fe, DENDRITE_INT ndof,
                            const DENDRITE_REAL *value, DENDRITE_REAL *val_d) {


    for (int dof = 0; dof < ndof; dof++) {
        for (int dir = 0; dir < m_uiDim; dir++) {
            val_d[m_uiDim * dof + dir] = 0;
            for (ElemNodeID a = 0; a < fe.nbf(); a++) {
                val_d[m_uiDim * dof + dir] += fe.dN(a, dir) * value[a * ndof + dof];
            }
        }
    }
}

void calcValueFEM(TALYFEMLIB::FEMElm &fe, DENDRITE_INT ndof, double *value, double *val_c) {

    for (int dof = 0; dof < ndof; dof++) {
        val_c[dof] = 0;
        for (ElemNodeID a = 0; a < fe.nbf(); a++) {
            val_c[dof] += fe.N(a) * value[a * ndof + dof];
        }
    }
}


void setScalarByFunction(ot::DA *da, double *u, int ndof,
                         const std::function<double(double, double, double, int)> &f,
                         const Point &domain_min, const Point &domain_max) {

    std::function<void(double, double, double,
                       double *)> f_init = [ndof, f, domain_min, domain_max](
            const double x, const double y, const double z, double *var) {
        Point oct(x, y, z);
        Point phys = convertOctToPhys(oct, domain_min, domain_max);
        for (int dof = 0; dof < ndof; dof++) {
            var[dof] = f(phys.x(), phys.y(), phys.z(), dof);
        }
    };
    da->setVectorByFunction(u, f_init, false, false, ndof);
}

void dendrite_init(int argc, char **argv) {
    PetscInitialize(&argc, &argv, NULL, NULL);
    registerglobalProfiler();
    PetscLogEventBegin(totalEvent, 0, 0, 0, 0);
    _InitializeHcurve(m_uiDim);
}

void dendrite_finalize(ot::DA *da) {

    delete da;
    PetscLogEventEnd(totalEvent, 0, 0, 0, 0);
    PetscFinalize();
}

ot::DA *createRegularDA(const DENDRITE_UINT refine_lvl, const DENDRITE_UINT max_depth, DENDRITE_UINT grainSz) {
    Globals global;
    std::vector<ot::TreeNode> oct;
    m_uiMaxDepth = max_depth;
    createRegularOctree(oct, refine_lvl, m_uiDim, m_uiMaxDepth, MPI_COMM_WORLD);

    ot::DA *octDA = NULL;
    octDA = new ot::DA(oct, MPI_COMM_WORLD, global.getBasisFunction(), grainSz, 0.3);
    return octDA;
}

void createGrid(const double *coords, TALYFEMLIB::GRID &grid, TALYFEMLIB::ELEM *elem) {
    using namespace TALYFEMLIB;

    grid.redimArrays(8, 1);

    for (int i = 0; i < 8; i++) {
        grid.node_array_[i] = new NODE();
    }

    grid.elm_array_[0] = elem;

    static const int node_id_array[8] = {
            0, 1, 2, 3, 4, 5, 6, 7
    };
    elem->redim(8, node_id_array);

    for (unsigned int i = 0; i < 8; i++) {
        grid.node_array_[i]->setCoor(coords[i * 3], coords[i * 3 + 1], coords[i * 3 + 2]);
    }
}

void computeTreeNodesAndCordinates(ot::DA* da, const double* problemSize, std::vector<NodeAndValues<DENDRITE_REAL , 3> >& vec){
    const int nsd = 3;
    const unsigned int maxOctCoord = (1u << (m_uiMaxDepth));
    unsigned int octCoords[nsd];
    const double physToOct[nsd] = {
            (1u << (m_uiMaxDepth)) / problemSize[0],
            (1u << (m_uiMaxDepth)) / problemSize[1],
            (1u << (m_uiMaxDepth)) / problemSize[2],
    };
    for (auto& n : vec) {
        for (unsigned int dim = 0; dim < nsd; dim++) {
            // octree coordinate
            octCoords[dim] = (unsigned int) (n.values[dim] * physToOct[dim]);

            /// TODO: Don't know what the hell it is.
            // special fix for positive boundaries... (explanation from Hari, May 7, 2018):
            /*
             * Basically, if the point is on the boundary, then the interpolation needs to happen on the face that overlaps
             * with the boundary. This face can be part of 2 elements, one that is inside the domain and one that is outside.
             * By default we always go for the "right" element, but this is not correct for those on the positive boundaries,
             * hence the error. You can fix it when you compute the TreeNode corresponding to the element. You will need a
             * correction like:
             *
             *   if (xint == xyzFac)
             *     xint = xyzFac - 1;
             *   if (yint == xyzFac)
             *     yint = xyzFac -1;
             *   if (zint == xyzFac)
             *     zint = xyzFac -1;
             */

            if (octCoords[dim] == maxOctCoord) {
                octCoords[dim] = maxOctCoord - 1;
            }
        }
        n.node = ot::TreeNode(octCoords[0], octCoords[1], octCoords[2], m_uiMaxDepth, 3, m_uiMaxDepth);
    }
}

void printDAStatistics(ot::DA * da){
  enum stat: DENDRITE_UINT {
    LOCAL_ELEMENTS = 0,
    GHOST_ELEMENTS = 1
  };
  DENDRITE_UINT activeNpes = da->getNpesActive();
  if(da->isActive()) {

    std::vector<DENDRITE_UINT> da_stat(2);
    da_stat[0] = da->getLocalElemSz();
    da_stat[1] = da->getGhostedElementSize();
    std::vector<DENDRITE_UINT> global(2);
    MPI_Reduce(da_stat.data(),global.data(),2,MPI_UINT32_T,MPI_SUM,0,da->getCommActive());

    PrintStatus("Total number of Active Processor = ",activeNpes , "/",TALYFEMLIB::GetMPISize());
    PrintStatus("Basis function = ",da->getElementOrder());
    PrintStatus("Total number of Local Elements = ",global[LOCAL_ELEMENTS]);
    PrintStatus("Total number of Ghosted Elements = ",global[GHOST_ELEMENTS]);
    PrintStatus("Average number of Local Elements = ", static_cast<DENDRITE_REAL>(global[LOCAL_ELEMENTS]/(activeNpes*1.0)));
    PrintStatus("Average number of Ghosted Elements = ", static_cast<DENDRITE_REAL>(global[GHOST_ELEMENTS]/(activeNpes*1.0)));

    MPI_Reduce(da_stat.data(),global.data(),2,MPI_UINT32_T,MPI_MIN,0,da->getCommActive());
    PrintStatus("Minimum number of Local Elements = ",global[LOCAL_ELEMENTS]);
    PrintStatus("Minimum number of Ghosted Elements = ",global[GHOST_ELEMENTS]);

    MPI_Reduce(da_stat.data(),global.data(),2,MPI_UINT32_T,MPI_MAX,0,da->getCommActive());
    PrintStatus("Maximum number of Local Elements = ",global[LOCAL_ELEMENTS]);
    PrintStatus("Maximum number of Ghosted Elements = ",global[GHOST_ELEMENTS]);

  }




}