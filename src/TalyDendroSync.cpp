//
// Created by maksbh on 11/23/18.
//

#include "TalyDendroSync.h"
#include <DataTypes.h>

unsigned int TalyDendroSync::get_surf_flags(const TALYFEMLIB::ZEROPTV & positon, const DENDRITE_REAL * problemSize, const DENDRITE_REAL * start){
    unsigned int flags = 0;

    double end[3];
    end[0] = start[0] + problemSize[0];
    end[1] = start[1] + problemSize[1];
    end[2] = start[2] + problemSize[2];

    if (FEQUALS(positon.x(),start[0])) {
        flags |= (1u << 1u);
    }
    if (FEQUALS(positon.x(),end[0])) {
        flags |= (1u << 2u);
    }
    if (FEQUALS(positon.y(),start[1])) {
        flags |= (1u << 3u);
    }
    if (FEQUALS(positon.y(),end[1])) {
        flags |= (1u << 4u);
    }
    if (FEQUALS(positon.z(),start[2])) {
        flags |= (1u << 5u);
    }
    if (FEQUALS(positon.z(),end[2])) {
        flags |= (1u << 6u);
    }

    return flags;
}

unsigned int TalyDendroSync::get_surf_flags(const TALYFEMLIB::ZEROPTV & positon, const DENDRITE_REAL * problemSize){
    const DENDRITE_REAL start[3]{0.,0.,0.};
    unsigned  int flag = get_surf_flags(positon,problemSize,start);
    return flag;

}
